QuadrionShader SkyLightingEffect
{
    #include LightUniforms.glslh
    #include CameraUniforms.glslh
    #include SkyUniforms.glslh
	#include CubemapCameraUniforms.glslh
    
    #version 410 core

    OutputBuffer = SkyCubemap, 6, 512, 512, RGBA16F, 0, Linear | Clamp;
    OutputBuffer = IrradianceMap, 6, 64, 64, RGBA16F, 0, Linear | Clamp;
    OutputBuffer = HDRSurface, 1, 1440, 900, RGBA16F, 0, Linear | Clamp;
    
    interface VertIn
    {
        vec3 position : 0;
		vec3 uv0 : 1;
    };
    
    interface FragIn
    {
        vec3 L;
        vec3 P;
        vec2 oTexCoords;
    };

	layout(triangles) in : geometry;
	layout(triangle_strip, max_vertices = 30) out : geometry;
	interface SkyboxGeomIn
    {
        vec4 outPosition[3];
		vec2 outTexCoords[3];
		vec3 L[3];
		vec3 P[3];
    };

	interface SkyboxFragIn
	{
		vec3 fragL;
		vec3 fragP;
		vec2 oTexCoords;
		float layerID;
	};
    
    interface FragOut
    {
        vec4 oColor : 0;
    };

	interface SkyboxOut
	{
		vec4 color : 0;
	};		
    
    const float spotBrightness = 80.0;
    const float intensity = 1.0;
    const float nSteps = 4;	
    const vec3 Kr = vec3(0.18867780436772762, 0.4978442963618773, 0.6616065586417131);
	const float nIrradianceSamples = 128;
	const float nInvIrradianceSamples=1.0/nIrradianceSamples;
	const float Pi = 3.141592654;	

    uniform sampler2D noiseTex;
    uniform bool renderInverted;
	uniform samplerCube SkyCubemap;

    float phase(float alpha, float g) : fragment
    {
    	float a = 3.0 * (1.0 - g * g);
    	float b = 2.0 * (2.0 + g * g);
    	float c = 1.0  + alpha * alpha;
    	float d = pow(1.0 + g * g - 2.0 * g * alpha, 1.5);
    	return (a / b) * (c / d);
    }


    float atmospheric_depth(vec3 pos, vec3 dir) : fragment
    {
    	float a = dot(dir, dir);
    	float b = 2.0 * dot(dir, pos);
    	float c = dot(pos, pos) - 1.0;
    	float det = b * b - 4.0 * a * c;
    	float detSqrt = sqrt(det);
    	float q = (-b - detSqrt) / 2.0;
    	float t1 = c / q;
    	return t1;
    }

    float horizon_extinction(vec3 pos, vec3 dir, float rad) : fragment
    {
    	float u = dot(dir, -pos);
    	if(u < 0.0)
    		return 1.0;
    	
    	vec3 near = pos + u * dir;
    	if(length(near) < rad)
    		return 0.0;
    
    	else
    	{
    		vec3 v2 = normalize(near) * rad - pos;
    		float diff = acos(dot(normalize(v2), dir));
    		return smoothstep(0.0, 1.0, pow(diff * 2.0, 3.0));
    	}
    }

    vec3 absorb(float dist, vec3 color, float factor) : fragment
    {
    	return color - color * pow(Kr, vec3(factor / dist));
    }

    vec3 get_view_vector() : fragment
    {
    	vec2 fragCoord = gl_FragCoord.xy / ViewportDimensions;
    	fragCoord = (fragCoord - 0.5) * 2.0;
    	vec4 ndc = vec4(fragCoord, 0.0, 1.0);
    	vec3 Veye = normalize((inverse(ProjectionMatrix) * ndc).xyz);
    	vec3 Vworld = normalize(inverse(mat3(ViewMatrix)) * Veye);
    	
    	return Vworld;
    }

	vec3 get_view_vector_cube(float layer, vec2 fragCoord) : fragment
	{
		if(layer > 5)
			return vec3(0.0);

		mat4 proj, view;
		if(layer == 0.0)
		{
			proj = ProjectionMatrixRight;
			view = ViewMatrixRight;
		}

		else if(layer == 1.0)
		{
			proj = ProjectionMatrixLeft;
			view = ViewMatrixLeft;
		}

		else if(layer == 2.0)
		{
			proj = ProjectionMatrixUp;
			view = ViewMatrixUp;
		}

		else if(layer == 3.0)
		{
			proj = ProjectionMatrixDown;
			view = ViewMatrixDown;
		}

		else if(layer == 4.0)
		{
			proj = ProjectionMatrixFront;
			view = ViewMatrixFront;
		}

		else if(layer == 5.0)
		{
			proj = ProjectionMatrixBack;
			view = ViewMatrixBack;
		}

		else	
			return vec3(0.0);

		fragCoord = (fragCoord - 0.5) * 2.0;
		vec4 ndc = vec4(fragCoord, 0.0, 1.0);

		vec3 Veye = normalize((inverse(proj) * ndc).xyz);
    	vec3 Vworld = normalize(inverse(mat3(view)) * Veye);
    	
    	return Vworld;
	}

	vec3 generateSkyColorCube(vec3 V, vec3 L, vec3 P) : fragment
	{
    	float alpha = dot(V, L);
    	float rayleighFactor = phase(alpha, -0.01) * rayleighBrightness;
    	float mieFactor = phase(alpha, mieDistribution) * mieBrightness;
    	float spot = smoothstep(0.0, 15.0, phase(alpha, 0.9995)) * spotBrightness;
	
    	float eyeDepth = atmospheric_depth(P, V);
    	float stepLen = eyeDepth / nSteps;
	
    	float eyeExtinction = horizon_extinction(P, V, P.y - 0.05);			// 0.15
	
    	vec3 rayleighCollected = vec3(0.0, 0.0, 0.0);
	   	vec3 mieCollected = vec3(0.0, 0.0, 0.0);
	
    	for(int i = 0; i < nSteps; i++)
    	{
    		float sampDist = stepLen * float(i);
    		vec3 pos = P + V * sampDist;
    		float extinction = horizon_extinction(pos, L, P.y - 0.25);		// 0.35
	   		float sampDepth = atmospheric_depth(pos, L);
		
    		vec3 influx = absorb(sampDepth, vec3(intensity), scatterStrength) * extinction;
		
    		rayleighCollected += absorb(sampDist, Kr * influx, rayleighStrength);
    		mieCollected += absorb(sampDist, influx, mieStrength);
    	}
    	rayleighCollected = (rayleighCollected * eyeExtinction * pow(eyeDepth, rayleighCollectionPower)) / float(nSteps);
    	mieCollected = (mieCollected * eyeExtinction * pow(eyeDepth, mieCollectionPower)) / float(nSteps);
	
    	vec3 color = vec3(spot * mieCollected + mieFactor * mieCollected + rayleighFactor * rayleighCollected);
    	float ditherFactor = mix(0.0, 4.0, length(color));

		return color;	
//		return vec3(1.0);
	}

	vec3 getIrradianceSamplingVector(vec2 fragPos, float layer)
	{
		vec2 uv = fragPos / cubemapViewportDimensions;
		uv = 2.0 * vec2(uv.x, 1.0 - uv.y) - vec2(1.0);
		vec3 vec;

		if(layer == 0.0)
			vec = vec3(1.0, uv.y, -uv.x);
		else if(layer == 1.0)
			vec = vec3(-1.0, uv.y, uv.x);
		else if(layer == 2.0)
			vec = vec3(uv.x, 1.0, -uv.y);
		else if(layer == 3.0)
			vec = vec3(uv.x, -1.0, uv.y);
		else if(layer == 4.0)
			vec = vec3(uv.x, uv.y, 1.0);
		else if(layer == 5.0)
			vec = vec3(-uv.x, uv.y, -1.0);
		else	
			vec = vec3(1.0);

		return normalize(vec);
	}

	void computeBasis(const vec3 N, out vec3 S, out vec3 T)
	{
		T = cross(N, vec3(0.0, 1.0, 0.0));
		T = mix(cross(N, vec3(1.0, 0.0, 0.0)), T, step(0.00001, dot(T, T)));
		T = normalize(T);
		S = normalize(cross(N, T));
	}

	float radicalInv(uint bits)
	{
		bits = (bits << 16u) | (bits >> 16u);
		bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
		bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
		bits = ((bits & 0xF0F0F0F0u) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
		bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);

		return float(bits) * 2.3283064365386963e-10;
	}

	vec2 hammersley(uint i)
	{
		return vec2(i * nInvIrradianceSamples, radicalInv(i));
	}

	vec3 tanToWorld(vec3 v, vec3 N, vec3 S, vec3 T)
	{
		return S * v.x - T * v.y + N * v.z;
	}

	vec3 hemiSample(float u1, float u2)
	{
		float u1p = sqrt(max(0.0, 1.0 - u1 * u1));
		return vec3(cos(2.0 * Pi * u2) * u1p, sin(2.0 * Pi * u2) * u1p, u1);
	}

    void vert(in VertIn, out FragIn)
    {
        L = normalize(-LightVec);
	    P = vec3(0.0, surfaceHeight, 0.0);
	    gl_Position = vec4(position, 1.0);

	    // render inverted for reflections //
	    if(renderInverted)
	    	P = vec3(0.0, -surfaceHeight, 0.0);

	    oTexCoords = position.xy * 0.5 + 0.5;
    }

	void frag(in FragIn, out FragOut)
    {
    	vec3 V = get_view_vector();

    	float alpha = dot(V, L);
    	float rayleighFactor = phase(alpha, -0.01) * rayleighBrightness;
    	float mieFactor = phase(alpha, mieDistribution) * mieBrightness;
    	float spot = smoothstep(0.0, 15.0, phase(alpha, 0.9995)) * spotBrightness;
    	
    	float eyeDepth = atmospheric_depth(P, V);
    	float stepLen = eyeDepth / nSteps;
    	
    	float eyeExtinction = horizon_extinction(P, V, P.y - 0.05);			// 0.15
    	
    	vec3 rayleighCollected = vec3(0.0, 0.0, 0.0);
    	vec3 mieCollected = vec3(0.0, 0.0, 0.0);
    	
    	for(int i = 0; i < nSteps; i++)
    	{
    		float sampDist = stepLen * float(i);
    		vec3 pos = P + V * sampDist;
    		float extinction = horizon_extinction(pos, L, P.y - 0.25);		// 0.35
    		float sampDepth = atmospheric_depth(pos, L);
    		
    		vec3 influx = absorb(sampDepth, vec3(intensity), scatterStrength) * extinction;
    		
    		rayleighCollected += absorb(sampDist, Kr * influx, rayleighStrength);
    		mieCollected += absorb(sampDist, influx, mieStrength);
    	}

    	rayleighCollected = (rayleighCollected * eyeExtinction * pow(eyeDepth, rayleighCollectionPower)) / float(nSteps);
    	mieCollected = (mieCollected * eyeExtinction * pow(eyeDepth, mieCollectionPower)) / float(nSteps);
    	
    	vec3 color = vec3(spot * mieCollected + mieFactor * mieCollected + rayleighFactor * rayleighCollected);
    	float ditherFactor = mix(0.0, 4.0, length(color));
    	color += vec3(mix(-ditherFactor / 255.0, ditherFactor / 255.0, texture(noiseTex, oTexCoords * 384.0).r));
    	vec3 o = vec3(color * vec3(LightIntensity));
    	oColor = vec4(o, 1.0);
//		oColor = vec4(1.0, 0.0, 0.0, 1.0);
    }

	// Skybox render programs //
	void SkyboxVertex(in VertIn, out SkyboxGeomIn)
    {
        outPosition = vec4(position, 1.0);
		outTexCoords = vec2(position.xy) * 0.5 + 0.5;
		L = normalize(-LightVec);
		P = vec3(0.0, surfaceHeight, 0.0);
    }

	void SkyboxGeom(in SkyboxGeomIn, out SkyboxFragIn)
	{
		int i, layer;
		for(layer = 0; layer < 6; layer++)
		{
			gl_Layer = layer;
			
			for(i = 0; i < 3; i++)
			{
				vec4 outPos = outPosition[i];
				gl_Position = outPos;				
				oTexCoords = outTexCoords[i];
				fragL = L[i];
				fragP = P[i];
				layerID = layer;
				EmitVertex();
			}

			EndPrimitive();
		}
	}

	void SkyboxFrag(in SkyboxFragIn, out SkyboxOut)
	{
		vec2 fc = gl_FragCoord.xy / cubemapViewportDimensions;
		vec3 V = get_view_vector_cube(layerID, fc);
		color = vec4(V, 1.0);
		color = vec4(generateSkyColorCube(V, fragL, fragP) * vec3(LightIntensity), 1.0);
	}

	void IrradianceFrag(in SkyboxFragIn, out SkyboxOut)
	{
		vec3 samplingVector = getIrradianceSamplingVector(gl_FragCoord.xy, layerID);
		vec3 S, T;
		computeBasis(samplingVector, S, T);

		vec3 irradiance = vec3(0.0);
		for(int i = 0; i < nIrradianceSamples; ++i)
		{
			vec2 u = hammersley(i);
			vec3 Li = tanToWorld(hemiSample(u.x, u.y), samplingVector, S, T);
			float cosTheta = max(0.0, dot(Li, samplingVector));

			irradiance += 2.0 * textureLod(SkyCubemap, Li, 0).rgb * cosTheta;
		}

		irradiance /= vec3(nIrradianceSamples);

		color = vec4(irradiance, 1.0);
//		color = vec4(samplingVector, 1.0);
	}


    technique SkyLighting
	{
		pass p0
		{
//			OutputBuffer = {DefaultSurface, 0, 0, 0, 0, 0, 0}
			OutputBuffer = HDRSurface;

			DepthTest = False;
			CullEnable = False;

			noiseTex = Media/textures/noise256.png;
			noiseTex[MinFilter] = Linear;
			noiseTex[MagFilter] = Linear;
			noiseTex[Anisotropy] = 1.0;

			VertexShader = vert;
			FragmentShader = frag;
		};
	};

	technique SkyLightingCubemap
	{
		pass p0
		{
			OutputBuffer = SkyCubemap;

			DepthTest = False;
			CullEnable = False;

			VertexShader = SkyboxVertex;
			GeometryShader = SkyboxGeom;
			FragmentShader = SkyboxFrag;
		};
	};

	technique SkyIrradianceCubemap
	{
		pass p0
		{
			OutputBuffer = IrradianceMap;

			DepthTest = False;
			CullEnable = False;

			VertexShader = SkyboxVertex;
			GeometryShader = SkyboxGeom;
			FragmentShader = IrradianceFrag;
		};
	};
}