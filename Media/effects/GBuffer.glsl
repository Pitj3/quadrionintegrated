QuadrionShader GBuffer
{
	#include CameraUniforms.glslh
	#include PBRMaterialUniforms.glslh
	#include PBRTextures.glslh
	#version 410 core

	OutputBuffer = GBufferFBO, 4, 1440, 900, RGBA32F, D24S8, Nearest | Clamp;

	interface VertIn
	{
		vec3 iPosition : 0;
		vec2 iTexCoords : 1;
		vec3 iNormal : 2;
		vec3 iTangent : 3;
		vec4 iColor : 4;
	};

	interface FragIn
	{
		vec3 outWSNormal;
		vec2 outTexCoords;
		vec3 outWSPos;
		vec4 outColor;
		vec3 outWSTangent;
	};

	interface FragOut
	{
		vec4 buf0 : 0;
		vec4 buf1 : 1;
		vec4 buf2 : 2;
		vec4 buf3 : 3;
	};


	uniform mat4 ModelMatrix;
	uniform vec3 surfaceColor : GUI_DEBUG_FLOAT3(0.0, 1.0);
	uniform float RenderObjectID;
	uniform float isGuiSelected;

	void vert(in VertIn, out FragIn)
	{
		vec3 pos = iPosition;
        vec4 ndcPos = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(pos, 1.0);
		outColor = iColor * vec4(surfaceColor, 1.0);
		outColor = vec4(surfaceColor, 1.0);

		outTexCoords = iTexCoords;
		outWSNormal = vec4(ModelMatrix * vec4(iNormal, 0.0)).xyz;
        outWSPos = vec4(ModelMatrix * vec4(pos, 1.0)).xyz;
		outWSTangent = vec4(ModelMatrix * vec4(iTangent, 0.0)).xyz;

		gl_Position = ndcPos;
	}


	void frag(in FragIn, out FragOut)
	{
		// Position
		buf0 = vec4(outWSPos, RenderObjectID);

		// Normals
		// TODO: Use normal map
		buf1 = vec4(outWSNormal, isGuiSelected);

		// Albedo
		buf2 = outColor;

		buf3 = vec4(1.0, 1.0, 1.0, 1.0);
	}

	void GBufferFrag(in FragIn, out FragOut)
    {
        vec3 nSample = outWSNormal.xyz;
        if(bHasNormalMap)
        {
            vec3 B = cross(outWSNormal.xyz, outWSTangent.xyz);
	        vec3 N = outWSNormal.xyz;
	        vec3 T = outWSTangent.xyz;
	        mat3 TBN = mat3(T, B, N);	        
            
            // Perturb normal by normalmap
	        nSample = texture(normalMap, outTexCoords).rgb;
	        nSample = nSample * 2.0 - 1.0;
	        nSample = (TBN * nSample);
	        nSample = normalize(nSample);
        }

        float thisRoughness = PBRMaterial_roughness;
        if(bHasRoughnessTexture)
            thisRoughness = texture(roughnessTexture, outTexCoords).r;

        vec3 albedo = PBRMaterial_albedo.xyz;
		albedo = vec3(1.0, 0.0, 0.0);
        if(bHasAlbedoTexture)
		{
            albedo = texture(albedoTexture, outTexCoords).rgb;
		}

        float ao = 1.0;
        if(bHasAOTexture)
            ao = texture(aoTexture, outTexCoords).r;

        float thisMetal = PBRMaterial_metal;
        if(bHasMetallicTexture)
            thisMetal = texture(metallicTexture, outTexCoords).r;

        buf0 = vec4(outWSPos.xyz, 1.0);          // x, x, x, x
        buf1 = vec4(nSample, 0.0);                // Nx, Ny, Nz, alpha
        buf2 = vec4(albedo, thisRoughness);       // albedoR, albedoG, albedoB, roughness
        buf3 = vec4(1.0, thisMetal, ao, 0.0);        // shadow, metal, ao, type
    }

	technique GBuffer
	{
		pass p0
		{
			DepthTest = True;
			
			OutputBuffer = GBufferFBO;

			VertexShader = vert;
			FragmentShader = frag;
		};
	};

	technique GBufferPBR
	{
		pass p0
		{
			DepthTest = True;
			DepthWrite = True;

			OutputBuffer = GBufferFBO;

			VertexShader = vert;
			FragmentShader = GBufferFrag;
		};
	};
}