QuadrionShader PBRLighting
{
    #version 410 core

    #include LightUniforms.glslh
    #include CameraUniforms.glslh
	#include GBufferUniforms.glslh

	OutputBuffer = HDRSurface, 1, 1440, 900, RGBA16F, 0, Linear | Clamp;
	OutputBuffer = CTIntegrationTexture, 1, 64, 64, RGBA16F, 0, Linear | Clamp;

    interface VertIn
	{
        vec3 position : 0;
        vec3 viewVec : 1;
	};

	interface FullscreenVertIn
	{
		vec3 position : 0;
	};

	interface CTIntegrationFragIn
	{
		vec3 outPosition;
	};

    interface FragIn
	{
		vec3 outViewVec;
        vec2 outTexCoords;
	};

    interface FragOut
	{
        vec4 color : 0;
	};
    

    const float shininessConstant = 1.0;
    const float Pi = 3.14159265358979;
    const float e = 2.7182818284;
    const float iorA = 1.000293;			
    const float iorB = 2.4;			
//    const float lightIntensity = 2.0;
    const float ssaoEnable = 0.0;
	const vec3 ambient = vec3(0.1, 0.1, 0.1);
	const float epsilon = 0.00001;
	const int CTSamples = 16;
	

	uniform vec2 textureDims;
	uniform samplerCube SkyCubemap;
	uniform samplerCube IrradianceMap;
	uniform sampler2D CTIntegrationTexture;

	float radicalInv(uint bits)
	{
		bits = (bits << 16u) | (bits >> 16u);
		bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
		bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
		bits = ((bits & 0xF0F0F0F0u) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
		bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);

		return float(bits) * 2.3283064365386963e-10;
	}

	vec2 hammersley(int i)
	{
		float invSamples = 1.0 / CTSamples;
		return vec2(i * invSamples, radicalInv(i));
	}

	vec3 sampleGeometry(float u1, float u2, float roughness)
	{
		float a = roughness * roughness;
		float cosAng = sqrt((1.0 - u2) / (1.0 + (a * a - 1.0) * u2));
		float sinAng = sqrt(1.0 - cosAng * cosAng);
		float phi = 2.0 * Pi * u1;

		return vec3(sinAng * cos(phi), sinAng * sin(phi), cosAng);
	}

	float schlickG1(float cosTheta, float k)
	{
		return cosTheta / (cosTheta * (1.0 - k) + k);
	}

	float schlickGGX_IBL(float cosLi, float cosLo, float roughness)
	{
		float r = roughness;
		float k = (r * r) * 2.0;
		return schlickG1(cosLi, k) * schlickG1(cosLo, k);
	}

    vec3 cook_torrance(vec3 N, vec3 V, vec3 L, float roughness, float metal)
    {

    	vec3 metallic = vec3(metal);
    	float ks = 0.0;

    	vec3 H = normalize(L + V);
    	float smoothness = 1.0 - roughness;
    
    	float NdL = max(dot(N, L), 0.0);
    	float NdH = max(dot(N, H), 0.0);
    	float NdV = max(dot(N, V), 0.0);
    	float VdH = max(dot(V, H), 0.0);
		float LdH = max(dot(L, H), 0.0);
    
    	// Calculate fresnel
    	float r0 = pow(((iorA - iorB) / (iorA + iorB)), 2.0); 
    	float F = r0 + (1.0 - r0) * pow((1.0 - LdH), 5.0);
    
    	// Calculate geom. atten 
    	float G = min(1.0, min((2.0 * NdH * NdV) / VdH, (2.0 * NdH * NdL) / VdH));
    
    	// Calculate microfacet slope (Gaussian approx.)
		float r2 = pow(roughness, 2.0); 
    	float a = exp(-acos(NdH) / r2);
    	float D = a / (Pi * r2 * pow(cos(NdH), 4));
    
    	ks += (F * D * G) / (4.0 * NdV * NdL);// * shininessConstant;

    	return vec3(ks);
//		return vec3(1.0, 0.0, 0.0);
    }

    vec3 oren_nayar(vec3 N, vec3 V, vec3 L, float roughness, vec3 albedo)
    {
    	float NdL = dot(N, L);
    	float NdV = dot(N, V);
    
    	float angVN = acos(NdV);
    	float angLN = acos(NdL);
    
    	float a = max(angVN, angLN);
    	float b = min(angVN, angLN);
    	float y = dot(V - N * NdV, L - N * NdL);
    
    	float rSQ = roughness * roughness;
    	float A = 1.0 - 0.5 * (rSQ / (rSQ + 0.33));
    	float B = 0.45 * (rSQ / (rSQ + 0.09));
    	float C = sin(a) * tan(b);
    	float L1 = max(0.0, NdL) * (A + (B * max(0.0, y) * C));

    //	return (lightColor * L1) / (2.0 * Pi);
    	return (albedo / Pi) * L1;
    }

	float Distribution(float ang, float roughness)
	{
		float a = roughness * roughness;
		float a2 = a * a;

		float den = (ang * ang) * (a2 - 1.0) + 1.0;
		return a2 / (Pi * den * den);
	}

	float Geometry(float NdL, float NdV, float roughness)
	{
		float r = (roughness + 1.0);
		float k = (r * r) / 8.0;

		float a = NdL / (NdL * (1.0 - k) + k);
		float b = NdV / (NdV * (1.0 - k) + k);

		return a * b;
	}

	vec3 Fresnel(float ang, vec3 F0)
	{
		return F0 + (vec3(1.0) - F0) * pow(1.0 - ang, 5.0);
	}

	void CTIntegrationVert(in FullscreenVertIn, out CTIntegrationFragIn)
	{
		outPosition = position;
		gl_Position = vec4(position.xyz, 1.0);
	}

    void vert(in VertIn, out FragIn)
	{
	    outViewVec = viewVec;
	    outTexCoords = vec2(position.xy) * 0.5 + 0.5;
	    gl_Position = vec4(position.xyz, 1.0);
	}

    void frag(in FragIn, out FragOut)
    {
    	// Sample GBuffer //
        vec4 GB0Sample = texture(GBufferFBO0, outTexCoords);
        vec4 GB1Sample = texture(GBufferFBO1, outTexCoords);
        vec4 GB2Sample = texture(GBufferFBO2, outTexCoords);
        vec4 GB3Sample = texture(GBufferFBO3, outTexCoords);
		vec4 CTIntegrationSample = texture(CTIntegrationTexture, outTexCoords);

		float alpha = 1.0;
        if(GB1Sample.w == 1.0)
            alpha = 0.0;

    	vec3 albedo = pow(GB2Sample.xyz, vec3(2.2));
    	vec3 normSample = GB1Sample.xyz;
        float AO = GB3Sample.z;
    	float shadowSample = GB3Sample.x;
    	vec3 shadow = vec3(shadowSample, shadowSample, shadowSample);
		float roughness = pow(GB2Sample.w, 2.2);
    	float metal = pow(GB3Sample.g, 2.2);
    	float depth = GB1Sample.w;
//		float lightMag = length(LightColor);
		float lightMag = LightIntensity;
		float lightingType = GB3Sample.w;
    
		vec3 P = GB0Sample.xyz;

		// Gen light vector (view space)
//       vec3 L = vec4(ViewMatrix * vec4(-lightVec, 0.0)).xyz;
//        L = normalize(L);
		vec3 L = normalize(-LightVec);
    
		// Gen normal vector (View space)
    	vec3 N = normSample.xyz;
    	N = normalize(N);

		// Gen view vector (view space)
//    	vec3 V = normalize(-outViewVec);
		vec3 V = normalize(CamPosWorldSpace - P);
		vec3 H = normalize(L + V);

//		vec4 Nw = inverse(ViewMatrix) * vec4(N, 0.0);
//		vec4 Vw = inverse(ViewMatrix) * vec4(outViewVec, 0.0);
//		vec3 reflVec = reflect(normalize(Vw.xyz), normalize(Nw.xyz));
		vec3 reflVec = reflect(normalize(V.xyz), normalize(N.xyz));
//		reflVec.y *= -1.0;
//		reflVec.z *= -1.0;
		reflVec.x *= -1.0;
		vec3 envSample = texture(SkyCubemap, reflVec).rgb;
//		vec3 irradianceSample = texture(irradianceMap, vec3(Nw.x, -Nw.y, -Nw.z)).rgb;
		vec3 irradianceSample = texture(IrradianceMap, vec3(-N.x, N.y, N.z)).rgb;

		// Cook Torrance
		if(lightingType == 0.0)
		{
			vec3 F0 = mix(vec3(0.04), albedo, vec3(metal));
			float NdL = max(0.0, dot(N, L));
			float NdV = max(0.0, dot(N, V));
			float NdH = max(0.0, dot(N, H));
			float VdH = max(0.0, dot(V, H));

			// Direct Lighting
			vec3 F = Fresnel(VdH, F0);
			float D = Distribution(NdH, roughness);
			float G = Geometry(NdL, NdV, roughness);

			vec3 KdDirect = mix(vec3(1.0) - F, vec3(0.0), vec3(metal));
			vec3 diffuseDirect = KdDirect * albedo;
			vec3 specularDirect = (F * D * G) / max(0.00001, 4.0 * NdL * NdV);
			vec3 direct = (diffuseDirect + specularDirect) * NdL * lightMag;

			// Ambient lighting
			vec3 Fambient = Fresnel(NdV, F0);
			vec3 KdAmbient = mix(vec3(1.0) - Fambient, vec3(0.0), metal);
			vec3 diffuseAmbient = KdAmbient * albedo * irradianceSample;

			vec2 specularAmbient = texture(CTIntegrationTexture, vec2(NdV, roughness)).rg;
			vec3 specularIBL = (F0 * specularAmbient.x + specularAmbient.y) * envSample;

			vec3 ambient = diffuseAmbient + specularIBL;

//			color = vec4(specularIBL, alpha);
			color = vec4(direct + ambient, alpha);	//WORKS

//			color = vec4(vec3(NdV), 1.0);
//			color = vec4(N, alpha);
		}

		// Oren Nayar
		else
		{
			vec3 ambient = vec3(0.01) * albedo * AO * shadow;

			vec3 Kd = oren_nayar(N, V, L, roughness, albedo);
			vec3 diffuse = Kd * shadow;

			vec3 final = pow(ambient + diffuse, vec3(1.0 / 2.2));

			color = vec4(pow(Kd * LightColor * shadow * AO, vec3(1.0 / 2.2)), alpha);
		}
    }


	void CTIntegrationFrag(in CTIntegrationFragIn, out FragOut)
	{
		float NdV = gl_FragCoord.x / textureDims.x;
		float roughness = gl_FragCoord.y / textureDims.y;

		NdV = max(0.00001, NdV);

		vec3 V = vec3(sqrt(1.0 - NdV * NdV), 0.0, NdV);
		float dfg1 = 0.0;
		float dfg2 = 0.0;

		for(int i = 0; i < CTSamples; i++)
		{
			vec2 u = hammersley(i);
			vec3 Lh = sampleGeometry(u.x, u.y, roughness);
			vec3 Li = 2.0 * dot(V, Lh) * Lh - V;

			float cosLi = Li.z;
			float cosLh = Lh.z;
			float cosLoLh = max(dot(V, Lh), 0.0);

			if(cosLi > 0.0)
			{
				float G = schlickGGX_IBL(cosLi, NdV, roughness);
				float Gv = G * cosLoLh / (cosLh * NdV);
				float Fc = pow(1.0 - cosLoLh, 5);

				dfg1 += (1.0 - Fc) * Gv;
				dfg2 += Fc * Gv;
			}
		}

		float invSamples = 1.0 / CTSamples;
		vec4 final = vec4(dfg1, dfg2, 0.0, 0.0) * vec4(invSamples);
		color = final;
	}


    technique PBRTechnique
    {
        pass p0
        {
            DepthTest = False;
			CullEnable = False;

			BlendEnabled = True;
			BlendFunc = SrcAlpha OneMinusSrcAlpha;

//			OutputBuffer = {DefaultSurface, 0, 0, 0, 0, 0, 0}
			OutputBuffer = HDRSurface;

            VertexShader = vert;
            FragmentShader = frag;
        };
    };


	technique CTIntegrationTechnique
	{
		pass p0
		{
			OutputBuffer = CTIntegrationTexture;

			DepthTest = False;
			CullEnable = False;

			VertexShader = CTIntegrationVert;
			FragmentShader = CTIntegrationFrag;
		};
	};
}