QuadrionShader Colored
{
    #include CameraUniforms.glslh
	#version 410 core

	interface VertIn
	{
        vec3 position : 0;
        vec3 normal : 1;
        vec2 uv0 : 2;
	};

	interface FragIn
	{
        vec4 oColor;
	};

	interface FragOut
	{
        vec4 color : 0;
	};

	uniform mat4 ModelMatrix;


	//////////////////////////////////////////////////////////////////////
	//
	//			SHADER FUNCS 
	//
	//////////////////////////////////////////////////////////////////////
	void vert(in VertIn, out FragIn)
	{
        vec4 ndcVertex = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(position, 1.0);
        oColor = vec4(1.0, 0.0, 0.0, 1.0);

        gl_Position = ndcVertex;
	}

	void frag(in FragIn, out FragOut)
	{
        color = oColor;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	//				TECHNIQUES 
	//
	////////////////////////////////////////////////////////////////////////////////
	technique FlatColor
	{
		pass p0
		{
			DepthTest = True;
			DepthFunc = Lequal;

			CullEnable = True;
			CullFace = Back;
			FrontFace = CCW;

			VertexShader = vert;
			FragmentShader = frag;
		};
	};
}