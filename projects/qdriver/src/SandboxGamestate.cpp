#include "SandboxGamestate.h"

#include <memory>
#include "Application.h"
#include "EngineContext.h"
#include "GameEngine.h"
#include "render/QGfx.h"
#include "assets/AssetManager.h"
#include "assets/TextureAsset.h"
#include "assets/CubemapAsset.h"
#include "render/VertexBufferLayout.h"
#include "render/VertexBuffer.h"
#include "render/IndexBuffer.h"
#include "render/QGfx.h"
#include "render/Render.h"
#include "QMath.h"
#include "core/Timer.h"

constexpr int Forward = 1, Backward = 2, Left = 4, Right = 8, Up = 16, Down = 24;
constexpr float		WASD_SPEED = 9.0f;

static float selectedObj = -1.0f;
static float lastSelectedObj = 1.0f;

int shit = 0;

static void SetPBRMaterials(ObjectMaterial* aObjMat, const char* aTechName, const char* aPassName,
							MeshNodeMaterial aMeshMat)
{
	Shader* shader = aObjMat->getEffect()->getTechnique(aTechName)->getPass(aPassName)->getShader();

	if (aMeshMat.albedoTexture > 0)
	{
//		aObjMat->setTexture("albedoTexture", aMeshMat.albedoTexture);
		shader->setTexture("albedoTexture", aMeshMat.albedoTexture);
		aObjMat->setUniform1b("bHasAlbedoTexture", true);
	}

	else
		aObjMat->setUniform1b("bHasAlbedoTexture", false);

	if (aMeshMat.emissiveTexture > 0)
	{
//		aObjMat->setTexture("emissiveTexture", aMeshMat.emissiveTexture);
		shader->setTexture("emissiveTexture", aMeshMat.emissiveTexture);
		aObjMat->setUniform1b("bHasEmissiveTexture", true);
	}

	else
		aObjMat->setUniform1b("bHasEmissiveTexture", false);

	if (aMeshMat.aoTexture > 0)
	{
//		aObjMat->setTexture("aoTexture", aMeshMat.aoTexture);
		shader->setTexture("aoTexture", aMeshMat.aoTexture);
		aObjMat->setUniform1b("bHasAOTexture", true);
	}

	else
		aObjMat->setUniform1b("bHasAOTexture", false);

	if (aMeshMat.roughnessTexture > 0)
	{
//		aObjMat->setTexture("roughnessTexture", aMeshMat.roughnessTexture);
		shader->setTexture("roughnessTexture", aMeshMat.roughnessTexture);
		aObjMat->setUniform1b("bHasRoughnessTexture", true);
	}

	else
		aObjMat->setUniform1b("bHasRoughnessTexture", false);

	if (aMeshMat.metallicTexture > 0)
	{
//		aObjMat->setTexture("metallicTexture", aMeshMat.metallicTexture);
		shader->setTexture("metallicTexture", aMeshMat.metallicTexture);
		aObjMat->setUniform1b("bHasMetallicTexture", true);
	}

	else
		aObjMat->setUniform1b("bHasMetallicTexture", false);

	if (aMeshMat.normalMap > 0)
	{
		shader->setTexture("normalMap", aMeshMat.normalMap);
		aObjMat->setUniform1b("bHasNormalMap", true);
	}

	else
		aObjMat->setUniform1b("bHasNormalMap", false);
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

SandboxGamestate::SandboxGamestate()
{
	mCharacterDirection = 0;
};

SandboxGamestate::~SandboxGamestate()
{
//	delete mSampleVAO;
	delete mEffectGui;
	delete mCamera;
};

bool SandboxGamestate::_isBufferCleared(uint32_t aId)
{
	for (uint32_t cleared : mClearedBuffers)
	{
		if(cleared == aId)
			return true;
	}

	return false;
}

void SandboxGamestate::_clearBuffer(uint32_t aId, vec4<float> aClearColor)
{
	if (!_isBufferCleared(aId))
	{
		glClearColor(aClearColor.x, aClearColor.y, aClearColor.z, aClearColor.w);
		GLboolean depthWriteEnabled;
		glGetBooleanv(GL_DEPTH_WRITEMASK, &depthWriteEnabled);
		glDepthMask(true);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if (!depthWriteEnabled)
			glDepthMask(false);
		mClearedBuffers.push_back(aId);
	}
}

void SandboxGamestate::_loadGeometry()
{
	mTestMesh = AssetManager::instance().load<MeshAsset>("TestMesh", ASSET_LOAD_MED_PRIO, "Media/models/lantern.FBX");

	TerrainInitializer terrainInit;
	terrainInit.worldMins = vec2<double>(0.0, 0.0);
	terrainInit.nPatchesX = 64;		//96
	terrainInit.nPatchesZ = 64;		//96
	terrainInit.nPixelsPerPatchX = 64;		//64
	terrainInit.nPixelsPerPatchZ = 64;		//64
	terrainInit.patchWidth = 1000.0f;
	terrainInit.patchDepth = 1000.0f;
	terrainInit.maxElevation = 4440.0f;				// 4440
	terrainInit.heightmapImagePath = "";
	terrainInit.heightmapImageFilename = "";
	terrainInit.entropy = 2.2;
	terrainInit.smoothing = 2.2;
	mTestTerrain = AssetManager::instance().load<ProceduralMeshAsset>("TestTerrain", ASSET_LOAD_MED_PRIO, terrainInit);
}

 
void SandboxGamestate::_loadEffects()
{
	std::string fxDir = "Media/effects";
	mEffectLoader = new CEffectLoader(fxDir.c_str());
}


void SandboxGamestate::onInitialize(const char** aArgs, size_t aNumArgs)
{
	GameEngine* ge = mEngineContext->engine;
	int32_t winWidth = ge->getWindowWidth();
	int32_t winHeight = ge->getWindowHeight();
	mViewportDimensions.set(static_cast<float>(winWidth), static_cast<float>(winHeight));

	mEngineContext->application->getApplicationWindow()->hideCursor();

	_loadEffects();
	_loadGeometry();
	_loadCamera(deg2Rad(65.0f), winWidth, winHeight);

	auto mColoredEffectAsset = AssetManager::instance().get<EffectAsset>("BlinnPhong");
	auto mColoredEffect = mColoredEffectAsset->getEffect();
	Effect* coloredEffect = mColoredEffect.get();
	Pass* coloredPass = mColoredEffect->getTechnique("BlinnPhongDeferred")->getPass("p0");

	auto mGBufferEffectAsset = AssetManager::instance().get<EffectAsset>("GBuffer");
	auto mGBufferEffect = mGBufferEffectAsset->getEffect();
	Effect* gBufferEffect = mGBufferEffect.get();
	Pass* gBufferPass = gBufferEffect->getTechnique("GBufferPBR")->getPass("p0");

	auto skyLightingEffectAsset = AssetManager::instance().get<EffectAsset>("SkyLightingEffect");
	auto skyLightingEffect = skyLightingEffectAsset->getEffect();
	Effect* pSkyLightingEffect = skyLightingEffect.get();
	Pass* pSkyLightingPass = pSkyLightingEffect->getTechnique("SkyLighting")->getPass("p0");
	Pass* pSkyCubemapPass = pSkyLightingEffect->getTechnique("SkyLightingCubemap")->getPass("p0");
	Pass* pIrradiancePass = pSkyLightingEffect->getTechnique("SkyIrradianceCubemap")->getPass("p0");

	auto PBREffectAsset = AssetManager::instance().get<EffectAsset>("PBRLighting");
	auto PBREffect = PBREffectAsset->getEffect();
	Effect* pPBREffect = PBREffect.get();
	Pass* pCTIntegrationPass = pPBREffect->getTechnique("CTIntegrationTechnique")->getPass("p0");
	Pass* pPBRPass = pPBREffect->getTechnique("PBRTechnique")->getPass("p0");

	auto HDREffectAsset = AssetManager::instance().get<EffectAsset>("HDR");
	auto HDREffect = HDREffectAsset->getEffect();
	Effect* pHDREffect = HDREffect.get();
	Pass* downScalePass = pHDREffect->getTechnique("HDRTechnique")->getPass("downScale4Pass");
	Pass* sampleLuminancePass = pHDREffect->getTechnique("HDRTechnique")->getPass("sampleLuminancePass");
	Pass* resampleLuminancePass1 = pHDREffect->getTechnique("HDRTechnique")->getPass("resampleLuminancePass1");
	Pass* resampleLuminancePass2 = pHDREffect->getTechnique("HDRTechnique")->getPass("resampleLuminancePass2");
	Pass* finalLuminancePass = pHDREffect->getTechnique("HDRTechnique")->getPass("finalLuminancePass");
	Pass* brightPass = pHDREffect->getTechnique("HDRTechnique")->getPass("brightPass");
	Pass* gaussPass = pHDREffect->getTechnique("HDRTechnique")->getPass("gaussPass");
	Pass* downScale2Pass = pHDREffect->getTechnique("HDRTechnique")->getPass("downScale2Pass");
	Pass* gaussPass2 = pHDREffect->getTechnique("HDRTechnique")->getPass("gaussPass2");
	Pass* bloomPass = pHDREffect->getTechnique("HDRTechnique")->getPass("bloomPass");
	Pass* bloomPass2 = pHDREffect->getTechnique("HDRTechnique")->getPass("bloomPass2");
	Pass* finalPass = pHDREffect->getTechnique("HDRTechnique")->getPass("finalPass");
	Pass* calcAdaptPass = pHDREffect->getTechnique("HDRTechnique")->getPass("adaptationPass");
	Pass* calcAdaptPass1 = pHDREffect->getTechnique("HDRTechnique")->getPass("adaptationPass1");

	mat4<float> T;
	T.loadTranslation(40.0f, 0.0f, 0.0f);

	// Add material to renderer
	// THIS HAPPENS IN ORDER! The order in which object materials are created is the order
	// in which they are rendered in the render loop
	QGfx::addObjectMaterial(gBufferEffect, "GBufferPBR", "p0", "gBufferMaterial1");
	QGfx::addObjectMaterial(gBufferEffect, "GBufferPBR", "p0", "gBufferMaterial2");
	QGfx::addObjectMaterial(gBufferEffect, "GBufferPBR", "p0", "gBufferMaterial3");
	QGfx::addObjectMaterial(pSkyLightingEffect, "SkyLighting", "p0", "skyLightingMaterial");
	QGfx::addObjectMaterial(coloredEffect, "BlinnPhongDeferred", "p0", "lightingDeferredMaterial");
	QGfx::addObjectMaterial(pSkyLightingEffect, "SkyLightingCubemap", "p0", "skyLightingCubeMaterial");
	QGfx::addObjectMaterial(pSkyLightingEffect, "SkyIrradianceCubemap", "p0", "skyIrradianceCubeMaterial");
	QGfx::addObjectMaterial(pPBREffect, "CTIntegrationTechnique", "p0", "ctIntegrationMaterial");
	QGfx::addObjectMaterial(pPBREffect, "PBRTechnique", "p0", "pbrMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "downScale4Pass", "hdrDownScaleMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "sampleLuminancePass", "hdrSampleLuminanceMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "resampleLuminancePass1", "hdrResampleLuminanceMaterial1");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "resampleLuminancePass2", "hdrResampleLuminanceMaterial2");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "finalLuminancePass", "hdrFinalLuminanceMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "brightPass", "hdrBrightPassMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "gaussPass", "hdrGaussMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "downScale2Pass", "hdrDownScale2Material");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "gaussPass2", "hdrGauss2Material");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "bloomPass", "hdrBloomMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "bloomPass2", "hdrBloom2Material");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "finalPass", "hdrFinalMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "adaptationPass", "hdrAdaptationMaterial");
	QGfx::addObjectMaterial(pHDREffect, "HDRTechnique", "adaptationPass1", "hdrAdaptation1Material");

	// Associate renderable mesh to material
	QGfx::addRenderableToMaterial("gBufferMaterial1", mTestMesh);
	QGfx::addRenderableToMaterial("gBufferMaterial2", mTestMesh, T);
	T.loadTranslation(80.0f, 0.0f, 0.0f);
	QGfx::addRenderableToMaterial("gBufferMaterial3", mTestMesh, T);
	
	std::shared_ptr<RenderObject> fullscreenRO = Render::getFullscreenRenderObj();
	QGfx::addRenderableToMaterial("skyLightingMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("lightingDeferredMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("skyLightingCubeMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("skyIrradianceCubeMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("ctIntegrationMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("pbrMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrDownScaleMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrSampleLuminanceMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrResampleLuminanceMaterial1", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrResampleLuminanceMaterial2", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrFinalLuminanceMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrBrightPassMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrGaussMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrDownScale2Material", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrGauss2Material", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrBloomMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrBloom2Material", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrFinalMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrAdaptationMaterial", fullscreenRO);
	QGfx::addRenderableToMaterial("hdrAdaptation1Material", fullscreenRO);

	
	// Set render order
	QGfx::pushToRenderQueue("skyLightingMaterial");
	QGfx::pushToRenderQueue("skyLightingCubeMaterial");
	QGfx::pushToRenderQueue("skyIrradianceCubeMaterial");
	QGfx::pushToRenderQueue("ctIntegrationMaterial");
	QGfx::pushToRenderQueue("gBufferMaterial1");
	QGfx::pushToRenderQueue("gBufferMaterial2");
	QGfx::pushToRenderQueue("gBufferMaterial3");
	QGfx::pushToRenderQueue("pbrMaterial");
	QGfx::pushToRenderQueue("hdrDownScaleMaterial");
	QGfx::pushToRenderQueue("hdrSampleLuminanceMaterial");
	QGfx::pushToRenderQueue("hdrResampleLuminanceMaterial1");
	QGfx::pushToRenderQueue("hdrResampleLuminanceMaterial2");
	QGfx::pushToRenderQueue("hdrFinalLuminanceMaterial");
//	QGfx::pushToRenderQueue("hdrAdaptationMaterial");
	QGfx::pushToRenderQueue("hdrBrightPassMaterial");
	QGfx::pushToRenderQueue("hdrGaussMaterial");
	QGfx::pushToRenderQueue("hdrDownScale2Material");
	QGfx::pushToRenderQueue("hdrGauss2Material");
	QGfx::pushToRenderQueue("hdrBloomMaterial");
	QGfx::pushToRenderQueue("hdrBloom2Material");
	QGfx::pushToRenderQueue("hdrFinalMaterial");
	
	// Generate skybox cubemaps
	vec3<float> skyboxCenter(8000.0f, 1.0f, 8000.0f);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(1.0f, 0.0f, 0.0f), 512, 512, mCubemapCameras[0]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(-1.0f, 0.0f, 0.0f), 512, 512, mCubemapCameras[1]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, 0.0f, -1.0f), 512, 512, mCubemapCameras[2]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, 0.0f, 1.0f), 512, 512, mCubemapCameras[3]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, -1.0f, 0.0f), 512, 512, mCubemapCameras[4]);
	GenerateCubemapCamera(skyboxCenter, vec3<float>(0.0f, 1.0f, 0.0f), 512, 512, mCubemapCameras[5]);

	// GUI must be initialized after all effets and object associations are made
	mEffectGui = new CEffectGui;
	mEffectGui->initialize(winWidth, winHeight);
}

void SandboxGamestate::_loadCamera(float aFov, uint32_t aWinWidth, uint32_t aWinHeight)
{
	mCamera = new Camera();

	vec3<float> up(0.0f, 1.0f, 0.0f);
	vec3<float> pos(0.0f, 40.0f, -40.0f);
	vec3<float> lookAt;
	vec3<float> lookDir(0.0f, -1.0f, 1.0f);

	lookAt = pos + lookDir;

	mCamera->setPerspective(aFov, (float)aWinWidth / (float)aWinHeight, 0.7f, 2000.0f);
	mCamera->setCamera(pos, lookAt, up);
}


void SandboxGamestate::spawnPlayer(vec3<float> aPosition)
{

}

void SandboxGamestate::tick(const UpdateContext& aUpdateContext)
{

}

void SandboxGamestate::update(const UpdateContext& aUpdateContext)
{
	float dt = static_cast<float>(aUpdateContext.elapsed);
	vec2<int32_t> mouse = InputManager::instance().getMousePositionFromCenter();


	// Update Camera pose //
	if(InputManager::instance().isCursorCentering())
	{
		auto pINI = mEngineContext->application->getApplicationINI();
		float sensitivity = pINI->getFloat("input", "sensitivity");

		mCamera->update(mouse.x, mouse.y, sensitivity);
	}

	vec2<int32_t> guiMouse = InputManager::instance().getMousePosition();
	mEffectGui->setMousePos(guiMouse);
	

	if ((mCharacterDirection & Forward) == Forward)
		mCamera->moveForward(WASD_SPEED * dt);

	if ((mCharacterDirection & Backward) == Backward)
		mCamera->moveBack(WASD_SPEED * dt);

	if ((mCharacterDirection & Left) == Left)
		mCamera->moveLeft(WASD_SPEED * dt);

	if ((mCharacterDirection & Right) == Right)
		mCamera->moveRight(WASD_SPEED * dt);

//	std::vector<std::string> changedFx = mEffectLoader->rescanEffects();
}

void SandboxGamestate::render(const UpdateContext& aUpdateContext)
{
	GLint vp[4];
	vec3<float> vLightVec, vLightColor, vSurfaceColor;
	float lightIntensity;
	float rayleighBrightness, rayleighCollectionPower, rayleighStrength, rayleighDistribution;
	float mieDistribution, mieBrightness, mieCollectionPower, mieStrength;
	float scatterStrength, surfaceHeight;
	float middleGrey, brightnessThreshold, brightnessOffset, bloomScale, whitePoint, exposure;
	bindDefaultSurface();
	_clearBuffer(0, vec4<float>(0.0f, 0.0f, 0.0f, 1.0f));

	// Update Camera
	mCamera->render();
	mat4<float> viewMatrix = QGfx::getMatrix(VIEW);
	mat4<float> projectionMatrix = QGfx::getMatrix(PROJECTION);
	vec3<float> camPos = mCamera->getPosition();

	// Execute GBuffer pass on all geometry bound
	Technique* currentTechnique = nullptr;
	Pass* currentPass = nullptr;
	Shader* currentShader = nullptr;
	Effect* currentEffect = nullptr;
	Material* currentUBO = nullptr;

	// GBuffer pass
	auto renderQueue = QGfx::getRenderQueue();
	size_t nRenderPasses = renderQueue.size();

	auto materialMap = QGfx::getObjectMaterialMap();
	mat4<float> worldMatrix, modelMatrix;
	while(renderQueue.size() > 0)
	{
		std::string currentMatName = renderQueue.front();
		renderQueue.pop();
		std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>> material = materialMap[currentMatName];

		size_t nRenderObjects = material.second.size();
		if(nRenderObjects <= 0)
			continue;

		const char* matName = material.first->getName();
		currentEffect = material.first->getEffect();
		const char* fxName = currentEffect->getName();
		char techName[64];
		strcpy(techName, material.first->getTechniqueName());
		 
		currentTechnique = currentEffect->getTechnique(techName);
		char passName[64];
		strcpy(passName, material.first->getPassName());
		currentPass = currentTechnique->getPass(passName);

		// Get GUI values 
		mEffectGui->getData(fxName, nullptr, "LightVec", &vLightVec.x);
		mEffectGui->getData(fxName, nullptr, "LightColor", &vLightColor.x);
		mEffectGui->getData(fxName, nullptr, "LightIntensity", &lightIntensity);
		mEffectGui->getData(fxName, matName, "surfaceColor", &vSurfaceColor.x);
		mEffectGui->getData(fxName, nullptr, "rayleighBrightness", &rayleighBrightness);
		mEffectGui->getData(fxName, nullptr, "rayleighCollectionPower", &rayleighCollectionPower);
		mEffectGui->getData(fxName, nullptr, "rayleighStrength", &rayleighStrength);
		mEffectGui->getData(fxName, nullptr, "mieDistribution", &mieDistribution);
		mEffectGui->getData(fxName, nullptr, "rayleighDistribution", &rayleighDistribution);
		mEffectGui->getData(fxName, nullptr, "mieBrightness", &mieBrightness);
		mEffectGui->getData(fxName, nullptr, "mieCollectionPower", &mieCollectionPower);
		mEffectGui->getData(fxName, nullptr, "mieStrength", &mieStrength);
		mEffectGui->getData(fxName, nullptr, "scatterStrength", &scatterStrength);
		mEffectGui->getData(fxName, nullptr, "surfaceHeight", &surfaceHeight);

		mEffectGui->getData(fxName, nullptr, "g_middleGrey", &middleGrey);
		mEffectGui->getData(fxName, nullptr, "g_brightnessThreshold", &brightnessThreshold);
		mEffectGui->getData(fxName, nullptr, "g_brightnessOffset", &brightnessOffset);
		mEffectGui->getData(fxName, nullptr, "g_bloomScale", &bloomScale);
		mEffectGui->getData(fxName, nullptr, "g_whitePoint", &whitePoint);
		mEffectGui->getData(fxName, nullptr, "g_exposure", &exposure);


		currentShader = currentPass->getShader();

		// Bind specified buffer
		OutputBufferInfo* obi = currentPass->getOutputBuffer();
		QGfx::bindRenderTarget(obi);
		vec4<float> clearColor = currentPass->getClearColor();
		_clearBuffer(QGfx::getRenderBuffer(obi->name).id, clearColor);
		currentPass->bind();
	
		glGetIntegerv(GL_VIEWPORT, vp);

		// Set and bind any UBOs
		vec3<float> white(1.0f, 1.0f, 1.0f);
		if (currentTechnique->hasUBO("CameraUniforms"))
		{
			currentUBO = currentTechnique->getMaterial("CameraUniforms");
			currentUBO->setUniform("ViewMatrix", &viewMatrix);
			currentUBO->setUniform("ProjectionMatrix", &projectionMatrix);
			currentUBO->setUniform("ViewportDimensions", &mViewportDimensions);
			currentUBO->setUniform("CamPosWorldSpace", &camPos);

			currentUBO->bind(currentShader);
		}

		if (currentTechnique->hasUBO("LightUniforms"))
		{
			currentUBO = currentTechnique->getMaterial("LightUniforms");
			currentUBO->setUniform("LightVec", &vLightVec.x);
			currentUBO->setUniform("LightColor", &vLightColor.x);
			currentUBO->setUniform("LightIntensity", &lightIntensity);
			currentUBO->bind(currentShader);
		}

		if (currentTechnique->hasUBO("SkyUniforms"))
		{
			currentUBO = currentTechnique->getMaterial("SkyUniforms");
			currentUBO->setUniform("rayleighBrightness", &rayleighBrightness);
			currentUBO->setUniform("rayleighCollectionPower", &rayleighCollectionPower);
			currentUBO->setUniform("rayleighStrength", &rayleighStrength);
			currentUBO->setUniform("mieDistribution", &mieDistribution);
			currentUBO->setUniform("mieBrightness", &mieBrightness);
			currentUBO->setUniform("mieCollectionPower", &mieCollectionPower);
			currentUBO->setUniform("mieStrength", &mieStrength);
			currentUBO->setUniform("scatterStrength", &scatterStrength);
			currentUBO->setUniform("surfaceHeight", &surfaceHeight);
			currentUBO->bind(currentShader);
		}

		if (currentTechnique->hasUBO("CubemapCameraUniforms"))
		{
			vec2<float> shit(64.0f, 64.0f);
			currentUBO = currentTechnique->getMaterial("CubemapCameraUniforms");
			currentUBO->setUniform("ProjectionMatrixRight", &mCubemapCameras[0].projMat);
			currentUBO->setUniform("ProjectionMatrixLeft", &mCubemapCameras[1].projMat);
			currentUBO->setUniform("ProjectionMatrixFront", &mCubemapCameras[2].projMat);
			currentUBO->setUniform("ProjectionMatrixBack", &mCubemapCameras[3].projMat);
			currentUBO->setUniform("ProjectionMatrixUp", &mCubemapCameras[4].projMat);
			currentUBO->setUniform("ProjectionMatrixDown", &mCubemapCameras[5].projMat);

			currentUBO->setUniform("ViewMatrixRight", &mCubemapCameras[0].viewMat);
			currentUBO->setUniform("ViewMatrixLeft", &mCubemapCameras[1].viewMat);
			currentUBO->setUniform("ViewMatrixFront", &mCubemapCameras[2].viewMat);
			currentUBO->setUniform("ViewMatrixBack", &mCubemapCameras[3].viewMat);
			currentUBO->setUniform("ViewMatrixUp", &mCubemapCameras[4].viewMat);
			currentUBO->setUniform("ViewMatrixDown", &mCubemapCameras[5].viewMat);

			if(strcmp(techName, "SkyIrradianceCubemap") == 0)
				currentUBO->setUniform("cubemapViewportDimensions", &shit);
			else
				currentUBO->setUniform("cubemapViewportDimensions", &mCubemapCameras[0].viewportDims);
			currentUBO->bind(currentShader);
		}

		if (currentTechnique->hasUBO("HDRUniforms"))
		{
			currentUBO = currentTechnique->getMaterial("HDRUniforms");

			currentUBO->setUniform("g_middleGrey", &middleGrey);
			currentUBO->setUniform("g_brightnessThreshold", &brightnessThreshold);
			currentUBO->setUniform("g_brightnessOffset", &brightnessOffset);
			currentUBO->setUniform("g_bloomScale", &bloomScale);
			currentUBO->setUniform("g_whitePoint", &whitePoint);
			currentUBO->setUniform("g_exposure", &exposure);
			currentUBO->bind(currentShader);
		}

		// TOTAL HACK! MOVE THIS!!!
		if (strcmp(techName, "BlinnPhongDeferred") == 0)
		{
			GLuint mGBuffer0 = QGfx::getRenderTarget("GBufferFBO0");
			GLuint mGBuffer1 = QGfx::getRenderTarget("GBufferFBO1");
			GLuint mGBuffer2 = QGfx::getRenderTarget("GBufferFBO2");
			GLuint mGBuffer3 = QGfx::getRenderTarget("GBufferFBO3");
			currentShader->setTexture("GBufferFBO0", mGBuffer0);
			currentShader->setTexture("GBufferFBO1", mGBuffer1);
			currentShader->setTexture("GBufferFBO2", mGBuffer2);
			currentShader->setTexture("GBufferFBO3", mGBuffer3);
		}

		if (strcmp(techName, "CTIntegrationTechnique") == 0)
		{
			vec2<float> dims(64.0f, 64.0f);
			currentShader->setUniform2f("textureDims", dims);
		}

		if (strcmp(techName, "HDRTechnique") == 0)
		{
			vec2<float> avSamples[16];
			vec2<float> sampOffsets[16];
			vec4<float> sampWeights[16];
			float fSampleOffsets[16];

			
			
			if (strcmp(passName, "downScale4Pass") == 0)
			{
				uint32_t hdrWidth = (uint32_t)1440;
				uint32_t hdrHeight = (uint32_t)900;
				get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
				currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
			}

			if (strcmp(passName, "sampleLuminancePass") == 0)
			{
				uint32_t hdrWidth = (uint32_t)360;
				uint32_t hdrHeight = (uint32_t)225;
				get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
				currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
			}

			if (strcmp(passName, "resampleLuminancePass1") == 0)
			{
				uint32_t hdrWidth = (uint32_t)64;
				uint32_t hdrHeight = (uint32_t)64;
				get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
				currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
			}

			if (strcmp(passName, "resampleLuminancePass2") == 0)
			{
				uint32_t hdrWidth = (uint32_t)16;
				uint32_t hdrHeight = (uint32_t)16;
				get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
				currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
			}

			if (strcmp(passName, "finalLuminancePass") == 0)
			{
				uint32_t hdrWidth = (uint32_t)4;
				uint32_t hdrHeight = (uint32_t)4;
				get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);
				currentShader->setUniform2fv("g_staticSampleOffsets", avSamples, 16);
			}
			
			if (strcmp(passName, "gaussPass") == 0)
			{
				uint32_t brightSurfWidth = 720;
				uint32_t brightSurfHeight = 450;
				get5x5GaussianOffsets(brightSurfWidth, brightSurfHeight, sampOffsets, sampWeights, 1.0F);
				currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
				currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
			}

			if (strcmp(passName, "downScale2Pass") == 0)
			{
				uint32_t brightSurfWidth = 180;
				uint32_t brightSurfHeight = 112;
				get2x2SampleOffsets(brightSurfWidth, brightSurfHeight, sampOffsets);
				currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
			}

			if (strcmp(passName, "gaussPass2") == 0)
			{
				uint32_t bloomWidth = 180;
				uint32_t bloomHeight = 112;
				get5x5GaussianOffsets(bloomWidth, bloomHeight, sampOffsets, sampWeights, 1.0F);
				currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
				currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
			}

			if (strcmp(passName, "bloomPass") == 0)
			{
				uint32_t tempBloomWidth2 = 180;
				getBloomOffsets(tempBloomWidth2, fSampleOffsets, sampWeights, 3.0F, 2.0F);

				for (int i = 0; i < 16; ++i)
				{
					sampOffsets[i].x = fSampleOffsets[i];
					sampOffsets[i].y = 0.0F;
				}

				currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
				currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
			}

			if (strcmp(passName, "bloomPass2") == 0)
			{
				uint32_t tempBloomHeight1 = 112;
				getBloomOffsets(tempBloomHeight1, fSampleOffsets, sampWeights, 3.0F, 2.0F);
				for (int i = 0; i < 16; ++i)
				{
					sampOffsets[i].x = 0.0F;
					sampOffsets[i].y = fSampleOffsets[i];
				}

				currentShader->setUniform2fv("g_staticSampleOffsets", sampOffsets, 16);
				currentShader->setUniform4fv("g_staticSampleWeights", sampWeights, 16);
			}
		}

		auto test = currentPass->getSamplerLoadPaths();
		size_t nTextures = test.size();
		if (nTextures > 0)
		{
			for (auto texName : test)
			{
				std::string texPath = texName.second.second;
				std::string name = texName.first;
				if (texName.second.first == EffectVariableType::SAMPLER2D)
				{
					auto texAsset = AssetManager::instance().get<TextureAsset>(name);
					if(texAsset->getTextureID() > 0)
						currentShader->setTexture(name, texAsset->getTextureID());
					else
					{
						if(texPath != "")
						{
							GLuint id = QGfx::getRenderTarget(texPath);
							if(id > 0)
								currentShader->setTexture(name, id);
						}
						else
						{
							GLuint id = QGfx::getRenderTarget(name);
							if (id > 0)
								currentShader->setTexture(name, id);
						}
					}
				}

				else if (texName.second.first == EffectVariableType::SAMPLERCUBE)
				{
					auto texAsset = AssetManager::instance().get<CubemapAsset>(name);
					currentShader->setCubemapTexture(name, texAsset->getTexture());
				}
			}
		}

		// loop over all renderable objects and render
		for(size_t i = 0; i < nRenderObjects; i++)
		{
			std::shared_ptr<RenderObject> ro = material.second[i];
			if(!ro)
				continue;
			float objID = static_cast<float>(ro->getID());
			worldMatrix = ro->getWorldTransform();
			
			std::vector<std::shared_ptr<MeshNodeRod>> thisMesh = ro->getMeshes();

			size_t nMeshes = thisMesh.size();
			if(nMeshes <= 0)
				break;

			bool isGuiSelected = ro->isGuiSelected() && mEffectGui->isVisible();
			float guiSelected = 0.0f;
			if(isGuiSelected)
				guiSelected = 1.0f;
			for(size_t j = 0; j < nMeshes; j++)
			{
				MeshNodeMaterial pbrMaterials = thisMesh[j]->meshMaterial;

				material.first->bind();
				modelMatrix = thisMesh[j]->transform;
				modelMatrix *= worldMatrix;
				material.first->setUniformMat4("ModelMatrix", modelMatrix);		// LEAK
				material.first->setUniform1f("isGuiSelected", guiSelected);
				material.first->setUniform3f("surfaceColor", vSurfaceColor);
				material.first->setUniform1f("RenderObjectID", objID);

				if (strcmp(techName, "GBufferPBR") == 0)
					SetPBRMaterials(material.first, techName, "p0", pbrMaterials);
				
				thisMesh[j]->mesh->bind();
				glDrawElements(GL_TRIANGLES, thisMesh[j]->mesh->getIndexCount(), GL_UNSIGNED_INT, 0);
				thisMesh[j]->mesh->unbind();

				material.first->unbind();
			}
		}

		currentPass->unbind();
	}
	
	bindDefaultSurface();

	glGetIntegerv(GL_VIEWPORT, vp);

	// render IMGUI //
	int mouseX;
	int mouseY;
	mEngineContext->application->getMousePosition(mouseX, mouseY);
	mEffectGui->setMousePos({ mouseX, mouseY });
	mEffectGui->begin();
	if(mEffectGui->isVisible())
	{
		mEffectGui->update(aUpdateContext.elapsed);	
		mEffectGui->updateSelected(aUpdateContext.elapsed, static_cast<uint32_t>(selectedObj));
	}

	EffectGuiStats stats;
	stats.camPos = mCamera->getPosition();
	stats.dt = aUpdateContext.elapsed;
	stats.selectedObjectID = selectedObj;
	if(mEffectGui->areStatsVisible())
		mEffectGui->updateStats(stats);

	mEffectGui->end();
	mEffectGui->render();

	QGfx::resetBufferClearFlags();
	mClearedBuffers.clear();
}

void SandboxGamestate::onKeyEvent(const int aKey, const EKeyAction aAction, const int aMods)
{
	if (aKey == VKEY_TAB && aAction == EKeyAction::PRESS)
	{
		bool isCursorCentered = InputManager::instance().isCursorCentering();

		InputManager::instance().setCenterCursor(!isCursorCentered);
		isCursorCentered = !isCursorCentered;

		if (isCursorCentered)
			mEngineContext->application->getApplicationWindow()->hideCursor();
		else
			mEngineContext->application->getApplicationWindow()->showCursor();

		mEffectGui->toggleVisibility();

		return;
	}

	if (aKey == VKEY_ESCAPE && aAction == EKeyAction::PRESS)
	{
		mEngineContext->engine->quit();
		return;
	}

	if ((aKey == 'W' || aKey == 'w') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Forward;

	if ((aKey == 'W' || aKey == 'w') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Forward;

	if ((aKey == 'S' || aKey == 's') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Backward;

	if ((aKey == 'S' || aKey == 's') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Backward;

	if ((aKey == 'A' || aKey == 'a') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Left;

	if ((aKey == 'A' || aKey == 'a') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Left;

	if ((aKey == 'D' || aKey == 'd') && aAction == EKeyAction::PRESS)
		mCharacterDirection |= Right;

	if ((aKey == 'D' || aKey == 'd') && aAction == EKeyAction::RELEASE)
		mCharacterDirection ^= Right;

	if ((aKey == 'Z' || aKey == 'z') && aAction == EKeyAction::PRESS)
	{
		mEffectGui->toggleStatsVisibility();
	}

	mEffectGui->keyEvent(aKey, static_cast<int>(aAction));
}

void SandboxGamestate::onCharEvent(const int aCodepoint)
{

}

void SandboxGamestate::onGainedFocus()
{

}

void SandboxGamestate::onMouseButton(const EMouseButton aButton, const EMouseButtonAction aAction)
{
	mEffectGui->mouseEvent(static_cast<int>(aButton), static_cast<int>(aAction));

	if (aAction == EMouseButtonAction::PRESS)
	{
		// Get GBuffer FBO and pass to GUI for object picking if GUI is visible.
		FboId fbo = QGfx::getRenderBuffer("GBufferFBO");
		vec2<int32_t> mousePos = InputManager::instance().getMousePosition();

		if (mEffectGui->isVisible() && !mEffectGui->isMouseHovering(mousePos))
		{
			selectedObj = mEffectGui->getSelectedObject(fbo, mousePos);
			if(selectedObj != lastSelectedObj)
			{
				std::shared_ptr<RenderObject> ro = QGfx::getRenderObject(static_cast<uint32_t>(selectedObj));
				if(ro)
					ro->setGuiSelected(true);

				ro = QGfx::getRenderObject(static_cast<uint32_t>(lastSelectedObj));
				if(ro)
					ro->setGuiSelected(false);

				lastSelectedObj = selectedObj;
			}
		}
	}
}

void SandboxGamestate::onMouseMove(const double aX, const double aY)
{

}

void SandboxGamestate::onMouseWheelScroll(const double aDeltaX, const double aDeltaY)
{

}
