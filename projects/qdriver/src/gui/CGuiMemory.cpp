#include "gui/CGuiMemory.h"

#include <string.h>

CGuiMemory::CGuiMemory()
{

}

CGuiMemory::~CGuiMemory()
{

}

void* CGuiMemory::addMemory(const char* aName, const char* aMaterialName, uint32_t nBytes, bool isGlobal)
{
	if (isGlobal)
	{
		auto iter = mGlobalMemory.find(aName);
		if (iter != mGlobalMemory.end())
		{
			return iter->second;
		}

		else
		{
			void* mem = malloc(nBytes);
			memset(mem, 0, nBytes);
			mGlobalMemory[aName] = mem;
			mGlobalMemorySize[aName] = nBytes;
			return mem;
		}
	}

	else
	{
		auto materialIter = mMemory.find(aMaterialName);
		if (materialIter != mMemory.end())
		{
			auto uniIter = materialIter->second.find(aName);
			if (uniIter != materialIter->second.end())
			{
				return uniIter->second;
			}

			else
			{
				void* mem = malloc(nBytes);
				memset(mem, 0, nBytes);
				mMemory[aMaterialName][aName] = mem;
				mGlobalMemorySize[aName] = nBytes;
				return mem;
			}
		}

		else
		{
			// material doesn't exist
			void* mem = malloc(nBytes);
			memset(mem, 0, nBytes);
			mMemory[aMaterialName][aName] = mem;
			mGlobalMemorySize[aName] = nBytes;
			return mem;
		}
	}
}

void* CGuiMemory::getMemory(const char* aName, const char* aMaterialName, bool isGlobal)
{
	if (isGlobal)
	{
		if (mGlobalMemory.size() > 0)
		{
			auto iter = mGlobalMemory.find(aName);
			if (iter != mGlobalMemory.end())
				return iter->second;

			else
				return nullptr;
		}
	}

	else
	{
		if(mMemory.size() > 0 && aMaterialName)
		{
			auto materialIter = mMemory.find(aMaterialName);
			if (materialIter != mMemory.end())
			{
				auto uniIter = materialIter->second.find(aName);
				if (uniIter != materialIter->second.end())
					return uniIter->second;
				else
					return nullptr;
			}

			else
				return nullptr;
		}

		else
			return nullptr;
	}

	return nullptr;
}

std::vector<std::string> CGuiMemory::getAttachedMaterials()
{
	std::vector<std::string> v;
	for (auto iter : mMemory)
	{
		v.push_back(iter.first);
	}

	return v;
}

size_t CGuiMemory::getUniformSize(const char* aUniformName)
{
	auto memIter = mGlobalMemorySize.find(aUniformName);
	if(memIter != mGlobalMemorySize.end())
		return memIter->second;

	return 0;
}


