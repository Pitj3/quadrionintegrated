#include "gui/CEffectGui.h"
#include "render/QGfx.h"
#include "render/VertexBufferLayout.h"
#include "render/VertexBuffer.h"
#include "render/IndexBuffer.h"
#include "render/VertexArray.h"
#include "render/Effect.h"
#include "assets/AssetManager.h"
#include "assets/EffectAsset.h"
#include "core/QLog.h"
#include "InputManager.h"
#include "core/Timer.h"

CEffectGui::CEffectGui()
{
	mIsVisible = false;
	mIsTestWindowVisible = true;
	mIsInitialized = false;
	mAreStatsVisible = true;
}

CEffectGui::~CEffectGui()
{

}


void CEffectGui::initialize(const int aFbWidth, const int aFbHeight)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGuiIO& io = ImGui::GetIO();
	io.DisplaySize = ImVec2((float)aFbWidth, (float)aFbHeight);

	// Init fontmap 
	uint8_t* pix;
	int32_t width, height;
	io.Fonts->GetTexDataAsRGBA32(&pix, &width, &height);
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pix);
	io.Fonts->TexID = (ImTextureID)(intptr_t)tex;

	// initialize effect debug windows
	std::vector<ObjectMaterial*> objectMaterials = QGfx::getObjectMaterials();
	std::vector<std::shared_ptr<EffectAsset>> fxAssets = AssetManager::instance().get<EffectAsset>();

	CGuiMemory* thisMem = new CGuiMemory;
	size_t nEffectAssets = fxAssets.size();
	for (size_t i = 0; i < nEffectAssets; i++)
	{
		std::shared_ptr<Effect> thisEffect = fxAssets[i]->getEffect();
		if(!thisEffect)
			continue;

//		CGuiMemory* thisMem = new CGuiMemory;
		std::vector<std::string> guiUniforms = thisEffect->getGUIUniformNames();
		mEffectNames.push_back(fxAssets[i]->getName());


		size_t nGuiUniforms = guiUniforms.size();
		for (size_t j = 0; j < nGuiUniforms; j++)
		{
			std::string uniName = guiUniforms[j];
			std::string uniType = thisEffect->getUniformGUIDebugElementType(uniName);
			std::pair<float, float> minmax = thisEffect->getUniformGUIDebugElementData(uniName);
			float defaultVal = thisEffect->getUniformElementDefaultValue(uniName);
			float max = minmax.second;
			if(defaultVal == FLT_MIN)
				defaultVal = max;
			

			if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), nullptr, sizeof(float), true);
				float* val = (float*)mem;
				*val = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT2") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), nullptr, sizeof(float) * 2, true);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT3") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), nullptr, sizeof(float) * 3, true);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT4") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), nullptr, sizeof(float) * 4, true);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_GLOBAL_COLORPICKER3") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), nullptr, sizeof(float) * 3, true);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_GLOBAL_COLORPICKER4") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), nullptr, sizeof(float) * 4, true);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
		}

//		mEffectMemory[thisEffect->getName()] = thisMem;
	}

	mEffectMemory = thisMem;

	// roll over materials
	size_t nObjectMaterials = objectMaterials.size();
	for (size_t i = 0; i < nObjectMaterials; i++)
	{
		std::string matName = objectMaterials[i]->getName();
		Effect* thisEffect = objectMaterials[i]->getEffect();

		if(!thisEffect)
			continue;

		std::vector<std::string> guiUniforms = thisEffect->getGUIUniformNames();
		size_t nGuiUniforms = guiUniforms.size();

//		auto mem = mEffectMemory.find(thisEffect->getName());
		CGuiMemory* thisMem = nullptr;
		thisMem = mEffectMemory;
//		if (mem->second)
//			thisMem = mem->second;
//		else
//			continue;

		for (size_t j = 0; j < nGuiUniforms; j++)
		{
			std::string uniName = guiUniforms[j];
			std::string uniType = thisEffect->getUniformGUIDebugElementType(uniName);
			std::pair<float, float> minmax = thisEffect->getUniformGUIDebugElementData(uniName);
			float defaultVal = thisEffect->getUniformElementDefaultValue(uniName);
			float max = minmax.second;
			if(defaultVal == FLT_MIN)
				defaultVal = max;
			
			if (uniType.compare("GUI_DEBUG_FLOAT") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), matName.c_str(), sizeof(float), false);
				float* val = (float*)mem;
				*val = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_FLOAT2") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), matName.c_str(), sizeof(float) * 2, false);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_FLOAT3") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), matName.c_str(), sizeof(float) * 3, false);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_FLOAT4") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), matName.c_str(), sizeof(float) * 4, false);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_COLORPICKER3") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), matName.c_str(), sizeof(float) * 3, false);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
			else if (uniType.compare("GUI_DEBUG_COLORPICKER4") == 0)
			{
				void* mem = thisMem->addMemory(uniName.c_str(), matName.c_str(), sizeof(float) * 4, false);
				float* vals = (float*)mem;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
				*vals++ = defaultVal;
			}
		}
	}

	mFbWidth = aFbWidth;
	mFbHeight = aFbHeight;
	mIsInitialized = true;
}

void CEffectGui::updateStats(const EffectGuiStats& aStats)
{
	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
	ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0, 0, 0, 0));
	ImGui::Begin("TEST", 0, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
							ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove |
							ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoInputs);

	double dt = aStats.dt;
	ImGui::Text("dt: %fms", dt * 1000.0);
	ImGui::Text("fps: %f", 1.0 / dt);
	ImGui::Text("Cam Pos: %fx   %fy    %fz", aStats.camPos.x, aStats.camPos.y, aStats.camPos.z);
	ImGui::Text("Selected Object: %f", aStats.selectedObjectID);
	ImGui::Text("Window Pos:  %fx   %fy", mWindowPos.x, mWindowPos.y);
	ImGui::Text("Window Size: %fx   %fy", mWindowSize.x, mWindowSize.y);
	ImGui::End();
	ImGui::PopStyleColor();
	ImGui::PopStyleColor();
}


void CEffectGui::update(const double aDt)
{
	size_t curWindow = 0;
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = (float)aDt;

	ImGui::Begin("Effect Debug Window");

	// effect browser window
	static int32_t selected = -1;
	ImGui::BeginChild("Effect Browser", ImVec2(150, 0), true); 
	int32_t i = 0;
//	for (auto effect : mEffectMemory)
	for(auto effectName : mEffectNames)
	{
//		if (ImGui::Selectable(effect.first.c_str(), selected == i))
		if(ImGui::Selectable(effectName.c_str(), selected == i))
		{
//			mSelectedEffect = effect.first;
			mSelectedEffect = effectName;
			selected = i;
		}

		i++;
	}

	ImGui::EndChild();
	ImGui::SameLine();

	static int32_t materialSelected = -1;
	if (mSelectedEffect.length() > 0)
	{
		// material browser window
		ImGui::BeginChild("Material Browser", ImVec2(250, 0), true);
		static std::string selectedMaterialStr;
//		auto effectMemIter = mEffectMemory.find(mSelectedEffect);
//		CGuiMemory* effectMem = nullptr;
		CGuiMemory* effectMem = mEffectMemory;
//		if (effectMemIter != mEffectMemory.end())
//			effectMem = effectMemIter->second;

		std::vector<std::string> attachedMaterials = effectMem->getAttachedMaterials();
		int32_t nAttachedMaterials = static_cast<int32_t>(attachedMaterials.size());
		for (size_t i = 0; i < nAttachedMaterials; i++)
		{
			if (ImGui::Selectable(attachedMaterials[i].c_str(), materialSelected == i))
			{
				selectedMaterialStr = attachedMaterials[i];
				materialSelected = static_cast<int32_t>(i);
			}
		}

		ImGui::EndChild();
		ImGui::SameLine();

		std::shared_ptr<EffectAsset> fxAsset = AssetManager::instance().get<EffectAsset>(mSelectedEffect);
		std::shared_ptr<Effect> thisEffect = fxAsset->getEffect();
		std::vector<std::string> guiUniforms = thisEffect->getGUIUniformNames();
		size_t nGuiUniforms = guiUniforms.size();
		ImGui::BeginChild(mSelectedEffect.c_str());
		for (size_t i = 0; i < nGuiUniforms; i++)
		{
			std::string uniName = guiUniforms[i];
			std::string uniType = thisEffect->getUniformGUIDebugElementType(guiUniforms[i]);
			std::pair<float, float> minmax = thisEffect->getUniformGUIDebugElementData(guiUniforms[i]);

			// Globals
			if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT") == 0)
			{
				void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
				ImGui::SliderFloat(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
			}

			else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT2") == 0)
			{
				void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
				ImGui::SliderFloat2(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
			}

			else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT3") == 0)
			{
				void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
				ImGui::SliderFloat3(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
			}

			else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT4") == 0)
			{
				void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
				ImGui::SliderFloat4(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
			}

			else if (uniType.compare("GUI_DEBUG_GLOBAL_COLORPICKER3") == 0)
			{
				void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
				ImGui::ColorPicker3(uniName.c_str(), (float*)mem);
			}

			else if (uniType.compare("GUI_DEBUG_GLOBAL_COLORPICKER4") == 0)
			{
				void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
				ImGui::ColorPicker4(uniName.c_str(), (float*)mem);
			}
		}

		if (selectedMaterialStr.length() > 0)
		{
			auto renderables = QGfx::getRenderablesFromMaterial(selectedMaterialStr.c_str());
			size_t nRenderables = renderables.second.size();
			ObjectMaterial* selectedMaterial = QGfx::getObjectMaterial(selectedMaterialStr.c_str());
			

			if (fxAsset)
			{
				std::vector<std::string> fxUniforms = thisEffect->getGUIUniformNames();

//				auto memIter = mEffectMemory.find(thisEffect->getName());
//				CGuiMemory* thisMem = nullptr;
				CGuiMemory* thisMem = mEffectMemory;
//				if (memIter != mEffectMemory.end())
//					thisMem = memIter->second;

				if (thisMem)
				{
					for (std::string uniName : fxUniforms)
					{
						// populate
						std::string uniType = thisEffect->getUniformGUIDebugElementType(uniName);
						std::pair<float, float> minmax = thisEffect->getUniformGUIDebugElementData(uniName);

						if (uniType.compare("GUI_DEBUG_FLOAT") == 0)
						{
							void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
							ImGui::SliderFloat(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
						}

						else if (uniType.compare("GUI_DEBUG_FLOAT2") == 0)
						{
							void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
							ImGui::SliderFloat2(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
						}

						else if (uniType.compare("GUI_DEBUG_FLOAT3") == 0)
						{
							void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
							ImGui::SliderFloat3(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
						}

						else if (uniType.compare("GUI_DEBUG_FLOAT4") == 0)
						{
							void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
							ImGui::SliderFloat4(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
						}

						else if (uniType.compare("GUI_DEBUG_COLORPICKER3") == 0)
						{
							void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
							ImGui::ColorPicker3(uniName.c_str(), (float*)mem);
						}

						else if (uniType.compare("GUI_DEBUG_COLORPICKER4") == 0)
						{
							void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
							ImGui::ColorPicker4(uniName.c_str(), (float*)mem);
						}
					}
				}
			}
		}

		ImGui::EndChild();
	}

	ImVec2 windowPos = ImGui::GetWindowPos();
	ImVec2 windowSize = ImGui::GetWindowSize();
	mWindowPos.set(windowPos.x, windowPos.y);
	mWindowSize.set(windowSize.x, windowSize.y);

	ImGui::End();
}

void CEffectGui::updateSelected(const double aDt, uint32_t aRenderObjId)
{
	if(aRenderObjId < 0.0f)
		return;

	ObjectMaterial* selectedMaterial = QGfx::getObjectMaterial(aRenderObjId);
	if(!selectedMaterial)
		return;

	Effect* selectedEffect = selectedMaterial->getEffect();
	if(!selectedEffect)
		return;

	size_t curWindow = 0;
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = (float)aDt;

	ImGui::Begin("Object Debug Window");

	// effect browser window
	static int32_t selected = -1;
	ImGui::BeginChild("Effect Browser", ImVec2(150, 0), true);
	bool selectTrue = true;
	ImGui::Selectable(selectedEffect->getName(), &selectTrue);
	ImGui::EndChild();
	ImGui::SameLine();

	// material browser window
	ImGui::BeginChild("Material Browser", ImVec2(250, 0), true);
	static std::string selectedMaterialStr;
	selectedMaterialStr = selectedMaterial->getName();
//	auto effectMemIter = mEffectMemory.find(selectedEffect->getName());
//	CGuiMemory* effectMem = nullptr;
	CGuiMemory* effectMem = mEffectMemory;
//	if (effectMemIter != mEffectMemory.end())
//		effectMem = effectMemIter->second;

	ImGui::Selectable(selectedMaterialStr.c_str(), &selectTrue);
	ImGui::EndChild();
	ImGui::SameLine();


	Effect* thisEffect = selectedEffect;
	std::vector<std::string> guiUniforms = thisEffect->getGUIUniformNames();
	size_t nGuiUniforms = guiUniforms.size();
	ImGui::BeginChild(thisEffect->getName());
	for (size_t i = 0; i < nGuiUniforms; i++)
	{
		std::string uniName = guiUniforms[i];
		std::string uniType = thisEffect->getUniformGUIDebugElementType(guiUniforms[i]);
		std::pair<float, float> minmax = thisEffect->getUniformGUIDebugElementData(guiUniforms[i]);

		// Globals
		if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT") == 0)
		{
			void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
			ImGui::SliderFloat(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
		}

		else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT2") == 0)
		{
			void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
			ImGui::SliderFloat2(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
		}

		else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT3") == 0)
		{
			void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
			ImGui::SliderFloat3(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
		}

		else if (uniType.compare("GUI_DEBUG_GLOBAL_FLOAT4") == 0)
		{
			void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
			ImGui::SliderFloat4(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
		}

		else if (uniType.compare("GUI_DEBUG_GLOBAL_COLORPICKER3") == 0)
		{
			void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
			ImGui::ColorPicker3(uniName.c_str(), (float*)mem);
		}

		else if (uniType.compare("GUI_DEBUG_GLOBAL_COLORPICKER4") == 0)
		{
			void* mem = effectMem->getMemory(uniName.c_str(), nullptr, true);
			ImGui::ColorPicker4(uniName.c_str(), (float*)mem);
		}
	}

	if (selectedMaterialStr.length() > 0)
	{
//		auto renderables = QGfx::getRenderablesFromMaterial(selectedMaterialStr.c_str());
//		size_t nRenderables = renderables.second.size();
		ObjectMaterial* selectedMaterial = QGfx::getObjectMaterial(selectedMaterialStr.c_str());

		if (thisEffect)
		{
			std::vector<std::string> fxUniforms = thisEffect->getGUIUniformNames();

//			auto memIter = mEffectMemory.find(thisEffect->getName());
//			CGuiMemory* thisMem = nullptr;
			CGuiMemory* thisMem = mEffectMemory;
//			if (memIter != mEffectMemory.end())
//				thisMem = memIter->second;

			if (thisMem)
			{
				for (std::string uniName : fxUniforms)
				{
					// populate
					std::string uniType = thisEffect->getUniformGUIDebugElementType(uniName);
					std::pair<float, float> minmax = thisEffect->getUniformGUIDebugElementData(uniName);

					if (uniType.compare("GUI_DEBUG_FLOAT") == 0)
					{
						void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
						ImGui::SliderFloat(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
					}

					else if (uniType.compare("GUI_DEBUG_FLOAT2") == 0)
					{
						void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
						ImGui::SliderFloat2(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
					}

					else if (uniType.compare("GUI_DEBUG_FLOAT3") == 0)
					{
						void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
						ImGui::SliderFloat3(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
					}

					else if (uniType.compare("GUI_DEBUG_FLOAT4") == 0)
					{
						void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
						ImGui::SliderFloat4(uniName.c_str(), (float*)mem, minmax.first, minmax.second);
					}

					else if (uniType.compare("GUI_DEBUG_COLORPICKER3") == 0)
					{
						void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
						ImGui::ColorPicker3(uniName.c_str(), (float*)mem);
					}

					else if (uniType.compare("GUI_DEBUG_COLORPICKER4") == 0)
					{
						void* mem = thisMem->getMemory(uniName.c_str(), selectedMaterial->getName(), false);
						ImGui::ColorPicker4(uniName.c_str(), (float*)mem);
					}
				}
			}
		}
	}
	ImGui::EndChild();


	ImVec2 windowPos = ImGui::GetWindowPos();
	ImVec2 windowSize = ImGui::GetWindowSize();
	mSelectedWindowPos.set(windowPos.x, windowPos.y);
	mSelectedWindowSize.set(windowSize.x, windowSize.y);

	ImGui::End();
}

void CEffectGui::render()
{
	ImDrawData* drawData = ImGui::GetDrawData();
	if (!drawData)
		return;

	/////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	Technique* currentTechnique = nullptr;
	Pass* currentPass = nullptr;
	Shader* currentShader = nullptr;
	std::shared_ptr<Effect> fx = AssetManager::instance().get<EffectAsset>("Fullscreen")->getEffect();
	currentTechnique = fx->getTechnique("FullscreenTechnique");
	Material* currentMaterial = nullptr;

	currentPass = currentTechnique->getPass("p0");
	currentPass->bind();
	currentShader = currentPass->getShader();

	glEnable(GL_BLEND);
	QGfx::GL::gl_BlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);

	ImVec2 clipOffset = drawData->DisplayPos;
	ImVec2 clipScale = drawData->FramebufferScale;
	for (int i = 0; i < drawData->CmdListsCount; i++)
	{
		const ImDrawList* cmdList = drawData->CmdLists[i];

		VertexBufferLayout vFormat;
		vFormat.push<vec2<float>>("position");
		vFormat.push<vec2<float>>("uv0");
		vFormat.push<uint8_t>("color0", 4, true);

		VertexBuffer* vBuf = new VertexBuffer(cmdList->VtxBuffer.Data,
			cmdList->VtxBuffer.Size * sizeof(ImDrawVert));
		vBuf->setLayout(vFormat);

		IndexBuffer* iBuf = new IndexBuffer(cmdList->IdxBuffer.Data,
			cmdList->IdxBuffer.Size * sizeof(ImDrawIdx));

		VertexArray* vArray = new VertexArray(vBuf, iBuf);

		for (int j = 0; j < cmdList->CmdBuffer.Size; j++)
		{
			const ImDrawCmd* pCmd = &cmdList->CmdBuffer[j];

			ImVec4 clip_rect;
			clip_rect.x = (pCmd->ClipRect.x - clipOffset.x) * clipScale.x;
			clip_rect.y = (pCmd->ClipRect.y - clipOffset.y) * clipScale.y;
			clip_rect.z = (pCmd->ClipRect.z - clipOffset.x) * clipScale.x;
			clip_rect.w = (pCmd->ClipRect.w - clipOffset.y) * clipScale.y;

			if (clip_rect.x < mFbWidth && clip_rect.y < mFbHeight && clip_rect.z >= 0.0f && clip_rect.w >= 0.0f)
			{
				glScissor((int)clip_rect.x, (int)(mFbHeight - clip_rect.w), (int)(clip_rect.z - clip_rect.x),
					(int)(clip_rect.w - clip_rect.y));
			}

			currentShader->setTexture("tex0", (GLuint)(intptr_t)pCmd->TextureId);

			GLint srcAlpha, dstAlpha;
			glGetIntegerv(GL_BLEND_SRC_ALPHA, &srcAlpha);
			glGetIntegerv(GL_BLEND_DST_ALPHA, &dstAlpha);

			vArray->bind();
			glDrawElements(GL_TRIANGLES, (GLsizei)pCmd->ElemCount,
				sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT,
				(void*)(intptr_t)(pCmd->IdxOffset * sizeof(ImDrawIdx)));
			vArray->unbind();
		}

		delete vBuf;
		delete iBuf;
		delete vArray;
	}

	currentPass->unbind();

	glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}


void CEffectGui::mouseEvent(const int aButton, const int aAction)
{
	ImGuiIO& io = ImGui::GetIO();
	if (aAction == static_cast<int>(EMouseButtonAction::PRESS) ||
		aAction == static_cast<int>(EMouseButtonAction::DBL_CLICK))
	{
		io.MouseDown[aButton] = true;
	}

	if (aAction == static_cast<int>(EMouseButtonAction::RELEASE))
	{
		io.MouseDown[aButton] = false;
	}
}

void CEffectGui::keyEvent(const int aKey, const int aAction)
{
	ImGuiIO& io = ImGui::GetIO();
	if (aAction == static_cast<int>(EKeyAction::PRESS) || aAction == static_cast<int>(EKeyAction::REPEAT))
	{
		io.AddInputCharacter(aKey);
	}
}

void CEffectGui::setMousePos(const vec2<int32_t> aMousePos)
{
	ImGuiIO& io = ImGui::GetIO();
	io.MousePos = ImVec2((float)aMousePos.x, (float)aMousePos.y);
}

void CEffectGui::toggleVisibility()
{
	mIsVisible = !mIsVisible;
}

void CEffectGui::setVisibility(const bool aVisibility)
{
	mIsVisible = aVisibility;
}

void CEffectGui::toggleStatsVisibility()
{
	mAreStatsVisible = !mAreStatsVisible;
}

void CEffectGui::setStatsVisibility(const bool aVisibility)
{
	mAreStatsVisible = aVisibility;
}

bool CEffectGui::checkInit()
{
	if (!mIsInitialized)
	{
		QUADRION_ERROR("GUI is not initialized");
		return false;
	}

	return true;
}

void CEffectGui::getData(const char* aEffectName, const char* aMaterialName,
	const char* aUniformName, void* aData)
{
	//try global
//	auto memIter = mEffectMemory.find(aEffectName);
//	CGuiMemory* thisMem = nullptr;
	CGuiMemory* thisMem = mEffectMemory;
//	if (memIter != mEffectMemory.end())
//		thisMem = memIter->second;
//	else
//		return;

	size_t nBytes = thisMem->getUniformSize(aUniformName);

	void* mem = thisMem->getMemory(aUniformName, nullptr, true);
	if (mem)
	{
		memcpy(aData, mem, nBytes);
		return;
	}

	//try local
	mem = thisMem->getMemory(aUniformName, aMaterialName, false);
	if (mem)
	{
		memcpy(aData, mem, nBytes);
		return;
	}
}

void CEffectGui::begin()
{
	ImGui::NewFrame();
}

void CEffectGui::end()
{
	ImGui::Render();
}

float CEffectGui::getSelectedObject(FboId gbuffer, vec2<int32_t> mousePos)
{
	Timer t;

	GLint x, y;
	GLsizei width, height;
	GLenum format = GL_RGBA;
	GLenum type = GL_FLOAT;
	x = y = 0;
	width = gbuffer.width;
	height = gbuffer.height;
	GLsizei bufSize = width * height * 4 * sizeof(float);
	float* data = (float*)malloc(bufSize);
	
	t.start();
	QGfx::GL::gl_BindFramebuffer(GL_FRAMEBUFFER, gbuffer.id);
	QGfx::GL::gl_ReadBuffer(GL_COLOR_ATTACHMENT0);
	QGfx::GL::gl_ReadPixels(x, y, width, height, format, type, (void*)data);
	double elapsed = t.getElapsedMilliSec();
	t.stop();
	QUADRION_TRACE("ReadBuffer: {0}", elapsed);

	vec2<int32_t> texturePos(mousePos.x, height - mousePos.y);
	float* pix = data;
	pix += (texturePos.y * width * 4) + (texturePos.x * 4);
	float r = *pix++;
	float g = *pix++;
	float b = *pix++;
	float a = *pix++;

	return a;
}


bool CEffectGui::isMouseHovering(const vec2<int32_t>& aMousePos)
{
	vec2<float> windowPos = mWindowPos;
	vec2<float> windowSize = mWindowSize;
	vec2<float> selectedWindowPos = mSelectedWindowPos;
	vec2<float> selectedWindowSize = mSelectedWindowSize;
	bool isMouseInWindow = false;
	bool isMouseInSelectedWindow = false;

	if (aMousePos.x > windowPos.x&& aMousePos.x < (windowPos.x + windowSize.x) &&
		aMousePos.y > windowPos.y&& aMousePos.y < (windowPos.y + windowSize.y))
	{
		isMouseInWindow = true;
	}

	if (aMousePos.x > selectedWindowPos.x&& aMousePos.x < (selectedWindowPos.x + selectedWindowSize.x) &&
		aMousePos.y > selectedWindowPos.y&& aMousePos.y < (selectedWindowPos.y + selectedWindowSize.y))
	{
		isMouseInSelectedWindow = true;
	}

	return (isMouseInWindow || isMouseInSelectedWindow);
}