#ifndef __CGUIMEMORY_H_
#define __CGUIMEMORY_H_

#include <map>
#include <string>
#include <vector>

class CGuiMemory
{
	public:

		CGuiMemory();
		~CGuiMemory();

		void* addMemory(const char* aName, const char* aMaterialName, uint32_t nBytes, bool isGlobal);
		void* getMemory(const char* aName, const char* aMaterialName, bool isGlobal);



		std::vector<std::string> getAttachedMaterials();

		size_t getUniformSize(const char* aUniformName);

	private:

		// uniform name->mem
		std::map<std::string, void*> mGlobalMemory;
		std::map<std::string, size_t> mGlobalMemorySize;

		// material name->uniform name->mem
		std::map<std::string, std::map<std::string, void*>> mMemory;

};


#endif