#ifndef __CEFFECTGUI_H_
#define __CEFFECTGUI_H_


#include <vector>
#include <map>
#include <string>
#include "QMath.h"
#include "imgui.h"
#include "gui/CGuiMemory.h"
#include "render/QGfx.h"


struct EffectGuiStats
{
	double	dt;
	vec3<float> camPos;
	float selectedObjectID;
};


class CEffectGui
{
	public:
	
		CEffectGui();
		~CEffectGui();
	
		void update(const double aDt);
		void updateStats(const EffectGuiStats& aStats);
		void updateSelected(const double aDt, uint32_t aRenderObjId);
		void render();
		void initialize(const int aFbWidth, const int aFbHeight);

		void begin();
		void end();
	
		void mouseEvent(const int aButton, const int aAction);
		void keyEvent(const int aKey, const int aAction);
		void setMousePos(const vec2<int32_t> aMousePos);
	
		void getData(const char* aEffectName, const char* aMaterialName,
			const char* aUniformName, void* aData);
	
		void toggleVisibility();
		void setVisibility(const bool aVisibility);
		inline bool isVisible() const
		{
			return mIsVisible;
		}

		void toggleStatsVisibility();
		void setStatsVisibility(const bool aVisibility);
		inline bool areStatsVisible() const
		{
			return mAreStatsVisible;
		}

		bool isMouseHovering(const vec2<int32_t>& aMousePos);

		float getSelectedObject(FboId gbuffer, vec2<int32_t> mousePos);

	private:
	
		int32_t mFbWidth;
		int32_t mFbHeight;
	
		bool    mIsVisible;
		bool    mIsInitialized;
		bool    checkInit();
		bool    mIsTestWindowVisible;
		bool	mAreStatsVisible;
	
//		std::map<std::string, CGuiMemory*> mEffectMemory;
		std::vector<std::string> mEffectNames;
		CGuiMemory* mEffectMemory;
	
		std::string mSelectedEffect;
		std::string mSelectedMaterial;
	
		char mLastMaterialName[64];

		vec2<float> mWindowPos;
		vec2<float> mWindowSize;
		vec2<float> mSelectedWindowPos;
		vec2<float> mSelectedWindowSize;
};



#endif

