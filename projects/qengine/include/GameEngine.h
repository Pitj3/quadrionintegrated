#ifndef gameengine_h__
#define gameengine_h__

#include "EngineExport.h"
#include "GameState.h"
#include "INI.h"

class QScene;

enum class EGameEngineMode : int
{
	PRIMARY,
	BACKGROUND
};

class GameEngineImpl;

class QENGINE_API GameEngine
{
	public:
		GameEngine(const char *aIniPath);
		~GameEngine();

		void setTickRate(const unsigned int aHz);
		const unsigned int getTickRate();
		
		template<class T>
		T* createGameState()
		{
			static_assert(std::is_base_of<GameState, T>::value, "T must be a descendant of GameState");
			auto gameState = new T();
			return gameState;
		}

		void preloadGameState(GameState* aGameState);
		void pushGameState(GameState* aGameState);
		void popGameState();

		void setScene(QScene* aScene);
		
		const int getWindowWidth();
		const int getWindowHeight();

		INI* getApplicationINI();

		void gameLoop();
		void quit();

		EngineContext* getEngineContext() const;

		static GameEngine* instance;

	private:
		GameEngineImpl* mImpl;
};

#endif // gameengine_h__
