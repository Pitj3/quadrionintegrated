#ifndef updatecontext_h__
#define updatecontext_h__

#include "EngineExport.h"

struct QENGINE_API UpdateContext
{
	double elapsed;
	double lag;
	bool windowFocused;
};

#endif // updatecontext_h__
