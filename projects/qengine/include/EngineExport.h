#ifndef engineexport_h__
#define engineexport_h__

//	Should we also put common STL exports here?

#ifdef QENGINE_EXPORTS
#	ifndef QENGINE_API
#       if defined(_WIN32) || defined(_WIN64)
#		    define QENGINE_API __declspec(dllexport)
#		    define QENGINE_EXPORT_TEMPLATE
#       else
#           define QENGINE_API
#       endif
#	endif
#else
#	ifndef QENGINE_API
#       if  defined(_WIN32) || defined(_WIN64)
#		    define QENGINE_API __declspec(dllimport)
#		    define QENGINE_EXPORT_TEMPLATE extern
#       else
#           define QENGINE_API
#       endif
#	endif
#endif

#endif // engineexport_h__
