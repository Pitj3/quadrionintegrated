#ifndef filesystem_h__
#define filesystem_h__

#include <vector>
#include <string>

#include "EngineExport.h"

class QENGINE_API FileSystem
{
	public:
		static std::string readFile(const std::string& path);
//		static std::vector<std::string> getDirectories(const std::string& aRoot);
		static std::vector<std::string> getFiles(const std::string& aRoot);
		static std::vector<std::string> getDirectories(const std::string& aRoot);
		static std::string getFileRecursively(const std::string& aRoot, const std::string& aFileName);

	private:

#if !defined(_WIN32) || !defined(_WIN64)
		static void _XGetFiles(const std::string& aRoot, std::vector<std::string>* aFiles);
#endif
};

#endif // filesystem_h__