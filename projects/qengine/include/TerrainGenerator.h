#ifndef __terraingenerator_h_
#define __terraingenerator_h_


#include <cstdint>
#include <string>
#include <memory>
#include "QMath.h"
#include "EngineExport.h"
#include "render/Mesh.h"


struct TerrainInitializer
{
	vec2<double>	worldMins;
	uint32_t		nPatchesX;
	uint32_t		nPatchesZ;
	uint32_t		nPixelsPerPatchX;
	uint32_t		nPixelsPerPatchZ;
	float			patchWidth;
	float			patchDepth;
	float			maxElevation;

	// from file
	const char*		heightmapImagePath;
	const char*		heightmapImageFilename;

	// Procedural 
	double			entropy;
	double			smoothing;
	uint16_t*		heightField;
};

/*
QENGINE_API QHandle<QEntity> generateTerrainEntity(const TerrainInitializer& aInit,
										   QHandle<QEntityManager>& aEntityMgr);

*/

// Generates a Mesh object and adds to the Asset Manager //
QENGINE_API std::shared_ptr<Mesh> generateHeightmapTerrainMesh(const TerrainInitializer& aInit,
															   const std::string& aName);


#endif
