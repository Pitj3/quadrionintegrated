/***************************************************************************************************
* Copyright (c) 2008 Jonathan 'Bladezor' Bastnagel.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the GNU Lesser Public License v2.1
* which accompanies this distribution, and is available at
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
* Contributors:
*     Jonathan 'Bladezor' Bastnagel - initial implementation and documentation
***************************************************************************************************/
#ifndef ini_h__
#define ini_h__

#include "EngineExport.h"

struct INIImpl;

class QENGINE_API INI
{
public:
	static const int INI_LOAD_ERROR = -1;
	static const int INI_LOAD_SUCCESS = 0;
	static const int INI_SAVE_SUCCESS = 0;

	INI();
	~INI();

	const int parseINI(const char* aPath);
	const int save();

	const char* getString(const char* aSection, const char* aVariable, const char* aDef = "");
	int getInt(const char* aSection, const char* aVariable, const int aDef = 0);
	bool getBool(const char* aSection, const char* aVariable, const bool aDef = false);
	double getDouble(const char* aSection, const char* aVariable, const double aDef = 0.0);
	float getFloat(const char* aSection, const char* aVariable, const float aDef = 0.0f);

	void setString(const char* aSection, const char* aVariable, const char* aValue);
	void setInt(const char* aSection, const char* aVariable, const int aValue);
	void setBool(const char* aSection, const char* aVariable, const bool aValue);
	void setDouble(const char* aSection, const char* aVariable, const double aValue);
	void setFloat(const char* aSection, const char* aVariable, const float aValue);

	bool exists(const char* aSection, const char* aVariable);

private:
	INIImpl* mImpl = nullptr;
};

#endif // ini_h__
