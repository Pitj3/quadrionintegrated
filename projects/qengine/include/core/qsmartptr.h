#ifndef qsmartptr_h__
#define qsmartptr_h__

#include <cstdint>
// Simple reference counter wrapper that contains the current
// reference count for SmartPtr
class RefCounter
{
	public:

		RefCounter()
		{
			mCount = 0;
		}
		
		void Increment()
		{
			++mCount;
		}

		int32_t Decrement()
		{
			if (mCount > 0)
				return --mCount;
			else
				return 0;
		}

	private:
		
		int32_t mCount;
};



// Implementation of Smart Pointer class. This is a rudimentary
// implementation of a reference counted pointer that IS NOT
// THREAD SAFE!! Do NOT use this pointer across threads.
template <typename T>
class QSmartPtr
{
	public:

		QSmartPtr() : mData(nullptr), mRefCount(nullptr)
		{
			mRefCount = new RefCounter;
			mRefCount->Increment();
		}

		QSmartPtr(T* aVal) : mData(aVal), mRefCount(nullptr)
		{
			mRefCount = new RefCounter;
			mRefCount->Increment();
		}

		QSmartPtr(const QSmartPtr<T>& aPtr) : mData(aPtr.mData), 
											 mRefCount(aPtr.mRefCount)
		{
			if (mRefCount)
				mRefCount->Increment();
		}

		~QSmartPtr()
		{
			if (mRefCount->Decrement() <= 0)
			{
				if (nullptr != mData)
				{
					delete mData;
					mData = nullptr;
				}

				if (mRefCount)
				{
					delete mRefCount;
					mRefCount = nullptr;
				}
			}
		}

		T& operator* ()
		{
			return *mData;
		}

		T* operator-> ()
		{
			return mData;
		}

		bool isNull()
		{
			return nullptr == mData;
		}

		QSmartPtr<T>& operator= (T* aPtr)
		{
			if (aPtr != mData)
			{
				// Assigning to a different object's address, decrease ref count before assignment.
				if (mRefCount->Decrement() <= 0)
				{
					if (nullptr != mData)
					{
						delete mData;
						mData = nullptr;
					}

					delete mRefCount;
					mRefCount = nullptr;
				}

				mData = aPtr;
				mRefCount = new RefCounter;
				mRefCount->Increment();
			}

			return *this;
		}

		QSmartPtr<T>& operator= (const QSmartPtr<T>& aPtr)
		{
			// Avoid self assignment
			if (this != &aPtr)
			{
				// Increment the source's ref count before Decrementing our own,
				// in order to prevent the object from getting destroyed unexpectedly
				// such as when both QuiRefCounted objects point to the same data.
				aPtr.mRefCount->Increment();

				if (mRefCount->Decrement() <= 0)
				{
					if (nullptr != mData)
					{
						delete mData;
						mData = nullptr;
					}

					delete mRefCount;
					mRefCount = nullptr;
				}

				mData = aPtr.mData;
				mRefCount = aPtr.mRefCount;
			}

			return *this;
		}

		T* getPtr()
		{
			return mData;
		}

	private:

		T* mData;
		RefCounter*	mRefCount;	
};

#endif // qsmartptr_h__