#ifndef qstring_h__
#define qstring_h__


#include "core/QArray.h"
#include "EngineExport.h"
#include "QMath.h"

struct QStringImpl;

class QENGINE_API QString
{
	private:
		QStringImpl* mImpl = nullptr;

	public:
		QString();
		QString(const char* aS);
		QString(uint32_t aV);
		QString(int32_t aV);
		QString(float aV);
		QString(vec3<float> aV);
		QString(vec4<float> aV);
		QString(mat4<float> aV);
		QString(const QString& aS);
		QString(QString&& aS);

		~QString();

		QString& operator= (const char* aF);
		QString& operator= (const QString& aF);

		bool operator== (QString& aF) const;
		bool operator== (QString* aF) const;
		bool operator== (const char* aF) const;

		bool operator!= (QString& aF) const;
		bool operator!= (QString* aF) const;
		bool operator!= (const char* aF) const;

		bool operator> (const QString& aRight) const;
		bool operator>= (const QString& aRight) const;
		bool operator< (const QString& aRight) const;
		bool operator<= (const QString& aRight) const;

		char& operator[] (const int aIndex) const;	

		static QString fromString(const void* aStr);

		const char* c_str() const;

		int				indexOf(const char* aFindstr, size_t aOffset = 0) const;

		void			insert(size_t aPos, QString aStr);

		size_t			length() const;

		QArray<QString>	split(QString aDelimiter, size_t aLimit = 0) const;

		size_t			size() const;

		QString			substr(size_t aOffset) const;

		QString			substr(size_t aOffset, size_t aLength) const;

		QString			toLower() const;

		mat4<float>			toMat4f() const;

		QString			toUpper() const;

		float			toFloat() const;

		int32_t			toInt32() const;

		uint32_t		toUInt32() const;

		int64_t			toInt64() const;

		uint64_t		toUInt64() const;

		vec2<float>			toVec2f() const;

		vec3<float>			toVec3f() const;

		vec4<float>			toVec4f() const;

		double			toDouble() const;
};

#endif // qstring_h__