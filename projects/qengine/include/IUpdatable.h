#ifndef iupdatable_h__
#define iupdatable_h__

#include "EngineExport.h"
#include "UpdateContext.h"

class QENGINE_API IUpdatable
{
	public:
		virtual ~IUpdatable() = 0;
		virtual void tick(const UpdateContext& aUpdateContext) = 0;
		virtual void update(const UpdateContext& aUpdateContext) = 0;
};

#endif // iupdatable_h__
