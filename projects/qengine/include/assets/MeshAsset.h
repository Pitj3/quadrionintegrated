#ifndef meshassetrod_h__
#define meshassetrod_h__

#include <vector>
#include <memory>
#include <unordered_map>
#include "EngineExport.h"
#include "QMath.h"
#include "render/Mesh.h"
#include "assets/Asset.h"

struct MeshNodeMaterial
{
	uint32_t albedoTexture;
	uint32_t emissiveTexture;
	uint32_t aoTexture;
	uint32_t roughnessTexture;
	uint32_t metallicTexture;
	uint32_t normalMap;
	uint32_t heightMap;

	vec3<float>		diffuseColor;
	vec3<float>		specularColor;
	vec3<float>		ambientColor;
	vec3<float>		emissiveColor;
	float			shininess;
	float			shininessStrength;
};



enum QTEXTURE_TYPE : uint32_t
{
	QTEXTURE_TYPE_NONE = 0,
	QTEXTURE_TYPE_DIFFUSE = 1,
	QTEXTURE_TYPE_SPECULAR = 2,
	QTEXTURE_TYPE_AMBIENT = 3,
	QTEXTURE_TYPE_EMISSIVE = 4,
	QTEXTURE_TYPE_HEIGHT = 5,
	QTEXTURE_TYPE_NORMALS = 6,
	QTEXTURE_TYPE_SHININESS = 7,
	QTEXTURE_TYPE_OPACITY = 8,
	QTEXTURE_TYPE_DISPLACEMENT = 9,
	QTEXTURE_TYPE_LIGHTMAP = 10,
	QTEXTURE_TYPE_REFLECTION = 11,
	QTEXTURE_TYPE_UNKNOWN = 12,
};

class AssimpDependencies;

struct MeshNodeRod
{
	mat4<float> transform;
	std::shared_ptr<Mesh> mesh = nullptr;
	MeshNodeMaterial meshMaterial;
	vec3<float> scale, position;
	mat4<float> rotation;
	vec3<float> center;
};

class QENGINE_API MeshAsset : public Asset
{
	public:
		explicit MeshAsset(const std::string& aName, const std::string& aPath);
		~MeshAsset();

		bool load() override;

		std::vector<std::shared_ptr<MeshNodeRod>>& getMeshes();

	private:

		std::string		mPath;

		AssimpDependencies* mAssimpDependencies;
};


#endif // meshassetrod_h__