#ifndef _heightmapterrainmeshasset_h
#define _heightmapterrainmeshasset_h

#include <string>
#include "assets/Asset.h"
#include "render/Mesh.h"
#include "EngineExport.h"

class QENGINE_API HeightmapTerrainMeshAsset : public Asset
{
	public:
		
		HeightmapTerrainMeshAsset(const std::string& aName, Mesh* aMesh);
		~HeightmapTerrainMeshAsset() = default;

		bool load() override;

		Mesh* const getMesh();

	private:

		Mesh*		mMeshRef;
};


#endif