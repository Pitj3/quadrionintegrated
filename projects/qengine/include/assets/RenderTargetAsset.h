#ifndef _rendertargetasset_h
#define _rendertargetasset_h


#include "render/Texture.h"
#include "assets/Asset.h"
#include "EngineExport.h"

class QENGINE_API RenderTargetAsset : public Asset
{
	public:
		
		RenderTargetAsset(const std::string& aName,
						  uint32_t aWidth, uint32_t aHeight,
						  ETexturePixelFormat aFormat,
						  uint32_t aDepthBits, uint32_t aStencilBits, uint32_t aFlags, uint32_t aNumTargets);

		const FboId getFBO() const { return mFBO; }
		const GLuint getTexture(uint32_t aIndex) const { return mTextures[aIndex]; }

		const uint32_t getWidth() const { return mWidth; }
		const uint32_t getHeight() const { return mHeight; }

		bool load() override;

	private:

		uint32_t				mWidth, mHeight, mDepthBits, mStencilBits, mFlags, mNumTargets;
		ETexturePixelFormat		mFormat;
		FboId					mFBO;
		GLuint*					mTextures;
		DepthStencilId			mDS;
};


#endif