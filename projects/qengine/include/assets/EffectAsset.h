#ifndef effectasset_h__
#define effectasset_h__

#include <memory>

#include "render/Effect.h"
#include "render/effectparser/EffectParser.h"
#include "assets/Asset.h"
#include "EngineExport.h"


class QENGINE_API EffectAsset : public Asset
{
	public:

		EffectAsset(const std::string& aName,
					const std::string& aFileName);
		~EffectAsset();

		bool load() override;

		std::shared_ptr<Effect> getEffect();
		void overrideEffect(std::shared_ptr<Effect> aEffect);
	
	private:

		bool						mIsEffect;

		std::string					mFileName;
		std::shared_ptr<Effect>		mEffect;
		std::string					mName;
};

#endif // effectasset_h__