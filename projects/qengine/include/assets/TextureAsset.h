#ifndef _textureasset_h
#define _textureasset_h


#include <cstdint>
#include "assets/Asset.h"
#include "EngineExport.h"

class TextureAssetImpl;
class QENGINE_API TextureAsset : public Asset
{
	public:
	
		TextureAsset(const std::string& aName,
					 const std::string& aFileName);
		~TextureAsset();
	
		bool load() override;

		uint32_t getTextureID() const { return mTextureID; }
	
	
	private:
		TextureAssetImpl* mImpl;
		uint32_t					mTextureID;
};


#endif
