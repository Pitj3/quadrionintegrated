
#ifndef __assetmanagerrod_h_
#define __assetmanagerrod_h_

#include <utility>
#include <unordered_map>
#include <string>
#include <type_traits>
#include <memory>
#include <list>
#include <thread>
#include <chrono>
#include <vector>

#include "assets/Asset.h"
#include "EngineExport.h"

constexpr uint8_t ASSET_LOAD_LOW_PRIO = 0;
constexpr uint8_t ASSET_LOAD_MED_PRIO = 1;
constexpr uint8_t ASSET_LOAD_HIGH_PRIO = 2;

class TBBDeps;

class QENGINE_API AssetManager
{
public:
	static AssetManager& instance();

	template<typename T, typename ... Arguments>
	std::shared_ptr<T> load(const std::string& aName,
		uint8_t aPrio,
		Arguments&& ... aArgs);

	template<typename T, typename ... Arguments>
	std::shared_ptr<T> loadAsync(const std::string& aName,
		uint8_t aPrio,
		Arguments&& ... aArgs);

	bool unload(const std::string& aName);
	void unloadAll();

	template<typename T>
	std::shared_ptr<T> get(const std::string& aName);

	template<typename T>
	std::vector<std::shared_ptr<T>> get();

private:

	void _loadAssetAsync(std::list<std::string>::iterator& aIter, uint8_t aPrio);

	//		tbb::task_group mAsyncTaskGroup;
	static TBBDeps* mTBBDependencies;

	// Cause fuck moving this
#pragma warning(push)
#pragma warning(disable: 4251)
	std::list<std::string> mAsyncAssetQueueHigh;
	std::list<std::string> mAsyncAssetQueueMedium;
	std::list<std::string> mAsyncAssetQueueLow;
	std::unordered_map<std::string, std::shared_ptr<Asset>> mAssets;
#pragma warning(pop)

	AssetManager();
};

template<typename T, typename ... Arguments>
std::shared_ptr<T> AssetManager::load(const std::string& aName,
	uint8_t aPrio,
	Arguments&& ... aArgs)
{
	static_assert(std::is_base_of<Asset, T>::value, "T is not derived from Asset");
	auto query = get<T>(aName);
	if (query)
	{
		query->load();
		mAssets[aName] = query;
		return query;
	}

	std::shared_ptr<T> asset = std::make_shared<T>(aName, std::forward<Arguments>(aArgs)...);
	mAssets[aName] = asset;
	mAssets[aName]->load();

	return asset;
}

template <typename T, typename ... Arguments>
std::shared_ptr<T> AssetManager::loadAsync(const std::string& aName,
	uint8_t aPrio,
	Arguments&& ... aArgs)
{
	static_assert(std::is_base_of<Asset, T>::value, "T is not derived from Asset");

	std::shared_ptr<T> asset = std::make_shared<T>(aName, std::forward<Arguments>(aArgs)...);
	mAssets[aName] = asset;

	switch (aPrio)
	{
	case ASSET_LOAD_HIGH_PRIO:
		mAsyncAssetQueueHigh.push_back(aName);
		break;

	case ASSET_LOAD_MED_PRIO:
		mAsyncAssetQueueMedium.push_back(aName);
		break;

	case ASSET_LOAD_LOW_PRIO:
		mAsyncAssetQueueLow.push_back(aName);
		break;

	default:
		break;
	}

	return asset;
}

template<typename T>
std::shared_ptr<T> AssetManager::get(const std::string& aName)
{
	static_assert(std::is_base_of<Asset, T>::value, "T is not derived from Asset");

	const auto element = mAssets.find(aName);
	if (element != mAssets.end())
		return std::static_pointer_cast<T>(element->second);

	return nullptr;
}

template<typename T>
std::vector<std::shared_ptr<T>> AssetManager::get()
{
	static_assert(std::is_base_of<Asset, T>::value, "T is not derived from Asset");

	std::vector<std::shared_ptr<T>> ret;
	for (auto element : mAssets)
	{
		if(dynamic_cast<T*>(element.second.get()))
			ret.push_back(std::static_pointer_cast<T>(element.second));
	}

	return ret;
}


#endif