#ifndef _PROCEDURAL_MESH_ASSET_H__
#define _PROCEDURAL_MESH_ASSET_H__

#include <memory>
#include "assets/Asset.h"
#include "assets/MeshAsset.h"
#include "core/QArray.h"
#include "EngineExport.h"
#include "render/Texture.h"
#include "render/RenderObject.h"
#include "QMath.h"
#include "render/Mesh.h"
#include "TerrainGenerator.h"

struct ProceduralMeshAsset_Impl;

//
//class QENGINE_API MeshPropPatch
//{
//	public:
//
//		MeshPropPatch();
//		~MeshPropPatch();
//
//		vec3<float>			patchMins;
//		vec3<float>			patchCenter;
//
//		double			patchWidth;
//		double			patchHeight;
//
//	private:
//		
//};
//
//class QENGINE_API MeshPropPartition
//{
//	public:
//
//		MeshPropPartition();
//		~MeshPropPartition();
//
//		const MeshPropPatch* const getPatchFromIndex(uint32_t aPatchIdx);
//
//		MeshPropPatch**	propPatchList;
//
//		uint32_t			numPropPatches;
//		uint32_t			numPropPatchesX;
//		uint32_t			numPropPatchesZ;
//
//	private:
//};
//
//class QENGINE_API MeshPatch
//{
//	public:
//		MeshPatch();
//		~MeshPatch();
//
//		Vertex*		verts;
//		Vertex*		physicsVerts;
//		uint32_t*	indices;
//
//		uint16_t	terrainType;
//
//		uint32_t	numVertsX;
//		uint32_t	numVertsZ;
//		uint32_t	numVerts;
//		uint32_t	numIndices;
//		uint32_t	vertexStride;
//		uint32_t	physicsVertexStride;
//		uint32_t	numPhysicsVerts;
//
//		GLuint		patchVAO;
//
//		vec3<float>		mins;
//		vec3<float>		maxs;
//		vec2<float>		patchDims;
//
//		vec3<float>		center;
//
//		float		boundingRad;
//
//		Mesh*		mesh;
//
//	private:
//
//};
//
//
//
//
//struct ProceduralMeshInitializer
//{
//	uint32_t		aSize;
//	double			aEntropy;
//	double			aSmoothing;
//
//	float			aHeightScale;
//	vec2<float>		aTerrainScale;
//	vec2<uint32_t>	aNumPatches;
//	uint32_t		aNumVertsPerPatch;
//	vec2<float>		aTerrainMins;
//	vec2<float>		aTerrainMaxs;
//	vec2<uint32_t>	aNumPropPatches;
//	vec2<uint32_t>  aNumTerrainPropDivs;
//};



class QENGINE_API ProceduralMeshAsset : public Asset
{
	public:
		ProceduralMeshAsset(const std::string aName);
		ProceduralMeshAsset(const std::string, const TerrainInitializer& aInit);
//		ProceduralMeshAsset(const std::string& aName,
//							const ProceduralMeshInitializer& aInit);
		~ProceduralMeshAsset();


		std::shared_ptr<RenderObject> getRenderObject();

		bool		load() override;

		

		//void		setHeightScale(const float aScale);
		//void		setXScale(const float aX);
		//void		setZScale(const float aZ);
		//void		setNumPatchesX(uint32_t aX);
		//void		setNumPatchesZ(uint32_t aZ);
		//void		setNumVertsPerPatch(uint32_t aNumVerts);
		//void		setTerrainMins(vec2<float> aMins);
		//void		setTerrainMaxs(vec2<float> aMaxs);
		//void		setNumPropPatches(uint32_t aNumX, uint32_t aNumZ);

		//uint32_t					getNumVertices(uint32_t aPatchIdx);
		//const Vertex* const			getBaseVertices();
		//uint32_t					getNbVertices(uint32_t aPatchIdx);
		//uint32_t					getNbIndices(uint32_t aPatchIdx);
		//uint32_t*					getIndices(uint32_t aPatchIdx);
		//vec2<float>					getPatchDimensions(uint32_t aPatchIdx);
		//uint32_t					getPatchIndex(vec3<float> aWorldPos);
		//uint32_t					getNumPatches();
		//uint32_t					getNumPhysicsPatches();
		//Vertex*						getPhysicsPatchVerts(uint32_t aPatchIdx);
		//const MeshPatch* const		getPatch(uint32_t aPatchIdx);
		//uint32_t					getVertexStride();
		//float						getHeightScale();
		//void						getMeshBounds(vec3<float>& aMins, vec3<float>& aMaxs);
		//float						getXScale();
		//float						getZScale();
		//uint32_t					getNumVertsX();
		//uint32_t					getNumVertsZ();
		//double						getElevationFromPos(vec3<float> aPos);
		//GLuint						getHeightmapTextureID();
		//GLuint						getNormalmapTextureID();
		//uint32_t					getPhysicsVertexStride();
		//GLuint						getShadowRenderHandle();
		//uint32_t					getNumShadowIndices();
		//uint32_t					getNumSamplesX();
		//uint32_t					getNumSamplesZ();
		//FboId						getTopDownRenderTarget();
		//GLuint						getTopDownTexture();
		//GLuint						getL0GrassTBO();
		//uint32_t					getMaxGrassClumps();
		//uint32_t					getNumTreeInstances();
		//GLuint						getTreeTBO();
		//GLuint						getGrassVAO();
		//uint32_t					getNumGrassIndices();
		//void						getAdjacentPropPatchList(uint32_t aCurIdx,
		//												     int32_t* aAdjPatches);
		//uint32_t					getPropPatchIndex(vec3<float> pos);
		//const MeshPropPatch* const  getPropPatchFromIndex(uint32_t aIdx);
		//const MeshPropPatch* const  getPatchFromPosition(vec3<float> aPos);

		//double*						getHeightField();

		//double						getMinHeight();
		//double						getMaxHeight();


		//bool						copyTopDownBuf(void* aBuf, uint32_t aSize);

		//Mesh* const					getBaseMesh() { return baseMesh; }
		//Mesh* const					getShadowMesh() { return shadowMesh; }

//		void						testPopulate();


//		void		packRenderComponent(MeshRenderComponent* rc, uint32_t patchIdx);
//		void		packMeshInstances(InstancedMeshRenderComponent* rc);

		// Only packs trees currently //
//		void		packPropInstances(InstancedMeshRenderComponent* rc);
	
	private:

		TerrainInitializer mInitializer;
		std::shared_ptr<Mesh> mMesh;
		std::shared_ptr<RenderObject> mRenderObject;
		std::shared_ptr<MeshNodeRod> mMeshNode;
//		Mesh* mMesh;
		
//		MeshPropPartition* pMeshPropPartition;
//
//		// Terrain Patches //
//		std::vector<MeshPatch*> patchList;
//		double* heightField;
//		float* perlinNoise;
//		uint32_t				numPerlinSamplesX;
//		uint32_t				numPerlinSamplesY;
//		uint32_t				numPatchesX;
//		uint32_t				numPatchesZ;
//		uint32_t				numVertsPerPatch;
//		Vertex*					baseVerts;
//		uint32_t				numGrassIndices;
//
//		// Prop patches //
//		uint32_t				numPropPatchesX;
//		uint32_t				numPropPatchesZ;
//
//		uint32_t	vertexStride;
//
//		// Height field //
//		float		heightScale = 0.6f;
//		float		xScale = 1.0f;
//		float		zScale = 1.0f;
//		uint32_t	numSamplesX;
//		uint32_t	numSamplesZ;
//
//		GLuint		heightmapTexture;
//		GLuint		normalmapTexture;
////		GLuint		shadowTerrainVBO;
////		GLuint		basePatchVBO;
//		FboId		topDownRT;
//		GLuint		topDownTexture;
//		uint32_t	topDownWidth;
//		uint32_t	topDownHeight;
//
//		// Grass Assets //
//		GLuint		grassVAO;
//		GLuint		L0GrassTBO;
//		GLuint		L0GrassHandle;
//
//		// Tree assets //
//		GLuint		L0TreeTBO;
//		GLuint		L0TreeHandle;
//
//
//		vec3<float>		meshMins;
//		vec3<float>		meshMaxs;
//
//		double		mMinHeight;
//		double		mMaxHeight;
//
//		uint32_t* topDownBuf = nullptr;
//
//		float		textureRepeatScale = 128.0f;
//
//		double		interpHeightFromPos(vec3<float> aPos,
//									    vec2<int32_t> aLowerLeft,
//									    vec2<int32_t> aLowerRight,
//									    vec2<int32_t> aUpperRight,
//									    vec2<int32_t> aUpperLeft);
//
//		vec2<float>		getHeightfieldPos(uint32_t aX, uint32_t aZ);
//
//		QArray<vec4<float>> propInstances;
//
//		uint32_t	size;
//		double		entropy;
//		double		smoothing;
//
//		Mesh*		shadowMesh;
//		Mesh*		baseMesh;
};

#endif
