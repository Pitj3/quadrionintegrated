#ifndef qmath_h__
#define qmath_h__

#include <cstring>
#include <cstdint>
#include <valarray>
#include <complex>
#include <random>
#include <cmath>
#include <math.h>
#include <cstdlib>

#if defined(_WIN32) || defined(_WIN64)
	#pragma warning(disable:4251)	// Template class type interface in dllimport/export classes
#endif

const double PI		= 3.141592653589793238463;
const float  PI_F	= 3.14159265358979f;
const float  PI2_F	= 1.570796326794895f;

constexpr uint32_t bitLeft(uint32_t x) 
{
	return 1 << x;
}

static inline bool equalsF(const float l, const float r, const float bias)
{
	if(fabs(l - r) < bias)
		return true;

	return false;
}

static inline bool equalsD(const double l, const double r, const double bias)
{
	if(abs(l - r) < bias)
		return true;

	return false;
}

static inline bool isPow2(uint32_t num)
{
	if((num != 1) && (num & (num - 1)))
		return false;
	return true;
}

static inline float deg2Rad(const float deg)
{
	return PI_F * deg / 180.0F;
}

static inline float rad2Deg(const float rad)
{
	return 180.0F / PI_F * rad;
}

static inline int32_t gcd(int32_t a, int32_t b)
{
	if(a == 0 ||b == 0)
		return 0;

	if(a == b)
		return a;
	
	if(a > b)
		return gcd(a - b, b);
	
	return gcd(a, b - a);
}

template<class T>
static inline T lerp(T x, T y, T a)
{
	return x * (T(1.0) - a) + y * a;
}

static inline void generateGaussianNoise(double* dat, uint32_t nSamps, const double stdDev = 0.1)
{
	if (nSamps <= 0)
		return;

	const double mean = 0.0;
	const double dev = stdDev;

	std::mt19937 gen(std::random_device{}());
	std::normal_distribution<double> dist(mean, dev);

	for (uint32_t i = 0; i < nSamps; ++i)
	{
		dat[i] = dist(gen);
	}
}

template <class T>
static void qswap(T* l, T* r)
{
	T tmp = *l;
	*l = *r;
	*r = tmp;
}

static inline float fastInverseSqrt(const float& a)
{
	float ha = 0.5F * a;
	float ret;
	int i = *(int*)&a;
	i = 0x5F3759D5 - (i >> 1);
	ret = *(float*)&i;
	ret = ret * (1.5F - ha * ret * ret);
	return ret;
}

template <class T>
struct vec3;

////////////////////////////////////////////////////////
//
//			mat2f
//
////////////////////////////////////////////////////////
template <class T>
struct mat2
{
	T m[4];

	mat2() 
	{
		loadIdentity();
	}

	~mat2() {}

	mat2(const T* in) { memcpy(m, in, sizeof(T) * 4); }
	mat2(const mat2<T>& in) { memcpy(m, in.m, sizeof(T) * 4); }

	const void set(const T* in) { memcpy(m, in, sizeof(T) * 4); }
	const void set(const mat2<T>& in) { memcpy(m, in.m, sizeof(T) * 4); }

	const void operator= (const T* in) { memcpy(m, in, sizeof(T) * 4); }
	const void operator= (const mat2<T>& in) { memcpy(m, in.m, sizeof(T) * 4); }

	const void loadIdentity()
	{
		m[0] = T(1.0);
		m[1] = T(0.0);
		m[2] = T(0.0);
		m[3] = T(1.0);
	}

	void rotate(T ang)
	{
		m[0] = cos(ang);
		m[1] = sin(ang);
		m[2] = -sin(ang);
		m[3] = cos(ang);
	}

	T& operator[] (size_t i)
	{
		if (i > 3)
			return m[0];

		return m[i];
	}
};



////////////////////////////////////////////////////////
//
//			mat3<float>
//
////////////////////////////////////////////////////////
template <class T>
struct mat3
{
	T m[9];

	mat3() {}
	~mat3() {}

	mat3(const T* in) { memcpy(m, in, sizeof(T) * 9); }
	mat3(const mat3<T>& in) { memcpy(m, in.m, sizeof(T) * 9); }

	const void set(const T* in) { memcpy(m, in, sizeof(T) * 9); }
	const void set(const mat3<T>& in) { memcpy(m, in.m, sizeof(T) * 9); }

	const void operator= (const T* in) { memcpy(m, in, sizeof(T) * 9); }
	const void operator= (const mat3<T>& in) { memcpy(m, in.m, sizeof(T) * 9); }

	T& operator[] (size_t i)
	{
		if (i > 8)
			return m[0];

		return m[i];
	}
};


////////////////////////////////////////////////////////
//
//			mat4<float>
//
////////////////////////////////////////////////////////
template <class T>
struct mat4
{
	T m[16];

	mat4()
	{
		loadIdentity();
	}

	mat4(T a1, T a2, T a3, T a4,
		T b1, T b2, T b3, T b4,
		T c1, T c2, T c3, T c4,
		T d1, T d2, T d3, T d4)
	{
		m[0] = a1; m[1] = a2; m[2] = a3; m[3] = a4;
		m[4] = b1; m[5] = b2; m[6] = b3; m[7] = b4;
		m[8] = c1; m[9] = c2; m[10] = c3; m[11] = c4;
		m[12] = d1; m[13] = d2; m[14] = d3; m[15] = d4;
	}

	~mat4() {}

	mat4(const T* in) { memcpy(m, in, sizeof(T) * 16); }
	mat4(const mat4<T>& in) { memcpy(m, in.m, sizeof(T) * 16); }

	const void set(const T* in) { memcpy(m, in, sizeof(T) * 16); }
	const void set(const mat4<T>& in) { memcpy(m, in.m, sizeof(T) * 16); }

	const void operator= (const T* in) { memcpy(m, in, sizeof(T) * 16); }
	const void operator= (const mat4<T>& in) { memcpy(m, in.m, sizeof(T) * 16); }


	const void operator*= (const mat4<T>& r)
	{
		mat4<T> res;
		res.m[0] = (m[0] * r.m[0]) + (m[4] * r.m[1]) + (m[8] * r.m[2]) + (m[12] * r.m[3]);
		res.m[4] = (m[0] * r.m[4]) + (m[4] * r.m[5]) + (m[8] * r.m[6]) + (m[12] * r.m[7]);
		res.m[8] = (m[0] * r.m[8]) + (m[4] * r.m[9]) + (m[8] * r.m[10]) + (m[12] * r.m[11]);
		res.m[12] = (m[0] * r.m[12]) + (m[4] * r.m[13]) + (m[8] * r.m[14]) + (m[12] * r.m[15]);

		res.m[1] = (m[1] * r.m[0]) + (m[5] * r.m[1]) + (m[9] * r.m[2]) + (m[13] * r.m[3]);
		res.m[5] = (m[1] * r.m[4]) + (m[5] * r.m[5]) + (m[9] * r.m[6]) + (m[13] * r.m[7]);
		res.m[9] = (m[1] * r.m[8]) + (m[5] * r.m[9]) + (m[9] * r.m[10]) + (m[13] * r.m[11]);
		res.m[13] = (m[1] * r.m[12]) + (m[5] * r.m[13]) + (m[9] * r.m[14]) + (m[13] * r.m[15]);

		res.m[2] = (m[2] * r.m[0]) + (m[6] * r.m[1]) + (m[10] * r.m[2]) + (m[14] * r.m[3]);
		res.m[6] = (m[2] * r.m[4]) + (m[6] * r.m[5]) + (m[10] * r.m[6]) + (m[14] * r.m[7]);
		res.m[10] = (m[2] * r.m[8]) + (m[6] * r.m[9]) + (m[10] * r.m[10]) + (m[14] * r.m[11]);
		res.m[14] = (m[2] * r.m[12]) + (m[6] * r.m[13]) + (m[10] * r.m[14]) + (m[14] * r.m[15]);

		res.m[3] = (m[3] * r.m[0]) + (m[7] * r.m[1]) + (m[11] * r.m[2]) + (m[15] * r.m[3]);
		res.m[7] = (m[3] * r.m[4]) + (m[7] * r.m[5]) + (m[11] * r.m[6]) + (m[15] * r.m[7]);
		res.m[11] = (m[3] * r.m[8]) + (m[7] * r.m[9]) + (m[11] * r.m[10]) + (m[15] * r.m[11]);
		res.m[15] = (m[3] * r.m[12]) + (m[7] * r.m[13]) + (m[11] * r.m[14]) + (m[15] * r.m[15]);

		*this = res;
	}

	T getDeterminant()
	{
		T d = (m[0] * m[5] * m[10] * m[15]) + (m[0] * m[9] * m[14] * m[7]) + (m[0] * m[13] * m[6] * m[11])
			+ (m[4] * m[1] * m[14] * m[11]) + (m[4] * m[9] * m[2] * m[15]) + (m[4] * m[13] * m[10] * m[3])
			+ (m[8] * m[1] * m[6] * m[15]) + (m[8] * m[5] * m[14] * m[3]) + (m[8] * m[13] * m[2] * m[7])
			+ (m[12] * m[1] * m[10] * m[7]) + (m[12] * m[5] * m[2] * m[11]) + (m[12] * m[9] * m[6] * m[3])
			- (m[0] * m[5] * m[14] * m[11]) - (m[0] * m[9] * m[6] * m[15]) - (m[0] * m[13] * m[10] * m[7])
			- (m[4] * m[1] * m[10] * m[15]) - (m[4] * m[9] * m[14] * m[3]) - (m[4] * m[13] * m[2] * m[11])
			- (m[8] * m[1] * m[14] * m[7]) - (m[8] * m[5] * m[2] * m[15]) - (m[8] * m[13] * m[6] * m[3])
			- (m[12] * m[1] * m[6] * m[11]) - (m[12] * m[5] * m[10] * m[3]) - (m[12] * m[9] * m[2] * m[7]);

		return d;
	}

	mat4<T> getInverse()
	{
		T d = getDeterminant();
		// Check for 0 determinant //
		mat4<T> out;
		if (fabs(d - T(0.0)) < T(0.00001) || d == T(0.0))
			return out;
			

		T i = T(1.0) / d;
		out[0] = i * ((m[5] * m[10] * m[15]) + (m[9] * m[14] * m[7]) + (m[13] * m[6] * m[11]) - (m[5] * m[14] * m[11]) - (m[9] * m[6] * m[15]) - (m[13] * m[10] * m[7]));
		out[4] = i * ((m[4] * m[14] * m[11]) + (m[8] * m[6] * m[15]) + (m[12] * m[10] * m[7]) - (m[4] * m[10] * m[15]) - (m[8] * m[14] * m[7]) - (m[12] * m[6] * m[11]));
		out[8] = i * ((m[4] * m[9] * m[15]) + (m[8] * m[13] * m[7]) + (m[12] * m[5] * m[11]) - (m[4] * m[13] * m[11]) - (m[8] * m[5] * m[15]) - (m[12] * m[9] * m[7]));
		out[12] = i * ((m[4] * m[13] * m[10]) + (m[8] * m[5] * m[14]) + (m[12] * m[9] * m[6]) - (m[4] * m[9] * m[14]) - (m[8] * m[13] * m[6]) - (m[12] * m[5] * m[10]));
		out[1] = i * ((m[1] * m[14] * m[11]) + (m[9] * m[2] * m[15]) + (m[13] * m[10] * m[3]) - (m[1] * m[10] * m[15]) - (m[9] * m[14] * m[3]) - (m[13] * m[2] * m[11]));
		out[5] = i * ((m[0] * m[10] * m[15]) + (m[8] * m[14] * m[3]) + (m[12] * m[2] * m[11]) - (m[0] * m[14] * m[11]) - (m[8] * m[2] * m[15]) - (m[12] * m[10] * m[3]));
		out[9] = i * ((m[0] * m[13] * m[11]) + (m[8] * m[1] * m[15]) + (m[12] * m[9] * m[3]) - (m[0] * m[9] * m[15]) - (m[8] * m[13] * m[3]) - (m[12] * m[1] * m[11]));
		out[13] = i * ((m[0] * m[9] * m[14]) + (m[8] * m[13] * m[2]) + (m[12] * m[1] * m[10]) - (m[0] * m[13] * m[10]) - (m[8] * m[1] * m[14]) - (m[3] * m[9] * m[2]));
		out[2] = i * ((m[1] * m[6] * m[15]) + (m[5] * m[14] * m[3]) + (m[13] * m[2] * m[7]) - (m[1] * m[14] * m[7]) - (m[5] * m[2] * m[15]) - (m[13] * m[6] * m[3]));
		out[6] = i * ((m[0] * m[14] * m[7]) + (m[4] * m[2] * m[15]) + (m[12] * m[6] * m[3]) - (m[0] * m[6] * m[15]) - (m[4] * m[14] * m[3]) - (m[12] * m[2] * m[7]));
		out[10] = i * ((m[0] * m[5] * m[15]) + (m[4] * m[13] * m[3]) + (m[12] * m[1] * m[7]) - (m[0] * m[13] * m[7]) - (m[4] * m[1] * m[15]) - (m[12] * m[5] * m[3]));
		out[14] = i * ((m[0] * m[13] * m[6]) + (m[4] * m[1] * m[14]) + (m[12] * m[5] * m[2]) - (m[0] * m[5] * m[14]) - (m[4] * m[13] * m[2]) - (m[12] * m[1] * m[6]));
		out[3] = i * ((m[1] * m[10] * m[7]) + (m[5] * m[2] * m[11]) + (m[9] * m[6] * m[3]) - (m[1] * m[6] * m[11]) - (m[5] * m[10] * m[3]) - (m[9] * m[2] * m[7]));
		out[7] = i * ((m[0] * m[6] * m[11]) + (m[4] * m[10] * m[3]) + (m[8] * m[2] * m[7]) - (m[0] * m[10] * m[7]) - (m[4] * m[2] * m[11]) - (m[8] * m[6] * m[3]));
		out[11] = i * ((m[0] * m[9] * m[7]) + (m[4] * m[1] * m[11]) + (m[8] * m[5] * m[3]) - (m[0] * m[5] * m[11]) - (m[4] * m[9] * m[3]) - (m[8] * m[1] * m[7]));
		out[15] = i * ((m[0] * m[5] * m[10]) + (m[4] * m[9] * m[2]) + (m[8] * m[1] * m[6]) - (m[0] * m[9] * m[6]) - (m[4] * m[1] * m[10]) - (m[8] * m[5] * m[2]));

		return out;
	}

	mat3<T> getRotationMatrix3X3()
	{
		mat3<T> ret;

		ret.m[0] = m[0];
		ret.m[1] = m[1];
		ret.m[2] = m[2];

		ret.m[3] = m[4];
		ret.m[4] = m[5];
		ret.m[5] = m[6];

		ret.m[6] = m[8];
		ret.m[7] = m[9];
		ret.m[8] = m[10];

		return ret;
	}

	mat4<T> getRotationMatrix4X4()
	{
		mat4<T> ret;

		ret = *this;
		ret.m[12] = T(0.0);
		ret.m[13] = T(0.0);
		ret.m[14] = T(0.0);
		ret.m[15] = T(1.0);

		return ret;
	}

	mat4<T> getScaleMatrix(T x, T y, T z)
	{
		mat4<T> ret;
		ret.loadIdentity();

		ret.m[0] = x;
		ret.m[5] = y;
		ret.m[10] = z;

		return ret;
	}

	mat4<T> sphereToRect(T x, T y, T z)
	{
		mat4<T> ret;

		// TODO: precomp sin/cos
		ret.m[0] = cos(x) * cos(z) - sin(x) * cos(y) * sin(z);
		ret.m[1] = -cos(x) * sin(z) - sin(x) * cos(y) * cos(z);
		ret.m[2] = sin(x) * sin(y);

		ret.m[4] = sin(x) * cos(z) + cos(x) * cos(y) * sin(z);
		ret.m[5] = -sin(x) * sin(z) + cos(x) * cos(y) * cos(z);
		ret.m[6] = -cos(x) * sin(y);

		ret.m[8] = sin(y) * sin(z);
		ret.m[9] = sin(y) * cos(z);
		ret.m[10] = cos(y);

		return ret;
	}

	T& operator[] (int i)
	{
		if (i > 15)
			return m[0];

		return m[i];
	}

	void transpose()
	{
		qswap(&m[1], &m[4]);
		qswap(&m[2], &m[8]);
		qswap(&m[3], &m[12]);
		qswap(&m[6], &m[9]);
		qswap(&m[7], &m[13]);
		qswap(&m[11], &m[14]);
	}

	void invert()
	{
		mat4<T> out = this->getInverse();
		*this = out;
	}


	void loadIdentity()
	{
		m[0] = T(1.0);	m[4] = T(0.0);	m[8] = T(0.0);		m[12] = T(0.0);
		m[1] = T(0.0);	m[5] = T(1.0);	m[9] = T(0.0);		m[13] = T(0.0);
		m[2] = T(0.0);	m[6] = T(0.0);	m[10] = T(1.0);		m[14] = T(0.0);
		m[3] = T(0.0);  m[7] = T(0.0);	m[11] = T(0.0);     m[15] = T(1.0);
	}

	void loadScale(T sx, T sy, T sz)
	{
		m[0] = sx;		m[4] = T(0.0);	m[8] = T(0.0);		m[12] = T(0.0);
		m[1] = T(0.0);	m[5] = sy;		m[9] = T(0.0);		m[13] = T(0.0);
		m[2] = T(0.0);	m[6] = T(0.0);	m[10] = sz;			m[14] = T(0.0);
		m[3] = T(0.0);  m[7] = T(0.0);	m[11] = T(0.0);     m[15] = T(1.0);
	}

	void loadScale(vec3<T> t)
	{
		loadScale(t.x, t.y, t.z);
	}

	void loadTranslation(T tx, T ty, T tz)
	{
		m[0] = T(1.0);	m[4] = T(0.0);	m[8] = T(0.0);		m[12] = tx;
		m[1] = T(0.0);	m[5] = T(1.0);	m[9] = T(0.0);		m[13] = ty;
		m[2] = T(0.0);	m[6] = T(0.0);	m[10] = T(1.0);		m[14] = tz;
		m[3] = T(0.0);  m[7] = T(0.0);	m[11] = T(0.0); 	m[15] = T(1.0);
	}

	void loadTranslation(vec3<T> t)
	{
		loadTranslation(t.x, t.y, t.z);
	}

	void loadXRotation(T ang)
	{
		m[0] = T(1.0);	m[4] = T(0.0);		m[8] = T(0.0);		m[12] = T(0.0);
		m[1] = T(0.0);	m[5] = cosf(ang);	m[9] = -sinf(ang);	m[13] = T(0.0);
		m[2] = T(0.0);	m[6] = sinf(ang);	m[10] = cosf(ang);	m[14] = T(0.0);
		m[3] = T(0.0);	m[7] = T(0.0);		m[11] = T(0.0);		m[15] = T(1.0);
	}

	inline void loadYRotation(T ang)
	{
		m[0] = cosf(static_cast<float>(ang));	m[4] = T(0.0);	m[8] = sinf(static_cast<float>(ang));	m[12] = T(0.0);
		m[1] = T(0.0);		m[5] = T(1.0);	m[9] = T(0.0);		m[13] = T(0.0);
		m[2] = -sinf(static_cast<float>(ang));	m[6] = T(0.0);	m[10] = cosf(static_cast<float>(ang));	m[14] = T(0.0);
		m[3] = T(0.0);	    m[7] = T(0.0);	m[11] = T(0.0);		m[15] = T(1.0);
	}

	void loadZRotation(T ang)
	{
		m[0] = cosf(ang);	m[4] = -sinf(ang);	m[8] = T(0.0);	m[12] = T(0.0);
		m[1] = sinf(ang);	m[5] = cosf(ang);	m[9] = T(0.0);	m[13] = T(0.0);
		m[2] = T(0.0);		m[6] = T(0.0);		m[10] = T(1.0);	m[14] = T(0.0);
		m[3] = T(0.0);	    m[7] = T(0.0);		m[11] = T(0.0);	m[15] = T(1.0);
	}
};

template <class T>
static mat4<T> operator* (const mat4<T>& l, const mat4<T>& r)
{
	mat4<T> ret;
	ret = l;
	ret *= r;

	return ret;
}



////////////////////////////////////////////
//
//		vec2
//
////////////////////////////////////////////
template <class T>
struct vec2
{
	T x, y;

	vec2(){}

	vec2(const T* v) { x = v[0]; y = v[1]; }
	vec2(const T& nx, const T& ny) { x = nx; y = ny; }

	const void set(const T* v) { x = v[0]; y = v[1]; }
	const void set(const vec2<T>& v) { x = v.x; y = v.y; }
	const void set(const T& nx, const T& ny) { x = nx; y = ny; }

	const void operator*= (const T& s) { x *= s; y *= s; }
	const void operator/= (const T& s) { x /= s; y /= s; }
	const void operator+= (const vec2<T>& v) { x += v.x; y += v.y; }
	const void operator+= (const T* v) { x += v[0]; y += v[1]; }
	const void operator-= (const vec2<T>& v) { x -= v.x; y -= v.y; }
	const void operator-= (const T* v) { x -= v[0]; y -= v[1]; }
	const bool operator== (const vec2<T>& v) { return ((x == v.x) && (y == v.y)); }
	const bool operator== (const T* v) { return ((x == v[0]) && (y == v[1])); }
	const bool operator!= (const vec2<T>& v) { return !(*this == v); }
	const bool operator!= (const T* v) { return !(*this == v); }

	float getLength()
	{
		// TODO: PRECISION?
		return T(1.0) / fastInverseSqrt(x * x + y * y);
	}

	const T dotProd(const vec2<T>& v)
	{
		return (x * v.x) + (y * v.y);
	}

	const vec2<T> getPerpVector()
	{
		return vec2<T>(-y, x);
	}

	const void normalize()
	{
		T iLen = T(1.0) / getLength();
		x *= iLen;
		y *= iLen;
	}
};

template <class T>
static vec2<T> operator+ (const vec2<T>& l, const vec2<T>& r)
{
	return vec2<T>(l.x + r.x, l.y + r.y);
}

template <class T>
static vec2<T> operator* (const vec2<T>& l, const T& r)
{
	return vec2<T>(l.x * r, l.y * r);
}

template <class T>
static vec2<T> operator* (const T& l, const vec2<T>& r)
{
	return vec2<T>(r.x * l, r.y * l);
}

template <class T>
static vec2<T> operator- (const vec2<T>& l, const vec2<T>& r)
{
	return vec2<T>(l.x - r.x, l.y - r.y);
}

template <class T>
static vec2<T> operator/ (const vec2<T>& l, const T r)
{
	return vec2<T>(l.x / r, l.y / r);
}

template <class T>
static vec2<T> operator/ (const vec2<T>& l, const vec2<T>& r)
{
	return vec2<T>(l.x / r.x, l.y / r.y);
}

template <class T>
static vec2<T> operator* (const mat2<T>& l, const vec2<T>& r)
{
	vec2<T> out;

	out.x = (r.x * l.m[0] + r.y * l.m[2]);
	out.y = (r.x * l.m[1] + r.y * l.m[3]);

	return out;
}


////////////////////////////////////////////////////
//
//				vec3<T>
//
////////////////////////////////////////////////////

template <class T>
struct vec3
{
	T x, y, z;

	vec3() noexcept {}

	vec3(const T* v) { x = v[0]; y = v[1]; z = v[2]; }
	vec3(const T& nx, const T& ny, const T& nz) { x = nx; y = ny; z = nz; }

	const void set(const T* v) { x = v[0]; y = v[1]; z = v[2]; }
	const void set(const T& nx, const T& ny, const T& nz) 
	{ 
		x = nx; y = ny; z = nz; 
	}

	const void set(const vec3<T>& v)
	{
		x = v.x; y = v.y; z = v.z;
	}

	const vec3<T> operator- () { return vec3<T>(-x, -y, -z); }
	const void operator*= (const T& s) { x *= s; y *= s; z *= s; }
	const void operator*= (const vec3<T>& v) { x *= v.x; y *= v.y; z *= v.z; }
	const void operator/= (const T& s) { float inv = 1.0F / s; x *= inv; y *= inv; z *= inv; }
	const void operator+= (const vec3<T>& v) { x += v.x; y += v.y; z += v.z; }
	const void operator+= (const T* v) { x += v[0]; y += v[1]; z += v[2]; }
	const void operator-= (const vec3<T>& v) { x -= v.x; y -= v.y; z -= v.z; }
	const void operator-= (const T* v) { x -= v[0]; y -= v[1]; z -= v[2]; }
	const bool operator== (const vec3<T>& v) { return ((x == v.x) && (y == v.y) && (z == v.z)); }
	const bool operator== (const T* v) { return ((x == v[0]) && (y == v[1]) && (z == v[2])); }
	const bool operator!= (const vec3<T>& v) { return !(*this == v); }
	const bool operator!= (const T* v) { return !(*this == v); }

	const vec3<T> lerp(const vec3<T>& op, const T& dv)
	{
		return vec3<T>(x + dv * (op.x - x), y + dv * (op.y - y), z + dv * (op.z - z));
	}

	T getLength()
	{
		return sqrt(x*x + y*y + z*z);
	}

	const T dotProd(const vec3<T>& v)
	{
		return ((x * v.x) + (y * v.y) + (z * v.z));
	}

	const void normalize()
	{
		T length = getLength();

		x /= length;
		y /= length;
		z /= length;
	}

	const vec3<T> crossProd(const vec3<T>& v)
	{
		return vec3<T>((y * v.z - z * v.y), (z * v.x - x * v.z), (x * v.y - y * v.x));
	}

	const vec3<T> crossProd(const T* v)
	{
		return vec3<T>((y * v[2] - z * v[1]), (z * v[0] - x * v[2]), (x * v[1] - y * v[0]));
	}

	const T tripleScalar(const vec3<T>& v, const vec3<T>& w)
	{
		return ((y * v.z - z * v.y) * w.x + (z * v.x - x * v.z) * w.y + (x * v.y - y * v.x) * w.z);
	}

	const void assignIfLess(const vec3<T>& v)
	{
		x = (v.x < x) ? v.x : x;
		y = (v.y < y) ? v.y : y;
		z = (v.z < z) ? v.z : z;
	}

	const void assignIfGreater(const vec3<T>& v)
	{
		x = (v.x > x) ? v.x : x;
		y = (v.y > y) ? v.y : y;
		z = (v.z > z) ? v.z : z;
	}

	const bool isNan() const
	{
		return (isnan(x) || isnan(y) || isnan(z));
	}
};

template <class T>
static vec3<T> operator- (const vec3<T>& l, const vec3<T>& r)
{
	return vec3<T>(l.x - r.x, l.y - r.y, l.z - r.z);
}

template <class T>
static vec3<T> operator+ (const vec3<T>& l, const vec3<T>& r)
{
	return vec3<T>(l.x + r.x, l.y + r.y, l.z + r.z);
}

template <class T>
static vec3<T> operator* (const vec3<T>& l, const T& r)
{
	return vec3<T>(l.x * r, l.y * r, l.z * r);
}

template <class T>
static vec3<T> operator* (const T& l, const vec3<T>& r)
{
	return vec3<T>(r.x * l, r.y * l, r.z * l);
}


////////////////////////////////////////////////////
//
//				vec4<T>
//
////////////////////////////////////////////////////
template <class T>
struct vec4
{
	T x, y, z, w;

	vec4() {}
	~vec4() {}

	vec4(const T* v) { x = v[0]; y = v[1]; z = v[2]; w = v[3]; }
	vec4(const vec4<T>& v) { x = v.x; y = v.y; z = v.z; w = v.w; }
	vec4(const T& nx, const T& ny, const T& nz, const T& nw) { x = nx; y = ny; z = nz; w = nw; }

	const void set(const T* v) { x = v[0]; y = v[1]; z = v[2]; w = v[3]; }
	const void set(const vec4<T>& v) { x = v.x; y = v.y; z = v.z; w = v.w; }
	const void set(const T& nx, const T& ny, const T& nz, const T& nw) 
	{ 
		x = nx; y = ny; z = nz; w = nw; 
	}

	const void operator=  (const vec4<T>& v) { x = v.x; y = v.y; z = v.z; w = v.w; }
	const void operator=  (const T* v) { x = v[0]; y = v[1]; z = v[2]; w = v[3]; }


	const void operator*= (const T& v) { x *= v; y *= v; z *= v; w *= w; }
	const void operator*= (const mat4<T>& m)
	{
		vec4<T> tmp = *this;
		x = m.m[0] * tmp.x + m.m[4] * tmp.y + m.m[8] * tmp.z + m.m[12] * tmp.w;
		y = m.m[1] * tmp.x + m.m[5] * tmp.y + m.m[9] * tmp.z + m.m[13] * tmp.w;
		z = m.m[2] * tmp.x + m.m[6] * tmp.y + m.m[10] * tmp.z + m.m[14] * tmp.w;
		w = m.m[3] * tmp.x + m.m[7] * tmp.y + m.m[11] * tmp.z + m.m[15] * tmp.w;
	}
};

template <class T>
static vec4<T> operator* (const mat4<T>& l, const vec4<T>& r)
{
	vec4<T> res;
	res.x = l.m[0] * r.x + l.m[4] * r.y + l.m[8] * r.z + l.m[12] * r.w;
	res.y = l.m[1] * r.x + l.m[5] * r.y + l.m[9] * r.z + l.m[13] * r.w;
	res.z = l.m[2] * r.x + l.m[6] * r.y + l.m[10] * r.z + l.m[14] * r.w;
	res.w = l.m[3] * r.x + l.m[7] * r.y + l.m[11] * r.z + l.m[15] * r.w;

	return res;
}


///////////////////////////////////////////////////////////////
//
//			line2<float>
//
///////////////////////////////////////////////////////////////
template <class T>
struct line2
{
	vec2<T> v0, v1;

	float pointIntersect(const vec2<T> pt)
	{
		T d = ((v1.x - v0.x) * (pt.y - v0.y)) - ((v1.y - v0.y) * (pt.x - v0.x));
		return d;
	}
};


/////////////////////////////////////////////////////////////////
//
//			line3f
//
//////////////////////////////////////////////////////////////////

template <class T>
struct line3
{
	vec3<T> v0, v1;
};


////////////////////////////////////////////////////////
//
// PinkNoise - Pink noise generator for smoothing noise
//
///////////////////////////////////////////////////////
struct PinkNoise
{	
	// Generates next value in sequence from the generated values //
	double NextValue()
	{
		std::default_random_engine gen;
		std::normal_distribution<double> dist(0.0, 1.0);
		double x = dist(gen);

		for(uint32_t i = 0; i < nPoles; ++i)
		{
			x -= multipliers[i] * values[i];
		}

		for(uint32_t i = 0; i < nPoles - 1; ++i)
		{
			values[i] = values[i + 1];	
		}
		values[0] = x;

		return x;
	}

	PinkNoise()
	{
		nPoles = 0;
		multipliers = nullptr;
		values = nullptr;
	}

	// Seed the generator.
	// alpha - Distribution exponent  1/f^alpha - Default value: 1.0
	// poles - number of IIR poles - Default value: 5
	PinkNoise(double alpha, uint32_t poles)
	{
		nPoles = poles;
		multipliers = new double[poles];
		values = new double[poles];

		double a = 1.0;
		for(uint32_t i = 0; i < nPoles; ++i)
		{
			a = (static_cast<double>(i) - alpha / 2.0) * a / (static_cast<double>(i) + 1.0);
			multipliers[i] = a;	
		}	

		for(uint32_t i = 0; i < 5 * nPoles; ++i)
		{
			NextValue();
		}
	}

	~PinkNoise()
	{
		if(multipliers)
			delete[] multipliers;
		if(values)	
			delete[] values;
	}

	uint32_t nPoles;
	double*  multipliers;
	double*	 values;
};


template <class T>
struct Rect
{
	T l, r, b, t;
};

/*
struct QRect
{
	int x, y, width, height;
};
*/

struct TextureRect
{
	float leftU;
	float topV;
	float rightU;
	float bottomV;
};


template <class T>
static void getTextureCoordinates(const vec2<T>& srcDims, 
								  const Rect<float>* src, 
								  const vec2<T>& destDims, 
								  const Rect<float>* dest, 
								  TextureRect* pCoords)
{
	float tU, tV;
	pCoords->leftU = 0.0f;
	pCoords->topV = 1.0f;
	pCoords->rightU = 1.0f;
	pCoords->bottomV = 0.0f;

	if(src)
	{
		tU = T(1.0) / srcDims.x;
		tV = T(1.0) / srcDims.y;
		
		pCoords->leftU += src->l * tU;
		pCoords->topV -= src->t * tV;
		pCoords->rightU -= (srcDims.x - src->r) * tU;
		pCoords->bottomV += (srcDims.y - src->b) * tV;	
	}

	if(dest)
	{
		tU = T(1.0) / destDims.x;
		tV = T(1.0) / destDims.y;

		pCoords->leftU -= dest->l * tU;
//		pCoords->topV += dest->t * tV;
		pCoords->topV += tV;
		pCoords->rightU += (destDims.x - dest->r) * tU;
//		pCoords->bottomV -= (destDims.y - dest->b) * tV;
		pCoords->bottomV -= tV;

		if(src)
		{
			pCoords->bottomV = 1.0f - pCoords->bottomV;
			pCoords->topV = 1.0f - pCoords->topV;
		}
	}
}

template <class T>
static void getTextureRect(const vec2<T>& dims, Rect<T>* rect)
{
	if(!rect)
		return;

	rect->l = T(0.0);
	rect->t = dims.y;
	rect->r = dims.x;
	rect->b = T(0.0);		
}

static inline void inflateRect(Rect<float>* rect, float dx, float dy)
{
	rect->r += dx;
	rect->l -= dx;
	rect->b -= dy;
	rect->t += dy;
}

static inline float getGaussianDistribution(const float& x, const float& y, const float& rho)
{
	float g = 1.0f / sqrtf(2.0f * PI_F * rho * rho);
	g *= expf(-(x * x + y * y) / (2.0f * rho * rho));
	return g;
}

template <class T>
static void get5x5GaussianOffsets(unsigned int w, 
								  unsigned int h, 
								  vec2<T>* offsets, 
								  vec4<T>* weights, 
								  T mul)
{
	float tu = 1.0f / (float)w;
	float tv = 1.0f / (float)h;

	vec4<float> white(1.0f, 1.0f, 1.0f, 1.0f);
	float totalWeight = 0.0f;
	int idx = 0;

	for(int x = -2; x <= 2; ++x)
	{
		for(int y = -2; y <= 2; ++y)
		{
			if(abs(x) + abs(y) > 2)
				continue;

			offsets[idx].x = x * tu;
			offsets[idx].y = y * tv;
			weights[idx].x = white.x * getGaussianDistribution((float)x, (float)y, 1.0f);
			weights[idx].y = white.y * getGaussianDistribution((float)x, (float)y, 1.0f);
			weights[idx].z = white.z * getGaussianDistribution((float)x, (float)y, 1.0f);
			weights[idx].w = white.w * getGaussianDistribution((float)x, (float)y, 1.0f);
			totalWeight += weights[idx].x;
			++idx;
		}
	}

	for(int i = 0; i < idx; ++i)
	{
		weights[i].x /= totalWeight; weights[i].x *= mul;
		weights[i].y /= totalWeight; weights[i].y *= mul;
		weights[i].z /= totalWeight; weights[i].z *= mul;
		weights[i].w /= totalWeight; weights[i].w *= mul;
	}
}

template <class T>
static void get2x2SampleOffsets(int32_t w, int32_t h, vec2<T>* avSampleOffsets)
{
	if(!avSampleOffsets)
		return;

	float tu = 1.0f / w;
	float tv = 1.0f / h;

	int32_t idx = 0;
	for(int32_t y = 0; y < 2; ++y)
	{
		for(int32_t x = 0; x < 2; ++x)
		{
			avSampleOffsets[idx].x = (x - 0.5f) * tu;
			avSampleOffsets[idx].y = (y - 0.5f) * tv;
			++idx;
		}
	}
}

template <class T>
static void get4x4SampleOffsets(const int32_t& bbWidth,
								const int32_t& bbHeight,
								vec2<T>* avSampleOffsets)
{
	if (!avSampleOffsets)
		return;

	float tu = 1.0F / bbWidth;
	float tv = 1.0F / bbHeight;

	int32_t index = 0;
	for (int32_t y = 0; y < 4; ++y)
	{
		for (int32_t x = 0; x < 4; ++x)
		{
			avSampleOffsets[index].x = (x - 1.5F) * tu;
			avSampleOffsets[index].y = (y - 1.5F) * tv;
			++index;
		}
	}
}

template <class T>
static void getBloomOffsets(int32_t size, 
							float texCoordOffset[15], 
							vec4<T>* colorWeight, 
							float dev, 
							float mul)
{
	int32_t i = 0;
	float tu = 1.0f / size;
	
	float weight = mul * getGaussianDistribution(0.0f, 0.0f, dev);
	colorWeight[0].x = weight;
	colorWeight[0].y = weight;
	colorWeight[0].z = weight;
	colorWeight[0].w = 1.0f;
	
	texCoordOffset[0] = 0.0f;
	
	for(i = 1; i < 8; ++i)
	{
		weight = mul * getGaussianDistribution((float)i, 0.0f, dev);
		texCoordOffset[i] = i * tu;
		colorWeight[i].x = weight;
		colorWeight[i].y = weight;
		colorWeight[i].z = weight;
		colorWeight[i].w = 1.0f;
	}	

	for(i = 8; i < 15; ++i)
	{
		colorWeight[i].set(colorWeight[i - 7]);
		texCoordOffset[i] = -(texCoordOffset[i - 7]);
	}
}

template <class T>
static void createVertexNormals(const vec3<T>* verts, 
								const uint32_t& nVerts, 
								const uint32_t* polys, 
								const uint32_t& nPolys, 
								float* norms)
{
	uint32_t* tmp;
	vec3<float> a, b, c, tmpNorm;
	vec3<float> tmpList[64];
	int32_t counter;
	
	// iterate through every vertex //
	for(uint32_t j = 0; j < nVerts; ++j)
	{
		counter = 0;
		
		// check every poly for this vert //
		for(uint32_t v = 0; v < nPolys; ++v)
		{
			tmp = (uint32_t*)&polys[v * 3];
			if(*tmp != j)
			{
				++tmp;
				if(*tmp != j)
				{
					++tmp;
					if(*tmp != j)
						continue;
				}
			}
			
			// if we're here, this vert is in the tri, so fetch verts //
			uint32_t i1, i2, i3;
			i1 = polys[v * 3];
			i2 = polys[v * 3 + 1];
			i3 = polys[v * 3 + 2];
			
			a.x = verts[i1].x; 
			a.y = verts[i1].y;
			a.z = verts[i1].z;
			b.x = verts[i2].x;
			b.y = verts[i2].y;
			b.z = verts[i2].z;
			c.x = verts[i3].x;
			c.y = verts[i3].y;
			c.z = verts[i3].z;
			
			vec3<float> ab, ac;
			ac = a - c;
			ab = a - b;
			tmpNorm = ac.crossProd(ab); 
			tmpNorm.normalize();

			// check through the buffer list for this vert and look for dupes //
			for(int32_t z = 0; z < counter; ++z)
			{
				if(tmpNorm == tmpList[z])
					goto end;
			}
			
			// add to buffer list //
			tmpList[counter].x = tmpNorm.x;
			tmpList[counter].y = tmpNorm.y;
			tmpList[counter].z = tmpNorm.z;
			
			++counter;
			end:;
		}
		
		tmpNorm.x = 0.0F;
		tmpNorm.y = 0.0F;
		tmpNorm.z = 0.0F;
		for(int32_t q = 0; q < counter; ++q)
		{
			tmpNorm.x += tmpList[q].x;
			tmpNorm.y += tmpList[q].y;
			tmpNorm.z += tmpList[q].z;
		}
		
		// copy to norm list //
		tmpNorm.normalize();
		norms[j * 3] = tmpNorm.x;
		norms[j * 3 + 1] = tmpNorm.y;
		norms[j * 3 + 2] = tmpNorm.z;
	}
}


// Perform fast fourier transofrm on array of complex values "x" 
static void fft(std::valarray<std::complex<double>>& x)
{
	const size_t N = x.size();
	if(N <= 1)
		return;

	std::valarray<std::complex<double>> even = x[std::slice(0, N/2, 2)];
	std::valarray<std::complex<double>> odd  = x[std::slice(1, N/2, 2)];

	fft(even);
	fft(odd);

	for(size_t k = 0; k < N / 2; ++k)
	{
		std::complex<double> t = std::polar(1.0, -2.0 * PI * k / N) * odd[k];
		x[k]		 = even[k] + t;
		x[k + N / 2] = even[k] - t;
	}
}

static inline void inverseFFT(std::valarray<std::complex<double>> & x)
{
	x = x.apply(std::conj);
	fft(x);
	x = x.apply(std::conj);

	double sz = static_cast<double>(x.size());
	x /= std::complex<double>(sz, sz);
}

template <class T>
static vec2<T> rectilinearToSpherical(vec3<T> rect, T r)
{
	vec2<float> ret(atan(rect.y / rect.x), acos(rect.z / r));
	return ret;
}

template <class T>
static void loadRHViewFromPose(mat3<T> pose, vec3<T> camPos, mat4<T>& output)
{
	output[0] = pose.m[0];        output[4] = pose.m[3];    output[8] = pose.m[6];
	output[1] = pose.m[1];        output[5] = pose.m[4];    output[9] = pose.m[7];
	output[2] = pose.m[2];        output[6] = pose.m[5];    output[10] = pose.m[8];
	output[3] = 0.0F;			  output[7] = 0.0F;			output[11] = 0.0F;

	vec3 ne = { -camPos.x, -camPos.y, -camPos.z };
	vec3 x = { pose.m[0], pose.m[3], pose.m[6] };
	vec3 y = { pose.m[1], pose.m[4], pose.m[7] };
	vec3 z = { pose.m[2], pose.m[5], pose.m[8] };

	output.m[12] = ne.dotProd(x); 
	output.m[13] = ne.dotProd(y); 
	output.m[14] = ne.dotProd(z); 
	output.m[15] = 1.0F;
}

template <class T>
static void loadRHView(vec3<T> pos, vec3<T> lookAt, vec3<T> up, mat4<T>& output)
{
	// Find normalized view vector //
	vec3<T> z, x, y;
	z = lookAt - pos;
	z.normalize();

	x = z.crossProd(up);
	x.normalize();

	// Cam y then becomes already normalized X X Z //
	y = x.crossProd(z);

	z *= -1.0;
	vec3<T> ne(-pos.x, -pos.y, -pos.z);

	output.m[0] = x.x;        output.m[4] = x.y;    output.m[8] = x.z;        output.m[12] = ne.dotProd(x);//vec3_dot(ne, x);
	output.m[1] = y.x;        output.m[5] = y.y;    output.m[9] = y.z;        output.m[13] = ne.dotProd(y);//vec3_dot(ne, y);
	output.m[2] = z.x;        output.m[6] = z.y;    output.m[10] = z.z;       output.m[14] = ne.dotProd(z);//vec3_dot(ne, z);
	output.m[3] = 0.0F;        output.m[7] = 0.0F;    output.m[11] = 0.0F;       output.m[15] = 1.0F;
}

template <class T>
static void loadRHPerspective(T fovy, T aspect, T nearClip, T farClip, mat4<T>& output)
{
	T iTan = 1.0F / tanf(fovy * 0.5F);
	T dNF = (nearClip - farClip);

	output.m[0] = iTan / aspect;     output.m[4] = 0.0F;      output.m[8] = 0.0F;
	output.m[1] = 0.0F;              output.m[5] = iTan;      output.m[9] = 0.0F;
	output.m[2] = 0.0F;              output.m[6] = 0.0F;      output.m[10] = (farClip + nearClip) / dNF;
	output.m[3] = 0.0F;              output.m[7] = 0.0F;      output.m[11] = -1.0F;

	output.m[12] = 0.0F;
	output.m[13] = 0.0F;
	output.m[14] = (2.0F* farClip * nearClip) / dNF;
	output.m[15] = 0.0F;

}

template <class T>
static void loadRHOrthographic(T left, T right, T bottom, T top, T zNear, T zFar, mat4<T>& output)
{
	output.m[0] = 2.0f / (right - left);	output.m[4] = 0.0f;						output.m[8] = 0.0f;
	output.m[1] = 0.0f;						output.m[5] = 2.0f / (top - bottom);	output.m[9] = 0.0f;
	output.m[2] = 0.0f;						output.m[6] = 0.0f;						output.m[10] = -2.0f / (zFar - zNear);
	output.m[3] = 0.0f;						output.m[7] = 0.0f;						output.m[11] = 0.0f;

	output.m[12] = -(right + left) / (right - left);
	output.m[13] = -(top + bottom) / (top - bottom);
	output.m[14] = -(zFar + zNear) / (zFar - zNear);
	output.m[15] = 1.0f;
}

template <class T>
static vec4<T> toQuaternion(const mat4<T>& m)
{
	T w = sqrt((T)1.0 + m.m[0] + m.m[5] + m.m[10]) / (T)2.0;
	T w4 = (4.0 * w);
	T x = (m.m[6] - m.m[9]) / w4;
	T y = (m.m[8] - m.m[2]) / w4;
	T z = (m.m[1] - m.m[4]) / w4;

	return vec4<T>(x, y, z, w);
}

template <class T>
static mat4<T> quatToMatrix(const vec4<T>& q)
{
	mat4<T> m;

	T sqw = q.w * q.w;
	T sqx = q.x * q.x;
	T sqy = q.y * q.y;
	T sqz = q.z * q.z;

	T inv = (T)1.0 / (sqx + sqy + sqz + sqw);
	m.m[0] = (sqx - sqy - sqz + sqw) * inv;
	m.m[5] = (-sqx + sqy - sqz + sqw) * inv;
	m.m[10] = (-sqx - sqy + sqz + sqw) * inv;

	T tmp1 = q.x * q.y;
	T tmp2 = q.z * q.w;
	m.m[1] = (T)2.0 * (tmp1 - tmp2) * inv;
	m.m[8] = (T)2.0 * (tmp1 + tmp2) * inv;

	tmp1 = q.y * q.z;
	tmp2 = q.x * q.w;
	m.m[6] = (T)2.0 * (tmp1 + tmp2) * inv;
	m.m[9] = (T)2.0 * (tmp1 - tmp2) * inv;

	m.m[3] = (T)0.0;
	m.m[7] = (T)0.0;
	m.m[11] = (T)0.0;
	m.m[12] = (T)0.0;
	m.m[13] = (T)0.0;
	m.m[14] = (T)0.0;
	m.m[15] = (T)0.0;

	return m;
}

#endif // qmath_h__
