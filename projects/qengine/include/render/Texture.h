#ifndef __qtexture_h_
#define __qtexture_h_

#include <stdint.h>
#include "EngineExport.h"
#include "Extensions.h"



#define SAMPLE_NEAREST				0x00000001
#define SAMPLE_LINEAR				0x00000002
#define SAMPLE_BILINEAR				0x00000004
#define SAMPLE_TRILINEAR			0x00000008
#define SAMPLE_BILINEAR_ANISO		0x00000010
#define SAMPLE_TRILINEAR_ANISO		0x00000020

#define SAMPLE_WRAP					0x00000040
#define SAMPLE_CLAMP				0x00000080


#define MSAA_2_SAMPLES				0x00000100
#define MSAA_4_SAMPLES				0x00000200
#define MSAA_8_SAMPLES				0x00000400
#define MSAA_16_SAMPLES				0x00000800
#define CSAA_8_SAMPLES_NV			0x00001000
#define CSAA_16_SAMPLES_NV			0x00002000
#define CSAA_8Q_SAMPLES_NV			0x00004000
#define CSAA_16Q_SAMPLES_NV			0x00008000

#define TEXTURE_WRITE_ONLY			0x00010000
#define TEXTURE_READ_WRITE			0x00020000


#define QTEXTURE_ALL_MIPMAPS		127


#define QTEXTURE_FILTER_NEAREST  0x00000001
#define QTEXTURE_FILTER_LINEAR  0x00000002
#define QTEXTURE_FILTER_BILINEAR  0x00000004
#define QTEXTURE_FILTER_TRILINEAR  0x00000008
#define QTEXTURE_FILTER_BILINEAR_ANISO  0x00000010
#define QTEXTURE_FILTER_TRILINEAR_ANISO  0x00000020

#define QTEXTURE_CLAMP_S  0x00000040
#define QTEXTURE_CLAMP_T  0x00000080
#define QTEXTURE_CLAMP_R  0x00000100
#define QTEXTURE_WRAP_S  0x00000200
#define QTEXTURE_WRAP_T  0x00000400
#define QTEXTURE_WRAP_R  0x00000800
#define QTEXTURE_CLAMP (QTEXTURE_CLAMP_S | QTEXTURE_CLAMP_T | QTEXTURE_CLAMP_R)
#define QTEXTURE_WRAP  (QTEXTURE_WRAP_S | QTEXTURE_WRAP_T | QTEXTURE_WRAP_R)
#define QTEXTURE_CUBEMAP  0x00001000
#define QTEXTURE_NORMALMAP  0x00002000
#define QTEXTURE_KEEPHEIGHT  0x00004000
#define QTEXTURE_NORMALHEIGHTMAP  (QTEXTURE_NORMALMAP | QTEXTURE_KEEPHEIGHT)
#define QTEXTURE_FILTER_REPEAT_S 0x00008000
#define QTEXTURE_FILTER_REPEAT_T 0x00010000
#define QTEXTURE_FILTER_REPEAT_R 0x00020000
#define QTEXTURE_FILTER_REPEAT (QTEXTURE_REPEAT_S	 \
								| QTEXTURE_REPEAT_T  \
								| QTEXTURE_REPEAT_R)


#define QTEXTURE_CUBEMAPFACE_POSITIVE_X  0x00040000
#define QTEXTURE_CUBEMAPFACE_NEGATIVE_X  0x00080000
#define QTEXTURE_CUBEMAPFACE_POSITIVE_Y  0x00100000
#define QTEXTURE_CUBEMAPFACE_NEGATIVE_Y  0x00200000
#define QTEXTURE_CUBEMAPFACE_POSITIVE_Z  0x00400000
#define QTEXTURE_CUBEMAPFACE_NEGATIVE_Z  0x00800000
#define QTEXTURE_CUBEMAP				 0x01000000


#define QTEXTURE_DYNAMIC  0x00010000
#define QTEXTURE_STATIC  0x00020000

#define QDEPTHTARGET_LOCKABLE  0x00040000

#define QTEXTURE_FBO_ALL_TARGETS 0x10000000


// Pixel format descriptor //
// TODO: Synchronize this with the internal opengl formats in qiktexture.c
enum ETexturePixelFormat
{
	QTEXTURE_FORMAT_NONE = 0,
	QTEXTURE_FORMAT_I8 = 1,
	QTEXTURE_FORMAT_IA8 = 2,
	QTEXTURE_FORMAT_RGB8 = 3,
	QTEXTURE_FORMAT_RGBA8 = 4,
	QTEXTURE_FORMAT_I16 = 5,
	QTEXTURE_FORMAT_IA16 = 6,
	QTEXTURE_FORMAT_RGB16 = 7,
	QTEXTURE_FORMAT_RGBA16 = 8,
	QTEXTURE_FORMAT_I16F = 9,
	QTEXTURE_FORMAT_IA16F = 10,
	QTEXTURE_FORMAT_RGB16F = 11,
	QTEXTURE_FORMAT_RGBA16F = 12,
	QTEXTURE_FORMAT_I32F = 13,
	QTEXTURE_FORMAT_IA32F = 14,
	QTEXTURE_FORMAT_RGB32F = 15,
	QTEXTURE_FORMAT_RGBA32F = 16,
	QTEXTURE_FORMAT_DEPTH16 = 17,
	QTEXTURE_FORMAT_DEPTH24 = 18,
	QTEXTURE_FORMAT_RGB332 = 19,
	QTEXTURE_FORMAT_DXT1 = 20,
	QTEXTURE_FORMAT_DXT3 = 21,
	QTEXTURE_FORMAT_DXT5 = 22,
	QTEXTURE_FORMAT_ATI2N = 23,
	QTEXTURE_FORMAT_ATI1N = 24,
	QTEXTURE_FORMAT_RGB10_A2 = 25,
	QTEXTURE_FORMAT_RGB10_A2UI = 26,
	QTEXTURE_FORMAT_SRGB8 = 27,
	QTEXTURE_FORMAT_SRGBA8 = 28,
	QTEXTURE_FORMAT_RG32F = 29,
	QTEXTURE_FORMAT_R8	  = 30,
	QTEXTURE_FORMAT_R16		= 31,
	QTEXTURE_FORMAT_BGRA	= 32,
	QTEXTURE_FORMAT_R32F	= 33,
	QTEXTURE_FORMAT_RG8		= 34,
	QTEXTURE_FORMAT_RG16F	= 35,
};



struct DepthStencilId
{
	GLuint depthId;
	GLuint stencilId;
};

struct FboId
{
	unsigned int width;
	unsigned int height;
	GLuint  id;
	int		nColorTargets;
	bool	clearTarget;
	bool	isCubemap = false;
};

struct ShadowmapId
{
	uint32_t width;
	uint32_t height;
	GLuint fboId;
	GLuint texId;
};

struct MultiShadowmapId
{
	uint32_t	width;
	uint32_t	height;
	uint32_t	nShadowMaps;
	GLuint		fboId;
	GLuint		textureIds[4];
};

static inline bool IS_FBO_VALID(const FboId aFbo)
{
	return (aFbo.id > 0) ? true : false;
}


struct TextureInfo
{
	unsigned int	width;
	unsigned int	height;
	unsigned int	depth;
	unsigned int	flags;
	int				isCubemap;
	int				cubemapFace;
	int				isS3TC;
	unsigned int	size;			// Size in bytes
	unsigned int	colorSamples;
	unsigned int	coverageSamples;
	unsigned char   pixelFormat;
};

static inline int hasMipmapFlag(const unsigned char aFlags)
{
	if ((aFlags & SAMPLE_BILINEAR) 
		|| (aFlags & SAMPLE_TRILINEAR) 
		|| (aFlags & SAMPLE_BILINEAR_ANISO) 
		|| (aFlags & SAMPLE_TRILINEAR_ANISO))
		return 1;

	return 0;
}



QENGINE_API GLuint addTexture(TextureInfo aTexInfo, void** aData);

QENGINE_API GLuint addRenderTarget(uint32_t aWidth, 
								   uint32_t aHeight, 
								   uint8_t aFormat, 
								   uint32_t aImgFlags);

QENGINE_API struct DepthStencilId addDepthStencilTarget(uint32_t aWidth, 
														  uint32_t aHeight, 
														  uint32_t aDepthBits, 
														  uint32_t aStencilBits, 
														  uint32_t aImgFlags);

QENGINE_API struct FboId createRenderableSurface(GLuint* aColorTargets, 
												  int32_t aNColorTargets, 
												  DepthStencilId aDs, 
												  uint32_t aWidth, 
												  uint32_t aHeight);

QENGINE_API struct FboId createRenderableCubemap(GLuint aCubemapTexture,
											     DepthStencilId aDs,
												 uint32_t aWidth,
												 uint32_t aHeight);

QENGINE_API GLuint createRenderableDepthSurface(GLuint aDepthTarget, 
												uint32_t aWidth, 
												uint32_t aHeight);

QENGINE_API ShadowmapId createShadowmapSurface(uint32_t aDepthBits, 
												uint32_t aWidth, 
												uint32_t aHeight, 
												uint32_t aFlags);

QENGINE_API MultiShadowmapId createMultiShadowmapSurface(uint32_t aDepthBits,
														 uint32_t aWidth,
														 uint32_t aHeight,
														 uint32_t aFlags,
														 uint32_t aNumMaps);

QENGINE_API void updateTextureDirtyRegion(GLuint aId, 
										  const int32_t aXOffset, 
										  const int32_t aYOffset, 
										  const int32_t aWidth, 
										  const int32_t aHeight, 
										  const uint8_t aFmt, 
										  const int32_t aGenMips, 
										  const void* aBuf);

QENGINE_API void deleteRenderTarget(GLuint aId);
QENGINE_API void deleteTexture(GLuint aId);
QENGINE_API void deleteDepthStencilTarget(DepthStencilId aObj);
QENGINE_API void deleteRenderableSurface(FboId aObj);

QENGINE_API void bindRenderableSurface(FboId aFbo);
QENGINE_API void bindRenderableCubemap(FboId aFbo);
QENGINE_API void bindShadowmapSurface(ShadowmapId aFbo);
QENGINE_API void bindMultiShadowmapSurface(MultiShadowmapId aFbo, uint32_t aBindTexture = QTEXTURE_FBO_ALL_TARGETS);
QENGINE_API void bindDefaultSurface();
QENGINE_API void bindTexture(GLuint aId, unsigned int aTexUnit);
QENGINE_API void bindRenderableDepthSurface(GLuint aFbo);


// TEXTURE generation //
QENGINE_API GLuint generateRoughnessLookup(unsigned int aDim);




#endif