#ifndef core_vertexarray_h__
#define core_vertexarray_h__

#include <cstdint>
#include <vector>
#include "EngineExport.h"

class VertexBuffer;
class VertexBufferLayout;
class IndexBuffer;

class VertexArrayImpl;
class QENGINE_API VertexArray
{
	public:
		VertexArray();
		VertexArray(VertexBuffer *aVertexBuffer, IndexBuffer *aIndexBuffer);
		~VertexArray();

		uint32_t getID() const { return mId; }

		void addVertexBuffer(VertexBuffer *aBuffer) const;
		void addIndexBuffer(IndexBuffer *aBuffer) const;

		void bind() const;
		void unbind() const;

		VertexBuffer* getVertexBuffer(const uint32_t aIndex = 0) const;
		IndexBuffer* getIndexBuffer(const uint32_t aIndex = 0) const;

	private:
		uint32_t mId;

		VertexArrayImpl* mImpl;

		void _applyLayout(const VertexBufferLayout &aLayout) const;
};

#endif // core_vertexarray_h__