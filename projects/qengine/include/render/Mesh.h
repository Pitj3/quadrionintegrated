#ifndef mesh_h__
#define mesh_h__

#include "render/VertexArray.h"
#include "render/IndexBuffer.h"
#include "QMath.h"
#include "EngineExport.h"

struct Vertex
{
	vec3<float> position;
	vec2<float> uv;
	vec3<float> normal;
	vec3<float> tangent;
	vec3<float> binormal;
};

struct MeshInstance
{
	vec4<float> position;
};

class QENGINE_API Mesh
{
	public:
		Mesh(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer);
		Mesh(VertexArray* aVertexArray, IndexBuffer* aIndexBuffer,
		     const MeshInstance* aMeshInstances, uint32_t aNumMeshInstances);
		~Mesh();

		VertexArray* const getVAO() { return mVAO; }
		IndexBuffer* const getIBO() { return mIBO; }

		void bind() const;
		void unbind() const;

		uint32_t getVertexCount() const;
		uint32_t getIndexCount() const;

		uint32_t		getNumMeshInstances() const;
		uint32_t	getInstanceTextureHandle() const;

	private:

		uint32_t	mNumMeshInstances;
		uint32_t		mInstanceBufferHandle;
		uint32_t		mInstanceTextureHandle;

		VertexArray* mVAO;
		IndexBuffer* mIBO;
};

#endif // mesh_h__