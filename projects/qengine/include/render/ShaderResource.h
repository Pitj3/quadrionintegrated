#ifndef shaderresource_h__
#define shaderresource_h__

#include <cstdint>
#include <string>
#include "EngineExport.h"

class QENGINE_API ShaderResourceDeclaration
{
	public:
		enum class Type
		{
			NONE, TEXTURE2D, TEXTURECUBE, TEXTURESHADOW, TEXTUREBUFFER
		};

		ShaderResourceDeclaration(const Type aType, const std::string& aName, uint32_t aCount);

		inline const char* getName() const { return mName; }
		inline uint32_t getRegister() const { return mRegister; }
		inline uint32_t getCount() const { return mCount; }

		inline Type getType() const { return mType; }

		static Type stringToType(const std::string& aType);
		static std::string typeToString(const Type aType);

	private:
		friend class Shader;

		char* mName;
		uint32_t mRegister;
		uint32_t mCount;
		Type mType;
};

#endif // shaderresource_h__