#ifndef __stencil_h_
#define __stencil_h_

#include "Extensions.h"
#include "EngineExport.h"

// Stencil Ops
#define QSTENCIL_KEEP				GL_KEEP
#define QSTENCIL_ZERO				GL_ZERO
#define QSTENCIL_REPLACE			GL_REPLACE
#define QSTENCIL_INCREMENT			GL_INCR
#define QSTENCIL_INCREMENT_WRAP		GL_INCR_WRAP
#define QSTENCIL_DECREMENT			GL_DECR
#define QSTENCIL_DECREMENT_WRAP		GL_DECR_WRAP
#define QSTENCIL_INVERT				GL_INVERT

// Stencil Funcs
#define QSTENCIL_NEVER				GL_NEVER
#define QSTENCIL_LESS				GL_LESS
#define QSTENCIL_LEQUAL				LEQUAL
#define QSTENCIL_GREATER			GL_GREATER
#define QSTENCIL_GEQUAL				GL_GEQUAL
#define QSTENCIL_EQUAL				GL_EQUAL
#define QSTENCIL_NOTEQUAL			GL_NOTEQUAL
#define QSTENCIL_ALWAYS				GL_ALWAYS

#endif // __stencil_h_