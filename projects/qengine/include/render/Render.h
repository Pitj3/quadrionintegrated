#ifndef QRENDER_H__
#define QRENDER_H__

#include <map>
#include "EngineExport.h"
#include "Extensions.h"
#include "QMath.h"
#include "Shader.h"
#include "core/qsmartptr.h"
#include "render/RenderObject.h"


enum TextureFilter : int
{
	NEAREST				= 1,
	LINEAR				= 2,
	BILINEAR			= 4,
	TRILINEAR			= 8,
	BILINEAR_ANISO		= 16,
	TRILINEAR_ANISO		= 32,

	REPEAT				= 64,
	CLAMP				= 128,
};

enum PrimitiveType
{
	GLPOINTS			= 0x0000,
	LINES				= 0x0001,
	LINE_LOOP			= 0x0002,
	LINE_STRIP			= 0x0003,
	TRIANGLES			= 0x0004,
	TRIANGLE_STRIP		= 0x0005,
	TRIANGLE_FAN		= 0x0006,
	QUADS				= 0x0007,
};

enum IndexSize
{
	UNSIGNED_SHORT		= 0x1403,
	UNSIGNED_INT		= 0x1405,
};

// Single point of contact if render handle passing must change //
struct RenderHandle
{
	GLuint	handle;
};

struct GeomData
{
	uint32_t nVertices;
	uint32_t nIndices;

	float* vertices;
	float* uvs;
	uint32_t* indices;
};

struct TriangleIndices
{
	uint32_t v1;
	uint32_t v2;
	uint32_t v3;

	TriangleIndices(const uint32_t aV1, const uint32_t aV2, const uint32_t aV3)
	{
		this->v1 = aV1;
		this->v2 = aV2;
		this->v3 = aV3;
	}
};


class QENGINE_API RenderImpl;

class QENGINE_API Render
{
	public:

		Render();
		~Render();

		// initialization Functions //
		static void initialize();

		static void setPreferredTextureFilter(TextureFilter aFilter);


		// Query Functions //
		static vec3<float> getWorldPosFromScreenspace(const int32_t aMouseX,
													  const int32_t aMouseY);

		static RenderHandle getFullscreenTexturedGeom();

		static TextureFilter getPreferredTextureFilter();

		static std::shared_ptr<RenderObject> getFullscreenRenderObj();

		static const Shader* const getTexturedEffect();

		static const Shader* const getTexturedOITEffect();

		// Render functions //
//		static void renderFullscreenTexturedQuad(bool aInvertV, uint16_t aTexId);

		static void renderFullscreenTexturedQuad();

		static void renderFullscreenQuad(const TextureRect& aRect,
									     const uint32_t aW,
										 const uint32_t aH);
		
//		static void renderFullscreenQuad(const float aR, const float aG,
//										 const float aB, const float aA);

		static void renderFullscreenQuad();
		static void renderFullscreenQuadNew();

//		static void renderLine(const vec3<float>& aA,
//							   const vec3<float>& aB,
//							   const vec4<float>& aColor);

		static void renderIndexedList(const PrimitiveType aPrimitiveType,
									  const uint32_t aNumIndices,
									  const IndexSize aIndexize,
									  const uint32_t aDataOffset);

		static void renderFullscreenFrustumQuad(float aFOV,
												float aFarClipDist,
												float aAspectRatio);

//		static void renderMeshAsset(QSmartPtr<MeshAsset> aMeshAsset,
//									Shader* aShader,
//									const uint32_t nInstances);

//		static void renderMeshAsset(const MeshAsset* aMeshAsset,
//								    Shader* aShader,
//									const uint32_t nInstances);

		// Geometry generation //
		static GeomData getStarBillboardGeometry(const float aWidth,
												 const float aHeight);

		static GeomData getQuadGeometry(const float aWidth,
										const float aHeight);

		static GeomData getFlatQuadGeometry(const float aWidth,
											const float aHeight);

		static GeomData getCubeGeometry(const float aWidth,
										const float aHeight,
										const float aDepth);

	private:

		static RenderImpl* mImpl;
};

#endif