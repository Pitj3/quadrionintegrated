
#ifndef __ICOSPHERECREATOR_H_
#define __ICOSPHERECREATOR_H_

#include "EngineExport.h"
#include "Render.h"

class QENGINE_API IcoSphereCreatorImpl;


class QENGINE_API IcoSphereCreator
{
	public:

		IcoSphereCreator();
		~IcoSphereCreator();

		void create(const int aRecursion, const float aRadius);

		const GeomData* const getData();

	private:

		IcoSphereCreatorImpl* mImpl;
};



#endif