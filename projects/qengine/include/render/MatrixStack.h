#ifndef QMATRIXSTACK_H__
#define QMATRIXSTACK_H__

#include "EngineExport.h"
#include "QMath.h"

struct MatrixStackImpl;

class QENGINE_API MatrixStack
{
	public:

		MatrixStack();
		~MatrixStack();

		void pushMatrix(const mat4<float>& aMat) const;
		mat4<float> popMatrix() const;
		void multiplyMatrix(const mat4<float>& aMat) const;
		mat4<float> top() const;
private:

		MatrixStackImpl* mImpl = nullptr;
};



#endif