#ifndef technique_h__
#define technique_h__

#include <map>
#include <memory>
#include "EngineExport.h"
#include "Pass.h"

class Effect;
class TechniqueImpl;
class Material;

class QENGINE_API Technique
{
public:
	friend class Effect;
	friend class EffectImpl;

	Technique(const std::string& aName, Effect* aEffect);
	//		Technique(const std::string& aName, 
	//					 Effect* aEffect);
	~Technique();

	size_t getNumPasses() const;
	Pass** getPasses() const;
	Pass* getPass(const char* aName) const;
	void addPass(Pass* pass);

	void addUBO(const std::string& name, std::vector<UniformBufferElement*> ubo);
	void addMaterial(const std::string& aName, Material* const aMaterial);
	Material* const getMaterial(const std::string& aName);

	std::map<std::string, std::vector<UniformBufferElement*>> getUBOs() const;
	std::vector<UniformBufferElement*> getUBO(const std::string& name) const;
	std::vector<std::string> getUBONames() const;
	bool hasUBO(const std::string& name) const;

	std::string& getName() const;

	Effect* getEffect() const;

private:
	TechniqueImpl* mImpl;

	void _setName(const std::string& aName) const;
	void _setSource(const std::string& aSource) const;

	void _addPass(Pass* aPass) const;
};

#endif // techniquerod_h__