#ifndef effectuniform_h__
#define effectuniform_h__

#include <string>
#include "render/effectparser/EffectVariableType.h"

struct EffectUniform
{
	EffectVariableType type;
	std::string uniformName;
	std::string defaultValue;
	std::string shader;
	std::string guiDebugElement;
	std::string guiDefaultValue;
	float min, max;
};

#endif // effectuniform_h__