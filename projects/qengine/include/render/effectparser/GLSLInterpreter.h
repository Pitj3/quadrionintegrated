#ifndef glslinterpreter_h__
#define glslinterpreter_h__

#include <vector>
#include <string>
#include <unordered_map>
#include <memory>
#include "render/effectparser/ParseNode.h"
#include "render/Effect.h"
#include "EngineExport.h"

// TODO (Roderick): Move this
struct RasterizerOption
{
	std::string option;
	std::vector<std::string> operands;
};

struct SamplerState
{
	std::string sampler;
	std::vector<std::string> operands;
};

class QENGINE_API GLSLInterpreter
{
    public:
		std::shared_ptr<Effect> construct(IParseNode* root, const std::string& aEffectSource = "", const std::string& aEffectName = "");

    private:
		void _interpretNode(IParseNode* node);
    
		std::shared_ptr<Effect> _createShaders(const std::string& aSource, std::shared_ptr<Effect> aEffect);
		std::string _getLayoutString(const std::string& shader);
		std::string _getInterfaceString(const std::string& interfaceName, const std::string& interfaceSpecifier, const bool includeLocation = false,
			const bool includeArray = false, const bool stripArray = false);
		EffectMainFunction _getMainFunction(const std::string& name);
		std::string _getVariables(const std::string& shader);
		std::string _getUniforms(const std::string& shader);
		std::string _getUniformBlocks(const std::string& shader);
		std::string _getFunctions(const std::string& shader);
		std::string _getMainFunctionString(const std::string& shader, const std::string& name);

		std::string _name;
		EffectVersion _version;
		std::vector<EffectExtension> _extensions;

		std::vector<EffectInclude> _includes;

		std::vector<EffectLayout> _layouts;

		std::vector<EffectInterface> _interfaces;

		std::vector<EffectVariable> _variables;
		std::vector<EffectUniform> _uniforms;

		std::vector<EffectUniformBlock> _uniformBlocks;
		std::vector<EffectUniformBlock> _bufferBlocks;

		std::vector<EffectFunction> _functions;
		std::vector<EffectMainFunction> _mainFunctions;

		std::vector<RasterizerOption> _rasterizerOptions;
		std::vector<SamplerState> _samplerStates;

		std::vector<EffectTechnique> _techniques;
		std::unordered_map<EffectTechnique, std::vector<EffectPass>> _passes;

//		QHandle<QEntityManager> _entityManager;

		std::vector<std::string> samplerNames;
		std::unordered_map<std::string, std::pair<EffectVariableType, std::string>> samplerLoadPaths;

		std::vector<OutputBufferInfo*> _outputbuffers;
		std::vector<EffectOutputBuffer> _outputbufferDescs;
		vec4<float> clearColor;
		uint32_t clearMode;

		std::string _effectSourceCopy;

		std::map<std::string, std::string> mUniformDebugElements;
		std::map<std::string, std::pair<float, float>> mUniformDebugData;
		std::map<std::string, float> mGUIDefaults;
};

#endif // glslinterpreter_h__