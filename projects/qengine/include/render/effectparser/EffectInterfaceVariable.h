#ifndef effectinterfacevariable_h__
#define effectinterfacevariable_h__

#include <string>
#include <vector>

#include "render/effectparser/EffectTypeQualifier.h"
#include "render/effectparser/EffectVariableType.h"
#include "render/effectparser/EffectSemantics.h"

struct EffectInterfaceVariable
{
	std::vector<EffectTypeQualifier> typeQualifiers;
	EffectVariableType type;
	std::string variableName;
	EffectSemantic location;
};

#endif // effectinterfacevariable_h__