#ifndef effectfunction_h__
#define effectfunction_h__

#include <string>
#include <vector>

#include "render/effectparser/EffectVariableType.h"
#include "render/effectparser/EffectTypeQualifier.h"
#include "render/effectparser/EffectVariable.h"
#include "render/effectparser/EffectShaderType.h"

struct EffectFunction
{
	std::vector<EffectTypeQualifier> typeQualifiers;
    EffectVariableType type;
	std::string name;

	std::vector<EffectVariable> parameters;

	std::vector<std::string> shaders;

	std::string block;
};

struct EffectMainFunction
{
	EffectVariableType type;
	std::string name;

	std::vector<EffectMainVariable> parameters;

	std::string shader;

	std::string block;
};

#endif // effectfunction_h__