#ifndef effectvariable_h__
#define effectvariable_h__

#include <string>
#include <vector>
#include "render/effectparser/EffectTypeQualifier.h"
#include "render/effectparser/EffectVariableType.h"
#include "render/effectparser/EffectInterfaceSpecifier.h"

struct EffectVariable
{
	std::vector<EffectTypeQualifier> typeQualifiers;
	EffectVariableType type;
	std::string variableName;
	std::string defaultValue;
	std::string shader;
};

struct EffectMainVariable
{
	EffectInterfaceSpecifier specifier;
	std::string name;
};

#endif // effectvariable_h__