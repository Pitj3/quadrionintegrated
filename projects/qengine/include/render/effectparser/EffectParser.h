#ifndef effectparser_h__
#define effectparser_h__

#include <string>
#include "render/effectparser/ParseNode.h"
#include "EngineExport.h"

class QENGINE_API EffectParser
{
    public:
	    IParseNode* parse(std::string& str) const;
    private:
	    void _parseRoot(std::string& str, IParseNode* node) const;
};

QENGINE_API std::vector<std::string> splitOnAny(std::string& str, const char* regex);

#endif // effectparser_h__