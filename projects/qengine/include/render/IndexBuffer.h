#ifndef indexbuffer_h__
#define indexbuffer_h__

#include <cstdint>
#include "EngineExport.h"

class QENGINE_API IndexBuffer
{
	public:
		IndexBuffer(uint32_t* aData, const uint32_t aCount, 
					const uint32_t aPrimType);

		IndexBuffer(void* aData, const uint32_t aSizeInBytes);
		~IndexBuffer();

		void bind() const;
		void unbind() const;

		uint32_t getCount() const { return mCount; }
		uint32_t getPrimType() const { return mPrimType; }

	private:

		uint32_t mPrimType;
		uint32_t mId;
		uint32_t mCount;
};

#endif // indexbuffer_h__