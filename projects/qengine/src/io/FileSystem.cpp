#include "io/FileSystem.h"

#include <ostream>
#include <fstream>
#include <sstream>

#if defined(_WIN32) || defined(_WIN64)
	#include <filesystem>
#else
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <dirent.h>
#endif


#include "core/QLog.h"

std::string FileSystem::readFile(const std::string& path)
{
	std::string data = "";

	std::ifstream dataStream(path.c_str(), std::ios::in);
	if (dataStream.is_open())
	{
		std::stringstream stringStream;
		stringStream << dataStream.rdbuf();
		data = std::string(stringStream.str().c_str());
		dataStream.close();

		return data;
	}

	else
	{
		// File does not exist
		QUADRION_INTERNAL_ERROR("File: {0} does not exist, check if you're in the right path", path);
		return "";
	}
}

/*
std::vector<std::string> FileSystem::getDirectories(const std::string& aRoot)
{
	std::vector<std::string> r;

	#if defined(_WIN32) || defined(_WIN64)
		for (auto& p : std::filesystem::recursive_directory_iterator(aRoot.c_str()))
			if (p.status().type() == std::filesystem::file_type::directory)
				r.push_back(p.path().string().c_str());
	#else	
//		dirent** directories;
//		int res = scandir(aRoot.c_str(), &directories, nullptr, nullptr);
//		if(res >= 0)
//		{
//			while(res--)
//			{
//				std::string entry = directories[res]->d_name;
//				r.push_back(entry);
//				free(directories[res]);
//			}

//			free(directories);
//		}

	#endif

	return r;
}
*/
#if !defined(_WIN32) || !defined(_WIN64)
void FileSystem::_XGetFiles(const std::string& aRoot, std::vector<std::string>* aFiles)
{
	dirent** directories;
	int res = scandir(aRoot.c_str(), &directories, nullptr, nullptr);
	if(res >= 0)
	{
		while(res--)
		{
			if(strcmp(directories[res]->d_name, ".") == 0 ||
			   strcmp(directories[res]->d_name, "..") == 0)
					continue;

			std::string entry = aRoot + "/" + directories[res]->d_name;
			struct stat fileStatus;
			if(stat(entry.c_str(), &fileStatus) == 0)
			{
				if(!S_ISDIR(fileStatus.st_mode))
					aFiles->push_back(entry);
				else
				{
					_XGetFiles(entry, aFiles);
				}
				
			}
		
			free(directories[res]);
		}
		free(directories);
	}
}
#endif

std::vector<std::string> FileSystem::getFiles(const std::string& aRoot)
{
	std::vector<std::string> r;

	#if defined(_WIN32) || defined(_WIN64)
		WIN32_FIND_DATAA ffd;
		HANDLE hFind;

		hFind = FindFirstFileA(aRoot.c_str(), &ffd);
		if(hFind == INVALID_HANDLE_VALUE)
			return r;

		
		do
		{
			if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				continue;

			else
				r.push_back(ffd.cFileName);
		}while(FindNextFileA(hFind, &ffd) != 0);

		FindClose(hFind);

//		for (auto& p : std::filesystem::recursive_directory_iterator(aRoot.c_str()))
//			if (p.status().type() == std::filesystem::file_type::regular)
//				r.push_back(p.path().string().c_str());
	#else
//		for (auto& p : std::experimental::filesystem::recursive_directory_iterator(aRoot.c_str()))
//			if (p.status().type() == std::experimental::filesystem::file_type::regular)
//				r.push_back(p.path().string().c_str());	

		_XGetFiles(aRoot, &r);	

	#endif
	
	return r;
}

std::vector<std::string> FileSystem::getDirectories(const std::string& aRoot)
{
	std::vector<std::string> r;

	#if defined(_WIN32) || defined(_WIN64)
		WIN32_FIND_DATAA ffd;
		HANDLE hFind;
	
		hFind = FindFirstFileA(aRoot.c_str(), &ffd);
		if (hFind == INVALID_HANDLE_VALUE)
			return r;

		do
		{
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (strcmp(ffd.cFileName, ".") != 0 &&
					strcmp(ffd.cFileName, "..") != 0)
				{
					r.push_back(ffd.cFileName);
				}
			}
	
//			else
//				r.push_back(ffd.cFileName);
		} while (FindNextFileA(hFind, &ffd) != 0);
	
		//		for (auto& p : std::filesystem::recursive_directory_iterator(aRoot.c_str()))
		//			if (p.status().type() == std::filesystem::file_type::regular)
		//				r.push_back(p.path().string().c_str());
	#else
		//		for (auto& p : std::experimental::filesystem::recursive_directory_iterator(aRoot.c_str()))
		//			if (p.status().type() == std::experimental::filesystem::file_type::regular)
		//				r.push_back(p.path().string().c_str());	
	
//		_XGetFiles(aRoot, &r);
	
	#endif

	return r;
}

std::string FileSystem::getFileRecursively(const std::string& aRoot, const std::string& aFileName)
{
	std::string ret = "";

	std::vector<std::string> directoryFiles = getFiles(aRoot);
	std::vector<std::string> directories = getDirectories(aRoot);
	for (uint32_t i = 0; i < directoryFiles.size(); i++)
	{
		if (directoryFiles[i].find(aFileName) != std::string::npos)
		{
			size_t asterisk = aRoot.find_last_of('*');
			if(asterisk != std::string::npos)
			{
				ret = aRoot.substr(0, asterisk);
				ret += directoryFiles[i];
//				ret = aRoot + directoryFiles[i];
			}
			else
				ret = aRoot + directoryFiles[i];
			return ret;
//			break;
		}
	}

	size_t nDirs = directories.size();
	if (nDirs > 0)
	{
		for(size_t i = 0; i < nDirs; i++)
		{
			size_t asterisk = aRoot.find_last_of('*');
			std::string searchDir = aRoot.substr(0, asterisk);
			searchDir += directories[i] + "/*";
			ret = getFileRecursively(searchDir, aFileName);
			if(ret.length() > 0)
				return ret;	
		}
	}

	return ret;
}
