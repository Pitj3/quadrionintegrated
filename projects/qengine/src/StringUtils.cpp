#include "StringUtils.h"

#include <sstream>
#include <locale>
#include <algorithm>
#include <regex>
#include "Extensions.h"

char qtolower(char aA)
{
	return char(aA + 32);
}

bool qstricmp(const char* aA, const char* aB)
{
	size_t lenA = strlen(aA);
	size_t lenB = strlen(aB);
	if (lenA != lenB)
		return false;

	for (size_t i = 0; i < lenA; ++i)
	{
		if (qtolower(aA[i]) != qtolower(aB[i]))
			return false;
	}

	return true;
}

std::vector<std::string> StringUtils::split(const std::string& aString, const char aDelimiter)
{
	std::vector<std::string> tokens;
	std::string token;
	std::istringstream tokenStream(aString.c_str());
	while (std::getline(tokenStream, token, aDelimiter))
	{
		tokens.push_back(token.c_str());
	}

	return tokens;
}

std::vector<std::string> StringUtils::split(const std::string& aString, const std::string& aDelimiter)
{
	std::vector<std::string> tokens;

	size_t begin = 0;
	size_t next = aString.find(aDelimiter, begin);

	if (aString.length() == 0)
	{
		return tokens;
	}

	do
	{
		std::string val = aString.substr(begin, next - begin);

		if (next == begin || val.size() == 0)
		{
			begin = next + 1;
			next = aString.find(aDelimiter, begin + 1);
			continue;
		}

		if (std::string(aDelimiter).find(val[0]) != std::string::npos)
		{
			begin++;
			continue;
		}

		tokens.push_back(val);

		if (next >= aString.length())
		{
			break;
		}

		begin = next + 1;
		next = aString.find(aDelimiter, begin + 1);

	} while (begin < aString.length());

	return tokens;
}

std::vector<std::string> StringUtils::splitWithRegex(const std::string& aString, const std::string& aDelimiters)
{
	std::vector<std::string> result;

	if (aDelimiters == "\\s+")
	{
		std::regex rgx(aDelimiters);

		std::sregex_token_iterator iter(aString.begin(), aString.end(), rgx, -1);
		std::sregex_token_iterator end;

		int i = 0;

		while (iter != end)
		{
			if (i != 0)
				result.push_back(*iter);

			i++;
			++iter;
		}
	}
	else
	{
		size_t start = 0;
		size_t end = aString.find_first_of(aDelimiters);

		while (end <= std::string::npos)
		{
			std::string token = aString.substr(start, end - start);
			if (!token.empty())
				result.push_back(token);

			if (end == std::string::npos)
				break;

			start = end + 1;
			end = aString.find_first_of(aDelimiters, start);
		}
	}

	return result;
}

std::vector<std::string> StringUtils::splitOnAny(const std::string& aString, const std::string& aDelimiters)
{
	std::vector<std::string> tokens;

	size_t begin = 0;
	size_t next = aString.find_first_of(aDelimiters, begin);

	if (aString.length() == 0)
	{
		return tokens;
	}

	do
	{
		std::string val = aString.substr(begin, next - begin);

		if (next == begin || val.size() == 0)
		{
			begin = next + 1;
			next = aString.find_first_of(aDelimiters, begin + 1);
			continue;
		}

		if (aDelimiters.find(val[0]) != std::string::npos)
		{
			begin++;
			continue;
		}

		tokens.push_back(val);

		if (next >= aString.length())
		{
			break;
		}

		begin = next + 1;
		next = aString.find_first_of(aDelimiters, begin + 1);

	} while (begin < aString.length());

	return tokens;
}

std::string StringUtils::reduceTrim(const std::string& str, const std::string& whitespace)
{
	const auto strBegin = str.find_first_not_of(whitespace);
	if (strBegin == std::string::npos)
		return ""; // no content

	const auto strEnd = str.find_last_not_of(whitespace);
	const auto strRange = strEnd - strBegin + 1;

	return str.substr(strBegin, strRange);
}

std::string StringUtils::reduce(const std::string& aStr, const std::string& aFill, const std::string& aWhitespace)
{
	// trim first
	auto result = reduceTrim(aStr, aWhitespace);

	// replace sub ranges
	auto beginSpace = result.find_first_of(aWhitespace);
	while (beginSpace != std::string::npos)
	{
		const auto endSpace = result.find_first_not_of(aWhitespace, beginSpace);
		const auto range = endSpace - beginSpace;

		result.replace(beginSpace, range, aFill);

		const auto newStart = beginSpace + aFill.length();
		beginSpace = result.find_first_of(aWhitespace, newStart);
	}

	return result;
}

std::string StringUtils::trimFront(const std::string& aString, const uint32_t aAmount)
{
	return aString.substr(aAmount + 1);
}

std::string StringUtils::trimEnd(const std::string& aString, const uint32_t aAmount)
{
	return aString.substr(0, aString.length() - aAmount - 1);
}

std::string StringUtils::strip(const std::string& aString, const char aDelimiter)
{
	std::string finalString = "";

	for (size_t i = 0; i < aString.length(); i++)
	{
		if (aString[i] != aDelimiter)
		{
			finalString += aString[i];
		}
	}

	return finalString;
}

std::string StringUtils::strip(const std::string& aString, const std::string& aDelimiter)
{
	std::string finalString = "";

	size_t index = 0;
	for (index = 0; index < aString.length(); index++)
	{
		uint32_t matchCount = 0;

		for (size_t j = 0; j < aDelimiter.length(); j++)
		{
			if (aString[index + j] == aDelimiter[j])
			{
				matchCount++;
			}
		}

		const bool match = (matchCount == aDelimiter.length());

		if (match)
		{
			index += matchCount;
			continue;
		}

		finalString += aString[index];
	}

	return finalString;
}

std::string StringUtils::stripFirst(const std::string& aString, const char aDelimiter)
{
	std::string finalString = "";

	bool found = false;
	size_t index = 0;
	for (index = 0; index < aString.length(); index++)
	{
		if (aString[index] != aDelimiter)
		{
			finalString += aString[index];
		}
		else
		{
			found = true;
			break;
		}
	}

	if (found)
	{
		finalString += aString.substr(index + 1);
	}

	return finalString;
}

std::string StringUtils::stripFirst(const std::string& aString, const std::string& aDelimiter)
{
	std::string finalString = "";

	bool found = false;
	size_t index = 0;
	for (index = 0; index < aString.length(); index++)
	{
		uint32_t matchCount = 0;
		bool match = false;

		for (size_t j = 0; j < aDelimiter.length(); j++)
		{
			if (aString[index + j] == aDelimiter[j])
			{
				matchCount++;
			}
		}

		match = (matchCount == aDelimiter.length());

		if (match)
		{
			index += matchCount;
			found = true;
			break;
		}

		finalString += aString[index];
	}

	if (found)
	{
		finalString += aString.substr(index + 1);
	}

	return finalString;
}

std::string StringUtils::stripLast(const std::string& aString, const char aDelimiter)
{
	return "";
}

std::string StringUtils::stripLast(const std::string& aString, const std::string& aDelimiter)
{
	return "";
}

std::string StringUtils::replace(const std::string& aString, const char aSearch, const char aReplacement)
{
	std::string finalString = "";

	for (size_t i = 0; i < aString.length(); i++)
	{
		if (aString[i] == aSearch)
		{
			finalString += aReplacement;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replace(const std::string& aString, const std::string& aSearch, const char aReplacement)
{
	std::string finalString = "";

	for (size_t i = 0; i < aString.length(); i++)
	{
		uint32_t matchCount = 0;

		for (size_t j = 0; j < aSearch.length(); j++)
		{
			if (aString[i + j] == aSearch[j])
			{
				matchCount++;
			}
		}

		if (matchCount == aSearch.length())
		{
			finalString += aReplacement;
			i += matchCount;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replace(const std::string& aString, const char aSearch, const std::string& aReplacement)
{
	std::string finalString = "";

	for (size_t i = 0; i < aString.length(); i++)
	{
		if (aString[i] == aSearch)
		{
			finalString += aReplacement;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replace(const std::string& aString, const std::string& aSearch,
	const std::string& aReplacement)
{
	std::string finalString = "";

	for (size_t i = 0; i < aString.length(); i++)
	{
		uint32_t matchCount = 0;

		for (size_t j = 0; j < aSearch.length(); j++)
		{
			if (aString[i + j] == aSearch[j])
			{
				matchCount++;
			}
		}

		if (matchCount == aSearch.length())
		{
			finalString += aReplacement;
			i += matchCount;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceFirst(const std::string& aString, const char aSearch, const char aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (size_t i = 0; i < aString.length(); i++)
	{
		if (aString[i] == aSearch && !replaced)
		{
			finalString += aReplacement;
			replaced = true;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceFirst(const std::string& aString, const std::string& aSearch, const char aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (size_t i = 0; i < aString.length(); i++)
	{
		uint32_t matchCount = 0;

		if (!replaced)
		{
			for (size_t j = 0; j < aSearch.length(); j++)
			{
				if (aString[i + j] == aSearch[j])
				{
					matchCount++;
				}
			}

			if (matchCount == aSearch.length())
			{
				finalString += aReplacement;
				i += matchCount;

				replaced = true;
				continue;
			}
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceFirst(const std::string& aString, const char aSearch, const std::string& aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (size_t i = 0; i < aString.length(); i++)
	{
		if (aString[i] == aSearch && !replaced)
		{
			finalString += aReplacement;
			replaced = true;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceFirst(const std::string& aString, const std::string& aSearch,
	const std::string& aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (size_t i = 0; i < aString.length(); i++)
	{
		uint32_t matchCount = 0;

		if (!replaced)
		{
			for (size_t j = 0; j < aSearch.length(); j++)
			{
				if (aString[i + j] == aSearch[j])
				{
					matchCount++;
				}
			}

			if (matchCount == aSearch.length())
			{
				finalString += aReplacement;
				i += matchCount;

				replaced = true;
				continue;
			}
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceLast(const std::string& aString, const char aSearch, const char aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (int32_t i = static_cast<int32_t>(aString.length()) - 1; i >= 0; i--)
	{
		if (aString[i] == aSearch && !replaced)
		{
			finalString += aReplacement;
			replaced = true;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceLast(const std::string& aString, const std::string& aSearch, const char aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (int32_t i = static_cast<int32_t>(aString.length()) - 1; i >= 0; i--)
	{
		uint32_t matchCount = 0;

		if (!replaced && (i - aSearch.length()) >= 0)
		{
			for (size_t j = 0; j < aSearch.length(); j++)
			{
				if (aString[i - j] == aSearch[aSearch.length() - 1 - j])
				{
					matchCount++;
				}
			}

			if (matchCount == aSearch.length())
			{
				finalString += aReplacement;
				i -= matchCount;

				replaced = true;
				continue;
			}
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceLast(const std::string& aString, const char aSearch, const std::string& aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (int32_t i = static_cast<int32_t>(aString.length()) - 1; i >= 0; i--)
	{
		if (aString[i] == aSearch && !replaced)
		{
			finalString += aReplacement;
			replaced = true;
			continue;
		}

		finalString += aString[i];
	}

	return finalString;
}

std::string StringUtils::replaceLast(const std::string& aString, const std::string& aSearch,
	const std::string& aReplacement)
{
	std::string finalString = "";
	bool replaced = false;

	for (int32_t i = static_cast<int32_t>(aString.length()) - 1; i >= 0; i--)
	{
		uint32_t matchCount = 0;

		if (!replaced && (i - aSearch.length()) >= 0)
		{
			for (size_t j = 0; j < aSearch.length(); j++)
			{
				if (aString[i - j] == aSearch[aSearch.length() - 1 - j])
				{
					matchCount++;
				}
			}

			if (matchCount == aSearch.length())
			{
				finalString += aReplacement;
				i -= matchCount;

				replaced = true;
				continue;
			}
		}

		finalString += aString[i];
	}

	finalString = reverse(finalString);

	return finalString;
}

std::string StringUtils::toLowercase(const std::string& aString)
{
	std::string str = aString.c_str();
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str.c_str();
}

std::string StringUtils::toUppercase(const std::string& aString)
{
	std::string str = aString.c_str();
	std::transform(str.begin(), str.end(), str.begin(), ::toupper);
	return str.c_str();
}

std::string StringUtils::swapCase(const std::string& aString)
{
	std::string finalString = "";

	for (size_t i = 0; i < aString.length(); i++)
	{
		if (isupper(aString[i]))
		{
			finalString += tolower(aString[i]);
			continue;
		}

		if (islower(aString[i]))
		{
			finalString += toupper(aString[i]);
		}
	}

	return finalString;
}

bool StringUtils::equalsIgnoreCase(const std::string& aString, const std::string& aOther)
{
	const std::string a = toLowercase(aString);
	const std::string b = toLowercase(aOther);

	return a == b;
}

bool StringUtils::contains(const std::string& aString, const char aToContain)
{
	return aString.find(aToContain) != std::string::npos;
}

bool StringUtils::contains(const std::string& aString, const std::string& aToContain)
{
	return aString.find(aToContain) != std::string::npos;
}

bool StringUtils::containsAny(std::string aString, std::vector<char> aToContain)
{
	for (auto c : aToContain)
	{
		if (aString.find(c) != std::string::npos)
		{
			return true;
		}
	}

	return false;
}

bool StringUtils::containsAny(std::string aString, std::vector<std::string> aToContain)
{
	for (auto c : aToContain)
	{
		if (aString.find(c) != std::string::npos)
		{
			return true;
		}
	}

	return false;
}

std::string StringUtils::deleteWhitespace(const std::string& aString)
{
	return strip(aString, "\t");
}

std::string StringUtils::reverse(const std::string& aString)
{
	std::string finalString = "";

	for (size_t i = aString.length() - 1; i >= 0; i--)
	{
		finalString += aString[i];
	}

	return finalString;
}

bool StringUtils::startsWith(const std::string& aString, const char aPrefix)
{
	return aString[0] == aPrefix;
}

bool StringUtils::startsWith(const std::string& aString, const std::string& aPrefix)
{
	return aString.rfind(aPrefix, 0) == 0;
}

bool StringUtils::startsWithAny(const std::string& aString, const std::vector<char> aPrefixes)
{
	for (const auto& p : aPrefixes)
	{
		if (startsWith(aString, p))
		{
			return true;
		}
	}

	return false;
}

bool StringUtils::startsWithAny(const std::string& aString, std::vector<std::string> aPrefixes)
{
	for (const auto& p : aPrefixes)
	{
		if (startsWith(aString, p))
		{
			return true;
		}
	}

	return false;
}

bool StringUtils::endsWith(const std::string& aString, const char aSuffix)
{
	return aString[aString.length() - 1] == aSuffix;
}

bool StringUtils::endsWith(const std::string& aString, const std::string& aSuffix)
{
	const std::string sub = aString.substr((aString.length() + 1) - (aSuffix.length() + 1));
	return sub == aSuffix;
}

bool StringUtils::endsWithAny(const std::string& aString, std::vector<char> aSuffixes)
{
	for (const auto& c : aSuffixes)
	{
		if (endsWith(aString, c))
		{
			return true;
		}
	}

	return false;
}

bool StringUtils::endsWithAny(const std::string& aString, std::vector<std::string> aSuffixes)
{
	for (const auto& c : aSuffixes)
	{
		if (endsWith(aString, c))
		{
			return true;
		}
	}

	return false;
}

//#include "StringUtils.h"

//#include <sstream>


//#include <cassert>
//#include <regex>

/*
std::vector<std::string> StringUtils::split(const std::string& aString, const std::string& aDelimiters)
{
	std::vector<std::string> result;

	if (aDelimiters == "\\s+")
	{
		std::regex rgx(aDelimiters);

		std::sregex_token_iterator iter(aString.begin(), aString.end(), rgx, -1);
		std::sregex_token_iterator end;

		int i = 0;

		while (iter != end)
		{
			if (i != 0)
				result.push_back(*iter);

			i++;
			++iter;
		}
	}
	else
	{
		size_t start = 0;
		size_t end = aString.find_first_of(aDelimiters);

		while (end <= std::string::npos)
		{
			std::string token = aString.substr(start, end - start);
			if (!token.empty())
				result.push_back(token);

			if (end == std::string::npos)
				break;

			start = end + 1;
			end = aString.find_first_of(aDelimiters, start);
		}
	}

	return result;
}
*/

const char* StringUtils::findToken(const char* aStr, const std::string& aToken)
{
	const char* t = aStr;
	while ((t = strstr(t, aToken.c_str())))
	{
		const bool left = aStr == t || isspace(t[-1]);
		const bool right = !t[aToken.size()] || isspace(t[aToken.size()]);
		if (left && right)
			return t;

		t += aToken.size();
	}
	return nullptr;
}

const char* StringUtils::findToken(const std::string& aString, const std::string& aToken)
{
	return findToken(aString.c_str(), aToken);
}

std::vector<std::string> StringUtils::getLines(const std::string& aSource)
{
	return split(aSource, "\n");
}

std::string StringUtils::getBlock(const char* aStr, const char** aOutPosition)
{
	const char* end = strstr(aStr, "}");
	if (!end)
		return std::string(aStr);

	if (aOutPosition)
		* aOutPosition = end;
	const uint32_t length = static_cast<const uint32_t>(end - aStr + 1);
	return std::string(aStr, length);
}

std::string StringUtils::getBlock(const std::string& aString, const uint32_t aOffset)
{
	const char* str = aString.c_str() + aOffset;
	return getBlock(str);
}

std::string StringUtils::getStatement(const char* aStr, const char** aOutPosition)
{
	const char* end = strstr(aStr, ";");
	if (!end)
		return std::string(aStr);

	if (aOutPosition)
		* aOutPosition = end;
	const uint32_t length = static_cast<const uint32_t>(end - aStr + 1);
	return std::string(aStr, length);
}

std::vector<std::string> StringUtils::tokenize(const std::string& aString)
{
	return splitWithRegex(aString, " \t\n");
}



uint32_t StringUtils::findEndOfBlock(const std::vector<std::string> aSource, const uint32_t aStart, const uint32_t aEnd, std::string* aWriteBuffer)
{
	std::vector<std::string> lines = aSource;
	uint32_t amountOpen = 0;
	for (size_t h = aStart; h < aStart + aEnd; h++)
	{
		std::string line = lines[h];
		if (aWriteBuffer != nullptr)
		{
			*aWriteBuffer += line + "\n";
		}
		if (strstr(line.c_str(), "{")) // Find open bracket
		{
			amountOpen++;
		}
		if (strstr(line.c_str(), "}")) // Find closing bracket
		{
			amountOpen--;
		}

		if (strstr(line.c_str(), "Technique") || strstr(line.c_str(), "Pass") ||
			strstr(line.c_str(), "void") || strstr(line.c_str(), "struct") ||
			strstr(line.c_str(), "#include") || strstr(line.c_str(), "interface") ||
			strstr(line.c_str(), "fragment") || strstr(line.c_str(), "vertex") ||
			strstr(line.c_str(), "geometry") ||
			strstr(line.c_str(), "fragout") || strstr(line.c_str(), "tcs") ||
			strstr(line.c_str(), "tes") || strstr(line.c_str(), "global") ||
			strstr(line.c_str(), "uniform"))
		{
			continue;
		}

		if (amountOpen == 0) // If our block is fully terminated, stop the loop and save end location
		{
			return static_cast<uint32_t>(h + 1);
		}
	}

	return 0;
}

uint32_t StringUtils::parseToGLEnum(const std::string& aValue)
{
	if (strcmp(aValue.c_str(), "Depth") == 0)
		return GL_DEPTH_BUFFER_BIT;

	if (strcmp(aValue.c_str(), "Color") == 0)
		return GL_COLOR_BUFFER_BIT;

	if (strcmp(aValue.c_str(), "Stencil") == 0)
		return GL_STENCIL_BUFFER_BIT;

	if (strcmp(aValue.c_str(), "Front") == 0)
		return GL_FRONT;

	if (strcmp(aValue.c_str(), "Back") == 0)
		return GL_BACK;

	if (strcmp(aValue.c_str(), "FrontAndBack") == 0)
		return GL_FRONT_AND_BACK;

	if (strcmp(aValue.c_str(), "Line") == 0)
		return GL_LINE;

	if (strcmp(aValue.c_str(), "Fill") == 0)
		return GL_FILL;

	if (strcmp(aValue.c_str(), "True") == 0)
		return GL_TRUE;

	if (strcmp(aValue.c_str(), "False") == 0)
		return GL_FALSE;

	if (strcmp(aValue.c_str(), "Never") == 0)
		return GL_NEVER;

	if (strcmp(aValue.c_str(), "Less") == 0)
		return GL_LESS;

	if (strcmp(aValue.c_str(), "Equal") == 0)
		return GL_EQUAL;

	if (strcmp(aValue.c_str(), "Lequal") == 0)
		return GL_LEQUAL;

	if (strcmp(aValue.c_str(), "Greater") == 0)
		return GL_GREATER;

	if (strcmp(aValue.c_str(), "NotEqual") == 0)
		return GL_NOTEQUAL;

	if (strcmp(aValue.c_str(), "Gequal") == 0)
		return GL_GEQUAL;

	if (strcmp(aValue.c_str(), "Always") == 0)
		return GL_ALWAYS;

	if (strcmp(aValue.c_str(), "CW") == 0)
		return GL_CW;

	if (strcmp(aValue.c_str(), "CCW") == 0)
		return GL_CCW;

	if (strcmp(aValue.c_str(), "Zero") == 0)
		return GL_ZERO;

	if (strcmp(aValue.c_str(), "One") == 0)
		return GL_ONE;

	if (strcmp(aValue.c_str(), "SrcColor") == 0)
		return GL_SRC_COLOR;

	if (strcmp(aValue.c_str(), "OneMinusSrcColor") == 0)
		return GL_ONE_MINUS_SRC_COLOR;

	if (strcmp(aValue.c_str(), "DstColor") == 0)
		return GL_DST_COLOR;

	if (strcmp(aValue.c_str(), "OneMinusDstColor") == 0)
		return GL_ONE_MINUS_DST_COLOR;

	if (strcmp(aValue.c_str(), "SrcAlpha") == 0)
		return GL_SRC_ALPHA;

	if (strcmp(aValue.c_str(), "OneMinusSrcAlpha") == 0)
		return GL_ONE_MINUS_SRC_ALPHA;

	if (strcmp(aValue.c_str(), "DstAlpha") == 0)
		return GL_DST_ALPHA;

	if (strcmp(aValue.c_str(), "OneMinusDstAlpha") == 0)
		return GL_ONE_MINUS_DST_ALPHA;

	if (strcmp(aValue.c_str(), "ConstantColor") == 0)
		return GL_CONSTANT_COLOR;

	if (strcmp(aValue.c_str(), "OneMinusConstantColor") == 0)
		return GL_ONE_MINUS_CONSTANT_COLOR;

	if (strcmp(aValue.c_str(), "ConstantAlpha") == 0)
		return GL_CONSTANT_ALPHA;

	if (strcmp(aValue.c_str(), "OneMinusConstantAlpha") == 0)
		return GL_ONE_MINUS_CONSTANT_ALPHA;

	if (strcmp(aValue.c_str(), "Increment") == 0)
		return GL_INCR;

	if (strcmp(aValue.c_str(), "Decrement") == 0)
		return GL_DECR;

	if (strcmp(aValue.c_str(), "IncrementWrap") == 0)
		return GL_INCR_WRAP;

	if (strcmp(aValue.c_str(), "DecrementWrap") == 0)
		return GL_DECR_WRAP;

	if (strcmp(aValue.c_str(), "Invert") == 0)
		return GL_INVERT;

	if (strcmp(aValue.c_str(), "Replace") == 0)
		return GL_REPLACE;

	if (strcmp(aValue.c_str(), "Nearest") == 0)
		return GL_NEAREST;

	if (strcmp(aValue.c_str(), "Linear") == 0)
		return GL_LINEAR;

	if (strcmp(aValue.c_str(), "NearestMipmapNearest") == 0)
		return GL_NEAREST_MIPMAP_NEAREST;

	if (strcmp(aValue.c_str(), "LinearMipmapNearest") == 0)
		return GL_LINEAR_MIPMAP_NEAREST;

	if (strcmp(aValue.c_str(), "NearestMipmapLinear") == 0)
		return GL_NEAREST_MIPMAP_LINEAR;

	if (strcmp(aValue.c_str(), "LinearMipmapLinear") == 0)
		return GL_LINEAR_MIPMAP_LINEAR;

	if (strcmp(aValue.c_str(), "Repeat") == 0)
		return GL_REPEAT;

	if (strcmp(aValue.c_str(), "MirroredRepeat") == 0)
		return GL_MIRRORED_REPEAT;

	if (strcmp(aValue.c_str(), "ClampToEdge") == 0)
		return GL_CLAMP_TO_EDGE;

	if (strcmp(aValue.c_str(), "ClampToBorder") == 0)
		return GL_CLAMP_TO_BORDER;

	if (strcmp(aValue.c_str(), "MirrorClampToEdge") == 0)
		return GL_MIRROR_CLAMP_TO_EDGE;

	return 0;
}

std::string StringUtils::getSemanticLocationString(const std::string& semantic)
{
	std::string a = std::to_string(static_cast<int>(getSemanticLocation(semantic)));
	std::string ret = std::string(a.c_str());

	return ret;
}

int32_t StringUtils::getSemanticLocation(const std::string& aSemantic)
{
	if (aSemantic == "POSITION")
	{
		return 0;
	}

	if (aSemantic == "TEXCOORD0")
	{
		return 1;
	}

	if (aSemantic == "TEXCOORD1")
	{
		return 2;
	}

	if (aSemantic == "NORMAL")
	{
		return 3;
	}

	if (aSemantic == "TANGENT")
	{
		return 4;
	}

	if (aSemantic == "BINORMAL")
	{
		return 5;
	}

	if (aSemantic == "COLOR")
	{
		return 6;
	}

	return -1;
}
