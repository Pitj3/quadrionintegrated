#include "assets/AssetManager.h"
#include "tbb/task_group.h"
#include "tbb/task_scheduler_init.h"


class TBBDeps
{
public:
	tbb::task_group mAsyncTaskGroup;
};

TBBDeps* AssetManager::mTBBDependencies = new TBBDeps();

AssetManager& AssetManager::instance()
{
	static AssetManager* instance = new AssetManager();
	return *instance;
}

void AssetManager::_loadAssetAsync(std::list<std::string>::iterator& aIter,
	uint8_t aPrio)
{
	auto asset = mAssets.find(*aIter);
	if (asset != mAssets.end())
	{
		mTBBDependencies->mAsyncTaskGroup.run([=] {asset->second->load(); });
	}

	switch (aPrio)
	{
	case ASSET_LOAD_HIGH_PRIO:
		mAsyncAssetQueueHigh.erase(aIter);
		aIter = mAsyncAssetQueueHigh.begin();
		break;

	case ASSET_LOAD_MED_PRIO:
		mAsyncAssetQueueMedium.erase(aIter);
		aIter = mAsyncAssetQueueMedium.begin();
		break;

	case ASSET_LOAD_LOW_PRIO:
		mAsyncAssetQueueLow.erase(aIter);
		aIter = mAsyncAssetQueueLow.begin();
		break;
	}


	// Cramming tasks down TBB's throat causes tasks to drop
	std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

AssetManager::AssetManager()
{

	tbb::task_scheduler_init(4);

	// Do Async Loads //
	mTBBDependencies->mAsyncTaskGroup.run([=]
		{
			while (true)
			{
				auto it = mAsyncAssetQueueHigh.begin();
				while (it != mAsyncAssetQueueHigh.end())
					_loadAssetAsync(it, ASSET_LOAD_HIGH_PRIO);

				it = mAsyncAssetQueueMedium.begin();
				while (it != mAsyncAssetQueueMedium.end())
				{
					if (mAsyncAssetQueueHigh.size() > 0)
						break;
					_loadAssetAsync(it, ASSET_LOAD_MED_PRIO);
				}

				it = mAsyncAssetQueueLow.begin();
				while (it != mAsyncAssetQueueLow.end())
				{
					if (mAsyncAssetQueueMedium.size() > 0 || mAsyncAssetQueueHigh.size() > 0)
						break;
					_loadAssetAsync(it, ASSET_LOAD_LOW_PRIO);
				}
			}
		});
}

void AssetManager::unloadAll()
{
	mAssets.clear();
}

bool AssetManager::unload(const std::string& aName)
{
	auto loc = mAssets.find(aName);
	if (loc == mAssets.end())
		return false;

	mAssets.erase(loc);
	return true;
}

