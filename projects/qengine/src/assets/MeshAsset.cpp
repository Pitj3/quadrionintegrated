#include "assets/MeshAsset.h"

#include <float.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "assets/AssetManager.h"
#include "assets/EffectAsset.h"
#include "assets/TextureAsset.h"
#include "QMath.h"
#include "core/QLog.h"
#include "render/VertexArray.h"
#include "render/VertexBuffer.h"
#include "render/IndexBuffer.h"
#include "render/VertexBufferLayout.h"
#include "io/FileSystem.h"
#include "StringUtils.h"
#include "render/Effect.h"
#include "render/Image.h"

inline mat4<float> AssimpToQuadrion(aiMatrix4x4 aMat)
{
	return mat4<float>(aMat.a1, aMat.a2, aMat.a3, aMat.a4, aMat.b1, aMat.b2, aMat.b3, aMat.b4, aMat.c1, aMat.c2, aMat.c3, aMat.c4, aMat.d1, aMat.d2, aMat.d3, aMat.d4);
}

inline mat4<float> AssimpToQuadrion(aiMatrix3x3 aMat)
{
	return mat4<float>(aMat.a1, aMat.a2, aMat.a3, 0.0f, aMat.b1, aMat.b2, aMat.b3, 0.0f, aMat.c1, aMat.c2, aMat.c3, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
}


class AssimpDependencies
{
	public:
		std::vector<std::shared_ptr<MeshNodeRod>> mMeshes;
		void processNode(aiNode* aNode, const aiScene* aScene, aiMatrix4x4 aParentMatrix);
		void processMesh(aiMesh* aMesh, const aiScene* aScene, std::shared_ptr<MeshNodeRod> aNode,
						 const aiMatrix4x4& aTransform);
		void processMaterials(aiNode* aNode, const aiScene* aScene);
		uint32_t getTextureByType(QTEXTURE_TYPE aType, uint32_t aMaterialIndex);
	
		std::vector<uint32_t> loadTextureMaps(aiMaterial* aMaterial, const aiTextureType aType, uint32_t aMaterialIndex);
		std::vector<std::unordered_map<uint32_t, uint32_t>>	mTextureTypeMap;
	
};

uint32_t AssimpDependencies::getTextureByType(QTEXTURE_TYPE aType, uint32_t aMaterialIndex)
{
	auto loc = mTextureTypeMap[aMaterialIndex].find(aType);
	if (loc != mTextureTypeMap[aMaterialIndex].end())
		return loc->second;

	return 0;
}

void AssimpDependencies::processNode(aiNode* aNode, const aiScene* aScene, aiMatrix4x4 aParentMatrix)
{
//	aiMatrix4x4 m = aParentMatrix * aNode->mTransformation.Transpose();
	aiMatrix4x4 m = aParentMatrix * aNode->mTransformation;
	aiVector3t<float> pScaling, pPosition;
	aiMatrix4x4t<float> mRotation;
	aiQuaterniont<float> pRotation;
	m.Decompose(pScaling, pRotation, pPosition);

	for (uint32_t i = 0; i < aNode->mNumMeshes; i++)
	{
		aiMesh* mesh = aScene->mMeshes[aNode->mMeshes[i]];
		std::shared_ptr<MeshNodeRod> meshNode = std::make_shared<MeshNodeRod>();
		processMesh(mesh, aScene, meshNode, m);

		meshNode->transform = AssimpToQuadrion(m);

		// Convert concatenated mat to Assimp and decompose for proper composition
		meshNode->scale = vec3<float>(pScaling.x, pScaling.y, pScaling.z);
		meshNode->rotation = AssimpToQuadrion(pRotation.GetMatrix());
		meshNode->position = vec3<float>(pPosition.x, pPosition.y, pPosition.z);

		mMeshes.push_back(meshNode);
	}

	for (uint32_t i = 0; i < aNode->mNumChildren; i++)
	{
		processNode(aNode->mChildren[i], aScene, m);
	}
}

void AssimpDependencies::processMesh(aiMesh* aMesh, const aiScene* aScene, std::shared_ptr<MeshNodeRod> aNode,
									 const aiMatrix4x4& aTransform)
{
	aiVector3t<float> pos, scale;
	aiQuaterniont<float> rot;
	aTransform.Decompose(scale, rot, pos);

	std::vector<vec3<float>> positions;
	std::vector<vec2<float>> uvs;
	std::vector<vec3<float>> normals;
	std::vector<vec3<float>> tangents;
	std::vector<vec3<float>> binormals;
	uint32_t* triangles = nullptr;

	// Process material references 
	uint32_t materialIndex = aMesh->mMaterialIndex;
	aiColor3D diffuseColor, specularColor, ambientColor, emissiveColor;
	aScene->mMaterials[materialIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);
	aScene->mMaterials[materialIndex]->Get(AI_MATKEY_COLOR_SPECULAR, specularColor);
	aScene->mMaterials[materialIndex]->Get(AI_MATKEY_COLOR_AMBIENT, ambientColor);
	aScene->mMaterials[materialIndex]->Get(AI_MATKEY_COLOR_EMISSIVE, emissiveColor);
	aNode->meshMaterial.diffuseColor = vec3<float>(diffuseColor.r, diffuseColor.g, diffuseColor.b);
	aNode->meshMaterial.specularColor = vec3<float>(specularColor.r, specularColor.g, specularColor.b);
	aNode->meshMaterial.ambientColor = vec3<float>(ambientColor.r, ambientColor.g, ambientColor.b);
	aNode->meshMaterial.emissiveColor = vec3<float>(emissiveColor.r, emissiveColor.g, emissiveColor.b);

	aNode->meshMaterial.albedoTexture = getTextureByType(QTEXTURE_TYPE_DIFFUSE, materialIndex);
	aNode->meshMaterial.normalMap = getTextureByType(QTEXTURE_TYPE_HEIGHT, materialIndex);
	aNode->meshMaterial.heightMap = getTextureByType(QTEXTURE_TYPE_HEIGHT, materialIndex);
	aNode->meshMaterial.emissiveTexture = getTextureByType(QTEXTURE_TYPE_EMISSIVE, materialIndex);
	aNode->meshMaterial.aoTexture = getTextureByType(QTEXTURE_TYPE_AMBIENT, materialIndex);
	aNode->meshMaterial.roughnessTexture = getTextureByType(QTEXTURE_TYPE_SPECULAR, materialIndex);
	aNode->meshMaterial.metallicTexture = getTextureByType(QTEXTURE_TYPE_SHININESS, materialIndex);

	// Process geometry
	vec3<float> min(FLT_MAX, FLT_MAX, FLT_MAX);
	vec3<float> max(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	for (uint32_t i = 0; i < aMesh->mNumVertices; i++)
	{
		vec4<float> vtx = vec4<float>(aMesh->mVertices[i].x, aMesh->mVertices[i].y, aMesh->mVertices[i].z, 1.0f);
		vtx *= AssimpToQuadrion(aTransform);
		if(vtx.x < min.x) min.x = vtx.x;
		if(vtx.y < min.y) min.y = vtx.y;
		if(vtx.z < min.z) min.z = vtx.z;
		if(vtx.x > max.x) max.x = vtx.x;
		if(vtx.y > max.y) max.y = vtx.y;
		if(vtx.z > max.z) max.z = vtx.z;

		vec3<float> vector = vec3<float>(aMesh->mVertices[i].x, aMesh->mVertices[i].y, aMesh->mVertices[i].z);
		vector.x *= scale.x;
		vector.y *= scale.y;
		vector.z *= scale.z;
		positions.push_back(vector);

		if(aMesh->HasNormals())
			vector = vec3<float>(aMesh->mNormals[i].x, aMesh->mNormals[i].y, aMesh->mNormals[i].z);
		else
			vector = vec3<float>(0.0f, 0.0f, 0.0f);
		normals.push_back(vector);

		if (aMesh->HasTextureCoords(0))
		{
			vec2<float> vec;
			vec.x = aMesh->mTextureCoords[0][i].x;
			vec.y = aMesh->mTextureCoords[0][i].y;
			uvs.push_back(vec);
		}
		else
		{
			uvs.push_back(vec2<float>(0, 0));
		}

		vec3<float> zero(0.0f, 0.0f, 0.0f);
		if (aMesh->HasTangentsAndBitangents())
		{
			vector = vec3<float>(aMesh->mTangents[i].x, aMesh->mTangents[i].y, aMesh->mTangents[i].z);
			tangents.push_back(vector);

			vector = vec3<float>(aMesh->mBitangents[i].x, aMesh->mBitangents[i].y, aMesh->mBitangents[i].z);
			binormals.push_back(vector);
		}

		else
		{
			tangents.push_back(zero);
			binormals.push_back(zero);
		}
	}

	uint32_t nIndices = 0;
	for (uint32_t i = 0; i < aMesh->mNumFaces; ++i)
		nIndices += aMesh->mFaces[i].mNumIndices;
	triangles = new uint32_t[nIndices];

	uint32_t iPtr = 0;
	for (uint32_t i = 0; i < aMesh->mNumFaces; i++)
	{
		const aiFace face = aMesh->mFaces[i];

		for (uint32_t j = 0; j < face.mNumIndices; j++)
		{
			triangles[iPtr++] = face.mIndices[j];
		}
	}

	for (size_t i = 0; i < positions.size(); i++)
	{
		vec3<float> n = normals[i];
		vec3<float> t = tangents[i];
		vec3<float> b = binormals[i];

		auto d = n.dotProd(t);
		auto v = (t - n * d);
		v.normalize();
		t = v;

		auto c = n.crossProd(t);
		if (c.dotProd(b) < 0.0f)
		{
			t = t * -1.0f;
		}

		tangents[i] = t;
	}

	Vertex* vList = new Vertex[aMesh->mNumVertices];

	for (uint32_t i = 0; i < aMesh->mNumVertices; i++)
	{
		Vertex v;
		v.position = positions[i];
		v.uv = uvs[i];
		v.normal = normals[i];
		v.tangent = tangents[i];
		v.binormal = binormals[i];

		vList[i] = v;
	}

	size_t vBufSize = sizeof(Vertex) * aMesh->mNumVertices;
	VertexBuffer* vbo = new VertexBuffer(vList, vBufSize);

	VertexBufferLayout layout;
	layout.push<vec3<float>>("position", 3, false);
	layout.push<vec2<float>>("uv0", 2, false);
	layout.push<vec3<float>>("normal", 3, true);
	layout.push<vec3<float>>("tangent", 3, true);
	layout.push<vec3<float>>("binormal", 3, true);
	vbo->setLayout(layout);

	IndexBuffer* ibo = new IndexBuffer(triangles, nIndices, GL_TRIANGLES);

	VertexArray* vao = new VertexArray(vbo, ibo);
	aNode->mesh = std::make_shared<Mesh>(vao, ibo);

	vec3<float> span = max - min;
	vec3<float> center = min + (span * 0.5f);
	aNode->center = center;
}

void AssimpDependencies::processMaterials(aiNode* aNode, const aiScene* aScene)
{
	if (aScene->HasMaterials() == false)
	{
		return;
	}

	const uint32_t numMaterials = aScene->mNumMaterials;
	mTextureTypeMap.resize(numMaterials);

	for (uint32_t i = 0; i < numMaterials; i++)
	{
		aiMaterial* material = aScene->mMaterials[i];
		std::vector<uint32_t> diffuseMaps = loadTextureMaps(material, aiTextureType_DIFFUSE, i);
		std::vector<uint32_t> specularMaps = loadTextureMaps(material, aiTextureType_SPECULAR, i);
		std::vector<uint32_t> normalMaps = loadTextureMaps(material, aiTextureType_NORMALS, i);
		std::vector<uint32_t> ambientMaps = loadTextureMaps(material, aiTextureType_AMBIENT, i);
		std::vector<uint32_t> emissiveMaps = loadTextureMaps(material, aiTextureType_EMISSIVE, i);
		std::vector<uint32_t> heightMaps = loadTextureMaps(material, aiTextureType_HEIGHT, i);
		std::vector<uint32_t> shininessMaps = loadTextureMaps(material, aiTextureType_SHININESS, i);
		std::vector<uint32_t> opacityMaps = loadTextureMaps(material, aiTextureType_OPACITY, i);
		std::vector<uint32_t> displacementMaps = loadTextureMaps(material, aiTextureType_DISPLACEMENT, i);
		std::vector<uint32_t> lightMaps = loadTextureMaps(material, aiTextureType_LIGHTMAP, i);
		std::vector<uint32_t> reflectionMaps = loadTextureMaps(material, aiTextureType_REFLECTION, i);
	}
}

std::vector<uint32_t> AssimpDependencies::loadTextureMaps(aiMaterial* aMaterial, const aiTextureType aType, uint32_t aMaterialIndex)
{
	const uint32_t numTextures = aMaterial->GetTextureCount(aType);
	std::vector<uint32_t> maps;
	maps.reserve(numTextures);

	for (uint32_t i = 0; i < numTextures; i++)
	{
		aiString path;
		aiTextureMapping mapping;
		uint32_t uvIndex;
		float blend;
		aiTextureOp op;

		aMaterial->GetTexture(aType, i, &path, &mapping, &uvIndex, &blend, &op);

		std::string pathString = std::string(path.data);
		std::vector<std::string> chunks = StringUtils::splitWithRegex(pathString, "\\");
		if(chunks.size() <= 0)
			continue;

		std::string textureName = chunks.back();

		// Path must be different for X11 and Win32
		std::string searchDir("Media/*");
		#if !defined(_WIN32) || !defined(_WIN64)
			searchDir = "Media";
		#endif
		std::string fullPath = FileSystem::getFileRecursively(searchDir, textureName);

		if (fullPath == "")
		{
			continue;
		}


		std::shared_ptr<TextureAsset> testTex = nullptr;
		testTex = AssetManager::instance().load<TextureAsset>(textureName, ASSET_LOAD_MED_PRIO, fullPath);
		uint32_t tex = testTex->getTextureID();

		if (tex > 0)
		{
			uint32_t type = aType;
			mTextureTypeMap[aMaterialIndex][type] = tex;
		}
	}

	return maps;
}


MeshAsset::MeshAsset(const std::string& aName, const std::string& aPath) : Asset(aName)
{
	mPath = aPath;
	mLoaded = false;

	mAssimpDependencies = new AssimpDependencies();
}

MeshAsset::~MeshAsset()
{
	if (mAssimpDependencies)
	{
		delete mAssimpDependencies;
		mAssimpDependencies = nullptr;
	}
}

std::vector<std::shared_ptr<MeshNodeRod>>& MeshAsset::getMeshes()
{
	return mAssimpDependencies->mMeshes;
}

bool MeshAsset::load()
{
	Assimp::Importer* importer = new Assimp::Importer;
	const aiScene* scene = importer->ReadFile(mPath.c_str(), aiProcess_CalcTangentSpace |
															 aiProcess_Triangulate |
															 aiProcess_RemoveRedundantMaterials |
															 aiProcess_ImproveCacheLocality |
															 aiProcess_FixInfacingNormals |
															 aiProcess_SplitLargeMeshes |
															 aiProcess_TransformUVCoords);


	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		QUADRION_INTERNAL_CRITICAL("Failed to load scene");
	}
	mAssimpDependencies->processMaterials(scene->mRootNode, scene);
	mAssimpDependencies->processNode(scene->mRootNode, scene, scene->mRootNode->mTransformation);

	mLoaded = true;
	delete importer;
	return true;
}


