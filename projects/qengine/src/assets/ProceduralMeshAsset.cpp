#include "assets/ProceduralMeshAsset.h"


#include "render/Buffer.h"
#include "render/Image.h"
#include "render/VertexBuffer.h"
#include "core/QLog.h"
#include "QMath.h"
#include "render/Render.h"
#include "render/QGfx.h"
#include <vector>
#include <list>
#include <time.h>
#include <cfloat>
#include <valarray>

//#include <qtl/thread/future.h>
//#include <qtl/fft.h>
//#include "ThreadArena.h"

constexpr uint32_t MAX_GRASS_CLUMPS = 1000;			// 6000
constexpr uint32_t MAX_TREES = 8192;



// Pairs are vec4<double>(pos.x, pos.y, pos.z, sample) //
static double bilinearInterp(vec3<float> aPos, vec4<double> ur, vec4<double> ul, vec4<double> ll, vec4<double> lr)
{
	double r1 = ((ur.x - aPos.x) / (ur.x - ul.x)) * ll.w + \
		((aPos.x - ul.x) / (ur.x - ul.x)) * lr.w;

	double r2 = ((ur.x - aPos.x) / (ur.x - ul.x)) * ul.w + \
		((aPos.x - ul.x) / (ur.x - ul.x)) * ur.w;

	double height = ((ul.z - aPos.z) / (ul.z - ll.z)) * r1 + \
		((aPos.z - ll.z) / (ul.z - ll.z)) * r2;

	/*if (isnan(height) || height < 0.0)
	{
		int trap = 0;
	}*/

	return height;
}

static uint32_t getIndexFromPair(vec2<int32_t> coords, uint32_t scanLineWidth)
{
	return scanLineWidth * coords.y + coords.x;
}

//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
//MeshPropPatch::MeshPropPatch()
//{
//
//}
//
//MeshPropPatch::~MeshPropPatch()
//{
//
//}
//
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//
//MeshPropPartition::MeshPropPartition()
//{
//	propPatchList = nullptr;
//	numPropPatches = 0;
//}
//
//MeshPropPartition::~MeshPropPartition()
//{
//	if(propPatchList)
//	{
//		for(uint32_t i = 0; i < numPropPatches; ++i)
//			delete propPatchList[i];
//		delete[] propPatchList;
//		numPropPatches = 0;
//	}
//}
//
//const MeshPropPatch* const MeshPropPartition::getPatchFromIndex(uint32_t aPatchIdx)
//{
//	return propPatchList[aPatchIdx];
//}
//
//
//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
//
//MeshPatch::MeshPatch()
//{
//	numVertsX = 0;
//	numVertsX = 0;
//	patchVAO = 0;
//	verts = nullptr;
//	physicsVerts = nullptr;
//	indices = nullptr;
//	numPhysicsVerts = 0;
//}
//
//MeshPatch::~MeshPatch()
//{
//	if(patchVAO > 0)
//		deleteBuffer(patchVAO);
//
//	if(verts)
//	{
//		delete[] verts;
//		verts = nullptr;
//	}
//
//	if(physicsVerts)
//	{
//		delete[] physicsVerts;
//		physicsVerts = nullptr;
//	}
//
//	if(indices)
//	{
//		delete[] indices;
//		indices = nullptr;
//	}
//}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


ProceduralMeshAsset::ProceduralMeshAsset(const std::string aName) : Asset(aName)
{
	//topDownWidth = 1024;
	//topDownHeight = 1024;
	//numPerlinSamplesX = 0;
	//numPerlinSamplesY = 0;
	//DepthStencilId no;
	//no.depthId = 0;
	//no.stencilId = 0;
	//topDownTexture = addRenderTarget(topDownWidth, topDownHeight,
	//							    QTEXTURE_FORMAT_RGBA8, SAMPLE_CLAMP | SAMPLE_LINEAR);
	//topDownRT = createRenderableSurface(&topDownTexture, 1,
	//										   no,
	//										   topDownWidth, topDownHeight);

	//topDownBuf = new uint32_t[topDownWidth * topDownHeight];
	//pMeshPropPartition = new MeshPropPartition();

	//propInstances.resize(MAX_GRASS_CLUMPS);
}

ProceduralMeshAsset::ProceduralMeshAsset(const std::string aName, const TerrainInitializer& aInit) : Asset(aName)
{
	mInitializer = aInit;	
}


ProceduralMeshAsset::~ProceduralMeshAsset()
{

//	if(heightField)
//	{
//		delete[] heightField;
//		heightField = nullptr;
//	}
//
//	deleteTexture(heightmapTexture);
//	deleteTexture(normalmapTexture);
//	deleteTexture(L0GrassTBO);
//	deleteTexture(L0TreeTBO);
////	deleteBuffer(shadowTerrainVBO);
////	deleteBuffer(basePatchVBO);
//	deleteRenderableSurface(topDownRT);
//	deleteRenderTarget(topDownTexture);
//	QGfx::GL::gl_DeleteVertexArrays(1, &grassVAO);
//	QGfx::GL::gl_DeleteBuffers(1, &L0GrassHandle);
//	QGfx::GL::gl_DeleteBuffers(1, &L0TreeHandle);
//
//	if(topDownBuf)
//	{
//		delete[] topDownBuf;
//		topDownBuf = nullptr;
//	}
//
//	for (uint32_t i = 0; i < patchList.size(); ++i)
//	{
//		if (patchList[i])
//		{
//			delete patchList[i];
//			patchList[i] = nullptr;
//		}
//	}
//	patchList.clear();
//
//	if(baseVerts)
//	{
//		delete[] baseVerts;
//		baseVerts = nullptr;
//	}
//
//	if(heightField)
//	{
//		delete[] heightField;
//		heightField = nullptr;
//	}
//
//	if(perlinNoise)
//	{
//		delete[] perlinNoise;
//		perlinNoise = nullptr;
//	}
//
//	if(pMeshPropPartition)
//	{
//		delete pMeshPropPartition;
//	}
}

bool ProceduralMeshAsset::load()
{
	uint32_t nPixelsX = mInitializer.nPatchesX * mInitializer.nPixelsPerPatchX;
	uint32_t nPixelsY = mInitializer.nPatchesZ * mInitializer.nPixelsPerPatchZ;
	uint32_t nPixels = nPixelsX * nPixelsY;

	if (nPixels <= 0 || !isPow2(nPixelsX) || !isPow2(nPixelsY))
	{
		QUADRION_INTERNAL_ERROR("Terrain is not Pow2");
		return false;
	}

	if (strlen(mInitializer.heightmapImageFilename) > 0)
	{
		QUADRION_INTERNAL_ERROR("Terrain is not procedural: file name specified");
		return false;
	}

	double* heightField = new double[nPixels];
	generateGaussianNoise(heightField, nPixels, mInitializer.entropy);
	double min = DBL_MAX;
	double max = DBL_MIN;
	for(uint32_t i = 0; i < nPixels; ++i)
	{
		if(heightField[i] < min)
			min = heightField[i];
		if(heightField[i] > max)
			max = heightField[i];
	}

	//vertexStride = 5;
	//VertexAttribute vAttribs[2];
	//vAttribs[0] = { LOC_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0 };
	//vAttribs[1] = { LOC_TEXCOORD0, 2, GL_FLOAT, GL_TRUE, sizeof(float) * 5, 12 };

	//VertexAttribute shadowVertAttribs;
	//shadowVertAttribs = {LOC_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0};

	//const uint32_t width = size;
	//const uint32_t height = size;

	//const uint32_t nVerts = width * height;
	//const uint32_t nVertsX = width;
	//const uint32_t nVertsZ = height;
	//numSamplesX = nVertsX;
	//numSamplesZ = nVertsZ;

	uint32_t idx = 0;
	uint32_t valsIdx = 0;
	float displ;
	std::valarray<std::complex<double>> vals(nPixels);
	std::valarray<std::complex<double>> tmp(nPixelsX);
	uint16_t* h;
//	const uint32_t nShadowVertsX = numSamplesX / 8;
//	const uint32_t nShadowVertsZ = numSamplesZ / 8;
////	float* shadowVerts = new float[nShadowVertsX * nShadowVertsZ * 3];
//	Vertex* shadowVerts = new Vertex[nShadowVertsX * nShadowVertsZ];
//	uint32_t* shadowIndices = new uint32_t[(nShadowVertsX - 1) * (nShadowVertsZ - 1) * 6];
	
//	qtl::function<void()> fft_func = [&]()
//	{
		//double dx = (meshMaxs.x - meshMins.x) / (nVertsX - 1);
		//double dz = (meshMaxs.z - meshMins.z) / (nVertsZ - 1);
		//uint32_t shadowVertIdx = 0;
		//uint32_t shadowIdx = 0;
		uint32_t sx = 0;
		uint32_t sz = 0;

		for (uint32_t y = 0; y < nPixelsY; ++y)			// j 
		{
			for (uint32_t x = 0; x < nPixelsX; ++x)		// i
			{
				double val = heightField[(y * nPixelsX + x)];
				displ = (float)val;

				// Build the height data in Y only for the fft //
				std::complex<double> a(displ, 0.0);
				vals[valsIdx++] = a;

//				// Build shadow geometry //
//				if (z % 8 == 0 && x % 8 == 0)
//				{
//					float xShadow = static_cast<float>(meshMins.x + (x * dx));
//					float zShadow = static_cast<float>(meshMins.z + (z * dz));
////					shadowVerts[shadowVertIdx++] = xShadow;
////					shadowVerts[shadowVertIdx++] = 0.0f;
////					shadowVerts[shadowVertIdx++] = zShadow;
//					shadowVerts[shadowVertIdx].position = vec3<float>(xShadow, 0.0f, zShadow);
//					shadowVertIdx++;
//
//					if (!(sx > nShadowVertsX - 2 || sz > nShadowVertsZ - 2))
//					{
//						uint32_t origin = (sz * nShadowVertsX) + sx;
//						shadowIndices[shadowIdx++] = (origin);
//						shadowIndices[shadowIdx++] = (origin + nShadowVertsX);
//						shadowIndices[shadowIdx++] = (origin + 1);
//						shadowIndices[shadowIdx++] = (origin + nShadowVertsX);
//						shadowIndices[shadowIdx++] = (origin + nShadowVertsX + 1);
//						shadowIndices[shadowIdx++] = (origin + 1);
//					}
//				}

//				if (x % 8 == 0)
//					sx++;
			}

//			sx = 0;
//			if (z % 8 == 0)
//				sz++;
		}

		// Apply fft horizontally //
		idx = 0;
		for (uint32_t i = 0; i < nPixelsX; ++i)
		{
			idx = i;

			// Pack the row vector //
			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				std::complex<double> a(vals[idx]);
				tmp[j] = a;
				idx += nPixelsY;
			}

			// Perform fft on row //
			idx = i;

			// Put values back into grid //
			for (uint32_t j = 0; j < nPixelsX; ++j)
			{
				vals[idx] = tmp[j];
				idx += nPixelsY;
			}
		}

		// Apply fft vertically //
		idx = 0;
		for (uint32_t i = 0; i < nPixelsX; ++i)
		{
			idx = i * nPixelsY;
			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				std::complex<double> a(vals[idx]);
				tmp[j] = a;
				idx++;
			}

			fft(tmp);
			idx = i * nPixelsY;

			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				vals[idx] = tmp[j];
				idx++;
			}
		}

		// Apply Pink Noise or 1/(f^p) filter //
		double f;
		//	float power = static_cast<float>(smoothing);		// 2.0
		double power = mInitializer.smoothing;
		double fN = nPixelsY;
		uint32_t iN2, jN2;
		for (uint32_t j = 0; j < nPixelsX; ++j)
		{
			for (uint32_t i = 0; i < nPixelsY; ++i)
			{
				idx = i + j * nPixelsY;

				iN2 = i - nPixelsY / 2;
				jN2 = j - nPixelsY / 2;
				f = sqrt((iN2) / fN * (iN2) / fN + (jN2) / fN * (jN2) / fN);
				f = f < 1.0 / nPixelsY ? 1.0 / nPixelsY : f;
				vals[idx] = vals[idx] * std::complex<double>(1.0 / pow(f, power));
			}
		}

		// Apply Ifft vertically //
		idx = 0;
		for (uint32_t i = 0; i < nPixelsX; ++i)
		{
			idx = i * nPixelsX;
			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				std::complex<double> a(vals[idx]);
				tmp[j] = a;
				idx++;
			}

//			qtl::ifft(tmp);
			inverseFFT(tmp);
			idx = i * nPixelsX;

			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				vals[idx] = tmp[j];
				idx++;
			}
		}

		// Apply Ifft horizontally //
		// Scale and pack values back into vertex list at the same time //
		idx = 0;
		uint32_t insertIdx = 0;
		double maxHeight = DBL_MIN;
		double minHeight = DBL_MAX;
		for (uint32_t i = 0; i < nPixelsX; ++i)
		{
			idx = i;

			// Pack the row vector //
			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				std::complex<double> a(vals[idx]);
				tmp[j] = a;
				idx += nPixelsY;
			}

			// Perform fft on row //
//			qtl::ifft(tmp);
			inverseFFT(tmp);
			idx = i;

			// Put values back into grid //
			for (uint32_t j = 0; j < nPixelsY; ++j)
			{
				vals[idx] = tmp[j];
				idx += nPixelsY;

				std::complex<double> conj;
				conj = std::conj(tmp[j]);
				conj = tmp[j] * conj;
				double val = std::abs(conj.real());
				if (val > maxHeight)
					maxHeight = val;
				if (val < minHeight)
					minHeight = val;

				heightField[insertIdx++] = val;
			}
		}

		// TODO: eliminate
		h = new uint16_t[nPixels];
		//	float* h = new float[width * height];

		double heightRange = maxHeight - minHeight;
//		minHeight = DBL_MAX;
//		maxHeight = DBL_MIN;
//		shadowVertIdx = 0;
		for (uint32_t i = 0; i < nPixels; ++i)
		{
//			const uint32_t zC = (i / nPixelsY);
//			const uint32_t xC = i - (zC * nPixelsX);
			double normalizedHeight = heightField[i] / heightRange; // floor?
//			double maxHeight = static_cast<double>(heightScale);
			h[i] = static_cast<uint16_t>(normalizedHeight * 65535.0);

//			heightField[i] /= heightRange;
//			heightField[i] *= heightScale;

			//if (heightField[i] < minHeight)
			//	mMinHeight = heightField[i];
			//if (heightField[i] > maxHeight)
			//	mMaxHeight = heightField[i];

//			if (zC % 8 == 0 && xC % 8 == 0)
//			{
////				shadowVerts[shadowVertIdx] = static_cast<float>(heightField[i]);
//				shadowVerts[shadowVertIdx].position.y = static_cast<float>(heightField[i]);
//				shadowVertIdx++;
//			}
		}

//	};

//	auto waiter = ThreadArena::submit(fft_func);
//	waiter.wait();

	//Image hmTex;
	//hmTex.setTexFilter(QTEXTURE_FILTER_LINEAR | QTEXTURE_CLAMP);
	//hmTex.loadTexture(width, height, QTEXTURE_FORMAT_R16, true, (void*)h);
	//hmTex.heightToNormal();
	//heightmapTexture = hmTex.getOpenGlid();
	//normalmapTexture = hmTex.getNormalOpenGlid();
	mInitializer.heightField = h;
	mMesh = generateHeightmapTerrainMesh(mInitializer, mName);
	if (!mMesh)
	{
		QUADRION_CRITICAL("Could not generate heightmap terrain mesh");
		delete[] h;
		return false;
	}

	delete[] h;

	vec2<double> worldMaxs = mInitializer.worldMins + vec2<double>(mInitializer.nPatchesX * mInitializer.patchWidth,
																   mInitializer.nPatchesZ * mInitializer.patchDepth);
	vec2<double> worldSpan = worldMaxs - mInitializer.worldMins;
	vec2<double> worldCenter = mInitializer.worldMins + (0.5 * worldSpan);
	vec3<float> fWorldCenter(static_cast<float>(worldCenter.x), 0.0f, static_cast<float>(worldCenter.y));
	vec3<float> fWorldMins(static_cast<float>(mInitializer.worldMins.x), 0.0f, static_cast<float>(mInitializer.worldMins.y));

	mMeshNode = std::make_shared<MeshNodeRod>();
	mMeshNode->center = fWorldCenter;
	mMeshNode->mesh = mMesh;
	mMeshNode->position = fWorldMins;
	mMeshNode->scale = vec3<float>(1.0f, 1.0f, 1.0f);

	uint32_t renderObjID = QGfx::createRenderObjectID();
	std::string name = mName;
	name.append("RO");
	std::vector<std::shared_ptr<MeshNodeRod>> meshes;
	meshes.push_back(mMeshNode);
	mRenderObject = std::make_shared<RenderObject>(name.c_str(), meshes, renderObjID);


//	Image perlinTex;
//	perlinTex.setTexFilter(QTEXTURE_FILTER_LINEAR);
//	perlinTex.loadTexture("perlin_noise.png", true, "Media/Textures/");
//	uint32_t perlinWidth = perlinTex.getWidth();
//	uint32_t perlinHeight = perlinTex.getHeight();
//	uint8_t* dat = perlinTex.getData();
//	perlinNoise = new float[perlinWidth * perlinHeight];
//
//	for(uint32_t j = 0; j < perlinHeight * perlinWidth; ++j)
//	{
//		perlinNoise[j] = (*dat) / 255.0f;	
//		dat += 3;
//	}
//	
//	meshMins.y = static_cast<float>(mMinHeight);
//	meshMaxs.y = static_cast<float>(mMaxHeight);
//	std::vector<double> heights;
//
//	const uint32_t nSamplesX = numSamplesX;
//	const uint32_t nSamplesZ = numSamplesZ;
////	const vec3<float> meshMins = meshMins;
//	const double meshWidth = meshMaxs.x - meshMins.x;
//	const double meshDepth = meshMaxs.z - meshMins.z;
//	const uint32_t nPatchesX = numPatchesX;
//	const uint32_t nPatchesZ = numPatchesZ;
////	const uint32_t vertexStride = vertexStride;
//	const uint32_t nVertsPerPatch = numVertsPerPatch;
//
//	vec2<float> curHeightPos(meshMins.x, meshMins.z);
//	vec2<int32_t> curHeightIndex(0, 0);
//	vec2<float> heightDiv(static_cast<float>(meshWidth) / (nSamplesX - 1), 
//						  static_cast<float>(meshDepth) / (nSamplesZ - 1));
//
//	patchList.reserve(nPatchesX * nPatchesZ);
//
//	// Generate Patch Vertices //
//	for(uint32_t z = 0; z < numPatchesZ; ++z)
//	{
//		for(uint32_t x = 0; x < numPatchesX; ++x)
//		{
//			MeshPatch* curPatch = new MeshPatch;
//			curPatch->physicsVertexStride = 3;
//			curPatch->vertexStride = vertexStride;
//			const double patchDimsX = meshWidth / nPatchesX;
//			const double patchDimsZ = meshDepth / nPatchesZ;
//			curPatch->patchDims.set(static_cast<float>(patchDimsX), 
//								    static_cast<float>(patchDimsZ));
//
//			curPatch->mins.x = meshMins.x + (x * static_cast<float>(patchDimsX));
//			curPatch->mins.z = meshMins.z + (z * static_cast<float>(patchDimsZ));
//			curPatch->mins.y = 0.0f;
//
//			curPatch->maxs.x = curPatch->mins.x + static_cast<float>(patchDimsX);
//			curPatch->maxs.z = curPatch->mins.z + static_cast<float>(patchDimsZ);
//			curPatch->maxs.y = 0.0f;
//
//			curPatch->center.x = static_cast<float>(curPatch->mins.x + (patchDimsX * 0.5f));
//			curPatch->center.y = 0.0f;
//			curPatch->center.z = static_cast<float>(curPatch->mins.z + (patchDimsZ * 0.5f));
//			curPatch->boundingRad = static_cast<float>(sqrt(pow(patchDimsX * 0.5, 2.0f) + pow(patchDimsZ * 0.5, 2.0)));
//
//			// Find min point at patch //
//			vec2<float> heightStart(meshMins.x, meshMins.z);
//			vec2<int32_t> heightIndexStart(0, 0);
//			for(uint32_t c = 0; c < nSamplesX; ++c)
//			{
//				if(heightStart.x + heightDiv.x >= curPatch->mins.x)
//					break;
//
//				heightStart.x += heightDiv.x;
//				heightIndexStart.x++;
//			}
//
//			for(uint32_t c = 0; c < numSamplesZ; ++c)
//			{
//				if (heightStart.y + heightDiv.y >= curPatch->mins.z)
//					break;
//
//				heightStart.y += heightDiv.y;
//				heightIndexStart.y++;
//			}
//
//			curHeightPos.x = heightStart.x;
//			curHeightPos.y = heightStart.y;
//			curHeightIndex.x = heightIndexStart.x;
//			curHeightIndex.y = heightIndexStart.y;
//
//			curPatch->numVertsX = numVertsPerPatch;
//			curPatch->numVertsZ = numVertsPerPatch;
//			curPatch->numIndices = (curPatch->numVertsX - 1) * (curPatch->numVertsZ - 1) * 6;
//			curPatch->numVerts = curPatch->numVertsX * curPatch->numVertsZ;
//			curPatch->numPhysicsVerts = curPatch->numVerts;
//
////			curPatch->verts = new float[curPatch->numVertsX * 
////									    curPatch->numVertsZ * 
////									    curPatch->vertexStride];
//			curPatch->verts = new Vertex[curPatch->numVertsX * curPatch->numVertsZ];
//
//			if(x == 0 && z == 0)
//			{
//				baseVerts = new Vertex[curPatch->numVertsX * curPatch->numVertsZ];
//			}
//
//			curPatch->physicsVerts = new Vertex[curPatch->numVertsX * curPatch->numVertsZ]; 
//											   
//
//			curPatch->indices = new uint32_t[(curPatch->numVertsX - 1) * 
//											 (curPatch->numVertsZ - 1) * 6];
//
//			double xDiv = curPatch->patchDims.x / (curPatch->numVertsX - 1);
//			double zDiv = curPatch->patchDims.y / (curPatch->numVertsZ - 1);
//			uint32_t vIdx = 0;
//			uint32_t baseVIdx = 0;
//			uint32_t vPhysicsIdx = 0;
//			uint32_t iIdx = 0;
//
//			double xVal = curPatch->mins.x;
//			double zVal = curPatch->mins.z;
//			float baseXVal = 0.0f;
//			float baseZVal = 0.0f;
//			float u, v;
//			float patchMin = 99999.0f;
//			float patchMax = -99999.0f;
//			for(uint32_t j = 0; j < nVertsPerPatch; ++j)
//			{
//				for(uint32_t i = 0; i < nVertsPerPatch; ++i)
//				{
//					vec3<float> pos(static_cast<float>(xVal), 
//									0.0f, 
//									static_cast<float>(zVal));
//
//					// Adjust height sample //
//					while (xVal >= curHeightPos.x + heightDiv.x)
//					{
//						curHeightPos.x += heightDiv.x;
//						curHeightIndex.x++;
//					}
//
//					u = static_cast<float>((xVal + (meshWidth / 2.0f)) / meshWidth);
//					v = static_cast<float>((zVal + (meshDepth / 2.0f)) / meshDepth);
//
//					vec2<int32_t> ul(curHeightIndex.x, curHeightIndex.y);
//					vec2<int32_t> ur(curHeightIndex.x + 1, curHeightIndex.y);
//					vec2<int32_t> ll(curHeightIndex.x, curHeightIndex.y + 1);
//					vec2<int32_t> lr(curHeightIndex.x + 1, curHeightIndex.y + 1);
//					double yVal = interpHeightFromPos(pos, ll, lr, ur, ul);
////					double yVal = 0.0;
//					if(yVal < patchMin)
//						patchMin = static_cast<float>(yVal);
//					if(yVal > patchMax)
//						patchMax = static_cast<float>(yVal);
//
////					curPatch->verts[vIdx++] = static_cast<float>(xVal);
////					curPatch->verts[vIdx++] = 0.0f;
////					curPatch->verts[vIdx++] = static_cast<float>(zVal);
////					curPatch->verts[vIdx++] = u;
////					curPatch->verts[vIdx++] = v;
//					curPatch->verts[vIdx].position = vec3<float>((float)xVal, 0.0f, (float)zVal);
//					curPatch->verts[vIdx].uv = vec2<float>(u, v);
//					vIdx++;
//
//					if(x == 0 && z == 0)
//					{
//						baseVerts[baseVIdx].position = vec3<float>(baseXVal, 0.0f, baseZVal);
//						baseVerts[baseVIdx].uv = vec2<float>(u, v);
//						baseVIdx++;
//	//					baseVerts[baseVIdx++] = baseXVal;
////						baseVerts[baseVIdx++] = 0.0f;
////						baseVerts[baseVIdx++] = baseZVal;
////						baseVerts[baseVIdx++] = u;
////						baseVerts[baseVIdx++] = v;
//					}
//
////					curPatch->physicsVerts[vPhysicsIdx++] = static_cast<float>(xVal);
////					curPatch->physicsVerts[vPhysicsIdx++] = static_cast<float>(yVal);
////					curPatch->physicsVerts[vPhysicsIdx++] = static_cast<float>(zVal);
//					curPatch->physicsVerts[vPhysicsIdx].position = vec3<float>((float)xVal, (float)yVal, (float)zVal);
//					//vPhysicsIdx;
//
//					if (j > numVertsPerPatch - 2 || i > numVertsPerPatch - 2)
//					{
//						xVal += xDiv;
//						baseXVal += static_cast<float>(xDiv);
//						continue;
//					}
//
//					uint32_t origin = (j * numVertsPerPatch) + i;
//					curPatch->indices[iIdx++] = origin;
//					curPatch->indices[iIdx++] = origin + numVertsPerPatch;
//					curPatch->indices[iIdx++] = origin + 1;
//					curPatch->indices[iIdx++] = origin + numVertsPerPatch;
//					curPatch->indices[iIdx++] = origin + numVertsPerPatch + 1;
//					curPatch->indices[iIdx++] = origin + 1;
//
//					xVal += xDiv;
//					baseXVal += static_cast<float>(xDiv);
//				}
//
//				zVal += zDiv;
//				baseZVal += static_cast<float>(zDiv);
//				while (zVal >= curHeightPos.y + heightDiv.y)
//				{
//					curHeightPos.y += heightDiv.y;
//					curHeightIndex.y++;
//				}
//
//				curHeightPos.x = heightStart.x;
//				curHeightIndex.x = heightIndexStart.x;
//				xVal = curPatch->mins.x;
//				baseXVal = 0.0f;
//			}
//
//			curPatch->mins.y = patchMin;
//			curPatch->maxs.y = patchMax;
//			patchList.push_back(curPatch);
//		}
//
//	}
//
//	uint32_t nPatches = static_cast<uint32_t>(patchList.size());
//	uint32_t patchIdx;
//	for (uint32_t z = 0; z < numPatchesZ; ++z)
//	{
//		for (uint32_t x = 0; x < numPatchesX; ++x)
//		{
//			if (x == 0 && z == 0)
//			{
//				VertexBuffer* baseVBO = new VertexBuffer(baseVerts,
//													     patchList[patchIdx]->numVerts * sizeof(Vertex));
//				VertexBufferLayout baseLayout;
//				baseLayout.push<vec3<float>>("iPosition");
//				baseLayout.push<vec2<float>>("iUV");
//				baseVBO->setLayout(baseLayout);
//
//				IndexBuffer* baseIBO = new IndexBuffer(patchList[patchIdx]->indices,
//													   patchList[patchIdx]->numIndices,
//													   GL_TRIANGLES);
//
//				VertexArray* baseVAO = new VertexArray(baseVBO, baseIBO);
//				baseMesh = new Mesh(baseVAO, baseIBO);
//			}
//
//			patchIdx = numPatchesX * z + x;
//			VertexBuffer* vbo = new VertexBuffer(patchList[patchIdx]->verts, 
//												 patchList[patchIdx]->numVerts * sizeof(Vertex));
//			VertexBufferLayout layout;
//			layout.push<vec3<float>>("iPosition");
//			layout.push<vec2<float>>("iUV");
//			vbo->setLayout(layout);
//
//			IndexBuffer* ibo = new IndexBuffer(patchList[patchIdx]->indices, 
//											   patchList[patchIdx]->numIndices,
//											   GL_TRIANGLES);
//
//			VertexArray* vao = new VertexArray(vbo, ibo);
//			patchList[patchIdx]->mesh = new Mesh(vao, ibo);
//		}
//	}
//
//	VertexBuffer* shadowVBO = new VertexBuffer(shadowVerts, nShadowVertsX * nShadowVertsZ * sizeof(Vertex));
//	VertexBufferLayout shadowLayout;
//	shadowLayout.push<vec3<float>>("iPosition");
//	shadowVBO->setLayout(shadowLayout);
//
//	IndexBuffer* shadowIBO = new IndexBuffer(shadowIndices, 
//											 (nShadowVertsX - 1) * (nShadowVertsZ - 1) * 6,
//											 GL_TRIANGLES);
//	VertexArray* shadowVAO = new VertexArray(shadowVBO, shadowIBO);
//
//	shadowMesh = new Mesh(shadowVAO, shadowIBO);

//	shadowTerrainVBO = createIndexedInterleavedArray(&shadowVertAttribs, 1, shadowVerts, 
//													  sizeof(float) * nShadowVertsX * nShadowVertsZ * 3, 
//													  BUFFER_STATIC, 
//													  shadowIndices, (nShadowVertsX - 1) * (nShadowVertsZ - 1) * 6);

//	delete[] shadowVerts;
//	delete[] shadowIndices;

	return true;
}
//
///*
//void ProceduralMeshAsset::packRenderComponent(MeshRenderComponent* rc, uint32_t patchIdx)
//{
//	if(patchIdx < 0 || patchList.size() <= 0 || !rc)
//		return;
//
//	MeshPatch* patch = patchList[patchIdx];
//
//	rc->setPrimitiveType(TRIANGLES);
//
//	rc->addVertexList(patch->verts, patch->numVerts / patch->vertexStride, patch->vertexStride);
//	rc->addIndexList(patch->indices, patch->numIndices);
//					
//	vec3<float> patchSpan = patch->maxs - patch->mins;
//	patchSpan *= 0.5;
//
//	vec3<float> center = patch->mins + patchSpan;
//	vec3<float> centerF(static_cast<float>(center.x), static_cast<float>(center.y), static_cast<float>(center.z));
//
//	rc->setCentroid(centerF);
//	rc->setRenderHandle(patch->patchVAO);
//}
//
//void ProceduralMeshAsset::packMeshInstances(InstancedMeshRenderComponent* rc)
//{
//	if(!rc || patchList.size() <= 0)
//		return;
//		
//	uint32_t nVerts = numVertsPerPatch;
//	MeshPatch* bp = patchList[0];
//	rc->setPrimitiveType(TRIANGLES);
//	rc->addVertexList(baseVerts, nVerts, bp->vertexStride);
//	rc->addIndexList(bp->indices, bp->numIndices);
//
//	// TODO: Potential Problem //
//	rc->setCentroid(vec3<float>(0.0f, 0.0f, 0.0f));
//	rc->setRenderHandle(basePatchVBO);
//
//	uint32_t nPatches = static_cast<uint32_t>(patchList.size());
//	vec3<float>* instances = new vec3<float>[nPatches];
//	for(uint32_t i = 0; i < nPatches; ++i)
//	{
//		bp = patchList[i];
//		instances[i] = bp->center;
//	}
//
//	rc->addInstances(instances, nPatches);
//	delete[] instances;
//}
//
//// Only packs trees currently //
//void ProceduralMeshAsset::packPropInstances(InstancedMeshRenderComponent* rc)
//{
//	if (!rc)
//		return;
//
////	uint32_t nVerts = numVertsPerPatch;
////	MeshPatch* bp = patchList[0];
//	rc->setPrimitiveType(0);
//	rc->addVertexList(nullptr, 0, 0);
//	rc->addIndexList(nullptr, 0);
//
//	// TODO: Potential Problem //
//	rc->setCentroid(vec3<float>(0.0f, 0.0f, 0.0f));
//	rc->setRenderHandle(0);
//
//	vec3<float>* instances = new vec3<float>[MAX_TREES];
//	for (uint32_t i = 0; i < MAX_TREES; ++i)
//	{
////		bp = patchList[i];
//		instances[i].x = propInstances[i].x;
//		instances[i].y = propInstances[i].y;
//		instances[i].z = propInstances[i].z;
//	}
//
//	rc->addInstances(instances, MAX_TREES);
//	delete[] instances;
//}
//*/
//void ProceduralMeshAsset::setNumPatchesX(uint32_t aX)
//{
//	if (aX <= 0)
//		return;
//
//	numPatchesX = aX;
//}
//
//void ProceduralMeshAsset::setNumPatchesZ(uint32_t aZ)
//{
//	if (aZ <= 0)
//		return;
//
//	numPatchesZ = aZ;
//}
//
//void ProceduralMeshAsset::setNumVertsPerPatch(uint32_t aNumVerts)
//{
//	if (aNumVerts < 2)
//		return;
//
//	numVertsPerPatch = aNumVerts;
//}
//
//void ProceduralMeshAsset::setTerrainMins(vec2<float> aMins)
//{
//	meshMins.set(aMins.x, 0.0f, aMins.y);
//}
//
//void ProceduralMeshAsset::setTerrainMaxs(vec2<float> aMaxs)
//{
//	meshMaxs.set(aMaxs.x, 0.0f, aMaxs.y);
//}
//
//void ProceduralMeshAsset::setNumPropPatches(uint32_t aNumX, uint32_t aNumZ)
//{
//	numPropPatchesX = aNumX;
//	numPropPatchesZ = aNumZ;
//	uint32_t nPatches = aNumX * aNumZ;
//	uint32_t idx = 0;
//
//	vec3<float> meshSpan = meshMaxs - meshMins;
//	double patchWidth = meshSpan.x / aNumX;
//	double patchHeight = meshSpan.z / aNumZ;
//
//	pMeshPropPartition->numPropPatches = nPatches;
//	pMeshPropPartition->numPropPatchesX = aNumX;
//	pMeshPropPartition->numPropPatchesZ = aNumZ;
//	pMeshPropPartition->propPatchList = new MeshPropPatch*[nPatches];
//
//	for (uint32_t j = 0; j < aNumZ; ++j)
//	{
//		for (uint32_t i = 0; i < aNumX; ++i)
//		{
//			vec3<float> patchMins(meshMins.x + (i * static_cast<float>(patchWidth)),
//								  0.0,
//								  meshMins.z + (j * static_cast<float>(patchHeight)));
//
//			MeshPropPatch* curPatch = new MeshPropPatch;
//			curPatch->patchWidth = patchWidth;
//			curPatch->patchHeight = patchHeight;
//			curPatch->patchMins = patchMins;
//
//			curPatch->patchCenter.set(curPatch->patchMins.x + 
//									  (static_cast<float>(curPatch->patchWidth) * 0.5f),
//									   0.0,
//									   curPatch->patchMins.z + 
//									   (static_cast<float>(curPatch->patchHeight) * 0.5f));
//
//			pMeshPropPartition->propPatchList[idx++] = curPatch;
//		}
//	}
//}
//
//void ProceduralMeshAsset::setHeightScale(const float aScale)
//{
//	heightScale = aScale;
//}
//
//void ProceduralMeshAsset::setXScale(const float aX)
//{
//	xScale = aX;
//}
//
//void ProceduralMeshAsset::setZScale(const float aZ)
//{
//	zScale = aZ;
//}
//
//
//uint32_t ProceduralMeshAsset::getNbVertices(uint32_t aPatchIdx)
//{
//	if (aPatchIdx < 0 || aPatchIdx > patchList.size())
//		return 0;
//	return patchList[aPatchIdx]->numPhysicsVerts;
//}
//
//uint32_t ProceduralMeshAsset::getNbIndices(uint32_t aPatchIdx)
//{
//	return patchList[aPatchIdx]->numIndices;
//}
//
//
//uint32_t* ProceduralMeshAsset::getIndices(uint32_t aPatchIdx)
//{
//	return patchList[aPatchIdx]->indices;
//}
//
//uint32_t ProceduralMeshAsset::getNumPatches()
//{
//	return static_cast<uint32_t>(patchList.size());
//}
//
//
//uint32_t ProceduralMeshAsset::getNumPhysicsPatches()
//{
//	return static_cast<uint32_t>(patchList.size());
//}
//
//Vertex* ProceduralMeshAsset::getPhysicsPatchVerts(uint32_t aPatchIdx)
//{
//	if (aPatchIdx < 0 || aPatchIdx > patchList.size())
//		return nullptr;
//
//	return patchList[aPatchIdx]->physicsVerts;
//}
//
//const MeshPatch* const	ProceduralMeshAsset::getPatch(uint32_t aPatchIdx)
//{
//	if (aPatchIdx < 0 || aPatchIdx >= patchList.size())
//		return NULL;
//
//	return patchList[aPatchIdx];
//}
//
//
//uint32_t ProceduralMeshAsset::getVertexStride()
//{
//	return vertexStride;
//}
//
//uint32_t ProceduralMeshAsset::getPhysicsVertexStride()
//{
//	if (patchList.size() > 0)
//		return patchList[0]->physicsVertexStride;
//
//	return 0;
//}
//
//float ProceduralMeshAsset::getHeightScale()
//{
//	return heightScale;
//}
//
//void ProceduralMeshAsset::getMeshBounds(vec3<float>& aMins, vec3<float>& aMaxs)
//{
//	aMins.set(meshMins);
//	aMaxs.set(meshMaxs);
//}
//
//
//float ProceduralMeshAsset::getXScale()
//
//{
//	return xScale;
//}
//
//float ProceduralMeshAsset::getZScale()
//{
//	return zScale;
//}
//
//void ProceduralMeshAsset::getAdjacentPropPatchList(uint32_t aCurIdx,
//												   int32_t* aAdjPatches)
//{
//	if (!aAdjPatches)
//		return;
//
//	uint32_t zIdx = aCurIdx / numPropPatchesX;
//	uint32_t xIdx = aCurIdx - (zIdx * numPropPatchesX);
//
//	// Get Left //
//	if (xIdx >= 1)
//		aAdjPatches[0] = aCurIdx - 1;
//	else
//		aAdjPatches[0] = -1;
//
//	// Get Right //
//	if (xIdx < numPropPatchesX - 1)
//		aAdjPatches[1] = aCurIdx + 1;
//	else
//		aAdjPatches[1] = -1;
//
//	// Get Top //
//	if (zIdx >= 1)
//		aAdjPatches[2] = aCurIdx - numPropPatchesX;
//	else
//		aAdjPatches[2] = -1;
//
//	// Get Bottom //
//	if (zIdx < numPropPatchesZ - 1)
//		aAdjPatches[3] = aCurIdx + numPropPatchesX;
//	else
//		aAdjPatches[3] = -1;
//
//	// Get Top Right //
//	if ((xIdx < numPropPatchesX - 1) && (zIdx >= 1))
//		aAdjPatches[4] = (aCurIdx - numPropPatchesX) + 1;
//	else
//		aAdjPatches[4] = -1;
//
//	// Get Top Left //
//	if ((xIdx >= 1) && (zIdx >= 1))
//		aAdjPatches[5] = (aCurIdx - numPropPatchesX) - 1;
//	else
//		aAdjPatches[5] = -1;
//
//	// Get Lower Left //
//	if ((xIdx >= 1) && (zIdx < numPropPatchesZ - 1))
//		aAdjPatches[6] = (aCurIdx + numPropPatchesX) - 1;
//	else
//		aAdjPatches[6] = -1;
//
//	// Get Lower Right //
//	if ((xIdx < numPropPatchesX - 1) && (zIdx < numPropPatchesZ - 1))
//		aAdjPatches[7] = (aCurIdx + numPropPatchesX) + 1;
//	else
//		aAdjPatches[7] = -1;
//}
//
//
//double ProceduralMeshAsset::getElevationFromPos(vec3<float> aPos)
//{
//	// Map from (-1, 1) to (0, 1) //
//	double lenX = (aPos.x * 2.0 + xScale) / (xScale * 2.0);
//	double lenZ = (aPos.z * 2.0 + zScale) / (zScale * 2.0);
//
//	uint32_t xIdx = static_cast<uint32_t>(floor(lenX * (numSamplesX - 1)));
//	uint32_t yIdx = static_cast<uint32_t>(floor(lenZ * (numSamplesZ - 1)));
//
//	vec2<int32_t> ul(xIdx, yIdx);
//	vec2<int32_t> ur(xIdx + 1, yIdx);
//	vec2<int32_t> ll(xIdx, yIdx + 1);
//	vec2<int32_t> lr(xIdx + 1, yIdx + 1);
//
//	double elev = interpHeightFromPos(aPos, ll, lr, ur, ul);
//	return elev;
//}
//
//uint32_t ProceduralMeshAsset::getPatchIndex(vec3<float> aPos)
//{
//	if (aPos.x < meshMins.x || aPos.x > meshMaxs.x ||
//		aPos.z < meshMins.z || aPos.z > meshMaxs.z)
//		return 0;
//
//	// Map from (-1, 1) to (0, 1) //
////	float lenX = (pos.x + impl->xScale) / (impl->xScale * 2.0);
//	double lenX = (aPos.x * 2.0 + xScale) / (xScale * 2.0);
//	float xPatch = static_cast<float>(floor(lenX * numPatchesX));
//
//	//	float lenZ = (pos.z + impl->zScale) / (impl->zScale * 2.0);
//	double lenZ = (aPos.z * 2.0 + zScale) / (zScale * 2.0);
//	float zPatch = static_cast<float>(floor(lenZ * numPatchesZ));
//
//	uint32_t idx = static_cast<uint32_t>((zPatch * numPatchesX) + xPatch);
//
//	return idx;
//}
//
//uint32_t ProceduralMeshAsset::getPropPatchIndex(vec3<float> aPos)
//{
//	if (aPos.x < meshMins.x || aPos.x > meshMaxs.x ||
//		aPos.z < meshMins.z || aPos.z > meshMaxs.z)
//		return 0;
//
//	// Map from (-1, 1) to (0, 1) //
////	float lenX = (pos.x + impl->xScale) / (impl->xScale * 2.0);
//	double lenX = (aPos.x * 2.0 + xScale) / (xScale * 2.0);
//	float xPatch = static_cast<float>(floor(lenX * numPropPatchesX));
//
//	//	float lenZ = (pos.z + impl->zScale) / (impl->zScale * 2.0);
//	double lenZ = (aPos.z * 2.0 + zScale) / (zScale * 2.0);
//	float zPatch = static_cast<float>(floor(lenZ * numPropPatchesZ));
//
//	uint32_t idx = static_cast<uint32_t>((zPatch * numPropPatchesX) + xPatch);
//
//	return idx;
//}
//
//const MeshPropPatch* const ProceduralMeshAsset::getPatchFromPosition(vec3<float> aPos)
//{
//	if (aPos.x < meshMins.x || aPos.x > meshMaxs.x ||
//		aPos.z < meshMins.z || aPos.z > meshMaxs.z)
//		return nullptr;
//
//	// Map from (-1, 1) to (0, 1) //
//	//	float lenX = (pos.x + impl->xScale) / (impl->xScale * 2.0);
//	double lenX = (aPos.x * 2.0 + xScale) / (xScale * 2.0);
//	float xPatch = static_cast<float>(floor(lenX * pMeshPropPartition->numPropPatchesX));
//
//	//	float lenZ = (pos.z + impl->zScale) / (impl->zScale * 2.0);
//	double lenZ = (aPos.z * 2.0 + zScale) / (zScale * 2.0);
//	float zPatch = static_cast<float>(floor(lenZ * pMeshPropPartition->numPropPatchesZ));
//
//	uint32_t idx = static_cast<uint32_t>((zPatch * pMeshPropPartition->numPropPatchesX) + xPatch);
//
//	return pMeshPropPartition->getPatchFromIndex(idx);
//}
//
//uint32_t ProceduralMeshAsset::getNumVertsX()
//{
//	return numSamplesX;
//}
//
//uint32_t ProceduralMeshAsset::getNumVertsZ()
//{
//	return numSamplesZ;
//}
//
//vec2<float> ProceduralMeshAsset::getPatchDimensions(uint32_t aPatchIdx)
//{
//	vec2<float> nul(0.0, 0.0);
//	if (aPatchIdx < 0 || aPatchIdx > patchList.size() - 1)
//		return nul;
//
//	return patchList[aPatchIdx]->patchDims;
//}
//
//GLuint ProceduralMeshAsset::getHeightmapTextureID()
//{
//	return heightmapTexture;
//}
//
//GLuint ProceduralMeshAsset::getNormalmapTextureID()
//{
//	return normalmapTexture;
//}
//
//GLuint ProceduralMeshAsset::getShadowRenderHandle()
//{
////	return shadowTerrainVBO;
//	return shadowMesh->getVAO()->getID();
//}
//
//uint32_t ProceduralMeshAsset::getNumShadowIndices()
//{
//	return (numSamplesX - 1) * (numSamplesZ - 1) * 6;
//}
//
//
//
//uint32_t ProceduralMeshAsset::getNumVertices(uint32_t aPatchIdx)
//{
//	if(aPatchIdx < 0 || aPatchIdx > patchList.size())
//		return 0;
//
//	return patchList[aPatchIdx]->numVerts;
//}
//
//const Vertex* const ProceduralMeshAsset::getBaseVertices()
//{
//	return baseVerts;
//}
//
//uint32_t ProceduralMeshAsset::getNumSamplesX()
//{
//	return numSamplesX;
//}
//
//uint32_t ProceduralMeshAsset::getNumSamplesZ()
//{
//	return numSamplesZ;
//}
//
//FboId ProceduralMeshAsset::getTopDownRenderTarget()
//{
//	return topDownRT;
//}
//
//GLuint ProceduralMeshAsset::getTopDownTexture()
//{
//	return topDownTexture;
//}
//
//
//const MeshPropPatch* const ProceduralMeshAsset::getPropPatchFromIndex(uint32_t aPatchIdx)
//{
//	if (aPatchIdx < 0)
//		return nullptr;
//
//	return pMeshPropPartition->propPatchList[aPatchIdx];
//}
//
//
//GLuint ProceduralMeshAsset::getL0GrassTBO()
//{
//	return L0GrassTBO;
//}
//
//uint32_t ProceduralMeshAsset::getMaxGrassClumps()
//{
//	return MAX_GRASS_CLUMPS;
//}
//
///*
//GLuint ProceduralMeshAsset::getTreeTBO(uint32_t aPatchIdx)
//{
//	return impl->patchList[aPatchIdx]->treeTBO;
//}
//*/
//
//GLuint ProceduralMeshAsset::getGrassVAO()
//{
//	return grassVAO;
//}
//
//uint32_t ProceduralMeshAsset::getNumGrassIndices()
//{
//	return numGrassIndices;
//}
//
//double* ProceduralMeshAsset::getHeightField()
//{
//	return heightField;
//}
//
//double ProceduralMeshAsset::getMinHeight()
//{
//	return mMinHeight;
//}
//
//double ProceduralMeshAsset::getMaxHeight()
//{
//	return mMaxHeight;
//}
//
//uint32_t ProceduralMeshAsset::getNumTreeInstances()
//{
//	return MAX_TREES;
//}
//
//GLuint ProceduralMeshAsset::getTreeTBO()
//{
//	return L0TreeTBO;
//}
//
//bool ProceduralMeshAsset::copyTopDownBuf(void* aBuf, uint32_t aSize)
//{
//	if(!aBuf || aSize <= 0)
//		return false;
//
//	memcpy(topDownBuf, aBuf, aSize);
//	return true;
//}
//
//
//vec2<float> ProceduralMeshAsset::getHeightfieldPos(uint32_t aX, uint32_t aZ)
//{
//	const double meshXSpan = meshMaxs.x - meshMins.x;
//	const double meshZSpan = meshMaxs.z - meshMins.z;
//	const double xDiv = meshXSpan / (numSamplesX - 1);
//	const double zDiv = meshZSpan / (numSamplesZ - 1);
//
//	vec2<float> pos;
//	pos.x = meshMins.x + (aX * static_cast<float>(xDiv));
//	pos.y = meshMins.z + (aZ * static_cast<float>(zDiv));
//
//	return pos;
//}
//
//
//double ProceduralMeshAsset::interpHeightFromPos(vec3<float> aPos,
//	vec2<int32_t> aLowerLeft,
//	vec2<int32_t> aLowerRight,
//	vec2<int32_t> aUpperRight,
//	vec2<int32_t> aUpperLeft)
//{
//
//	if (aPos.x > meshMaxs.x || aPos.x < meshMins.x ||
//		aPos.z > meshMaxs.z || aPos.z < meshMins.z)
//		return 0.0;
//
//	vec3<float> meshSpan(meshMaxs.x - meshMins.x, 0.0, meshMaxs.z - meshMins.z);
//	double u = (aPos.x - meshMins.x) / meshSpan.x;
//	double v = (aPos.z - meshMins.z) / meshSpan.z;
//
//	int32_t px = static_cast<uint32_t>(floor(u * (numSamplesX - 1)));
//	int32_t py = static_cast<uint32_t>(floor(v * (numSamplesZ - 1)));
//	int32_t pIdx = numSamplesX * py + px;
//
//	double texelWidth = (1.0 / numSamplesX) * meshSpan.x;
//	double texelHeight = (1.0 / numSamplesZ) * meshSpan.z;
//	//	double pxr = ((px / (numSamplesX - 1)) * meshSpan.x) + meshMins.x;
//	double pxr = meshMins.x + ((px + 1) * texelWidth);
//	//	pxr += texelWidth;
//	double pxl = pxr - texelWidth;
//	double pxh = pxl + (0.5 * texelWidth);
//
//	//	double pxt = ((py / (numSamplesZ - 1)) * meshSpan.z) + meshMins.z;
//	double pxt = meshMins.z + (py * texelHeight);
//	double pxd = pxt + (texelHeight * 0.5);
//
//	int32_t rCoord = (px + 1 > static_cast<int32_t>(numSamplesX - 1)) ? px : px + 1;
//	int32_t lCoord = (px - 1 < 0) ? 0 : px - 1;
//	int32_t tCoord = (py - 1 < 0) ? 0 : py - 1;
//	int32_t bCoord = (py + 1 > static_cast<int32_t>(numSamplesZ - 1)) ? py : py + 1;
//
//	vec2<int32_t> r(rCoord, py);
//	vec2<int32_t> l(lCoord, py);
//	vec2<int32_t> t(px, tCoord);
//	vec2<int32_t> b(px, bCoord);
//	vec2<int32_t> ur(rCoord, tCoord);
//	vec2<int32_t> ul(lCoord, tCoord);
//	vec2<int32_t> ll(lCoord, bCoord);
//	vec2<int32_t> lr(rCoord, bCoord);
//
//	double sample = heightField[pIdx];
//	double rSample = heightField[getIndexFromPair(r, numSamplesX)];
//	double lSample = heightField[getIndexFromPair(l, numSamplesX)];
//	double bSample = heightField[getIndexFromPair(b, numSamplesX)];
//	double tSample = heightField[getIndexFromPair(t, numSamplesX)];
//	double urSample = heightField[getIndexFromPair(ur, numSamplesX)];
//	double ulSample = heightField[getIndexFromPair(ul, numSamplesX)];
//	double llSample = heightField[getIndexFromPair(ll, numSamplesX)];
//	double lrSample = heightField[getIndexFromPair(lr, numSamplesX)];
//
//	vec4<double> pos(pxh, 0.0, pxd, sample);
//	vec4<double> rPos(pxh + texelWidth, 0.0, pxd, rSample);
//	vec4<double> lPos(pxh - texelWidth, 0.0, pxd, lSample);
//	vec4<double> bPos(pxh, 0.0, pxd + texelHeight, bSample);
//	vec4<double> tPos(pxh, 0.0, pxd - texelHeight, tSample);
//	vec4<double> urPos(pxh + texelWidth, 0.0, pxd - texelHeight, urSample);
//	vec4<double> ulPos(pxh - texelWidth, 0.0, pxd - texelHeight, ulSample);
//	vec4<double> llPos(pxh - texelWidth, 0.0, pxd + texelHeight, llSample);
//	vec4<double> lrPos(pxh + texelWidth, 0.0, pxd + texelHeight, lrSample);
//
//	// get horizontal sample //
//	bool sampleRight = false;
//	bool sampleTop = true;
//	if (aPos.x > pxh)
//		sampleRight = true;
//	if (aPos.z > pxd)
//		sampleTop = false;
//
//	if (sampleRight && sampleTop)
//		return bilinearInterp(aPos, urPos, tPos, pos, rPos);
//	else if (!sampleRight && sampleTop)
//		return bilinearInterp(aPos, tPos, ulPos, lPos, pos);
//	else if (!sampleRight && !sampleTop)
//		return bilinearInterp(aPos, pos, lPos, llPos, bPos);
//	else
//		return bilinearInterp(aPos, rPos, pos, bPos, lrPos);
//}

std::shared_ptr<RenderObject> ProceduralMeshAsset::getRenderObject()
{
	return mRenderObject;
}
