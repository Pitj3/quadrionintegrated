#include "assets/TextureAsset.h"

#include "render/Image.h"

class TextureAssetImpl
{
	public:
		std::string					fileName;
};

TextureAsset::TextureAsset(const std::string& aName,
						   const std::string& aFileName) : Asset(aName)
{
	mImpl = new TextureAssetImpl();
	mImpl->fileName = aFileName.c_str();
	mTextureID = 0;
}

TextureAsset::~TextureAsset()
{
	delete mImpl;
}

bool TextureAsset::load()
{
	Image* imgPtr = new Image();
	imgPtr->setTexFilter(QTEXTURE_FILTER_TRILINEAR_ANISO | QTEXTURE_WRAP);
	const bool succeed = imgPtr->loadTexture(mImpl->fileName.c_str(), false, "", false);

	if (succeed)
	{
		mTextureID = imgPtr->getOpenGlid();
		mLoaded = true;
	}

	else
		mLoaded = false;

	delete imgPtr;
	return mLoaded;
}

