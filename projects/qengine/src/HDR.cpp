#include <cstdint>
#include "HDR.h"

/*
std::vector<float> weightTrap;


CHDRPipeline::CHDRPipeline()
{
	m_isInitialized = false;
	m_deltaTime = 0.0;

	m_depthTarget = 0;
	m_bUsingLogLuv = false;

	m_middleGrey = 0.15F;				// 0.15
	m_bloomScale = 4.0F;
	m_brightnessThreshold = 4.0F;
	m_brightnessOffset = 10.0F;
	m_adaptationFactor = 4.0F;			// 12
	m_finalGrey = 2.0F;					//2
										//	initialize();
}

CHDRPipeline::CHDRPipeline(unsigned int w, unsigned int h)
{
	m_framebufferWidth = w;
	m_framebufferHeight = h;

	m_isInitialized = false;
	m_deltaTime = 0.0;

	m_depthTarget = 0;
	m_bUsingLogLuv = false;

	m_middleGrey = 0.5F;				// 0.5
	m_bloomScale = 0.2F;				// 0.5
	m_brightnessThreshold = 8.0F;		// 6
	m_brightnessOffset = 10.0F;			// 10
	m_adaptationFactor = 24.0F;			// 24
	m_finalGrey = 5.5F;					// 3

	m_whitePoint = 1.0f;
	m_exposure = 1.0f;
}

CHDRPipeline::~CHDRPipeline()
{
	destroy();
}





/////////////////////////////////////////////////////////////
// initHDRPipeline
// Must be called prior to using the HDR pipeline to render
// Loads textures, effect, and rendertargets
bool CHDRPipeline::initialize()
{
	// do not re-initialize //
	if (m_isInitialized)
		return FALSE;

	// obtain a luminance format //
	//	SQuadrionDeviceCapabilities* caps = (SQuadrionDeviceCapabilities*)g_pRender->GetDeviceCapabilities();
	m_luminanceFormat = QTEXTURE_FORMAT_I32F;

	// keep crops divisible by 8 //
	uint32_t fbWidth = m_framebufferWidth;
	uint32_t fbHeight = m_framebufferHeight;
	m_cropWidth = fbWidth - fbWidth % 8;
	m_cropHeight = fbHeight - fbHeight % 8;

	unsigned int rttFlags = QTEXTURE_FILTER_LINEAR | QTEXTURE_CLAMP;
	unsigned int linFlag = QTEXTURE_FILTER_LINEAR | QTEXTURE_CLAMP;
	unsigned int texFlag = QTEXTURE_FILTER_LINEAR | QTEXTURE_CLAMP;
	ETexturePixelFormat hdr_format;
	ETexturePixelFormat luminance_format;
	bool msaa = false;

	hdr_format = QTEXTURE_FORMAT_RGBA16F;
	luminance_format = QTEXTURE_FORMAT_I32F;
	m_luminanceFormat = QTEXTURE_FORMAT_RGBA16F;
	m_bUsingLogLuv = false;
	depth_stencil_id no;
	no.depthID = m_depthTarget;
	no.stencilID = m_stencilTarget;

	m_sceneHDR = addRenderTarget(fbWidth, fbHeight, hdr_format, texFlag);
	m_sceneHDRFBO = createRenderableSurface(&m_sceneHDR, 1, no, fbWidth, fbHeight);

//	m_scaledHDRScene = addRenderTarget(m_cropWidth / 8, m_cropHeight / 8, hdr_format, texFlag);
//	m_scaledHDRSceneFBO = createRenderableSurface(&m_scaledHDRScene, 1, no, m_cropWidth / 8, m_cropHeight / 8);

	m_brightPass = addRenderTarget(m_cropWidth / 2 + 2, m_cropHeight / 2 + 2, hdr_format, texFlag);
	m_brightPassFBO = createRenderableSurface(&m_brightPass, 1, no, m_cropWidth / 2 + 2, m_cropHeight / 2 + 2);

	m_bloom = addRenderTarget(m_cropWidth / 8 + 2, m_cropHeight / 8 + 2, hdr_format, texFlag);
	m_bloomFBO = createRenderableSurface(&m_bloom, 1, no, m_cropWidth / 8 + 2, m_cropHeight / 8 + 2);

	// obtain temporary bloom textures for bloom accumulation //
	for (int i = 1; i < 3; ++i)
	{
		m_tempBloom[i] = addRenderTarget(m_cropWidth / 8 + 2, m_cropHeight / 8 + 2, hdr_format, texFlag);
		m_tempBloomFBO[i] = createRenderableSurface(&m_tempBloom[i], 1, no, m_cropWidth / 8 + 2, m_cropHeight / 8 + 2);
	}

	// obtain final bloom texture //
	m_tempBloom[0] = addRenderTarget(m_cropWidth / 8, m_cropHeight / 8, hdr_format, texFlag);
	m_tempBloomFBO[0] = createRenderableSurface(&m_tempBloom[0], 1, no, m_cropWidth / 8, m_cropHeight / 8);

	// obtain sequentially scaled down I32F or I16F luminance textures //
	for (int i = 0; i < 4; ++i)
	{
		int sampleLen = 1 << (2 * i);

		m_luminance[i] = addRenderTarget(sampleLen, sampleLen, m_luminanceFormat, texFlag);
		m_luminanceFBO[i] = createRenderableSurface(&m_luminance[i], 1, no, sampleLen, sampleLen);
	}

	// obtain adapted luminance target //
//	m_lumAdaptCur = addRenderTarget(1, 1, m_luminanceFormat, texFlag);
//	m_lumAdaptCurFBO = createRenderableSurface(&m_lumAdaptCur, 1, no, 1, 1);

	// obtain last adapted luminance target //
//	m_lumAdaptLast = addRenderTarget(1, 1, m_luminanceFormat, texFlag);
//	m_lumAdaptLastFBO = createRenderableSurface(&m_lumAdaptLast, 1, no, 1, 1);

	// obtain a scaled down version of the bloom texture //
	m_intermediateBloom = addRenderTarget(m_cropWidth / 8 + 2, m_cropHeight / 8 + 2, hdr_format, texFlag);
	m_intermediateBloomFBO = createRenderableSurface(&m_intermediateBloom, 1, no, m_cropWidth / 8 + 2, m_cropHeight / 8 + 2);

	// add effect //
	color_output colorOut;
	colorOut.outputLocation = 0;
	colorOut.outputSemantic = "outColor";
	attrib_loc attribs[2];
	attribs[0] = { LOC_POSITION, "iPosition" };
	attribs[1] = { LOC_TEXCOORD0, "iTexCoords" };

	m_pCalcAdaptEffect = new CQuadrionEffect;
	m_pCalcAdaptEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_calcadapt.frag", NULL, &colorOut, 1, attribs, 2);
	m_pSampleLumEffect = new CQuadrionEffect;
	m_pSampleLumEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_samplelum.frag", NULL, &colorOut, 1, attribs, 2);
	m_pResampleLumEffect = new CQuadrionEffect;
	m_pResampleLumEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_resamplelum.frag", NULL, &colorOut, 1, attribs, 2);
	m_pFinalLumEffect = new CQuadrionEffect;
	m_pFinalLumEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_finallum.frag", NULL, &colorOut, 1, attribs, 2);
	m_pBrightPassEffect = new CQuadrionEffect;
	m_pBrightPassEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/brightpass.frag", NULL, &colorOut, 1, attribs, 2);
	//	m_pGaussEffect = new CQuadrionEffect;
	//	m_pGaussEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_gauss.frag", NULL, &colorOut, 1, attribs, 2);
	m_pGaussEffect = new CQuadrionEffect;
	m_pGaussEffect->_create("Media/Effects/hdr_gauss.vert", "Media/Effects/hdr_gauss.frag", NULL, &colorOut, 1, attribs, 2);
	m_pDownScale2 = new CQuadrionEffect;
	m_pDownScale2->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_scale2.frag", NULL, &colorOut, 1, attribs, 2);
	m_pBloomEffect = new CQuadrionEffect;
	m_pBloomEffect->_create("Media/Effects/hdr_gauss.vert", "Media/Effects/hdr_bloom.frag", NULL, &colorOut, 1, attribs, 2);
	m_pDownScale4 = new CQuadrionEffect;
	m_pDownScale4->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_scale4.frag", NULL, &colorOut, 1, attribs, 2);
	m_pFinalEffect = new CQuadrionEffect;
	m_pFinalEffect->_create("Media/Effects/hdr_gauss.vert", "Media/Effects/hdr_final.frag", NULL, &colorOut, 1, attribs, 2);
	m_pReinhardEffect = new CQuadrionEffect;
	m_pReinhardEffect->_create("Media/Effects/fxaa.vert", "Media/Effects/hdr_reinhard.frag", NULL, &colorOut, 1, attribs, 2);

	// init variables //
	m_deltaTime = 0.0;
	m_isInitialized = true;

	return true;
}



//////////////////////////////////////////////////
// destroyHDRPipeline
VOID CHDRPipeline::destroy()
{
	if (!m_isInitialized)
		return;

	deleteRenderTarget(m_scaledHDRScene);
	deleteRenderTarget(m_brightPass);
	deleteRenderTarget(m_bloom);
	deleteRenderTarget(m_lumAdaptLast);
	deleteRenderTarget(m_lumAdaptCur);
	deleteRenderTarget(m_intermediateBloom);
	deleteRenderTarget(m_sceneHDR);

	deleteRenderableSurface(m_scaledHDRSceneFBO);
	deleteRenderableSurface(m_brightPassFBO);
	deleteRenderableSurface(m_bloomFBO);
	deleteRenderableSurface(m_lumAdaptLastFBO);
	deleteRenderableSurface(m_lumAdaptCurFBO);
	deleteRenderableSurface(m_intermediateBloomFBO);
	deleteRenderableSurface(m_sceneHDRFBO);

	for (int i = 0; i < 4; ++i)
	{
		deleteRenderTarget(m_luminance[i]);
		deleteRenderableSurface(m_luminanceFBO[i]);
	}
	for (int i = 0; i < 3; ++i)
	{
		deleteRenderTarget(m_tempBloom[i]);
		deleteRenderableSurface(m_tempBloomFBO[i]);
	}

	delete m_pCalcAdaptEffect;
	delete m_pSampleLumEffect;
	delete m_pResampleLumEffect;
	delete m_pFinalLumEffect;
	delete m_pBrightPassEffect;
	delete m_pGaussEffect;
	delete m_pDownScale2;
	delete m_pBloomEffect;
	delete m_pDownScale4;
	delete m_pFinalEffect;
	delete m_pReinhardEffect;

	m_isInitialized = false;
	m_adaptedTimer.Stop();
}



////////////////////////////////////////////////////////////////////////////
// calculateAdaptation
// Adaptive pass which uses a timer to determine light adaptation
// simulating human eye reaction to intense light sources.
// Uses 1x1 Luminance source texture and makes a pass into a 
// temporary texture downscaling the luminance value by the adaptive time

VOID CHDRPipeline::CalculateAdaptation()
{
	// we need to swap the last and current luminance textures for sampling //
	GLuint swapTex = m_lumAdaptLast;
	m_lumAdaptLast = m_lumAdaptCur;
	m_lumAdaptCur = swapTex;

	fbo_id swapFBO = m_lumAdaptLastFBO;
	m_lumAdaptLastFBO = m_lumAdaptCurFBO;
	m_lumAdaptCurFBO = swapFBO;

	// obtain an elapsed time in milliseconds //
	if (m_adaptedTimer.IsRunning)
	{
		m_deltaTime = m_adaptedTimer.GetElapsedSec();

		m_adaptedTimer.Stop();
		m_adaptedTimer.Start();

	}
	else
	{
		m_adaptedTimer.Start();
		m_deltaTime = 0.01F;
	}

	float fDeltaTime = (float)m_deltaTime;

	texture_rect rect;
	rect.leftU = 0.0F;
	rect.topV = 1.0F;
	rect.rightU = 1.0F;
	rect.bottomV = 0.0F;

	m_pCalcAdaptEffect->bind();
	m_pCalcAdaptEffect->gl_bindTexture("s0", m_lumAdaptLast);
	m_pCalcAdaptEffect->gl_bindTexture("s1", m_luminance[0]);
	m_pCalcAdaptEffect->bindFloat("g_adaptationFactor", (const float*)&m_adaptationFactor, 1);
	m_pCalcAdaptEffect->bindFloat("g_elapsedTime", (const float*)&fDeltaTime, 1);
	bindRenderableSurface(m_lumAdaptCurFBO);
	Render::renderFullscreenQuad(rect, 1, 1);
	bindDefaultSurface();
	m_pCalcAdaptEffect->evictTextures();
	m_pCalcAdaptEffect->unbind();
}



////////////////////////////////////////////////////////////////////////
//measureLuminance
//Luminance pass, takes the scaled floating point scene texture
//and subdivides it by 4 in each direction sampling and accumulating
//avg luminance until the dest texture is 1 pixel in size, representing
//the avg luminance for the scene

VOID CHDRPipeline::MeasureLuminance()
{
	//	SQuadrionDeviceCapabilities* caps = (SQuadrionDeviceCapabilities*)g_pRender->GetDeviceCapabilities();
	int x, y, index;
	vec2<float> sampleOffsets[16];
	int curTexture = 3;  // 4 - 1 

	uint32_t curTexWidth = 1 << (curTexture * 2);
	uint32_t curTexHeight = 1 << (curTexture * 2);

	// calculate texel sample offsets //
	float tu, tv;
	tu = 1.0F / (3.0F * curTexWidth);
	tv = 1.0F / (3.0F * curTexHeight);

	index = 0;

	// generate sample offsets //
	for (x = -1; x <= 1; ++x)
	{
		for (y = -1; y <= 1; ++y)
		{
			sampleOffsets[index].x = x * tu;
			sampleOffsets[index].y = y * tv;
			++index;
		}
	}

	// render via initial luminance pass //
	texture_rect rect;
	rect.leftU = 0.0F;
	rect.topV = 1.0F;
	rect.rightU = 1.0F;
	rect.bottomV = 0.0F;

	m_pSampleLumEffect->bind();
	m_pSampleLumEffect->gl_bindTexture("s0", m_sceneHDR);
	m_pSampleLumEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampleOffsets[0], 16);
	bindRenderableSurface(m_luminanceFBO[curTexture]);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(rect, curTexWidth, curTexHeight);
	bindDefaultSurface();
	m_pSampleLumEffect->evictTextures();
	m_pSampleLumEffect->unbind();


	--curTexture;

	// Now subdivide original scaled luminance texture down to 1 pixel //
	m_pResampleLumEffect->bind();
	while (curTexture > 0)
	{
		curTexWidth = 1 << ((curTexture + 1) * 2);
		curTexHeight = 1 << ((curTexture + 1) * 2);

		unsigned int lumCurWidth = 1 << (curTexture * 2);
		unsigned int lumCurHeight = 1 << (curTexture * 2);

		get4x4SampleOffsets(curTexWidth, curTexHeight, sampleOffsets);

		m_pResampleLumEffect->gl_bindTexture("s0", m_luminance[curTexture + 1]);
		m_pResampleLumEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampleOffsets[0], 16);
		bindRenderableSurface(m_luminanceFBO[curTexture]);
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Render::renderFullscreenQuad(rect, lumCurWidth, lumCurHeight);
		bindDefaultSurface();
		m_pResampleLumEffect->evictTextures();

		--curTexture;
	}
	m_pResampleLumEffect->unbind();





	// last pass to render final luminance to a 1 pixel I32F or I16F texture //
	curTexWidth = 1 << (2);
	curTexHeight = 1 << (2);
	get4x4SampleOffsets(curTexWidth, curTexHeight, sampleOffsets);

	m_pFinalLumEffect->bind();
	m_pFinalLumEffect->gl_bindTexture("s0", m_luminance[1]);
	m_pFinalLumEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampleOffsets[0], 16);
	bindRenderableSurface(m_luminanceFBO[0]);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenTexturedQuad();
	bindDefaultSurface();
	m_pFinalLumEffect->evictTextures();
	m_pFinalLumEffect->unbind();
}


//////////////////////////////////////////////////////////////////////
//brightPass
//Takes average luminance along with scaled HDR scene texture to
//perform a bright pass on the entire scene
VOID CHDRPipeline::BrightPass()
{
	uint32_t brightPassWidth = m_cropWidth / 2 + 2;
	uint32_t brightPassHeight = m_cropHeight / 2 + 2;
	uint32_t scaledSceneWidth = m_cropWidth;
	uint32_t scaledSceneHeight = m_cropHeight;

	Rect<float> src;
	vec2<float> srcDims;
	srcDims.x = (float)scaledSceneWidth;
	srcDims.y = (float)scaledSceneHeight;
	getTextureRect(srcDims, &src);
	inflateRect(&src, -1.0f, -1.0f);

	Rect<float> dest;
	vec2<float> destDims;
	destDims.x = (float)brightPassWidth;
	destDims.y = (float)brightPassHeight;
	getTextureRect(destDims, &dest);
	inflateRect(&dest, -1.0f, -1.0f);


	texture_rect coordRect;
	getTextureCoordinates(srcDims, &src, destDims, &dest, &coordRect);

	m_pBrightPassEffect->bind();
	m_pBrightPassEffect->gl_bindTexture("s0", m_sceneHDR);
	m_pBrightPassEffect->gl_bindTexture("s1", m_luminance[0]);
	m_pBrightPassEffect->bindFloat("g_whitePoint", (const float*)&m_whitePoint, 1);
	m_pBrightPassEffect->bindFloat("g_brightnessThreshold", (const float*)&m_brightnessThreshold, 1);
	m_pBrightPassEffect->bindFloat("g_brightnessOffset", (const float*)&m_brightnessOffset, 1);
	bindRenderableSurface(m_brightPassFBO);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(coordRect, brightPassWidth, brightPassHeight);
	bindDefaultSurface();
	m_pBrightPassEffect->evictTextures();
	m_pBrightPassEffect->unbind();
}



///////////////////////////////////////////////////////////////
//applyBloom
//Creates the source bloom texture by which the bloom texture
//will be used to _create 4 intermediate textures representing
//-x, x, -y, y samples of the original and will be stored
//in the final temporary bloom texture
VOID CHDRPipeline::ApplyBloom()
{
	vec2<float> sampOffsets[16];
	vec4<float> sampWeights[16];

	uint32_t intermediateBloomWidth = m_cropWidth / 8 + 2;
	uint32_t intermediateBloomHeight = m_cropHeight / 8 + 2;

	Rect<float> src, dest;
	texture_rect coords;
	vec2<float> destDims, srcDims;
	destDims.x = (float)intermediateBloomWidth;
	destDims.y = (float)intermediateBloomHeight;
	getTextureRect(destDims, &dest);
	inflateRect(&dest, -2.0f, -2.0f);

	getTextureCoordinates(srcDims, NULL, destDims, &dest, &coords);


	uint32_t brightSurfWidth = m_cropWidth / 8 + 2;
	uint32_t brightSurfHeight = m_cropHeight / 8 + 2;
	get5x5GaussianOffsets(brightSurfWidth, brightSurfHeight, sampOffsets, sampWeights, 1.0F);

	coords.leftU = 2.0f / brightSurfWidth;
	coords.rightU = 1.0f - (2.0f / brightSurfWidth);
	coords.bottomV = 2.0f / brightSurfHeight;
	coords.topV = 1.0f - (2.0f / brightSurfHeight);

	uint32_t ibw = intermediateBloomWidth;
	uint32_t ibh = intermediateBloomHeight;
	m_pGaussEffect->bind();
	m_pGaussEffect->gl_bindTexture("s0", m_brightPass);
	m_pGaussEffect->bindFloat4Array("g_staticSampleWeights[0]", &sampWeights[0], 16);
	m_pGaussEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampOffsets[0], 16);
	bindRenderableSurface(m_intermediateBloomFBO);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(coords, ibw, ibh);
	bindDefaultSurface();
	m_pGaussEffect->evictTextures();
	m_pGaussEffect->unbind();

	uint32_t bloomSurfWidth = m_cropWidth / 16 + 2;
	uint32_t bloomSurfHeight = m_cropHeight / 16 + 2;

	destDims.x = (float)bloomSurfWidth;
	destDims.y = (float)bloomSurfHeight;
	getTextureRect(destDims, &dest);
	inflateRect(&dest, -1.0f, -1.0f);
	srcDims.x = (float)intermediateBloomWidth;
	srcDims.y = (float)intermediateBloomHeight;
	getTextureRect(srcDims, &src);
	inflateRect(&src, -1.0f, -1.0f);

	getTextureCoordinates(srcDims, &src, destDims, &dest, &coords);
	QMATH_GET_SAMPLE2X2_OFFSETS(brightSurfWidth, brightSurfHeight, sampOffsets);

	m_pDownScale2->bind();
	m_pDownScale2->gl_bindTexture("s0", m_intermediateBloom);
	m_pDownScale2->bindFloat2Array("g_staticSampleOffsets[0]", &sampOffsets[0], 16);
	bindRenderableSurface(m_bloomFBO);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(coords, bloomSurfWidth, bloomSurfHeight);
	bindDefaultSurface();
	m_pDownScale2->evictTextures();
	m_pDownScale2->unbind();
}



////////////////////////////////////////////////////////////////
//renderBloom
//renders original bloom texture to 4 offsets and accumulates
//the result into the last temporary bloom texture
VOID CHDRPipeline::RenderBloom()
{
	vec2<float> sampleOffsets[16];
	vec4<float> sampleWeights[16];
	float fSampleOffsets[16];

	uint32_t tempBloomWidth1 = m_cropWidth / 16 + 2;
	uint32_t tempBloomHeight1 = m_cropHeight / 16 + 2;

	uint32_t tempBloomWidth2 = tempBloomWidth1;
	uint32_t tempBloomHeight2 = tempBloomHeight1;

	uint32_t bloomWidth = tempBloomWidth1;
	uint32_t bloomHeight = tempBloomHeight1;

	uint32_t bloomWidth0 = m_cropWidth / 16;
	uint32_t bloomHeight0 = m_cropHeight / 16;

	Rect<float> src;
	vec2<float> srcDims, destDims;
	srcDims.x = (float)bloomWidth;
	srcDims.y = (float)bloomHeight;
	getTextureRect(srcDims, &src);
	inflateRect(&src, -1.0f, -1.0f);

	Rect<float> dest;
	destDims.x = (float)tempBloomWidth2;
	destDims.y = (float)tempBloomHeight2;
	getTextureRect(destDims, &dest);
	inflateRect(&dest, -1.0f, -1.0f);

	texture_rect coords;
	getTextureCoordinates(srcDims, &src, destDims, &dest, &coords);
	get5x5GaussianOffsets(bloomWidth, bloomHeight, sampleOffsets, sampleWeights, 1.0F);

	coords.leftU = 2.0f / bloomWidth;
	coords.rightU = 1.0f - (2.0f / bloomWidth);
	coords.bottomV = 2.0f / bloomHeight;
	coords.topV = 1.0f - (2.0f / bloomHeight);

	m_pGaussEffect->bind();
	m_pGaussEffect->gl_bindTexture("s0", m_bloom);
	m_pGaussEffect->bindFloat4Array("g_staticSampleWeights[0]", &sampleWeights[0], 16);
	m_pGaussEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampleOffsets[0], 16);
	bindRenderableSurface(m_tempBloomFBO[2]);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(coords, tempBloomWidth2, tempBloomHeight2);
	bindDefaultSurface();
	m_pGaussEffect->evictTextures();
	m_pGaussEffect->unbind();


	getBloomOffsets(tempBloomWidth2, fSampleOffsets, sampleWeights, 3.0F, 2.0F);
	weightTrap.push_back(sampleWeights[0].x);

	for (int i = 0; i < 16; ++i)
	{
		sampleOffsets[i].x = fSampleOffsets[i];
		sampleOffsets[i].y = 0.0F;
	}

	coords.leftU = 2.0f / tempBloomWidth2;
	coords.rightU = 1.0f - (2.0f / tempBloomWidth2);
	coords.bottomV = 2.0f / tempBloomHeight2;
	coords.topV = 1.0f - (2.0f / tempBloomHeight2);

	m_pBloomEffect->bind();
	m_pBloomEffect->gl_bindTexture("s0", m_tempBloom[2]);
	m_pBloomEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampleOffsets[0], 16);
	m_pBloomEffect->bindFloat4Array("g_staticSampleWeights[0]", &sampleWeights[0], 16);
	bindRenderableSurface(m_tempBloomFBO[1]);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(coords, tempBloomWidth1, tempBloomHeight1);
	bindDefaultSurface();
	m_pBloomEffect->evictTextures();

	getBloomOffsets(tempBloomHeight1, fSampleOffsets, sampleWeights, 3.0F, 2.0F);
	for (INT i = 0; i < 16; ++i)
	{
		sampleOffsets[i].x = 0.0F;
		sampleOffsets[i].y = fSampleOffsets[i];
	}

	srcDims.x = (float)tempBloomWidth1;
	srcDims.y = (float)tempBloomHeight1;
	getTextureRect(srcDims, &src);
	inflateRect(&src, -1.0f, -1.0f);
	destDims.x = (float)bloomWidth0;
	destDims.y = (float)bloomHeight0;

	getTextureCoordinates(srcDims, &src, destDims, NULL, &coords);

	coords.leftU = 2.0f / tempBloomWidth1;
	coords.rightU = 1.0f - (2.0f / tempBloomWidth1);
	coords.bottomV = 2.0f / tempBloomHeight1;
	coords.topV = 1.0f - (2.0f / tempBloomHeight1);

	m_pBloomEffect->gl_bindTexture("s0", m_tempBloom[1]);
	m_pBloomEffect->bindFloat2Array("g_staticSampleOffsets[0]", &sampleOffsets[0], 16);
	m_pBloomEffect->bindFloat4Array("g_staticSampleWeights[0]", &sampleWeights[0], 16);
	bindRenderableSurface(m_tempBloomFBO[0]);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Render::renderFullscreenQuad(coords, bloomWidth0, bloomHeight0);
	bindDefaultSurface();
	m_pBloomEffect->evictTextures();
	m_pBloomEffect->unbind();
}




/////////////////////////////////////////////////////////////////////
//renderHDRScene
//Final pass for hDR scene, will store the result in the frame buffer
VOID CHDRPipeline::render(fbo_id dest)
{
	int isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

//	Rect<float> src;
//	uint32_t dWidth = m_framebufferWidth;
//	uint32_t dHeight = m_framebufferHeight;
//	src.l = (float)((dWidth - m_cropWidth)) / 2;
//	src.t = (float)((dHeight - m_cropHeight)) / 2;
//	src.r = src.l + (float)m_cropWidth;
//	src.b = src.t + (float)m_cropHeight;

//	texture_rect texRec;
//	vec2<float> srcDims, destDims;
//	srcDims.x = (float)m_framebufferWidth;
//	srcDims.y = (float)m_framebufferHeight;
//	getTextureCoordinates(srcDims, &src, destDims, NULL, &texRec);

//	vec2<float> avSamples[16];
//	uint32_t hdrWidth = (uint32_t)m_framebufferWidth;
//	uint32_t hdrHeight = (uint32_t)m_framebufferHeight;
//	get4x4SampleOffsets((const int32_t)hdrWidth, (const int32_t)hdrHeight, avSamples);

//	unsigned int scaledWidth, scaledHeight;
//	scaledWidth = m_cropWidth / 8;
//	scaledHeight = m_cropHeight / 8;

//	m_pDownScale4->bind();
//	m_pDownScale4->gl_bindTexture("s0", m_sceneHDR);
//	m_pDownScale4->bindFloat2Array("g_staticSampleOffsets[0]", &avSamples[0], 16);
//	bindRenderableSurface(m_scaledHDRSceneFBO);
//	glClearColor(0, 0, 0, 1);
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	Render::renderFullscreenQuad(texRec, scaledWidth, scaledHeight);
//	bindDefaultSurface();
//	m_pDownScale4->evictTextures();
//	m_pDownScale4->unbind();

	// _create luminance textures //
	MeasureLuminance();


	// _create adaptive luminance //
//	CalculateAdaptation();

	// Perform bright pass //
	BrightPass();


	// _create bloom source  from bright pass //
	ApplyBloom();


	// render bloom to texture or soften filter //
	RenderBloom();

	float dw, dh;
	dw = (float)m_framebufferWidth;
	dh = (float)m_framebufferHeight;

	texture_rect rect;
	rect.leftU = 0.0F;
	rect.topV = 1.0F;
	rect.rightU = 1.0F;
	rect.bottomV = 0.0F;
	if (dest.id <= 0 || dest.nColorTargets <= 0)
	{
		dw = (float)m_framebufferWidth;
		dh = (float)m_framebufferHeight;
		bindDefaultSurface();
	}

	else
	{
		dw = (float)dest.width;
		dh = (float)dest.height;
		bindRenderableSurface(dest);
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	uint32_t bloomWidth0 = m_cropWidth / 16;
	uint32_t bloomHeight0 = m_cropHeight / 16;
	rect.leftU = 2.0f / bloomWidth0;
	rect.rightU = 1.0f - (2.0f / bloomWidth0);
	rect.bottomV = 2.0f / bloomHeight0;
	rect.topV = 1.0f - (2.0f / bloomHeight0);

	m_pFinalEffect->bind();
	m_pFinalEffect->gl_bindTexture("s0", m_sceneHDR);
	m_pFinalEffect->gl_bindTexture("s1", m_tempBloom[0]);
	m_pFinalEffect->gl_bindTexture("s2", m_lumAdaptCur);
	m_pFinalEffect->bindFloat("g_bloomScale", (const float*)&m_bloomScale, 1);
	m_pFinalEffect->bindFloat("g_middleGrey", (const float*)&m_middleGrey, 1);
	m_pFinalEffect->bindFloat("g_finalGrey", (const float*)&m_finalGrey, 1);
	Render::renderFullscreenQuad(rect, (uint32_t)dw, (uint32_t)dh);
	bindDefaultSurface();
	m_pFinalEffect->evictTextures();
	m_pFinalEffect->unbind();

	if (isDepthEnabled)
		QGfx::enableDepthTest();

}


void CHDRPipeline::ClearTargets()
{

	CQuadrionRenderTarget* rt;
	//	rt = g_pRender->GetRenderTarget(m_sceneHDR);
	//	rt->Clear();
	rt = g_pRender->GetRenderTarget(m_scaledHDRScene);
	rt->Clear();
	for(int i = 0; i < 4; ++i)
	{
	rt = g_pRender->GetRenderTarget(m_luminance[i]);
	rt->Clear();
	}

	rt = g_pRender->GetRenderTarget(m_brightPass);
	rt->Clear();
	rt = g_pRender->GetRenderTarget(m_bloom);
	rt->Clear();

	for(int i = 0; i < 3; ++i)
	{
	rt = g_pRender->GetRenderTarget(m_tempBloom[i]);
	rt->Clear();
	}

	rt = g_pRender->GetRenderTarget(m_intermediateBloom);
	rt->Clear();
	
}

*/