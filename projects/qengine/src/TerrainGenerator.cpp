 #include "TerrainGenerator.h"

#include <fstream>
#include "Heightmap.h"
#include "render/VertexBuffer.h"
#include "render/IndexBuffer.h"
#include "render/VertexArray.h"

#include "assets/AssetManager.h"
#include "assets/MaterialAsset.h"
#include "assets/TextureAsset.h"
#include "assets/HeightmapTerrainMeshAsset.h"
#include "render/effectparser/EffectParser.h"
#include "render/effectparser/GLSLInterpreter.h"

std::shared_ptr<Mesh> generateHeightmapTerrainMesh(const TerrainInitializer& aInit,
												   const std::string& aName)
{
	Heightmap* heightmap = nullptr;
	GLuint heightmapTexture = 0;
	uint32_t heightmapWidthInPels(0), heightmapHeightInPels(0), numInstances(0);
	if (aInit.heightmapImagePath != nullptr && strlen(aInit.heightmapImagePath) > 0 &&
		aInit.heightmapImageFilename != nullptr && strlen(aInit.heightmapImageFilename) > 0)
	{
		vec2<double> worldMaxs = aInit.worldMins + vec2<double>(aInit.nPatchesX * aInit.patchWidth, 
																aInit.nPatchesZ * aInit.patchDepth);

		heightmap = new Heightmap;
		heightmap->loadFromFile(aInit.heightmapImageFilename, aInit.heightmapImagePath, 
								aInit.nPixelsPerPatchX, aInit.nPixelsPerPatchZ,
								aInit.worldMins,
								worldMaxs);

		heightmapTexture = heightmap->getHeightmapGlid();
		heightmapWidthInPels = static_cast<uint32_t>(heightmap->getNumSourcePixelsX());
		heightmapHeightInPels = static_cast<uint32_t>(heightmap->getNumSourcePixelsY());
	}

	else if (aInit.heightField)
	{
		uint32_t nPixelsX = aInit.nPatchesX * aInit.nPixelsPerPatchX;
		uint32_t nPixelsY = aInit.nPatchesZ * aInit.nPixelsPerPatchZ;
		vec2<double> worldMaxs = aInit.worldMins + vec2<double>(aInit.nPatchesX * aInit.patchWidth,
																aInit.nPatchesZ * aInit.patchDepth);

		heightmap = new Heightmap;
		heightmap->loadFromData(aInit.heightField, nPixelsX, nPixelsY, aInit.nPixelsPerPatchX,
								aInit.nPixelsPerPatchZ, aInit.worldMins, worldMaxs);
		heightmapTexture = heightmap->getHeightmapGlid();
		heightmapWidthInPels = static_cast<uint32_t>(heightmap->getNumSourcePixelsX());
		heightmapHeightInPels = static_cast<uint32_t>(heightmap->getNumSourcePixelsY());
	}

	else
	{
		QUADRION_ERROR("Error generating terrain: height field null and file name not specified");
		return nullptr;
	}

	numInstances = aInit.nPatchesX * aInit.nPatchesZ;

	GLuint indices[6] = { 0, 1, 2, 0, 2, 3 };

						// positions //
	GLfloat verts[32] = { 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
						 aInit.patchWidth, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
						 aInit.patchWidth, 0.0f, -aInit.patchDepth, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
						 0.0f, 0.0f, -aInit.patchDepth, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
						
	MeshInstance* patchCenters = new MeshInstance[numInstances];
	uint32_t idx = 0;
	for (uint32_t i = 0; i < aInit.nPatchesX; ++i)
	{
		for (uint32_t j = 0; j < aInit.nPatchesZ; ++j)
		{
			float x = (i * aInit.patchWidth);
			float y = (j * aInit.patchDepth);
			float z = i / static_cast<float>(aInit.nPatchesX);
			float w = 1.0f - (j / static_cast<float>(aInit.nPatchesZ));

			patchCenters[idx++].position = vec4<float>(x, y, z, w);
		}
	}

	VertexBufferLayout vBufLayout;
	vBufLayout.push<vec3<float>>("position", 3, false);
	vBufLayout.push<vec3<float>>("normal", 3, true);
	vBufLayout.push<vec2<float>>("uv", 2, true);

	VertexBuffer* vBuf = new VertexBuffer(verts, 128);
	vBuf->setLayout(vBufLayout);
	IndexBuffer* iBuf = new IndexBuffer(indices, 6, GL_PATCHES);
	VertexArray* vArray = new VertexArray(vBuf, iBuf);

//	Mesh* terrainMesh = new Mesh(vArray, iBuf, patchCenters, numInstances);
	std::shared_ptr<Mesh> terrainMesh = std::make_shared<Mesh>(vArray, iBuf, patchCenters, numInstances);
	return terrainMesh;
//	AssetManager::instance().load<HeightmapTerrainMeshAsset>(aName, 
//															 ASSET_LOAD_MED_PRIO, 
//															 terrainMesh);


	//auto materialAsset = AssetManager::instance().get<MaterialAsset>("TerrainUniforms");

	//vec3<float> terrain_position = vec3<float>((float)aInit.worldMins.x, 0.0f, (float)aInit.worldMins.y);
	//vec2<float> snowElevations = vec2<float>(500.0f, 4400.0f);
	//vec2<float> uvPatchWidth = vec2<float>(1.0f / aInit.nPatchesX, 1.0f / aInit.nPatchesZ);
	//vec2<float> terrainAspect = vec2<float>(aInit.patchWidth * aInit.nPatchesX, 
	//										aInit.patchDepth * aInit.nPatchesZ) /
	//							vec2<float>(static_cast<float>(heightmapWidthInPels), static_cast<float>(heightmapHeightInPels));
	//vec2<float> texelSize = vec2<float>(1.0f / heightmapWidthInPels, 
	//								    1.0f / heightmapHeightInPels);
	//vec2<float> terrainSpan = vec2<float>(aInit.nPatchesX * aInit.patchWidth,
	//									  aInit.nPatchesZ * aInit.patchDepth);
	//vec2<float> meshMins = vec2<float>(static_cast<float>(aInit.worldMins.x), static_cast<float>(aInit.worldMins.y));
	//float maxHeight = aInit.maxElevation;

	//Material* mat = materialAsset->getMaterial();
	//GLuint textureBuffer = terrainMesh->getInstanceTextureHandle();

	//std::shared_ptr<TextureAsset> grassTexAsset =
	//		AssetManager::instance().load<TextureAsset>("grassTexture", ASSET_LOAD_MED_PRIO, "Media/Textures/Terrain/grass3.jpg");
	//std::shared_ptr<TextureAsset> rockTexAsset = 
	//		AssetManager::instance().load<TextureAsset>("rockTexture", ASSET_LOAD_MED_PRIO, "Media/Textures/Terrain/rock1.PNG");
	//std::shared_ptr<TextureAsset> snowTexAsset =
	//		AssetManager::instance().load<TextureAsset>("snowTexture", ASSET_LOAD_MED_PRIO, "Media/Textures/Terrain/Snow.jpg");

	//aMaterial->setUniform("terrain_position", &terrain_position.x);
	//aMaterial->setUniform("uvPatchWidth", &uvPatchWidth.x);
	//aMaterial->setUniform("terrainAspect", &terrainAspect.x);
	//aMaterial->setUniform("texelSize", &texelSize.x);
	//aMaterial->setUniform("maxHeight", &maxHeight);
	//aMaterial->setUniform("terrainSpan", &terrainSpan.x);
	//aMaterial->setUniform("snowElevations", &snowElevations.x);
	//aMaterial->setUniform("meshMins", &meshMins.x);
	//aMaterial->setTexture("heightmap", heightmapTexture);
	//aMaterial->setTextureBuffer("patchInstanceBuffer", textureBuffer);
	//aMaterial->setTexture("rockTexture", rockTexAsset->getTextureID());
	//aMaterial->setTexture("grassTexture", grassTexAsset->getTextureID());
	//aMaterial->setTexture("snowTexture", snowTexAsset->getTextureID());
}
