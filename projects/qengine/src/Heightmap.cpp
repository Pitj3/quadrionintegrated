#define NOMINMAX

#include "Heightmap.h"
#include <algorithm>
#include <cstdint>
#include "render/Image.h"
#include "HeightmapPatch.h"


struct HeightmapImpl
{
	~HeightmapImpl()
	{
		for (uint32_t i = 0; i < heightmapPatches.size(); ++i)
		{
			if (heightmapPatches[i])
			{
				delete heightmapPatches[i];
				heightmapPatches[i] = nullptr;
			}
		}
		heightmapPatches.clear();

		if (heightmapImage)
		{
			delete heightmapImage;
			heightmapImage = nullptr;
		}

//		glDeleteTextures(1, &heightmapTextureID);
	}

	std::vector<HeightmapPatch*> heightmapPatches;

	float minElevation;
	float maxElevation;

	GLuint heightmapTextureID;

	uint32_t numPixelsX;
	uint32_t numPixelsY;

	Image* heightmapImage;
};


Heightmap::Heightmap()
{
	mImpl = new HeightmapImpl;
}


Heightmap::~Heightmap()
{
	if (mImpl)
	{
		delete mImpl;
		mImpl = nullptr;
	}
}

bool Heightmap::loadFromFile(const char* fName,
							 const char* path,
							 const uint32_t nPixelsPerPatchX,
							 const uint32_t nPixelsPerPatchY,
							 vec2<double> worldMins,
							 vec2<double> worldMaxs)
{
	mImpl->heightmapImage = new Image;
	mImpl->heightmapImage->setTexFilter(QTEXTURE_FILTER_TRILINEAR_ANISO | QTEXTURE_CLAMP);
	mImpl->heightmapImage->loadTexture(fName, true, path, false);
	mImpl->heightmapTextureID = mImpl->heightmapImage->getOpenGlid();
	unsigned char* pixels = mImpl->heightmapImage->getData(0);
	uint32_t pixelStride = mImpl->heightmapImage->getBitsPerPel() / 8;

	mImpl->numPixelsX = mImpl->heightmapImage->getWidth();
	mImpl->numPixelsY = mImpl->heightmapImage->getHeight();

	double worldWidth = worldMaxs.x - worldMins.x;
	double worldDepth = worldMaxs.y - worldMins.y;

	double distBtwPixX = worldWidth / (mImpl->numPixelsX - 1);
	double distBtwPixY = worldDepth / (mImpl->numPixelsY - 1);

	double patchWidth = 0.0;
	double patchDepth = 0.0;

	vec2<double> patchMins, patchMaxs;
	uint32_t startX = 0;
	uint32_t startY = 0;
	while (startY < mImpl->numPixelsY)
	{
		while (startX < mImpl->numPixelsX)
		{
			uint32_t nPixX = std::min(nPixelsPerPatchX, mImpl->numPixelsX - startX);
			uint32_t nPixY = std::min(nPixelsPerPatchY, mImpl->numPixelsY - startY);

			patchWidth = distBtwPixX * (nPixX - 1);
			patchDepth = distBtwPixY * (nPixY - 1);

			vec2<double> patchMins = vec2<double>(startX * distBtwPixX, startY * distBtwPixY);
			vec2<double> patchMaxs = patchMins + vec2<double>(patchWidth, patchDepth);

			HeightfieldVertex* hfVerts = new HeightfieldVertex[nPixX * nPixY];
			uint32_t writeIdx = 0;
			for (uint32_t y = 0; y < nPixY; y++)
			{
				for (uint32_t x = 0; x < nPixX; ++x)
				{
					uint32_t sx = startX + x;
					uint32_t sy = startY + y;
					sy = (mImpl->numPixelsY - 1) - sy; 
					uint32_t idx = sy * mImpl->numPixelsX + sx;
//					idx *= 6;
					idx *= pixelStride;

					uint16_t heightSample = 0;
					memcpy(&heightSample, &pixels[idx], sizeof(uint16_t));
					hfVerts[writeIdx++].vertex = heightSample;
				}
			}

			HeightmapPatch* newPatch = new HeightmapPatch(hfVerts, nPixX, nPixY, patchMins, patchMaxs);
			mImpl->heightmapPatches.push_back(newPatch);

			delete[] hfVerts;
			hfVerts = nullptr;

			startX += (nPixelsPerPatchX - 1);
		}

		startY += (nPixelsPerPatchY - 1);
		startX = 0;
	}

	return true;
}

bool Heightmap::loadFromData(void* data,
							 const uint32_t nPixelsX,
							 const uint32_t nPixelsY,
							 const uint32_t nPixelsPerPatchX,
							 const uint32_t nPixelsPerPatchY,
							 vec2<double> worldMins,
							 vec2<double> worldMaxs)
{
	mImpl->heightmapImage = new Image;
	mImpl->heightmapImage->setTexFilter(QTEXTURE_FILTER_LINEAR | QTEXTURE_CLAMP);
//	mImpl->heightmapImage->loadTexture(fName, true, path, false);
	mImpl->heightmapImage->loadTexture(nPixelsX, nPixelsY, QTEXTURE_FORMAT_R16, true, data);
	mImpl->heightmapTextureID = mImpl->heightmapImage->getOpenGlid();
	unsigned char* pixels = mImpl->heightmapImage->getData(0);
	uint32_t pixelStride = mImpl->heightmapImage->getBitsPerPel() / 8;

	mImpl->numPixelsX = mImpl->heightmapImage->getWidth();
	mImpl->numPixelsY = mImpl->heightmapImage->getHeight();

	double worldWidth = worldMaxs.x - worldMins.x;
	double worldDepth = worldMaxs.y - worldMins.y;

	double distBtwPixX = worldWidth / (mImpl->numPixelsX - 1);
	double distBtwPixY = worldDepth / (mImpl->numPixelsY - 1);

	double patchWidth = 0.0;
	double patchDepth = 0.0;

	vec2<double> patchMins, patchMaxs;
	uint32_t startX = 0;
	uint32_t startY = 0;
	while (startY < mImpl->numPixelsY)
	{
		while (startX < mImpl->numPixelsX)
		{
			uint32_t nPixX = std::min(nPixelsPerPatchX, mImpl->numPixelsX - startX);
			uint32_t nPixY = std::min(nPixelsPerPatchY, mImpl->numPixelsY - startY);

			patchWidth = distBtwPixX * (nPixX - 1);
			patchDepth = distBtwPixY * (nPixY - 1);

			vec2<double> patchMins = vec2<double>(startX * distBtwPixX, startY * distBtwPixY);
			vec2<double> patchMaxs = patchMins + vec2<double>(patchWidth, patchDepth);

			HeightfieldVertex* hfVerts = new HeightfieldVertex[nPixX * nPixY];
			uint32_t writeIdx = 0;
			for (uint32_t y = 0; y < nPixY; y++)
			{
				for (uint32_t x = 0; x < nPixX; ++x)
				{
					uint32_t sx = startX + x;
					uint32_t sy = startY + y;
					sy = (mImpl->numPixelsY - 1) - sy;
					uint32_t idx = sy * mImpl->numPixelsX + sx;
					//					idx *= 6;
					idx *= pixelStride;

					uint16_t heightSample = 0;
					memcpy(&heightSample, &pixels[idx], sizeof(uint16_t));
					hfVerts[writeIdx++].vertex = heightSample;
				}
			}

			HeightmapPatch* newPatch = new HeightmapPatch(hfVerts, nPixX, nPixY, patchMins, patchMaxs);
			mImpl->heightmapPatches.push_back(newPatch);

			delete[] hfVerts;
			hfVerts = nullptr;

			startX += (nPixelsPerPatchX - 1);
		}

		startY += (nPixelsPerPatchY - 1);
		startX = 0;
	}

	return true;
}


GLuint Heightmap::getHeightmapGlid()
{
	return mImpl->heightmapTextureID;
}

const size_t Heightmap::getNumPatches()
{
//	return mImpl->numPatchesX * mImpl->numPatchesZ;
	return mImpl->heightmapPatches.size();
}

const HeightmapPatch* const Heightmap::getPatch(const size_t idx)
{
	if(idx < 0 || idx > getNumPatches())
		return nullptr;

	return mImpl->heightmapPatches[idx];
}

const Image* const Heightmap::getHeightmapSource() const
{
	return mImpl->heightmapImage;
}

const size_t Heightmap::getNumSourcePixelsX()
{
	return mImpl->heightmapImage->getWidth();
}

const size_t Heightmap::getNumSourcePixelsY()
{
	return mImpl->heightmapImage->getHeight();
}