/*
* timer.cpp
*
* Created on: 25/01/2008
*     Author: Jonathan 'Bladezor' Bastnagel
*
* Copyright (C) <2008>  <Odorless Entertainment>
*
*     This program is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include "core/Timer.h"

void Timer::start()
{
#if defined(_WIN32) || defined(_WIN64)
	QueryPerformanceCounter(&mStartCount);
#else
	gettimeofday(&mStartCount, NULL);
#endif
	isRunning = true;
}

void Timer::stop()
{
#if defined(_WIN32) || defined(_WIN64)
	QueryPerformanceCounter(&mEndCount);
#else
	gettimeofday(&mEndCount, NULL);
#endif
	isRunning = false;
}

void Timer::reset()
{
#if defined(_WIN32) || defined(_WIN64)

#else
//	StartCount.LowPart = 0;
//	StartCount.HighPart = 0;
//	EndCount.LowPart = 0;
//	EndCount.HighPart = 0;
    mStartCount.tv_sec = 0;
    mStartCount.tv_usec = 0;
    mEndCount.tv_sec = 0;
    mEndCount.tv_usec = 0;
	isRunning = false;
#endif
}

double Timer::getElapsedMicroSec()
{
#if defined(_WIN32) || defined(_WIN64)
	if(isRunning)
		QueryPerformanceCounter(&mEndCount);

	mStartTime = mStartCount.QuadPart * (1000000.0 / mFreq.QuadPart);
	mEndTime = mEndCount.QuadPart * (1000000.0 / mFreq.QuadPart);
#else
	if (isRunning)
		gettimeofday(&mEndCount, NULL);

	mStartTime = (mStartCount.tv_sec * 1000000.0) + mStartCount.tv_usec;
	mEndTime = (mEndCount.tv_sec * 1000000.0) + mEndCount.tv_usec;
#endif
	return mEndTime - mStartTime;
}

double Timer::getElapsedMilliSec()
{
	return getElapsedMicroSec() * 0.001;
}

double Timer::getElapsedSec()
{
	return getElapsedMicroSec() * 0.000001;
}

QTime Timer::getElapsed()
{
	QTime temp;
	temp.microseconds = getElapsedMicroSec();
	temp.milliseconds = getElapsedMilliSec();
	temp.seconds = getElapsedSec();
	temp.minutes = getElapsedSec() * 0.01666666;
	temp.hours = temp.minutes * 0.01666666;
	temp.days = temp.hours * 0.04166666;
	temp.years = temp.days * 0.00277777;

	return temp;
}

