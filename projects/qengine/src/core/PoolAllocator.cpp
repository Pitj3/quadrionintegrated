#include "core/PoolAllocator.h"

#include <cstdint>

constexpr size_t DEFAULT_ALIGNMENT = 64;

class PoolAllocatorImpl
{
	public:
		std::list<void*>	freeBlocks;
};

PoolAllocator::PoolAllocator()
{
	mImpl = new PoolAllocatorImpl();

	mAlignment = DEFAULT_ALIGNMENT;
	mNumBlocksTotal = 0;
	mSizeofBlock = 0;
	mMem = nullptr;
}

PoolAllocator::PoolAllocator(size_t aAlignment, size_t aNumBlocks, size_t aSizePerBlock)
{
	mImpl = new PoolAllocatorImpl();

	mAlignment = aAlignment;
	mNumBlocksTotal = aNumBlocks;
	mSizeofBlock = aSizePerBlock;

#if !defined(_WIN32) || !defined(_WIN64)
	mMem = aligned_alloc(mAlignment, mNumBlocksTotal * mSizeofBlock);
#else
	mMem = _aligned_malloc(mNumBlocksTotal * mSizeofBlock, mAlignment);
#endif

	void* mover = mMem;
	for(uint32_t i = 0; i < mNumBlocksTotal; ++i)
	{
		mImpl->freeBlocks.push_back(mover);
		mover = (uint8_t*)mover + mSizeofBlock;
	}
}

PoolAllocator::~PoolAllocator()
{
	if(mMem)
	{
#if !defined(_WIN32) || !defined(_WIN64)
		free(mMem);
#else
		_aligned_free(mMem);
#endif
		mMem = nullptr;
	}

	mImpl->freeBlocks.clear();
}


void* PoolAllocator::getBlock()
{
	void* freeMem = mImpl->freeBlocks.back();
	mImpl->freeBlocks.pop_back();

	return freeMem;
}

void PoolAllocator::freeBlock(void* aBlock)
{
	mImpl->freeBlocks.push_front(aBlock);
}