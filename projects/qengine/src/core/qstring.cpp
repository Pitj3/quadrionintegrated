#include "core/qstring.h"
#include "QMath.h"
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>

struct QStringImpl
{
	std::string value;
};

QString::QString()
{
	mImpl = new QStringImpl();
	mImpl->value = "";
}

QString::QString(const char* aS) : QString()
{
	if (aS)
		mImpl->value = aS;
	else
		mImpl->value = "";
}

QString::QString(uint32_t aV) : QString()
{
	mImpl->value = std::to_string(aV).c_str();
}

QString::QString(int32_t aV) : QString()
{
	mImpl->value = std::to_string(aV).c_str();
}

QString::QString(float aV) : QString()
{
	mImpl->value = std::to_string(aV).c_str();
}

QString::QString(vec3<float> aV) : QString()
{
	std::stringstream converter;
	converter << aV.x << " " << aV.y << " " << aV.z;

	mImpl->value = converter.str().c_str();
}

QString::QString(vec4<float> aV) : QString()
{
	std::stringstream converter;
	converter << aV.x << " " << aV.y << " " << aV.z << " " << aV.w;

	mImpl->value = converter.str().c_str();
}

QString::QString(mat4<float> aV) : QString()
{
	std::stringstream converter;
	
	for (size_t i = 0; i < 16; ++i)
	{
		if (i)
			converter << " " << aV[static_cast<int32_t>(i)];
		else
			converter << aV[static_cast<int32_t>(i)];
	}

	mImpl->value = converter.str().c_str();
}

//QString::QString(char c) : QString()
//{
//	if (!mImpl)
//		mImpl = new QString_Impl();
//
//	this->mImpl->value = std::string((size_t)1, c);
//}

QString::QString(const QString& aS) : QString()
{
	if (aS.mImpl)
		mImpl->value = aS.c_str();
	else
		mImpl->value = "";
}

QString::QString(QString&& aS) : QString()
{
	if (aS.mImpl)
		mImpl->value = aS.c_str();
	else
		mImpl->value = "";
}

//QString::QString(const QString* s) : QString()
//{
//	if (!mImpl)
//		mImpl = new QString_Impl();
//
//	if (s)
//		mImpl->value = s->mImpl->value.c_str();
//	else
//		mImpl->value = "";
//}

//QString& QString::operator= (const QString& f)
//{
//	if (!mImpl)
//		mImpl = new QString_Impl();
//
//	mImpl->value = std::string(f.mImpl->value.c_str());
//	return *this;
//}

QString& QString::operator= (const char* aF)
{
	if (!mImpl)
		mImpl = new QStringImpl();

	mImpl->value = aF;
	return *this;
}

QString& QString::operator= (const QString& aF)
{
	if (!mImpl)
		mImpl = new QStringImpl();

	mImpl->value = aF.c_str();
	return *this;
}

bool QString::operator== (QString& aF) const
{
	if (!mImpl)
		return false;

	return strcmp(this->c_str(), aF.c_str()) == 0;
}

bool QString::operator== (QString* aF) const
{
	if (!mImpl)
		return (aF == nullptr);
	else if (!aF)
		return (mImpl == nullptr);

	return strcmp(this->c_str(), aF->c_str()) == 0;
}

bool QString::operator== (const char* aF) const
{
	if (!mImpl)
		return (aF == nullptr);

	return (strcmp(mImpl->value.c_str(), aF) == 0);
}

bool QString::operator!= (QString& aF) const
{
	if (!mImpl)
		return false;

	return (mImpl->value != aF.mImpl->value);
}

bool QString::operator!= (QString* aF) const
{
	if (!mImpl)
		return (aF == nullptr);
	else if (!aF)
		return (mImpl == nullptr);

	return (mImpl->value != aF->mImpl->value);
}

bool QString::operator!= (const char* aF) const
{
	if (!mImpl)
		return (aF == nullptr);

	return (strcmp(mImpl->value.c_str(), aF) != 0);
}

bool QString::operator> (const QString& aRight) const
{
	if (!mImpl)
		return false;

	return (mImpl->value < aRight.mImpl->value);
}

bool QString::operator>= (const QString& aRight) const
{
	if (!mImpl)
		return false;

	return (mImpl->value <= aRight.mImpl->value);
}

bool QString::operator< (const QString& aRight) const
{
	if (!mImpl)
		return false;

	return (mImpl->value < aRight.mImpl->value);
}

bool QString::operator<= (const QString& aRight) const
{
	if (!mImpl)
		return false;

	return (mImpl->value <= aRight.mImpl->value);
}

char& QString::operator[] (const int aIndex) const
{
	return mImpl->value[aIndex];
}

QString::~QString()
{
	if (mImpl)
	{
		delete mImpl;
		mImpl = nullptr;
	}
}

QString QString::fromString(const void* aStr)
{
	if (!aStr)
		return QString();

	std::string* ptr = static_cast<std::string*>(const_cast<void*>(aStr));

	if (ptr)
		return ptr->c_str();
	else
		return "";
}

const char* QString::c_str() const
{
	if (mImpl)
		return mImpl->value.c_str();
	else
		return "";
}

int QString::indexOf(const char* aFindStr, size_t aOffset) const
{
	if (!mImpl)
		return -1;
	
	std::string::size_type loc = mImpl->value.find(aFindStr, aOffset);

	if (loc != std::string::npos)
		return (int)loc;
	else
		return -1;
}

void QString::insert(size_t aPos, QString aStr)
{
	if (!mImpl)
		return;

	mImpl->value.insert(aPos, aStr.c_str());
}

size_t QString::length() const
{
	if (mImpl)
		return mImpl->value.length();
	else
		return 0;
}

QArray<QString> QString::split(QString aDelimeter, size_t aLimit) const
{
	QArray<QString> ary;
	//std::vector<std::string> test;

	if (!mImpl || mImpl->value.size() == 0)
		return ary;

	size_t nbSplits = 0;
	size_t pos = 0;
	size_t delimeterLen = aDelimeter.size();

	std::stringstream stm;

	auto find = mImpl->value.find(aDelimeter.c_str(), pos);

	if (find == std::string::npos)
		pos = 0;

	while (find != std::string::npos)
	{
		ary.push_back(QString(mImpl->value.substr(pos, (find - pos)).c_str()));

		nbSplits++;
		pos = (find + delimeterLen);

		if ((aLimit != 0 && nbSplits >= aLimit) || pos >= mImpl->value.size())
			return ary;

		find = mImpl->value.find(aDelimeter.c_str(), pos);
	}

	if (find == std::string::npos && pos < mImpl->value.size())
		ary.push_back(QString(mImpl->value.substr(pos, mImpl->value.length() - pos).c_str()));

	return ary;
}

size_t QString::size() const
{
	if (mImpl)
		return mImpl->value.size();
	else
		return 0;
}

QString QString::substr(size_t aOffset) const
{
	if (!mImpl)
		return "";

	return mImpl->value.substr(aOffset).c_str();
}

QString QString::substr(size_t aOffset, size_t aLength) const
{
	if (!mImpl)
		return "";

	return mImpl->value.substr(aOffset, aLength).c_str();
}

QString QString::toLower() const
{
	std::string conv(mImpl->value.c_str());
	std::transform(conv.begin(), conv.end(), conv.begin(), ::tolower);

	return conv.c_str();
}

QString QString::toUpper() const
{
	std::string conv(mImpl->value.c_str());
	std::transform(conv.begin(), conv.end(), conv.begin(), ::toupper);

	return conv.c_str();
}

mat4<float> QString::toMat4f() const
{
	mat4<float> result;
	QArray<QString> split;

	split = this->split(" ", 0);

	if (split.size() >= 15)
	{
		for (size_t i = 0; i < 16; ++i)
			result[static_cast<int32_t>(i)] = static_cast<float>(atof(split[static_cast<int32_t>(i)].c_str()));
	}

	return result;
}

float QString::toFloat() const
{
	return static_cast<float>(atof(mImpl->value.c_str()));
}


int32_t QString::toInt32() const
{
	return atoi(mImpl->value.c_str());
}

uint32_t QString::toUInt32() const
{
	uint32_t val = 0;

	std::istringstream ss(mImpl->value.c_str());

	if (!(ss >> val))
		return 0;
	else
		return val;
}

int64_t QString::toInt64() const
{
	return strtoll(mImpl->value.c_str(), NULL, 10);
}

uint64_t QString::toUInt64() const
{
	uint64_t val = 0;

	std::istringstream ss(mImpl->value.c_str());

	if (!(ss >> val))
		return 0;
	else
		return val;
}

vec2<float> QString::toVec2f() const
{
	vec2<float> result;
	QArray<QString> split;

	split = this->split(" ", 0);

	if (split.size() >= 2)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = static_cast<float>(atof(split[1].c_str()));
	}

	return result;
}

vec3<float> QString::toVec3f() const
{
	vec3<float> result;
	QArray<QString> split;

	split = this->split(" ", 0);

	auto size = split.size();

	if (size >= 3)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = static_cast<float>(atof(split[1].c_str()));
		result.z = static_cast<float>(atof(split[2].c_str()));
	}
	else if (size == 2)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = static_cast<float>(atof(split[1].c_str()));
		result.z = 0.0f;
	}
	else if (size == 1)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = 0.0f;
		result.z = 0.0f;
	}
	else
	{
		result.x = 0.0f;
		result.y = 0.0f;
		result.z = 0.0f;
	}

	return result;
}

vec4<float> QString::toVec4f() const
{
	vec4<float> result;
	QArray<QString> split;

	split = this->split(" ", 0);

	auto size = split.size();

	if (size >= 4)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = static_cast<float>(atof(split[1].c_str()));
		result.z = static_cast<float>(atof(split[2].c_str()));
		result.w = static_cast<float>(atof(split[3].c_str()));
	}
	else if (size == 3)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = static_cast<float>(atof(split[1].c_str()));
		result.z = static_cast<float>(atof(split[2].c_str()));
		result.w = 0.0f;
	}
	else if (size == 2)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = static_cast<float>(atof(split[1].c_str()));
		result.z = 0.0f;
		result.w = 0.0f;
	}
	else if (size == 1)
	{
		result.x = static_cast<float>(atof(split[0].c_str()));
		result.y = 0.0f;
		result.z = 0.0f;
		result.w = 0.0f;
	}
	else
	{
		result.x = 0.0f;
		result.y = 0.0f;
		result.z = 0.0f;
		result.w = 0.0f;
	}

	return result;
}

double QString::toDouble() const
{
	double val = 0;

	std::istringstream ss(mImpl->value.c_str());

	if (!(ss >> val))
		return 0;
	else
		return val;
}