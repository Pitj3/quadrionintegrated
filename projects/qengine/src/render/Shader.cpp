#include <unordered_map>
#include <utility>

#include "render/Material.h"
#include "render/Shader.h"
#include "render/Render.h"
#include "render/Effect.h"
#include "core/QLog.h"
#include "render/QGfx.h"
#include "Extensions.h"
#include "render/ShaderUniform.h"
#include "render/ShaderStruct.h"
#include "render/ShaderResource.h"
#include "io/FileSystem.h"
#include "StringUtils.h"
#include "render/Pass.h"
#include <map>
#include <memory>
#include "StringUtils.h"
#include "assets/AssetManager.h"
#include "assets/MaterialAsset.h"


//Shader* Shader::sCurrentlyBound;

std::vector<std::string> ssplitOnAny(const std::string& str, const char* regex)
{
	size_t begin = 0;
	size_t next = str.find_first_of(regex, begin);
	std::vector<std::string> res;

	if (str.length() == 0)
	{
		return res;
	}

	do
	{
		std::string val = str.substr(begin, next - begin);
		if (next == begin || val.size() == 0)
		{
			begin = next + 1;
			next = str.find_first_of(regex, begin + 1);
			continue;
		}
		else if (std::string(regex).find(val[0]) != std::string::npos)
		{
			begin++;
			continue;
		}
		res.push_back(val);
		if (next >= str.length())
		{
			break;
		}
		begin = next + 1;
		next = str.find_first_of(regex, begin + 1);

	} while (begin < str.length());

	return res;
}

class ShaderImpl
{
public:
	~ShaderImpl()
	{
		glDeleteProgram(handle);
	}

	uint32_t handle;

	std::string name;
	std::string path;

	std::string source;

	std::string VSSource;
	std::string TCSource;
	std::string TESource;
	std::string GSSource;
	std::string FSSource;

	std::vector<ShaderUniformBufferDeclaration*> VSUniformBuffers;
	std::vector<ShaderUniformBufferDeclaration*> GSUniformBuffers;
	std::vector<ShaderUniformBufferDeclaration*> PSUniformBuffers;
	std::vector<ShaderUniformBufferDeclaration*> TEUniformBuffers;
	std::vector<ShaderUniformBufferDeclaration*> TCUniformBuffers;

	ShaderUniformBufferDeclaration* VSUserUniformBuffer;
	ShaderUniformBufferDeclaration* GSUserUniformBuffer;
	ShaderUniformBufferDeclaration* PSUserUniformBuffer;
	ShaderUniformBufferDeclaration* TEUserUniformBuffer;
	ShaderUniformBufferDeclaration* TCUserUniformBuffer;

	std::map<std::string, uint32_t> samplers;
	std::vector<ShaderResourceDeclaration*> resources;

	std::vector<ShaderStruct*> structs;

	std::vector<std::string> uboNames;
	std::map<std::string, std::vector<UniformBufferElement*>> uboElements;
	std::map<std::string, std::vector<std::string>> uboUniformMap;
	std::unordered_map<std::string, GLuint> uboBindPointMap;

	// Uniform shit //
	std::vector<std::string> uniformNames;
	std::unordered_map<std::string, GLint> uniformLocationMap;

	//		QHandle<QEntityManager> entityManager;

	std::map<std::string, uint32_t> uboBindPoints;
	std::vector<std::string> mUBOsInUse;

	Technique* technique;
	Pass* pass;
	Effect* parentEffect;

	std::map<std::string, SamplerObject*> mSamplerObjects;
};

Shader::Shader(const std::string& aName, const std::string& aVertexSource, const std::string& aFragmentSource, const std::string& aGeometrySource, const std::string& aTCSource, const std::string& aTESource)
{
	mImpl = new ShaderImpl();
	mImpl->name = aName;
	mImpl->VSSource = aVertexSource;
	mImpl->GSSource = aGeometrySource;
	mImpl->FSSource = aFragmentSource;
	mImpl->TCSource = aTCSource;
	mImpl->TESource = aTESource;
	_init();
}


Shader::Shader(Technique* aTechnique,
	Pass* aPass,
	Effect* aParent,
	const std::string& aName,
	const std::string& aVertexSource,
	const std::string& aFragmentSource,
	const std::string& aGeometrySource,
	const std::string& aTCSource,
	const std::string& aTESource)
{
	mImpl = new ShaderImpl();
	mImpl->name = aName;
	mImpl->VSSource = aVertexSource;
	mImpl->GSSource = aGeometrySource;
	mImpl->FSSource = aFragmentSource;
	mImpl->TCSource = aTCSource;
	mImpl->TESource = aTESource;
	//	mImpl->entityManager = aEntityManager;
	mImpl->technique = aTechnique;
	mImpl->pass = aPass;
	mImpl->parentEffect = aParent;
	_init();
	_parseSamplers();
}

Shader::~Shader()
{
	delete mImpl;
}

std::vector<ShaderResourceDeclaration*>& Shader::getResources() const
{
	return mImpl->resources;
}

std::vector<ShaderStruct*> Shader::getStructs() const
{
	return mImpl->structs;
}

std::vector<std::string> Shader::getUboNames() const
{
	return mImpl->uboNames;
}

void Shader::_init()
{
	std::map<GLint, uint32_t> uniformSizeMap
	{
		{GL_FLOAT, 4},
		{GL_FLOAT_VEC2, 8},
		{GL_FLOAT_VEC3, 16},
		{GL_FLOAT_VEC4, 16},
		{GL_DOUBLE, 8},
		{GL_DOUBLE_VEC2, 16},
		{GL_DOUBLE_VEC3, 32},
		{GL_DOUBLE_VEC4, 32},
		{GL_INT, 4},
		{GL_INT_VEC2, 8},
		{GL_INT_VEC3, 16},
		{GL_INT_VEC4, 16},
		{GL_UNSIGNED_INT, 4},
		{GL_UNSIGNED_INT_VEC2, 8},
		{GL_UNSIGNED_INT_VEC3, 16},
		{GL_UNSIGNED_INT_VEC4, 16},
		{GL_BOOL, 4},
		{GL_BOOL_VEC2, 8},
		{GL_BOOL_VEC3, 16},
		{GL_BOOL_VEC4, 16},
		{GL_FLOAT_MAT2, 32},
		{GL_FLOAT_MAT3, 48},
		{GL_FLOAT_MAT4, 64},
		{GL_FLOAT_MAT2x3, 48},
		{GL_FLOAT_MAT2x4, 64},
		{GL_FLOAT_MAT3x2, 32},
		{GL_FLOAT_MAT3x4, 64},
		{GL_FLOAT_MAT4x2, 32},
		{GL_FLOAT_MAT4x3, 48},
	};

	mImpl->VSUserUniformBuffer = nullptr;
	mImpl->GSUserUniformBuffer = nullptr;
	mImpl->PSUserUniformBuffer = nullptr;

	std::string* shaders[5] = { &mImpl->VSSource, &mImpl->FSSource, &mImpl->GSSource, &mImpl->TCSource, &mImpl->TESource };

	_parse(mImpl->VSSource, mImpl->FSSource, mImpl->GSSource, mImpl->TCSource, mImpl->TESource);
	ShaderErrorInfo error;
	mImpl->handle = _compile(shaders, error);
	if (!mImpl->handle)
	{
		for (uint32_t i = 0; i < 5; i++)
		{
			// for all shaders
			if (!error.message[i].empty())
			{
				std::vector<std::string> errorLines = StringUtils::getLines(error.message[i]);

				for (const auto& line : errorLines)
				{
					if (line.find('(') == std::string::npos)
					{
						QUADRION_INTERNAL_ERROR(line);
						continue;
					}

					auto startLoc = line.find_first_of('(');
					auto endLoc = line.find_first_of(')');
					std::string lineNumberString = line.substr(startLoc + 1, (endLoc - startLoc) - 1);
					int lineNumber = atoi(lineNumberString.c_str()) - 1;

					std::string glslShaderLine = "";

					switch (i)
					{
					case 0:
						// vertex
					{
						std::vector<std::string> l = StringUtils::split(mImpl->VSSource, '\n');
						glslShaderLine = l[lineNumber];
						break;
					}
					case 1:
					{
						// fragment
						std::vector<std::string> l = StringUtils::split(mImpl->FSSource, '\n');
						glslShaderLine = l[lineNumber];
						break;
					}
					case 2:
					{
						// geometry
						std::vector<std::string> l = StringUtils::split(mImpl->GSSource, '\n');
						glslShaderLine = l[lineNumber];
						break;
					}
					case 3:
					{
						// tc
						std::vector<std::string> l = StringUtils::split(mImpl->TCSource, '\n');
						glslShaderLine = l[lineNumber];
						break;
					}
					case 4:
					{
						// te
						std::vector<std::string> l = StringUtils::split(mImpl->TESource, '\n');
						glslShaderLine = l[lineNumber];
						break;
					}
					}


					if (mImpl->parentEffect != nullptr)
					{
						std::string effectSource = mImpl->parentEffect->getSourceCopy();
						if (effectSource != "")
						{
							uint32_t eLineNumber = 0;
							std::vector<uint32_t> eLineNumbers;

							for (const auto& include : mImpl->parentEffect->getIncludes())
							{
								std::vector<std::string> includeLines = StringUtils::split(include.second.c_str(), '\n');
								for (auto& includeLine : includeLines)
								{
									includeLine = StringUtils::reduce(includeLine.c_str()).c_str();
									glslShaderLine = StringUtils::reduce(glslShaderLine.c_str()).c_str();

									if (includeLine.find("//") != std::string::npos)
									{
										if (includeLine.rfind("//", 0) != 0)
										{
											// ends with comment
											const size_t commentLocation = includeLine.find("//");
											includeLine = includeLine.substr(0, commentLocation);
											includeLine += "\n";
										}
									}

									if (includeLine == glslShaderLine)
									{
										eLineNumbers.push_back(eLineNumber + 1);
									}

									eLineNumber++;
								}

								for (size_t ii = 0; ii < eLineNumbers.size(); ii++)
								{
									QUADRION_INTERNAL_ERROR("Error at line: {0} : {1} in file: {2}", eLineNumbers[ii], line, include.first.c_str());
								}

								eLineNumbers.clear();
								eLineNumber = 0;
							}

							std::vector<std::string> effectLines = StringUtils::split(effectSource.c_str(), '\n');
							for (auto& effectLine : effectLines)
							{
								effectLine = StringUtils::reduce(effectLine.c_str()).c_str();
								glslShaderLine = StringUtils::reduce(glslShaderLine.c_str()).c_str();

								if (effectLine.find("//") != std::string::npos)
								{
									if (effectLine.rfind("//", 0) != 0)
									{
										// ends with comment
										const size_t commentLocation = effectLine.find("//");
										effectLine = effectLine.substr(0, commentLocation);
										effectLine += "\n";
									}
								}

								if (effectLine == glslShaderLine)
								{
									eLineNumbers.push_back(eLineNumber + 1);
								}

								eLineNumber++;
							}

							for (size_t ii = 0; ii < eLineNumbers.size(); ii++)
							{
								QUADRION_INTERNAL_ERROR("Error at line: {0} : {1} in file: {2}", eLineNumbers[ii], line, mImpl->parentEffect->getName());
							}
						}
					}
				}
			}
		}
	}

	assert(mImpl->handle);

	_resolveUniforms();
	_validateUniforms();

	//	GLuint index = 0;
	//	const char* uniformName = "camPos";
	//	glGetUniformIndices(getProgramID(), 1, &uniformName, &index);

	//	GLint uniformOffset;
	//	glGetActiveUniformsiv(mImpl->handle, 1, &index, GL_UNIFORM_OFFSET, &uniformOffset);

	GLint maxUniformIndices;
	uint32_t nUBOBindPoints = 0;
	glGetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &maxUniformIndices);
	auto iterStart = mImpl->uboUniformMap.begin();
	auto iterEnd = mImpl->uboUniformMap.end();
	for (; iterStart != iterEnd; iterStart++)
	{
		auto referenceIter = iterStart;
		std::vector uniformNames = referenceIter->second;
		for (auto uniformName : uniformNames)
		{
			// Query GL and init uniform objects //
			GLuint uniformLoc;
			GLint uniOffset, uniType, arrayLen;
			uint32_t uniSize = 0;
			if (uniformName.find('[') != std::string::npos)
			{
				uniformName = uniformName.substr(0, uniformName.find('['));
			}
			const char* uniformNameCStr = uniformName.c_str();

			glGetUniformIndices(getProgramID(), 1, &uniformNameCStr, &uniformLoc);
			if (uniformLoc == GL_INVALID_INDEX)
				continue;

			glGetActiveUniformsiv(mImpl->handle, 1, &uniformLoc, GL_UNIFORM_OFFSET, &uniOffset);
			glGetActiveUniformsiv(mImpl->handle, 1, &uniformLoc, GL_UNIFORM_TYPE, &uniType);
			glGetActiveUniformsiv(mImpl->handle, 1, &uniformLoc, GL_UNIFORM_SIZE, &arrayLen);

			auto sizeEntry = uniformSizeMap.find(uniType);
			uniSize = sizeEntry->second * arrayLen;

			uint32_t offs = static_cast<uint32_t>(uniOffset);
			mImpl->uboElements[referenceIter->first].push_back(new UniformBufferElement{ offs, uniSize, uniformName });
		}

		uint32_t i = 0;
		//		referenceIter = mImpl->uboUniformMap.begin();
		referenceIter = iterStart;
		//		for(auto uboElement : mImpl->uboElements[referenceIter->first])
		//		{
		//			uint32_t size = 0;
		//			if(i == 0)
		//				size = mImpl->uboElements[referenceIter->first].at(1)->offset;
		//
		//			else
		//				size = mImpl->uboElements[referenceIter->first].at(i)->offset - mImpl->uboElements[referenceIter->first].at(i - 1)->offset;
		//				
		//			mImpl->uboElements[referenceIter->first].at(i)->size = size;
		//			++i;
		//		}

		referenceIter = iterStart;
		std::string name = referenceIter->first;

		// Cache uniform locations //
		for (auto uniformName : mImpl->uniformNames)
		{
			auto uniLocIter = mImpl->uniformLocationMap.find(uniformName);
			if (uniLocIter == mImpl->uniformLocationMap.end())
			{
				GLint uniLoc = glGetUniformLocation(mImpl->handle, uniformName.c_str());
				if (uniLoc != -1)
					mImpl->uniformLocationMap.insert({ uniformName, uniLoc });
			}
		}

		// Cache UBO Bind points //
		auto iter = mImpl->uboBindPointMap.find(name);
		if (iter == mImpl->uboBindPointMap.end())
		{
			GLuint bindPoint = glGetUniformBlockIndex(mImpl->handle, name.c_str());
			mImpl->uboBindPointMap.insert({ name, bindPoint });
		}

		Material* newMat = nullptr;
		mImpl->technique->addUBO(name, mImpl->uboElements[name]);
		bool found = false;
		for (auto s : mImpl->mUBOsInUse)
		{
			if (s == name)
			{
				found = true;
			}
		}

		bool materialExists = false;
		auto existingMaterials = AssetManager::instance().get<MaterialAsset>(name);
		if (existingMaterials != nullptr)
		{
			materialExists = true;
			newMat = existingMaterials->getMaterial();
			mImpl->technique->addMaterial(name, newMat);
		}

		if (!found)
		{
			mImpl->mUBOsInUse.push_back(name);

			if (!materialExists)
			{
				auto newMatAsset = AssetManager::instance().load<MaterialAsset>(name,
					ASSET_LOAD_MED_PRIO,
					mImpl->uboElements[name]);
				newMat = newMatAsset->getMaterial();
				mImpl->technique->addMaterial(name, newMat);
			}

			mImpl->uboBindPoints[name] = nUBOBindPoints++;
		}

		/*
				auto existingMaterials = mImpl->entityManager->getComponents<MaterialComponentNew>();
				bool materialExists = false;
				for(auto existingMaterial : existingMaterials)
				{
					if(existingMaterial.material->getName().compare(name) == 0)
					{
						materialExists = true;

						newMat = existingMaterial.material;
						mImpl->technique->addMaterial(name, newMat);

						break;
					}
				}

				if (!found)
				{
					mImpl->mUBOsInUse.push_back(name);

					if(!materialExists)
					{
						newMat = std::make_shared<MaterialRod>();
						newMat->addUniforms(mImpl->uboElements[name]);
						newMat->setName(name);
						mImpl->technique->addMaterial(name, newMat);

						QHandle<QEntity> newMatEnt = mImpl->entityManager->create();
						MaterialComponentNew newMaterialComp(newMat, name);
						newMatEnt->addComponentWithTag<MaterialComponentNew>(name.c_str(), newMaterialComp);
					}

					mImpl->uboBindPoints[name] = nUBOBindPoints++;
				}
		*/
	}
}

void Shader::_shutdown() const
{
	glDeleteProgram(mImpl->handle);
}

void Shader::_parseSamplers()
{
	std::string source = mImpl->pass->getPassSource();

	const uint32_t bracketOpenLocation = static_cast<uint32_t>(source.find_first_of('{'));
	const uint32_t bracketCloseLocation = static_cast<uint32_t>(source.find_last_of('}'));

	std::string data = source.substr(bracketOpenLocation + 1, bracketCloseLocation - bracketOpenLocation - 1);

	std::vector<std::string> parts = StringUtils::getLines(data);

	for (size_t k = 0; k < parts.size(); k++)
	{
		std::string param = parts[k];

		if (strstr(param.c_str(), "MinFilter"))
		{
			std::string modeBlock;
			StringUtils::findEndOfBlock(parts, static_cast<uint32_t>(k), static_cast<uint32_t>(parts.size() - k), &modeBlock);

			const uint32_t bracketStart = static_cast<uint32_t>(modeBlock.find_first_of('{'));
			const uint32_t bracketEnd = static_cast<uint32_t>(modeBlock.find_last_of('}'));

			std::string paramValuesString = modeBlock.substr(bracketStart + 1, bracketEnd - bracketStart - 1);
			paramValuesString.erase(std::remove_if(paramValuesString.begin(), paramValuesString.end(), isspace), paramValuesString.end());

			std::vector<std::string> paramValues = StringUtils::splitWithRegex(paramValuesString, ",");

			std::string samplerName = paramValues[0];
			const std::string samplerValue = paramValues[1];

			SamplerObject* object;
			if (mImpl->mSamplerObjects.find(samplerName.c_str()) != mImpl->mSamplerObjects.end())
			{
				object = mImpl->mSamplerObjects[samplerName.c_str()];
				object->minFilter = samplerValue;
			}
			else
			{
				object = new SamplerObject();
				object->minFilter = samplerValue;
			}
			mImpl->mSamplerObjects[samplerName.c_str()] = object;
		}

		if (strstr(param.c_str(), "MagFilter"))
		{
			std::string modeBlock;
			StringUtils::findEndOfBlock(parts, static_cast<uint32_t>(k), static_cast<uint32_t>(parts.size() - k), &modeBlock);

			const uint32_t bracketStart = static_cast<uint32_t>(modeBlock.find_first_of('{'));
			const uint32_t bracketEnd = static_cast<uint32_t>(modeBlock.find_last_of('}'));

			std::string paramValuesString = modeBlock.substr(bracketStart + 1, bracketEnd - bracketStart - 1);
			paramValuesString.erase(std::remove_if(paramValuesString.begin(), paramValuesString.end(), isspace), paramValuesString.end());

			std::vector<std::string> paramValues = StringUtils::splitWithRegex(paramValuesString, ",");

			std::string samplerName = paramValues[0];
			const std::string samplerValue = paramValues[1];

			SamplerObject* object;
			if (mImpl->mSamplerObjects.find(samplerName.c_str()) != mImpl->mSamplerObjects.end())
			{
				object = mImpl->mSamplerObjects[samplerName.c_str()];
				object->magFilter = samplerValue;
			}
			else
			{
				object = new SamplerObject();
				object->magFilter = samplerValue;
			}
			mImpl->mSamplerObjects[samplerName.c_str()] = object;
		}

		if (strstr(param.c_str(), "WrapS"))
		{
			std::string modeBlock;
			StringUtils::findEndOfBlock(parts, static_cast<uint32_t>(k), static_cast<uint32_t>(parts.size() - k), &modeBlock);

			const uint32_t bracketStart = static_cast<uint32_t>(modeBlock.find_first_of('{'));
			const uint32_t bracketEnd = static_cast<uint32_t>(modeBlock.find_last_of('}'));

			std::string paramValuesString = modeBlock.substr(bracketStart + 1, bracketEnd - bracketStart - 1);
			paramValuesString.erase(std::remove_if(paramValuesString.begin(), paramValuesString.end(), isspace), paramValuesString.end());

			std::vector<std::string> paramValues = StringUtils::splitWithRegex(paramValuesString, ",");

			std::string samplerName = paramValues[0];
			const std::string samplerValue = paramValues[1];

			SamplerObject* object;
			if (mImpl->mSamplerObjects.find(samplerName.c_str()) != mImpl->mSamplerObjects.end())
			{
				object = mImpl->mSamplerObjects[samplerName.c_str()];
				object->wrapS = samplerValue;
			}
			else
			{
				object = new SamplerObject();
				object->wrapS = samplerValue;
			}
			mImpl->mSamplerObjects[samplerName.c_str()] = object;
		}

		if (strstr(param.c_str(), "WrapT"))
		{
			std::string modeBlock;
			StringUtils::findEndOfBlock(parts, static_cast<uint32_t>(k), static_cast<uint32_t>(parts.size() - k), &modeBlock);

			const uint32_t bracketStart = static_cast<uint32_t>(modeBlock.find_first_of('{'));
			const uint32_t bracketEnd = static_cast<uint32_t>(modeBlock.find_last_of('}'));

			std::string paramValuesString = modeBlock.substr(bracketStart + 1, bracketEnd - bracketStart - 1);
			paramValuesString.erase(std::remove_if(paramValuesString.begin(), paramValuesString.end(), isspace), paramValuesString.end());

			std::vector<std::string> paramValues = StringUtils::splitWithRegex(paramValuesString, ",");

			std::string samplerName = paramValues[0];
			const std::string samplerValue = paramValues[1];

			SamplerObject* object;
			if (mImpl->mSamplerObjects.find(samplerName.c_str()) != mImpl->mSamplerObjects.end())
			{
				object = mImpl->mSamplerObjects[samplerName.c_str()];
				object->wrapT = samplerValue;
			}
			else
			{
				object = new SamplerObject();
				object->wrapT = samplerValue;
			}
			mImpl->mSamplerObjects[samplerName.c_str()] = object;
		}

		if (strstr(param.c_str(), "WrapR"))
		{
			std::string modeBlock;
			StringUtils::findEndOfBlock(parts, static_cast<uint32_t>(k), static_cast<uint32_t>(parts.size() - k), &modeBlock);

			const uint32_t bracketStart = static_cast<uint32_t>(modeBlock.find_first_of('{'));
			const uint32_t bracketEnd = static_cast<uint32_t>(modeBlock.find_last_of('}'));

			std::string paramValuesString = modeBlock.substr(bracketStart + 1, bracketEnd - bracketStart - 1);
			paramValuesString.erase(std::remove_if(paramValuesString.begin(), paramValuesString.end(), isspace), paramValuesString.end());

			std::vector<std::string> paramValues = StringUtils::splitWithRegex(paramValuesString, ",");

			std::string samplerName = paramValues[0];
			const std::string samplerValue = paramValues[1];

			SamplerObject* object;
			if (mImpl->mSamplerObjects.find(samplerName.c_str()) != mImpl->mSamplerObjects.end())
			{
				object = mImpl->mSamplerObjects[samplerName.c_str()];
				object->wrapR = samplerValue;
			}
			else
			{
				object = new SamplerObject();
				object->wrapR = samplerValue;
			}
			mImpl->mSamplerObjects[samplerName.c_str()] = object;
		}
	}

	std::map<std::string, SamplerObject*>::iterator it;
	for (it = mImpl->mSamplerObjects.begin(); it != mImpl->mSamplerObjects.end(); ++it)
	{
		SamplerObject* object = it->second;
		glGenSamplers(1, &object->samplerID);
		if (object->minFilter != "NONE")
			glSamplerParameteri(object->samplerID, GL_TEXTURE_MIN_FILTER, StringUtils::parseToGLEnum(object->minFilter));

		if (object->magFilter != "NONE")
			glSamplerParameteri(object->samplerID, GL_TEXTURE_MAG_FILTER, StringUtils::parseToGLEnum(object->magFilter));

		if (object->wrapS != "NONE")
			glSamplerParameteri(object->samplerID, GL_TEXTURE_WRAP_S, StringUtils::parseToGLEnum(object->wrapS));

		if (object->wrapR != "NONE")
			glSamplerParameteri(object->samplerID, GL_TEXTURE_WRAP_R, StringUtils::parseToGLEnum(object->wrapR));

		if (object->wrapT != "NONE")
			glSamplerParameteri(object->samplerID, GL_TEXTURE_WRAP_T, StringUtils::parseToGLEnum(object->wrapT));
	}
}

uint32_t Shader::_compile(std::string** aShaders, ShaderErrorInfo& info) const
{
	const char* vertexSource = aShaders[0]->c_str();
	const char* fragmentSource = aShaders[1]->c_str();
	const char* geometrySource = aShaders[2]->c_str();
	const char* tcSource = aShaders[3]->c_str();
	const char* teSource = aShaders[4]->c_str();

	const uint32_t program = glCreateProgram();
	const uint32_t vertex = glCreateShader(GL_VERTEX_SHADER);

	uint32_t geometry = 0;
	uint32_t te = 0;
	uint32_t tc = 0;
	uint32_t fragment;

	if (strcmp(geometrySource, "") != 0)
	{
		geometry = glCreateShader(GL_GEOMETRY_SHADER);
	}

	if (strcmp(tcSource, "") != 0)
	{
		tc = glCreateShader(GL_TESS_CONTROL_SHADER);
	}

	if (strcmp(teSource, "") != 0)
	{
		te = glCreateShader(GL_TESS_EVALUATION_SHADER);
	}

	if (strcmp(fragmentSource, "") != 0)
	{
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
	}

	glShaderSource(vertex, 1, &vertexSource, nullptr);
	glCompileShader(vertex);

	int32_t result;
	glGetShaderiv(vertex, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		int32_t length;
		glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &length);
		std::vector<char> error(length);
		glGetShaderInfoLog(vertex, length, &length, &error[0]);
		const std::string errorMessage(&error[0]);

		int32_t lineNumber;
		sscanf(&error[0], "%*s %*d:%d", &lineNumber);

		info.shader = 0;
		info.message[info.shader] += "Failed to compile vertex shader!\n";
		info.line[info.shader] = lineNumber;
		info.message[info.shader] += errorMessage;
		glDeleteShader(vertex);
		return 0;
	}

	if (strcmp(fragmentSource, "") != 0)
	{
		glShaderSource(fragment, 1, &fragmentSource, nullptr);
		glCompileShader(fragment);

		glGetShaderiv(fragment, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE)
		{
			int32_t length;
			glGetShaderiv(fragment, GL_INFO_LOG_LENGTH, &length);
			std::vector<char> error(length);
			glGetShaderInfoLog(fragment, length, &length, &error[0]);
			const std::string errorMessage(&error[0]);

			int32_t lineNumber;
			sscanf(&error[0], "%*s %*d:%d", &lineNumber);

			info.shader = 1;
			info.message[info.shader] += "Failed to compile fragment shader!\n";
			info.line[info.shader] = lineNumber;
			info.message[info.shader] += errorMessage;
			glDeleteShader(fragment);
			return 0;
		}
	}

	if (strcmp(geometrySource, "") != 0)
	{
		glShaderSource(geometry, 1, &geometrySource, nullptr);
		glCompileShader(geometry);

		glGetShaderiv(geometry, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE)
		{
			int32_t length;
			glGetShaderiv(geometry, GL_INFO_LOG_LENGTH, &length);
			std::vector<char> error(length);
			glGetShaderInfoLog(geometry, length, &length, &error[0]);
			const std::string errorMessage(&error[0]);

			int32_t lineNumber;
			sscanf(&error[0], "%*s %*d:%d", &lineNumber);

			info.shader = 2;
			info.message[info.shader] += "Failed to compile geometry shader!\n";
			info.line[info.shader] = lineNumber;
			info.message[info.shader] += errorMessage;
			glDeleteShader(geometry);
			return 0;
		}
	}

	if (strcmp(tcSource, "") != 0)
	{
		glShaderSource(tc, 1, &tcSource, nullptr);
		glCompileShader(tc);

		glGetShaderiv(tc, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE)
		{
			int32_t length;
			glGetShaderiv(tc, GL_INFO_LOG_LENGTH, &length);
			std::vector<char> error(length);
			glGetShaderInfoLog(tc, length, &length, &error[0]);
			const std::string errorMessage(&error[0]);

			int32_t lineNumber;
			sscanf(&error[0], "%*s %*d:%d", &lineNumber);

			info.shader = 3;
			info.message[info.shader] += "Failed to compile TC shader!\n";
			info.line[info.shader] = lineNumber;
			info.message[info.shader] += errorMessage;
			glDeleteShader(tc);
			return 0;
		}
	}

	if (strcmp(teSource, "") != 0)
	{
		glShaderSource(te, 1, &teSource, nullptr);
		glCompileShader(te);

		glGetShaderiv(te, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE)
		{
			int32_t length;
			glGetShaderiv(te, GL_INFO_LOG_LENGTH, &length);
			std::vector<char> error(length);
			glGetShaderInfoLog(te, length, &length, &error[0]);
			const std::string errorMessage(&error[0]);

			int32_t lineNumber;
			sscanf(&error[0], "%*s %*d:%d", &lineNumber);

			info.shader = 4;
			info.message[info.shader] += "Failed to compile TE shader!\n";
			info.line[info.shader] = lineNumber;
			info.message[info.shader] += errorMessage;
			glDeleteShader(te);
			return 0;
		}
	}

	glAttachShader(program, vertex);
	if (strcmp(fragmentSource, "") != 0)
	{
		glAttachShader(program, fragment);
	}
	if (strcmp(geometrySource, "") != 0)
	{
		glAttachShader(program, geometry);
	}
	if (strcmp(tcSource, "") != 0)
	{
		glAttachShader(program, tc);
	}
	if (strcmp(teSource, "") != 0)
	{
		glAttachShader(program, te);
	}

	glLinkProgram(program);
	glValidateProgram(program);

	if (strcmp(geometrySource, "") != 0)
	{
		glDetachShader(program, geometry);
		glDeleteShader(geometry);
	}
	if (strcmp(tcSource, "") != 0)
	{
		glDetachShader(program, tc);
		glDeleteShader(tc);
	}
	if (strcmp(teSource, "") != 0)
	{
		glDetachShader(program, te);
		glDeleteShader(te);
	}

	glDetachShader(program, vertex);
	glDeleteShader(vertex);

	if (strcmp(fragmentSource, "") != 0)
	{
		glDetachShader(program, fragment);
		glDeleteShader(fragment);
	}

	return program;
}

void Shader::bind() const
{
	glUseProgram(mImpl->handle);
	//	sCurrentlyBound = const_cast<ShaderRod*>(this);
}

void Shader::unbind() const
{
	glUseProgram(0);
	//	sCurrentlyBound = nullptr;
}

void Shader::_parse(const std::string& aVertexSource, const std::string& aFragmentSource, const std::string& aGeometrySource, const std::string& aTcSource, const std::string& aTeSource) const
{
	mImpl->VSUniformBuffers.push_back(new ShaderUniformBufferDeclaration("Global", 0));
	mImpl->PSUniformBuffers.push_back(new ShaderUniformBufferDeclaration("Global", 1));
	mImpl->GSUniformBuffers.push_back(new ShaderUniformBufferDeclaration("Global", 2));
	mImpl->TEUniformBuffers.push_back(new ShaderUniformBufferDeclaration("Global", 3));
	mImpl->TCUniformBuffers.push_back(new ShaderUniformBufferDeclaration("Global", 4));

	const char* token;
	const char* vstr;
	const char* fstr;
	const char* gstr;
	const char* testr;
	const char* tcstr;

	std::string vs;
	std::string fs;
	std::string gs;
	std::string tc;
	std::string te;

	std::vector<std::string> parts = StringUtils::getLines(aVertexSource);
	for (size_t i = 0; i < parts.size(); i++)
	{
		std::vector<std::string> p = ssplitOnAny(parts[i].c_str(), "\t ");
		if (p.size() == 2)
			continue;

		vs += parts[i] + "\n";
	}

	parts = StringUtils::getLines(aFragmentSource);
	for (size_t i = 0; i < parts.size(); i++)
	{
		std::vector<std::string> p = ssplitOnAny(parts[i].c_str(), "\t ");
		if (p.size() == 2)
			continue;

		fs += parts[i] + "\n";
	}

	parts = StringUtils::getLines(aGeometrySource);
	for (size_t i = 0; i < parts.size(); i++)
	{
		std::vector<std::string> p = ssplitOnAny(parts[i].c_str(), "\t ");
		if (p.size() == 2)
			continue;

		gs += parts[i];
	}

	parts = StringUtils::getLines(aTeSource);
	for (size_t i = 0; i < parts.size(); i++)
	{
		std::vector<std::string> p = ssplitOnAny(parts[i].c_str(), "\t ");
		if (p.size() == 2)
			continue;

		te += parts[i];
	}

	parts = StringUtils::getLines(aTcSource);
	for (size_t i = 0; i < parts.size(); i++)
	{
		std::vector<std::string> p = ssplitOnAny(parts[i].c_str(), "\t ");
		if (p.size() == 2)
			continue;

		tc += parts[i];
	}

	vstr = vs.c_str();
	while ((token = StringUtils::findToken(vstr, "struct")))
	{
		_parseUniformStruct(StringUtils::getStatement(token, &vstr), 0);
	}

	vstr = vs.c_str();
	while ((token = StringUtils::findToken(vstr, "uniform")))
	{
		_parseUniform(StringUtils::getStatement(token, &vstr), 0);
	}

	fstr = fs.c_str();
	while ((token = StringUtils::findToken(fstr, "struct")))
	{
		_parseUniformStruct(StringUtils::getStatement(token, &fstr), 1);
	}

	fstr = fs.c_str();
	while ((token = StringUtils::findToken(fstr, "uniform")))
	{
		_parseUniform(StringUtils::getStatement(token, &fstr), 1);
	}

	gstr = gs.c_str();
	while ((token = StringUtils::findToken(gstr, "struct")))
	{
		_parseUniformStruct(StringUtils::getStatement(token, &gstr), 2);
	}

	gstr = gs.c_str();
	while ((token = StringUtils::findToken(gstr, "uniform")))
	{
		_parseUniform(StringUtils::getStatement(token, &gstr), 2);
	}

	testr = te.c_str();
	while ((token = StringUtils::findToken(testr, "struct")))
	{
		_parseUniformStruct(StringUtils::getStatement(token, &testr), 3);
	}

	testr = te.c_str();
	while ((token = StringUtils::findToken(testr, "uniform")))
	{
		_parseUniform(StringUtils::getStatement(token, &testr), 3);
	}

	tcstr = tc.c_str();
	while ((token = StringUtils::findToken(tcstr, "struct")))
	{
		_parseUniformStruct(StringUtils::getStatement(token, &tcstr), 4);
	}

	tcstr = tc.c_str();
	while ((token = StringUtils::findToken(tcstr, "uniform")))
	{
		_parseUniform(StringUtils::getStatement(token, &tcstr), 4);
	}

	std::vector<std::string> vsLines = StringUtils::getLines(aVertexSource);
	for (size_t l = 0; l < vsLines.size(); l++)
	{
		std::string line = vsLines[l];
		if (strstr(line.c_str(), "uniform"))
		{
			if (line.compare(0, 1, "\t") != 0)
			{
				line = "\t" + line;
			}
			std::vector<std::string> p = StringUtils::splitWithRegex(line, "\\s+");
			if (p.size() != 2)
				continue;

			std::vector<std::string> pparts = ssplitOnAny(line.c_str(), " \t");
			std::string name = pparts[1].c_str();

			mImpl->uboNames.push_back(name);

			std::string buffer;
			StringUtils::findEndOfBlock(vsLines, static_cast<uint32_t>(l), static_cast<uint32_t>(vsLines.size()), &buffer);

			uint32_t startBracket = static_cast<uint32_t>(buffer.find_first_of('{'));
			uint32_t endBracket = static_cast<uint32_t>(buffer.find_last_of('}'));

			std::string listItems = buffer.substr(startBracket + 1, endBracket - startBracket - 1);

			std::vector<std::string> items = StringUtils::split(listItems, ';');

			uint32_t size = 0;
			uint32_t offset = 0;

			for (size_t u = 0; u < items.size() - 1; u++)
			{
				std::string uni = items[u];
				std::vector<std::string> uniParts = StringUtils::splitWithRegex(uni, "\\s+");

				std::string uniType = uniParts[0];
				uniType.erase(std::remove_if(uniType.begin(), uniType.end(), isspace), uniType.end());

				std::string uniName = uniParts[1];
				uniName.erase(std::remove_if(uniName.begin(), uniName.end(), isspace), uniName.end());

				uint32_t amountNumber = 1;

				if (strstr(uniType.c_str(), "["))
				{
					// this is an array
					uint32_t arrayStart = static_cast<uint32_t>(uniType.find_first_of('['));
					uint32_t arrayEnd = static_cast<uint32_t>(uniType.find_last_of(']'));

					std::string amount = uniType.substr(arrayStart + 1, arrayEnd - arrayStart - 1);

					amountNumber = atoi(amount.c_str());
				}

				auto t = ShaderUniformDeclaration::stringToType(uniType);
				uint32_t uniSize = ShaderUniformDeclaration::sizeofUniformBufferType(t, amountNumber > 1, amountNumber);
				uint32_t uniOffset = ShaderUniformDeclaration::offsetofUniformBufferType(t, amountNumber > 1, amountNumber);

				//				GLuint uniformIndex;
				//				const char* uniformNameCStr = uniName.c_str();
				//				glGetUniformIndices(mImpl->handle, 1, &uniformNameCStr, &uniformIndex);

				//				if(uniformIndex == GL_INVALID_INDEX)
				//					continue;

				//				GLint uniformSize, uniformOffset;
				//				glGetActiveUniformsiv(mImpl->handle, 1, &uniformIndex, GL_UNIFORM_SIZE, &uniformSize);
				//				glGetActiveUniformsiv(mImpl->handle, 1, &uniformIndex, GL_UNIFORM_OFFSET, &uniformOffset);

								// store uniform
				//				mImpl->uboElements[name].push_back(new UniformBufferElement{ offset, uniSize, uniName });

				offset += uniOffset;
				size += uniOffset;
				mImpl->uboUniformMap[name].push_back(uniName);
			}

			//			std::shared_ptr<MaterialRod> newMat = std::make_shared<MaterialRod>();
			//			newMat->addUniforms(mImpl->uboElements[name]);
			//			newMat->setSize(size);
			//			newMat->setName(name);
			//			mImpl->technique->addUBO(name, mImpl->uboElements[name]);
			//			bool found = false;
			//            for(auto s : mImpl->mUBOsInUse)
			//            {
			//                if(s == name)
			//                {
			//					found = true;
			//                }
			//            }

			//			if (!found)
			//			{
			//				mImpl->mUBOsInUse.push_back(name);
			//
			//				QEntity* newMatEnt = mImpl->entityManager->create();
			//				MaterialComponentNew newMaterialComp(newMat, name);
			//				newMatEnt->addComponent<MaterialComponentNew>(newMaterialComp);
			//			}
		}
	}
}

void Shader::_parseUniform(const std::string& aStatement, const uint32_t aShaderType) const
{
	std::vector<std::string> tokens = StringUtils::tokenize(aStatement);
	uint32_t index = 0;

	index++;
	const std::string typeString = tokens[index++];
	std::string name = tokens[index++];

	if (const char* s = strstr(name.c_str(), ";"))
	{
		name = std::string(name.c_str(), s - name.c_str());
	}

	std::string n(name);
	int32_t count = 1;
	const char* namestr = n.c_str();
	if (const char* s = strstr(namestr, "["))
	{
		name = std::string(namestr, s - namestr);

		const char* end = strstr(namestr, "]");
		std::string c(s + 1, end - s);
		count = atoi(c.c_str());
	}

	mImpl->uniformNames.push_back(namestr);

	if (_isTypeStringResource(typeString))
	{
		ShaderResourceDeclaration* declaration = new ShaderResourceDeclaration(ShaderResourceDeclaration::stringToType(typeString), name, count);
		mImpl->samplers.insert(std::pair(name, static_cast<uint32_t>(mImpl->samplers.size())));

		mImpl->resources.push_back(declaration);
	}
	else
	{
		const ShaderUniformDeclaration::Type t = ShaderUniformDeclaration::stringToType(typeString);
		ShaderUniformDeclaration* declaration = nullptr;

		if (t != ShaderUniformDeclaration::Type::NONE)
		{
			declaration = new ShaderUniformDeclaration(t, name, count);
		}

		if (declaration == nullptr)
			return;

		if (StringUtils::startsWith(name, "sys_"))
		{
			if (aShaderType == 0)
			{
				mImpl->VSUniformBuffers.front()->pushUniform(declaration);
			}
			else if (aShaderType == 1)
			{
				mImpl->PSUniformBuffers.front()->pushUniform(declaration);
			}
			else if (aShaderType == 2)
			{
				mImpl->GSUniformBuffers.front()->pushUniform(declaration);
			}
			else if (aShaderType == 3)
			{
				mImpl->TEUniformBuffers.front()->pushUniform(declaration);
			}
			else if (aShaderType == 4)
			{
				mImpl->TCUniformBuffers.front()->pushUniform(declaration);
			}
		}
		else
		{
			if (aShaderType == 0)
			{
				if (mImpl->VSUserUniformBuffer == nullptr)
				{
					mImpl->VSUserUniformBuffer = new ShaderUniformBufferDeclaration("", 0);
				}

				mImpl->VSUserUniformBuffer->pushUniform(declaration);
			}
			else if (aShaderType == 1)
			{
				if (mImpl->PSUserUniformBuffer == nullptr)
				{
					mImpl->PSUserUniformBuffer = new ShaderUniformBufferDeclaration("", 1);
				}

				mImpl->PSUserUniformBuffer->pushUniform(declaration);
			}
			else if (aShaderType == 2)
			{
				if (mImpl->GSUserUniformBuffer == nullptr)
				{
					mImpl->GSUserUniformBuffer = new ShaderUniformBufferDeclaration("", 2);
				}

				mImpl->GSUserUniformBuffer->pushUniform(declaration);
			}
			else if (aShaderType == 3)
			{
				if (mImpl->TEUserUniformBuffer == nullptr)
				{
					mImpl->TEUserUniformBuffer = new ShaderUniformBufferDeclaration("", 3);
				}

				mImpl->TEUserUniformBuffer->pushUniform(declaration);
			}
			else if (aShaderType == 4)
			{
				if (mImpl->TCUserUniformBuffer == nullptr)
				{
					mImpl->TCUserUniformBuffer = new ShaderUniformBufferDeclaration("", 4);
				}

				mImpl->TCUserUniformBuffer->pushUniform(declaration);
			}
		}
	}
}

void Shader::_parseUniformStruct(const std::string& aBlock, uint32_t aShaderType) const
{
	std::vector<std::string> tokens = StringUtils::tokenize(aBlock);

	uint32_t index = 0;
	index++; // struct;

	std::string name = tokens[index++];
	ShaderStruct* uniformStruct = new ShaderStruct(name);
	index++; // {
	while (index < tokens.size())
	{
		if (tokens[index] == "}")
		{
			break; // end of struct
		}

		std::string uniformType = tokens[index++];
		std::string uniformName = tokens[index++];

		if (const char* s = strstr(uniformName.c_str(), ";"))
		{
			uniformName = std::string(uniformName.c_str(), s - uniformName.c_str());
		}

		uint32_t count = 1;
		const char* nameStr = uniformName.c_str();
		if (const char* s = strstr(nameStr, "["))
		{
			uniformName = std::string(nameStr, s - nameStr);

			const char* end = strstr(nameStr, "]");
			std::string c(s + 1, end - s);
			count = atoi(c.c_str());
		}

		ShaderUniformDeclaration* field = new ShaderUniformDeclaration(ShaderUniformDeclaration::stringToType(uniformType), uniformName, count);
		uniformStruct->addField(field);
	}

	mImpl->structs.push_back(uniformStruct);
}

ShaderStruct* Shader::_findStruct(const std::string& aName) const
{
	for (ShaderStruct* s : mImpl->structs)
	{
		if (s->getName() == aName)
		{
			return s;
		}
	}

	return nullptr;
}

bool Shader::_isTypeStringResource(const std::string& aType) const
{
	if (aType == "sampler2D")		return true;
	if (aType == "samplerCube")		return true;
	if (aType == "sampler2DShadow")	return true;
	if (aType == "samplerBuffer")	return true;
	return false;
}

void Shader::_resolveUniforms()
{
	bind();

	for (uint32_t i = 0; i < mImpl->VSUniformBuffers.size(); i++)
	{
		ShaderUniformBufferDeclaration* decl = mImpl->VSUniformBuffers[i];
		const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
		for (uint32_t j = 0; j < uniforms.size(); j++)
		{
			ShaderUniformDeclaration* uniform = uniforms[j];
			uniform->mLocation = _getUniformLocation(uniform->mName);
		}

		for (uint32_t i = 0; i < mImpl->PSUniformBuffers.size(); i++)
		{
			ShaderUniformBufferDeclaration* decl = mImpl->PSUniformBuffers[i];
			const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
			for (uint32_t j = 0; j < uniforms.size(); j++)
			{
				ShaderUniformDeclaration* uniform = uniforms[j];
				uniform->mLocation = _getUniformLocation(uniform->mName);
			}
		}

		for (uint32_t i = 0; i < mImpl->GSUniformBuffers.size(); i++)
		{
			ShaderUniformBufferDeclaration* decl = mImpl->GSUniformBuffers[i];
			const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
			for (uint32_t j = 0; j < uniforms.size(); j++)
			{
				ShaderUniformDeclaration* uniform = uniforms[j];
				uniform->mLocation = _getUniformLocation(uniform->mName);
			}
		}

		for (uint32_t i = 0; i < mImpl->TEUniformBuffers.size(); i++)
		{
			ShaderUniformBufferDeclaration* decl = mImpl->TEUniformBuffers[i];
			const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
			for (uint32_t j = 0; j < uniforms.size(); j++)
			{
				ShaderUniformDeclaration* uniform = uniforms[j];
				uniform->mLocation = _getUniformLocation(uniform->mName);
			}
		}

		for (uint32_t i = 0; i < mImpl->TCUniformBuffers.size(); i++)
		{
			ShaderUniformBufferDeclaration* decl = mImpl->TCUniformBuffers[i];
			const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
			for (uint32_t j = 0; j < uniforms.size(); j++)
			{
				ShaderUniformDeclaration* uniform = uniforms[j];
				uniform->mLocation = _getUniformLocation(uniform->mName);
			}
		}

		{
			ShaderUniformBufferDeclaration* decl = mImpl->VSUserUniformBuffer;
			if (decl)
			{
				const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
				for (uint32_t j = 0; j < uniforms.size(); j++)
				{
					ShaderUniformDeclaration* uniform = uniforms[j];
					uniform->mLocation = _getUniformLocation(uniform->mName);
				}
			}
		}

		{
			ShaderUniformBufferDeclaration* decl = mImpl->PSUserUniformBuffer;
			if (decl)
			{
				const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
				for (uint32_t j = 0; j < uniforms.size(); j++)
				{
					ShaderUniformDeclaration* uniform = uniforms[j];
					uniform->mLocation = _getUniformLocation(uniform->mName);
				}
			}
		}

		{
			ShaderUniformBufferDeclaration* decl = mImpl->GSUserUniformBuffer;
			if (decl)
			{
				const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
				for (uint32_t j = 0; j < uniforms.size(); j++)
				{
					ShaderUniformDeclaration* uniform = uniforms[j];
					uniform->mLocation = _getUniformLocation(uniform->mName);
				}
			}
		}

		{
			ShaderUniformBufferDeclaration* decl = mImpl->TEUserUniformBuffer;
			if (decl)
			{
				const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
				for (uint32_t j = 0; j < uniforms.size(); j++)
				{
					ShaderUniformDeclaration* uniform = uniforms[j];
					uniform->mLocation = _getUniformLocation(uniform->mName);
				}
			}
		}

		{
			ShaderUniformBufferDeclaration* decl = mImpl->TCUserUniformBuffer;
			if (decl)
			{
				const std::vector<ShaderUniformDeclaration*>& uniforms = decl->getUniformDeclarations();
				for (uint32_t j = 0; j < uniforms.size(); j++)
				{
					ShaderUniformDeclaration* uniform = uniforms[j];
					uniform->mLocation = _getUniformLocation(uniform->mName);
				}
			}
		}

		uint32_t sampler = 0;
		for (uint32_t i = 0; i < mImpl->resources.size(); i++)
		{
			ShaderResourceDeclaration* resource = mImpl->resources[i];
			const uint32_t location = _getUniformLocation(resource->mName);
			if (resource->getCount() == 1)
			{
				resource->mRegister = sampler;
				setUniform1i(location, sampler++);
			}
			else if (resource->getCount() > 1)
			{
				resource->mRegister = 0;
				const uint32_t count = resource->getCount();
				int32_t* samplers = new int32_t[count];
				for (uint32_t s = 0; s < count; s++)
					samplers[s] = s;
				setUniform1iv(resource->getName(), samplers, count);
				delete[] samplers;
			}
		}
		unbind();
	}
}

void Shader::_validateUniforms() const
{

}

bool Shader::_isSystemUniform(ShaderUniformDeclaration* aUniform) const
{
	return StringUtils::startsWith(aUniform->getName(), "sys_");
}

int32_t Shader::_getUniformLocation(const std::string& aName) const
{
	const int32_t result = glGetUniformLocation(mImpl->handle, aName.c_str());
	return result;
	//	auto iter = mImpl->uniformLocationMap.find(aName);
	//	if(iter != mImpl->uniformLocationMap.end())
	//		return iter->second;

	//	return -1;
}

ShaderUniformDeclaration* Shader::_findUniformDeclaration(const std::string& aName, const ShaderUniformBufferDeclaration* aBuffer) const
{
	if (!aBuffer)
		return nullptr;

	const std::vector<ShaderUniformDeclaration*>& uniforms = aBuffer->getUniformDeclarations();
	for (uint32_t i = 0; i < uniforms.size(); i++)
	{
		if (uniforms[i]->getName() == aName)
			return uniforms[i];
	}
	return nullptr;
}

ShaderUniformDeclaration* Shader::_findUniformDeclaration(const std::string& aName) const
{
	ShaderUniformDeclaration* result = nullptr;
	for (uint32_t i = 0; i < mImpl->VSUniformBuffers.size(); i++)
	{
		result = _findUniformDeclaration(aName, mImpl->VSUniformBuffers[i]);
		if (result)
			return result;
	}

	for (uint32_t i = 0; i < mImpl->PSUniformBuffers.size(); i++)
	{
		result = _findUniformDeclaration(aName, mImpl->PSUniformBuffers[i]);
		if (result)
			return result;
	}

	for (uint32_t i = 0; i < mImpl->GSUniformBuffers.size(); i++)
	{
		result = _findUniformDeclaration(aName, mImpl->GSUniformBuffers[i]);
		if (result)
			return result;
	}

	for (uint32_t i = 0; i < mImpl->TEUniformBuffers.size(); i++)
	{
		result = _findUniformDeclaration(aName, mImpl->TEUniformBuffers[i]);
		if (result)
			return result;
	}

	for (uint32_t i = 0; i < mImpl->TCUniformBuffers.size(); i++)
	{
		result = _findUniformDeclaration(aName, mImpl->TCUniformBuffers[i]);
		if (result)
			return result;
	}

	result = _findUniformDeclaration(aName, mImpl->VSUserUniformBuffer);
	if (result)
		return result;

	result = _findUniformDeclaration(aName, mImpl->PSUserUniformBuffer);
	if (result)
		return result;

	result = _findUniformDeclaration(aName, mImpl->GSUserUniformBuffer);
	if (result)
		return result;

	result = _findUniformDeclaration(aName, mImpl->TEUserUniformBuffer);
	if (result)
		return result;

	result = _findUniformDeclaration(aName, mImpl->TCUserUniformBuffer);
	if (result)
		return result;

	return result;
}

void Shader::setVSSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot)
{
	bind();
	assert(mImpl->VSUniformBuffers.size() > aSlot);
	ShaderUniformBufferDeclaration* declaration = mImpl->VSUniformBuffers[aSlot];
	_resolveAndSetUniforms(declaration, aData, aSize);
}

void Shader::setPSSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot)
{
	bind();
	assert(mImpl->PSUniformBuffers.size() > aSlot);
	ShaderUniformBufferDeclaration* declaration = mImpl->PSUniformBuffers[aSlot];
	_resolveAndSetUniforms(declaration, aData, aSize);
}

void Shader::setGSSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot)
{
	bind();
	assert(mImpl->GSUniformBuffers.size() > aSlot);
	ShaderUniformBufferDeclaration* declaration = mImpl->GSUniformBuffers[aSlot];
	_resolveAndSetUniforms(declaration, aData, aSize);
}

void Shader::setTESystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot)
{
	bind();
	assert(mImpl->TEUniformBuffers.size() > aSlot);
	ShaderUniformBufferDeclaration* declaration = mImpl->TEUniformBuffers[aSlot];
	_resolveAndSetUniforms(declaration, aData, aSize);
}

void Shader::setTCSystemUniformBuffer(uint8_t* aData, const uint32_t aSize, const uint32_t aSlot)
{
	bind();
	assert(mImpl->TCUniformBuffers.size() > aSlot);
	ShaderUniformBufferDeclaration* declaration = mImpl->TCUniformBuffers[aSlot];
	_resolveAndSetUniforms(declaration, aData, aSize);
}

void Shader::setVSUserUniformBuffer(uint8_t* aData, const uint32_t aSize)
{
	_resolveAndSetUniforms(mImpl->VSUserUniformBuffer, aData, aSize);
}

void Shader::setPSUserUniformBuffer(uint8_t* aData, const uint32_t aSize)
{
	_resolveAndSetUniforms(mImpl->PSUserUniformBuffer, aData, aSize);
}

void Shader::setGSUserUniformBuffer(uint8_t* aData, const uint32_t aSize)
{
	_resolveAndSetUniforms(mImpl->GSUserUniformBuffer, aData, aSize);
}

void Shader::setTEUserUniformBuffer(uint8_t* aData, const uint32_t aSize)
{
	_resolveAndSetUniforms(mImpl->TEUserUniformBuffer, aData, aSize);
}

void Shader::setTCUserUniformBuffer(uint8_t* aData, const uint32_t aSize)
{
	_resolveAndSetUniforms(mImpl->TCUserUniformBuffer, aData, aSize);
}

const std::string& Shader::getName() const
{
	return mImpl->name;
}

void Shader::_resolveAndSetUniforms(ShaderUniformBufferDeclaration* aBuffer, uint8_t* aData, const uint32_t aSize) const
{
	const std::vector<ShaderUniformDeclaration*>& uniforms = aBuffer->getUniformDeclarations();
	for (uint32_t i = 0; i < uniforms.size(); i++)
	{
		ShaderUniformDeclaration* uniform = uniforms[i];
		_resolveAndSetUniform(uniform, aData, aSize);
	}
}

void Shader::_setUniformStruct(ShaderUniformDeclaration* aUniform, uint8_t* aData, uint32_t aOffset) const
{
	const ShaderStruct& s = aUniform->getShaderUniformStruct();
	const auto& fields = s.getFields();
	for (size_t k = 0; k < fields.size(); k++)
	{
		ShaderUniformDeclaration* field = fields[k];
		resolveAndSetUniformField(*field, aData, aOffset);
		aOffset += field->mSize;
	}
}

void Shader::_resolveAndSetUniform(ShaderUniformDeclaration* aUniform, uint8_t* aData, const uint32_t aSize) const
{
	if (aUniform->getLocation() == -1)
		return;

	if (aData == nullptr)
		return;

	uint32_t offset = aUniform->getOffset();
	int32_t res;
	switch (aUniform->getType())
	{
	case ShaderUniformDeclaration::Type::BOOL:
		offset = 0;
		res = *reinterpret_cast<bool*>(&aData[offset]);
		setUniform1i(aUniform->getLocation(), *reinterpret_cast<bool*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::FLOAT32:
		setUniform1f(aUniform->getLocation(), *reinterpret_cast<float*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::INT32:
		setUniform1i(aUniform->getLocation(), *reinterpret_cast<int32_t*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::VEC2:
		setUniform2f(aUniform->getLocation(), *reinterpret_cast<vec2<float>*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::VEC3:
		setUniform3f(aUniform->getLocation(), *reinterpret_cast<vec3<float>*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::VEC4:
		setUniform4f(aUniform->getLocation(), *reinterpret_cast<vec4<float>*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::MAT3:
		setUniformMat3(aUniform->getLocation(), *reinterpret_cast<mat3<float>*>(&aData[offset]));
		break;
	case ShaderUniformDeclaration::Type::MAT4:
		setUniformMat4(aUniform->getLocation(), *reinterpret_cast<mat4<float>*>(&aData[offset]));
		break;
	default:
		assert(false);
	}
}

void Shader::setUniform(const std::string& aName, uint8_t* aData) const
{
	ShaderUniformDeclaration* uniform = _findUniformDeclaration(aName);
	if (!uniform)
		return;
	_resolveAndSetUniform(uniform, aData, 0);
}

void Shader::resolveAndSetUniformField(const ShaderUniformDeclaration& aField, uint8_t* aData, const int32_t aOffset) const
{
	switch (aField.getType())
	{
	case ShaderUniformDeclaration::Type::FLOAT32:
		setUniform1f(aField.getLocation(), *reinterpret_cast<float*>(&aData[aOffset]));
		break;
	case ShaderUniformDeclaration::Type::INT32:
	case ShaderUniformDeclaration::Type::BOOL:
		setUniform1i(aField.getLocation(), *reinterpret_cast<int32_t*>(&aData[aOffset]));
		break;
	case ShaderUniformDeclaration::Type::VEC2:
		setUniform2f(aField.getLocation(), *reinterpret_cast<vec2<float>*>(&aData[aOffset]));
		break;
	case ShaderUniformDeclaration::Type::VEC3:
		setUniform3f(aField.getLocation(), *reinterpret_cast<vec3<float>*>(&aData[aOffset]));
		break;
	case ShaderUniformDeclaration::Type::VEC4:
		setUniform4f(aField.getLocation(), *reinterpret_cast<vec4<float>*>(&aData[aOffset]));
		break;
	case ShaderUniformDeclaration::Type::MAT3:
		setUniformMat3(aField.getLocation(), *reinterpret_cast<mat3<float>*>(&aData[aOffset]));
		break;
	case ShaderUniformDeclaration::Type::MAT4:
		setUniformMat4(aField.getLocation(), *reinterpret_cast<mat4<float>*>(&aData[aOffset]));
		break;
	default:
		assert(false);
	}
}

std::vector<ShaderUniformBufferDeclaration*>& Shader::getVSSystemUniformBuffer() const
{
	return mImpl->VSUniformBuffers;
}

std::vector<ShaderUniformBufferDeclaration*>& Shader::getPSSystemUniformBuffer() const
{
	return mImpl->PSUniformBuffers;
}

std::vector<ShaderUniformBufferDeclaration*>& Shader::getGSSystemUniformBuffer() const
{
	return mImpl->GSUniformBuffers;
}

std::vector<ShaderUniformBufferDeclaration*>& Shader::getTESystemUniformBuffer() const
{
	return mImpl->TEUniformBuffers;
}

std::vector<ShaderUniformBufferDeclaration*>& Shader::getTCSystemUniformBuffer() const
{
	return mImpl->TCUniformBuffers;
}

ShaderUniformBufferDeclaration* Shader::getVSUserUniformBuffer() const
{
	return mImpl->VSUserUniformBuffer;
}

ShaderUniformBufferDeclaration* Shader::getPSUserUniformBuffer() const
{
	return mImpl->PSUserUniformBuffer;
}

ShaderUniformBufferDeclaration* Shader::getGSUserUniformBuffer() const
{
	return mImpl->GSUserUniformBuffer;
}

ShaderUniformBufferDeclaration* Shader::getTEUserUniformBuffer() const
{
	return mImpl->TEUserUniformBuffer;
}

ShaderUniformBufferDeclaration* Shader::getTCUserUniformBuffer() const
{
	return mImpl->TCUserUniformBuffer;
}

uint32_t Shader::getProgramID()
{
	return mImpl->handle;
}

void Shader::setUniform1b(const std::string& aName, const bool aValue) const
{
	setUniform1b(_getUniformLocation(aName), aValue);
}

void Shader::setUniform1f(const std::string& aName, const float aValue) const
{
	setUniform1f(_getUniformLocation(aName), aValue);
}

void Shader::setUniform1fv(const std::string& aName, float* aValue, const int32_t aCount) const
{
	setUniform1fv(_getUniformLocation(aName), aValue, aCount);
}

void Shader::setUniform1i(const std::string& aName, const int32_t aValue) const
{
	setUniform1i(_getUniformLocation(aName), aValue);
}

void Shader::setUniform1iv(const std::string& aName, int32_t* aValue, const int32_t aCount) const
{
	setUniform1iv(_getUniformLocation(aName), aValue, aCount);
}

void Shader::setUniform2f(const std::string& aName, const vec2<float>& aVector) const
{
	setUniform2f(_getUniformLocation(aName), aVector);
}

void Shader::setUniform2fv(const std::string& aName, const vec2<float>* aValues, const int32_t aCount) const
{
	setUniform2fv(_getUniformLocation(aName), aValues, aCount);
}

void Shader::setUniform3f(const std::string& aName, const vec3<float>& aVector) const
{
	setUniform3f(_getUniformLocation(aName), aVector);
}

void Shader::setUniform4f(const std::string& aName, const vec4<float>& aVector) const
{
	setUniform4f(_getUniformLocation(aName), aVector);
}

void Shader::setUniform4fv(const std::string& aName, const vec4<float>* aVector, const int32_t aCount) const
{
	setUniform4fv(_getUniformLocation(aName), aVector, aCount);
}

void Shader::setUniformMat3(const std::string& aName, const mat3<float>& aValue) const
{
	setUniformMat3(_getUniformLocation(aName), aValue);
}

void Shader::setUniformMat4(const std::string& aName, const mat4<float>& aMatrix) const
{
	setUniformMat4(_getUniformLocation(aName), aMatrix);
}

void Shader::setTexture(const std::string& aName, const uint32_t aTextureId) const
{
	if (aTextureId <= 0)
		return;

	const auto find = mImpl->samplers.find(aName);

	if (find == mImpl->samplers.end())
	{
		return;
	}

	if (mImpl->mSamplerObjects.find(aName) != mImpl->mSamplerObjects.end())
	{
		SamplerObject* sampler = mImpl->mSamplerObjects[aName];
		glBindSampler(find->second, sampler->samplerID);
	}

	
	glActiveTexture(GL_TEXTURE0 + find->second);
	glBindTexture(GL_TEXTURE_2D, aTextureId);
	setUniform1i(aName, find->second);
}

void Shader::setCubemapTexture(const std::string& aName, const uint32_t aTextureId) const
{
	if (aTextureId <= 0)
		return;

	const auto find = mImpl->samplers.find(aName);

	if (find == mImpl->samplers.end())
	{
		return;
	}

	if (mImpl->mSamplerObjects.find(aName) != mImpl->mSamplerObjects.end())
	{
		SamplerObject* sampler = mImpl->mSamplerObjects[aName];
		glBindSampler(find->second, sampler->samplerID);
	}

	glActiveTexture(GL_TEXTURE0 + find->second);
	glBindTexture(GL_TEXTURE_CUBE_MAP, aTextureId);
	setUniform1i(aName, find->second);
}

void Shader::setTextureBuffer(const std::string& aName, const uint32_t aTextureId) const
{
	const auto find = mImpl->samplers.find(aName);

	if (find == mImpl->samplers.end())
	{
		return;
	}

	setUniform1i(aName, find->second);
	glActiveTexture(GL_TEXTURE0 + find->second);
	glBindTexture(GL_TEXTURE_BUFFER, aTextureId);
}

uint32_t Shader::getTextureUnit(const std::string& aName) const
{
	const auto find = mImpl->samplers.find(aName);

	if (find == mImpl->samplers.end())
	{
		return 0;
	}

	return find->second;
}

bool Shader::hasTexture(const std::string& aName) const
{
	const auto find = mImpl->samplers.find(aName.c_str());

	if (find == mImpl->samplers.end())
	{
		return false;
	}

	return true;
}

void Shader::setUniform1b(const uint32_t aLocation, const bool aValue) const
{
	glUniform1i(aLocation, aValue);
}

void Shader::setUniform1f(const uint32_t aLocation, const float aValue) const
{
	glUniform1f(aLocation, aValue);
}

void Shader::setUniform1fv(const uint32_t aLocation, float* aValue, const int32_t aCount) const
{
	glUniform1fv(aLocation, aCount, aValue);
}

void Shader::setUniform1i(const uint32_t aLocation, const int32_t aValue) const
{
	glUniform1i(aLocation, aValue);
}

void Shader::setUniform1iv(const uint32_t aLocation, int32_t* aValue, const int32_t aCount) const
{
	glUniform1iv(aLocation, aCount, aValue);
}

void Shader::setUniform2fv(const uint32_t aLocation, const vec2<float>* aValues, const int32_t aCount) const
{
	glUniform2fv(aLocation, aCount, const_cast<float*>(&aValues[0].x));
}

void Shader::setUniform2f(const uint32_t aLocation, const vec2<float>& aVector) const
{
	glUniform2f(aLocation, aVector.x, aVector.y);
}

void Shader::setUniform3f(const uint32_t aLocation, const vec3<float>& aVector) const
{
	glUniform3f(aLocation, aVector.x, aVector.y, aVector.z);
}

void Shader::setUniform4f(const uint32_t aLocation, const vec4<float>& aVector) const
{
	glUniform4f(aLocation, aVector.x, aVector.y, aVector.z, aVector.w);
}

void Shader::setUniform4fv(const uint32_t aLocation, const vec4<float>* aVector, const int32_t aCount) const
{
	glUniform4fv(aLocation, aCount, const_cast<float*>(&aVector[0].x));
}

void Shader::setUniformMat3(const uint32_t aLocation, const mat3<float>& aValue) const
{
	glUniformMatrix3fv(aLocation, 1, false, &aValue.m[0]);
}

void Shader::setUniformMat4(const uint32_t aLocation, const mat4<float>& aMatrix) const
{
	glUniformMatrix4fv(aLocation, 1, false, &aMatrix.m[0]);
}

uint32_t Shader::getUBOBindPoint(const std::string& aName)
{
	const auto iter = mImpl->uboBindPoints.find(aName);
	if (iter != mImpl->uboBindPoints.end())
		return mImpl->uboBindPoints.at(aName);

	return GL_INVALID_INDEX;
}

uint32_t Shader::getUBOBindLocation(const std::string& uboName)
{
	const auto iter = mImpl->uboBindPointMap.find(uboName);
	if (iter != mImpl->uboBindPointMap.end())
		return iter->second;

	return GL_INVALID_INDEX;
}

int32_t Shader::getUniformBindLocation(const std::string& aUniformName)
{
	const auto iter = mImpl->uniformLocationMap.find(aUniformName);
	if (iter != mImpl->uniformLocationMap.end())
		return iter->second;

	return -1;
}

void Shader::bindMaterials()
{
	/*
		for (const auto & materialName : mImpl->mUBOsInUse)
		{
			for (auto material : mImpl->entityManager->getComponents<MaterialComponentNew>(materialName.c_str()))
			{
				material.material->bind(this);
			}
		}
	*/
}