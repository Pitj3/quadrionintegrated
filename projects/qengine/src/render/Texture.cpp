#include <stdint.h>
#include <math.h>
#include "render/Texture.h"
#include "render/QGfx.h"


static GLuint g_roughnessLookupTexture = 0;
static uint32_t g_nViewportAttribs = 0;

static GLint GL_INTERNAL_FORMATS[] =
{
	0,											// 0							
	GL_INTENSITY8,								// 1
	GL_LUMINANCE8_ALPHA8,						// 2
	GL_RGB8,									// 3
	GL_RGBA8,									
	GL_INTENSITY16,
	GL_LUMINANCE16_ALPHA16,
	GL_RGB16,
	GL_RGBA16,
	GL_INTENSITY16F_ARB,
	GL_LUMINANCE_ALPHA16F_ARB,
	GL_RGB16F_ARB,
	GL_RGBA16F_ARB,
	GL_R32F, //GL_INTENSITY32F_ARB,				
	GL_LUMINANCE_ALPHA32F_ARB,
	GL_RGB32F_ARB,
	GL_RGBA32F_ARB,
	GL_DEPTH_COMPONENT16,
	GL_DEPTH_COMPONENT24,
	GL_R3_G3_B2,
	GL_COMPRESSED_RGB_S3TC_DXT1_EXT,
	GL_COMPRESSED_RGBA_S3TC_DXT3_EXT,
	GL_COMPRESSED_RGBA_S3TC_DXT5_EXT,
	0,
	0,
	GL_RGB10_A2,
	GL_RGB10_A2UI,
	GL_SRGB8,
	GL_SRGB8_ALPHA8,
	GL_RG32F,
	GL_R8,
	GL_R16,
	GL_RGBA8,
	GL_R32F,
	GL_RG8,
	GL_RG16F,
};

static GLenum GL_FORMATS[] =
{
	0,
	GL_LUMINANCE,
	GL_LUMINANCE_ALPHA,
	GL_RGB,
	GL_RGBA,
	GL_LUMINANCE,
	GL_LUMINANCE_ALPHA,
	GL_RGB,
	GL_RGBA,
	GL_LUMINANCE,
	GL_LUMINANCE_ALPHA,
	GL_RGB,
	GL_RGBA,
	GL_RED, //	GL_LUMINANCE,
	GL_LUMINANCE_ALPHA,
	GL_RGB,
	GL_RGBA,
	GL_DEPTH_COMPONENT,
	GL_DEPTH_COMPONENT,
	GL_RGB,
	0,
	0,
	0,
	0,
	0,
	GL_RGBA,
	GL_RGBA_INTEGER,
	GL_RGB,
	GL_RGBA,
	GL_RG,
	GL_RED,
	GL_RED,
	GL_BGRA,
	GL_RED,
	GL_RG,
	GL_RG,
};

static GLenum GL_TYPES[] =
{
	0,
	GL_UNSIGNED_BYTE,
	GL_UNSIGNED_BYTE,
	GL_UNSIGNED_BYTE,
	GL_UNSIGNED_BYTE,
	GL_UNSIGNED_SHORT,
	GL_UNSIGNED_SHORT,
	GL_UNSIGNED_SHORT,
	GL_UNSIGNED_SHORT,
	GL_HALF_FLOAT_ARB,
	GL_HALF_FLOAT_ARB,
	GL_HALF_FLOAT_ARB,
	GL_HALF_FLOAT_ARB,
	GL_FLOAT,
	GL_FLOAT,
	GL_FLOAT,
	GL_FLOAT,
	GL_FLOAT,								// GL_UNSIGNED_SHORT
	GL_FLOAT,								// GL_UNSIGNED_INT
	GL_UNSIGNED_BYTE_3_3_2,
	0,
	0,
	0,
	0,
	0,
	GL_UNSIGNED_INT_2_10_10_10_REV,
	GL_UNSIGNED_INT_2_10_10_10_REV,
	GL_UNSIGNED_BYTE,
	GL_UNSIGNED_BYTE,
	GL_FLOAT,
	GL_UNSIGNED_BYTE,
	GL_UNSIGNED_SHORT,
	GL_UNSIGNED_BYTE,
	GL_FLOAT,
	GL_UNSIGNED_BYTE,
	GL_FLOAT,
};


static bool hasCubemapFlags(uint32_t aFlags)
{
	if (aFlags & QTEXTURE_CUBEMAPFACE_POSITIVE_X ||
		aFlags & QTEXTURE_CUBEMAPFACE_NEGATIVE_X ||
		aFlags & QTEXTURE_CUBEMAPFACE_POSITIVE_Y ||
		aFlags & QTEXTURE_CUBEMAPFACE_NEGATIVE_Y ||
		aFlags & QTEXTURE_CUBEMAPFACE_POSITIVE_Z ||
		aFlags & QTEXTURE_CUBEMAPFACE_NEGATIVE_Z)
	{
		return true;
	}

	return false;
}

static uint16_t getGLCubeFace(uint32_t aFlags)
{
	if(aFlags & QTEXTURE_CUBEMAPFACE_POSITIVE_X)
		return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
	else if(aFlags & QTEXTURE_CUBEMAPFACE_NEGATIVE_X)
		return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
	else if (aFlags & QTEXTURE_CUBEMAPFACE_POSITIVE_Y)
		return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
	else if (aFlags & QTEXTURE_CUBEMAPFACE_NEGATIVE_Y)
		return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
	else if (aFlags & QTEXTURE_CUBEMAPFACE_POSITIVE_Z)
		return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
	else if (aFlags & QTEXTURE_CUBEMAPFACE_NEGATIVE_Z)
		return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
	else 
		return 0;
}



QENGINE_API GLuint addTexture(const TextureInfo aTexInfo, void** aData)
{
	GLuint id;
	GLenum target;

	if(aTexInfo.width <= 0 || aTexInfo.height <= 0)
		target = GL_TEXTURE_1D;
	else if(aTexInfo.depth > 1)
		target = GL_TEXTURE_3D;
	else if(aTexInfo.isCubemap)
		target = GL_TEXTURE_CUBE_MAP;
	else
		target = GL_TEXTURE_2D;

	GLint* ct = static_cast<GLint*>(malloc(65536));
	glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, ct);

	const GLint internalFormat = GL_INTERNAL_FORMATS[aTexInfo.pixelFormat];
	const GLenum format = GL_FORMATS[aTexInfo.pixelFormat];
	const GLenum type = GL_TYPES[aTexInfo.pixelFormat];
	const GLint minFilter = (aTexInfo.flags & SAMPLE_NEAREST) 
		                  ? GL_NEAREST : (aTexInfo.flags & SAMPLE_LINEAR) 
		                  ? GL_LINEAR : (aTexInfo.flags & SAMPLE_BILINEAR) 
		                  ? GL_LINEAR_MIPMAP_NEAREST : (aTexInfo.flags & SAMPLE_TRILINEAR) 
		                  ? GL_LINEAR_MIPMAP_LINEAR : (aTexInfo.flags & SAMPLE_BILINEAR_ANISO) 
		                  ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR;
	const GLint magFilter = (aTexInfo.flags & SAMPLE_NEAREST) ? GL_NEAREST : GL_LINEAR;

	glGenTextures(1, &id);
	glBindTexture(target, id);

	const GLint clamp = (aTexInfo.flags & SAMPLE_WRAP) ? GL_REPEAT : GL_CLAMP_TO_EDGE;
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, magFilter);
	glTexParameteri(target, GL_TEXTURE_WRAP_S, clamp);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, clamp);
	if(target == GL_TEXTURE_3D)
		glTexParameteri(target, GL_TEXTURE_WRAP_R, clamp);

	// TODO: DON'T HARDCODE ANISOTROPY //
	if(aTexInfo.flags & SAMPLE_BILINEAR_ANISO 
	   || aTexInfo.flags & SAMPLE_TRILINEAR_ANISO)
		glTexParameteri(target, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);

	const uint32_t mipSize = aTexInfo.size;

	if(aTexInfo.isS3TC)
		glCompressedTexImage2D(target, 0, internalFormat, aTexInfo.width, 
							   aTexInfo.height, 0, mipSize, aData[0]);
	else if (aTexInfo.isCubemap)
	{
		const uint32_t xDiv = aTexInfo.width / 4;
		const uint32_t yDiv = aTexInfo.height / 3;
		for(uint32_t i = 0; i < 6; ++i)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 
						 internalFormat, xDiv, yDiv, 0, format, type, aData[i]);
	}	
	else
		glTexImage2D(target, 0, internalFormat, aTexInfo.width, aTexInfo.height, 
					 0, format, type, aData[0]);	

	if (hasMipmapFlag(aTexInfo.flags))
		glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(target, 0);
	return id;
}


QENGINE_API void updateTextureDirtyRegion(const GLuint aId, 
										  const int32_t aXOffset, 
										  const int32_t aYOffset, 
										  const int32_t aWidth, 
										  const int32_t aHeight, 
										  const uint8_t aFmt, 
										  const int32_t aGenMips, 
										  const void* aBuf)
{
	if(aId <= 0 || !aBuf)
		return;
	
	const GLenum format = GL_FORMATS[aFmt];
	const GLenum type = GL_TYPES[aFmt];
	glBindTexture(GL_TEXTURE_2D, aId);
	glTexSubImage2D(GL_TEXTURE_2D, 0, aXOffset, aYOffset, aWidth, aHeight, 
					format, type, aBuf);
	if(aGenMips)
		glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
}


QENGINE_API GLuint addRenderTarget(const uint32_t aWidth,
                                   const uint32_t aHeight,
                                   const uint8_t aFmt,
                                   const uint32_t aImgFlags)
{
	GLuint id;

	const GLint internalFormat = GL_INTERNAL_FORMATS[aFmt];
	const GLenum format = GL_FORMATS[aFmt];
	const GLenum type = GL_TYPES[aFmt];

	if(aWidth <= 0 || aHeight <= 0)
		return 0;

	bool bHasCubemapFlags = (aImgFlags & QTEXTURE_CUBEMAP);
	uint16_t textureType = GL_TEXTURE_2D;
	if (bHasCubemapFlags)
		textureType = GL_TEXTURE_CUBE_MAP;


	glGenTextures(1, &id);
	glBindTexture(textureType, id);

	const GLint minFilter = (aImgFlags & SAMPLE_NEAREST) 
		                  ? GL_NEAREST : (aImgFlags & SAMPLE_LINEAR) 
		                  ? GL_LINEAR : (aImgFlags & SAMPLE_BILINEAR) 
		                  ? GL_LINEAR_MIPMAP_NEAREST : (aImgFlags & SAMPLE_TRILINEAR) 
		                  ? GL_LINEAR_MIPMAP_LINEAR : (aImgFlags & SAMPLE_BILINEAR_ANISO) 
		                  ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR;

	const GLint magFilter = (aImgFlags & SAMPLE_NEAREST) ? GL_NEAREST : GL_LINEAR;

	const GLint clamp = (aImgFlags & SAMPLE_WRAP) ? GL_REPEAT : GL_CLAMP_TO_EDGE;

	

	glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, magFilter);
	glTexParameteri(textureType, GL_TEXTURE_WRAP_S, clamp);
	glTexParameteri(textureType, GL_TEXTURE_WRAP_T, clamp);
	if(bHasCubemapFlags)
		glTexParameteri(textureType, GL_TEXTURE_WRAP_R, clamp);


	// TODO: DO NOT USE 16 AS A HARD CODED ANISOTROPY LEVEL
	if(aImgFlags & SAMPLE_BILINEAR_ANISO || aImgFlags & SAMPLE_TRILINEAR_ANISO)
		glTexParameteri(textureType, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);

	if(!bHasCubemapFlags)
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, aWidth, aHeight, 0, format, type, 0);
	if(bHasCubemapFlags)
	{
		for (uint32_t i = 0; i < 6; ++i)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, aWidth, aHeight, 0, format, type, 0);
	}

	if(hasMipmapFlag(aImgFlags))
		glGenerateMipmap(textureType);

	glBindTexture(textureType, 0);
	return id;
}


QENGINE_API void deleteRenderTarget(GLuint aId)
{
	glDeleteTextures(1, &aId);
}

QENGINE_API void deleteTexture(GLuint aId)
{
	glDeleteTextures(1, &aId);
}

QENGINE_API struct DepthStencilId addDepthStencilTarget(const uint32_t aWidth,
                                                        const uint32_t aHeight,
                                                        const uint32_t aDepthBits,
                                                        const uint32_t aStencilBits, 
														uint32_t aImgFlags)
{
	struct DepthStencilId id;
	memset(&id, 0, sizeof(struct DepthStencilId));

	if(aWidth <= 0 || aHeight <= 0)
		return id;
	
	if(aDepthBits > 0 && aStencilBits <= 0)
	{
		glGenRenderbuffers(1, &id.depthId);
		glBindRenderbuffer(GL_RENDERBUFFER_EXT, id.depthId);
		glRenderbufferStorage(GL_RENDERBUFFER_EXT, (aDepthBits == 16) 
							  ? GL_DEPTH_COMPONENT16 : (aDepthBits == 24) 
							  ? GL_DEPTH_COMPONENT24 : GL_DEPTH_COMPONENT32, 
							  aWidth, aHeight);
		glBindRenderbuffer(GL_RENDERBUFFER_EXT, 0);
	}

	else //(stencilBits > 0 && (depthBits != 32))
	{
		if(aStencilBits <= 8 && aStencilBits > 0)
		{
			glGenRenderbuffers(1, &id.depthId);
			glBindRenderbuffer(GL_RENDERBUFFER_EXT, id.depthId);
			glRenderbufferStorage(GL_RENDERBUFFER_EXT, 
								  GL_DEPTH24_STENCIL8, 
								  aWidth, 
								  aHeight);
			glBindRenderbuffer(GL_RENDERBUFFER_EXT, 0);

			id.stencilId = id.depthId;
		}
	}

	return id;
}

QENGINE_API GLuint createRenderableDepthSurface(const GLuint aDepthTarget,
                                                const uint32_t aWidth,
                                                const uint32_t aHeight)
{
	if(aWidth <= 0 || aHeight <= 0)
		return -1;

	GLuint depthFbo;
	glGenFramebuffers(1, &depthFbo);
	if(depthFbo <= 0)
		return -1;



	glBindFramebuffer(GL_FRAMEBUFFER, depthFbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 
						   aDepthTarget, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return aDepthTarget;
}

QENGINE_API ShadowmapId createShadowmapSurface(const uint32_t aDepthBits,
                                               const uint32_t aWidth,
                                               const uint32_t aHeight, const uint32_t aFlags)
{
	ShadowmapId ret;
	ret.fboId = -1;
	ret.texId = -1;
	if(aWidth <= 0 || aHeight <= 0 || aDepthBits < 16 || aDepthBits > 32)
		return ret;

	GLushort depthMode;
	(aDepthBits == 32) ? depthMode = GL_DEPTH_COMPONENT32 : (aDepthBits == 24) 
					  ? depthMode = GL_DEPTH_COMPONENT24 : GL_DEPTH_COMPONENT16;

	GLuint buf = 0;
	glGenFramebuffers(1, &buf);
	glBindFramebuffer(GL_FRAMEBUFFER, buf);

	const GLushort minFilter = (aFlags & SAMPLE_NEAREST) ? GL_NEAREST : (aFlags & SAMPLE_LINEAR) 
		                     ? GL_LINEAR : (aFlags & SAMPLE_BILINEAR) 
		                     ? GL_LINEAR_MIPMAP_NEAREST : (aFlags & SAMPLE_TRILINEAR) 
		                     ? GL_LINEAR_MIPMAP_LINEAR : (aFlags & SAMPLE_BILINEAR_ANISO) 
		                     ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR;

	const GLushort magFilter = (aFlags & SAMPLE_NEAREST) ? GL_NEAREST : GL_LINEAR;

	GLuint depthTex;
	GLint clamp = (aFlags & SAMPLE_WRAP) ? GL_REPEAT : GL_CLAMP_TO_EDGE;
	glGenTextures(1, &depthTex);
	glBindTexture(GL_TEXTURE_2D, depthTex);
	glTexImage2D(GL_TEXTURE_2D, 0, depthMode, aWidth, aHeight,
				 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTex, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		ret.fboId = -1;
		ret.texId = -1;
		return ret;
	}
		

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	ret.fboId = buf;
	ret.texId = depthTex;
	ret.width = aWidth;
	ret.height = aHeight;
	return ret;
 }

QENGINE_API MultiShadowmapId createMultiShadowmapSurface(uint32_t aDepthBits,
														 uint32_t aWidth,
														 uint32_t aHeight,
														 uint32_t aFlags,
														 uint32_t aNumMaps)
{
	MultiShadowmapId ret;
	ret.fboId = 0;
	memset(&ret.textureIds, 0, sizeof(GLuint) * 4);
	if (aWidth <= 0 || aHeight <= 0 || aDepthBits < 16 || aDepthBits > 32)
		return ret;

	GLushort depthMode;
	(aDepthBits == 32) ? depthMode = GL_DEPTH_COMPONENT32 : (aDepthBits == 24)
					   ? depthMode = GL_DEPTH_COMPONENT24 : GL_DEPTH_COMPONENT16;

	GLuint buf = 0;
	glGenFramebuffers(1, &buf);
	glBindFramebuffer(GL_FRAMEBUFFER, buf);

	const GLushort minFilter = (aFlags & SAMPLE_NEAREST) ? GL_NEAREST : (aFlags & SAMPLE_LINEAR)
														 ? GL_LINEAR : (aFlags & SAMPLE_BILINEAR)
														 ? GL_LINEAR_MIPMAP_NEAREST : (aFlags & SAMPLE_TRILINEAR)
														 ? GL_LINEAR_MIPMAP_LINEAR : (aFlags & SAMPLE_BILINEAR_ANISO)
														 ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR;

	const GLushort magFilter = (aFlags & SAMPLE_NEAREST) ? GL_NEAREST : GL_LINEAR;

	GLuint depthTex[4];
	for (uint32_t i = 0; i < aNumMaps; ++i)
	{
		GLint clamp = (aFlags & SAMPLE_WRAP) ? GL_REPEAT : GL_CLAMP_TO_EDGE;
		glGenTextures(1, &depthTex[i]);
		glBindTexture(GL_TEXTURE_2D, depthTex[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, depthMode, aWidth, aHeight,
					 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTex[i], 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
	}

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		ret.fboId = 0;
		memset(&ret.textureIds, 0, sizeof(GLuint) * 4);
		return ret;
	}


	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	ret.fboId = buf;
	for(uint32_t i = 0; i < aNumMaps; ++i)
		ret.textureIds[i] = depthTex[i];
	ret.width = aWidth;
	ret.height = aHeight;
	ret.nShadowMaps = aNumMaps;
	return ret;
}

QENGINE_API void deleteDepthStencilTarget(DepthStencilId aObj)
{
	if(aObj.depthId > 0)
		glDeleteRenderbuffers(1, &aObj.depthId);
	if(aObj.stencilId > 0)
		glDeleteRenderbuffers(1, &aObj.stencilId);
}


QENGINE_API struct FboId createRenderableSurface(GLuint* aColorTargets,
                                                 const int32_t aNColorTargets,
                                                 const DepthStencilId aDs,
                                                 const uint32_t aWidth,
                                                 const uint32_t aHeight)
{
	struct FboId ret;
	GLuint fbo;
	int32_t i = 0;
	
	memset(&ret, 0, sizeof(struct FboId));

	if(!aColorTargets || aNColorTargets <= 0)
		return ret;

	// generate the FBO //
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// bind color targets first //
	for(i = 0; i < aNColorTargets; ++i)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, 
							   GL_COLOR_ATTACHMENT0_EXT + i, 
							   GL_TEXTURE_2D, 
							   aColorTargets[i], 
							   0);
	}

	// bind depth stencil target //
	if(aDs.depthId > 0)
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, 
								  GL_DEPTH_ATTACHMENT, 
								  GL_RENDERBUFFER_EXT, 
								  aDs.depthId);
	if(aDs.stencilId > 0)
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, 
								  GL_STENCIL_ATTACHMENT, 
								  GL_RENDERBUFFER_EXT, 
								  aDs.stencilId);

	const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE)
	{
		glDeleteFramebuffers(1, &fbo);
		return ret;
	}

	ret.id = fbo;
	ret.nColorTargets = aNColorTargets;
	ret.width = aWidth;
	ret.height = aHeight;


	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	return ret;
}

QENGINE_API struct FboId createRenderableCubemap(GLuint aCubemapTexture,
											     DepthStencilId aDs,
											     uint32_t aWidth,
											     uint32_t aHeight)
{
	struct FboId ret;
	GLuint fbo;
	int32_t i = 0;

	memset(&ret, 0, sizeof(struct FboId));
	ret.isCubemap = true;

	// generate the FBO //
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// bind color targets first //
//	glFramebufferTexture2D(GL_FRAMEBUFFER,
//						   GL_COLOR_ATTACHMENT0_EXT,
//						   GL_TEXTURE_CUBE_MAP_POSITIVE_X,
//						   aCubemapTexture,
//						   0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, aCubemapTexture, 0);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	
	// bind depth stencil target //
	if (aDs.depthId > 0)
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,
			GL_DEPTH_ATTACHMENT,
			GL_RENDERBUFFER_EXT,
			aDs.depthId);
	if (aDs.stencilId > 0)
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,
			GL_STENCIL_ATTACHMENT,
			GL_RENDERBUFFER_EXT,
			aDs.stencilId);

	const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		glDeleteFramebuffers(1, &fbo);
		return ret;
	}

	ret.id = fbo;
	ret.nColorTargets = 6;
	ret.width = aWidth;
	ret.height = aHeight;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	return ret;
}

QENGINE_API void deleteRenderableSurface(FboId aObj)
{
	glDeleteFramebuffers(1, &aObj.id);
}

QENGINE_API void bindRenderableSurface(const FboId aFbo)
{
	int i = 0;
	if (aFbo.id <= 0 || aFbo.nColorTargets > 8)
		return;
	GLenum drawBufs[8];

	glBindFramebuffer(GL_FRAMEBUFFER, aFbo.id);
	for(i = 0; i < aFbo.nColorTargets; ++i)
		drawBufs[i] = GL_COLOR_ATTACHMENT0_EXT + i;

	glDrawBuffersARB(aFbo.nColorTargets, drawBufs);

//	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0, 0, aFbo.width, aFbo.height);
//	++g_nViewportAttribs;
}

QENGINE_API void bindRenderableCubemap(FboId aFbo)
{
	int i = 0;
	if (aFbo.id <= 0 || aFbo.nColorTargets > 8)
		return;
	//GLenum drawBufs[8];

	glBindFramebuffer(GL_FRAMEBUFFER, aFbo.id);
//	for (i = 0; i < aFbo.nColorTargets; ++i)
//		drawBufs[i] = GL_COLOR_ATTACHMENT0_EXT + i;

//	glDrawBuffersARB(aFbo.nColorTargets, drawBufs);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

//	glPushAttrib(GL_VIEWPORT_BIT);
//	glViewport(0, 0, aFbo.width, aFbo.height);
//	++g_nViewportAttribs;
}

QENGINE_API void bindShadowmapSurface(const ShadowmapId aFbo)
{
	glBindFramebuffer(GL_FRAMEBUFFER, aFbo.fboId);
	glDrawBuffer(GL_NONE);

//	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0, 0, aFbo.width, aFbo.height);
//	++g_nViewportAttribs;
}

QENGINE_API void bindMultiShadowmapSurface(MultiShadowmapId aFbo, uint32_t aBindTexture)
{
	if (aBindTexture == QTEXTURE_FBO_ALL_TARGETS)
	{
		uint32_t i = 0;
		GLenum drawBufs[4];

		if (aFbo.fboId <= 0)
			return;

		glBindFramebuffer(GL_FRAMEBUFFER, aFbo.fboId);
		for (i = 0; i < aFbo.nShadowMaps; ++i)
			drawBufs[i] = GL_COLOR_ATTACHMENT0_EXT + i;

		glDrawBuffersARB(aFbo.nShadowMaps, drawBufs);

//		glPushAttrib(GL_VIEWPORT_BIT);
		glViewport(0, 0, aFbo.width, aFbo.height);
//		++g_nViewportAttribs;
	}

	else
	{
		if(aBindTexture > 3)
			return;

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, aFbo.fboId);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, aFbo.textureIds[aBindTexture], 0);
	}
}

QENGINE_API void bindRenderableDepthSurface(const GLuint aFbo)
{
	if(aFbo < 0)
		return;

	glBindFramebuffer(GL_FRAMEBUFFER, aFbo);
}

QENGINE_API void bindDefaultSurface()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	vec4<int32_t> vp = QGfx::getDefaultViewportDims();

//	for(uint32_t i = 0; i < g_nViewportAttribs; ++i)
//		glPopAttrib();
	glViewport(vp.x, vp.y, vp.z, vp.w);

	g_nViewportAttribs = 0;
}

QENGINE_API void bindTexture(const GLuint aId, const unsigned int aTexUnit)
{
	glEnable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0 + aTexUnit);
	glBindTexture(GL_TEXTURE_2D, aId);
}

QENGINE_API GLuint generateRoughnessLookup(const unsigned int aDim)
{
	if(g_roughnessLookupTexture > 0)
		return g_roughnessLookupTexture;

	struct TextureInfo inf;
	inf.colorSamples = 0;
	inf.coverageSamples = 0;
	inf.depth = 1;
	inf.flags = SAMPLE_CLAMP | SAMPLE_LINEAR;
	inf.height = aDim;
	inf.isCubemap = 0;
	inf.isS3TC = 0;
	inf.pixelFormat = QTEXTURE_FORMAT_I32F;
	inf.size = sizeof(float) * aDim * aDim;
	inf.width = aDim;

	float* buf = static_cast<float*>(malloc(sizeof(float) * aDim * aDim));

	for (unsigned int x = 0; x < aDim; ++x)
	{
		for (unsigned int y = 0; y < aDim; ++y)
		{
			float ndh = static_cast<float>(x) / static_cast<float>(aDim);
			const float roughness = static_cast<float>(y) / static_cast<float>(aDim);

			ndh *= 2.0f;
			ndh -= 1.0f;

			const float rSq = roughness * roughness;
			const float rA = 1.0f / (4.0f * rSq * static_cast<float>(pow(ndh, 4.0f)));
			const float rB = ndh * ndh - 1.0f;
			const float rC = rSq * ndh * ndh;
			buf[x + y * aDim] = static_cast<float>(static_cast<double>(rA) * exp(rB / rC)); 
		}
	}

	g_roughnessLookupTexture = addTexture(inf, (void**)(&buf));
	return g_roughnessLookupTexture;
}