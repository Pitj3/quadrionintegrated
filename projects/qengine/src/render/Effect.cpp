#include "render/Effect.h"

#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include "StringUtils.h"
#include "io/FileSystem.h"
#include "core/QLog.h"

//Effect* Effect::current;

/*
class EffectImpl
{
	public:
		explicit EffectImpl(Effect* aEffect)
		{
			effect = aEffect;
		}

		~EffectImpl()
		{
			for (uint32_t i = 0; i < techniques.size(); i++)
			{
				delete techniques[i];
			}

			techniques.clear();
		}

		std::vector<Technique*> techniques;
		std::string name;

		std::string source;

		void parseEffect(const std::vector<std::string>& aSourceLines);

		QHandle<QEntityManager> entityManager;

		Effect* effect;

		std::string sourceCopy;

		std::vector<qtl::pair<std::string, std::string>> includes;
};
*/

/*
Effect::Effect(QHandle<QEntityManager> aEntityMgr)
{
	mImpl = new EffectImpl(this);
	mImpl->entityManager = aEntityMgr;
}
*/

Effect::Effect()
{
	//	mImpl = new EffectImpl(this);

	/*
		std::ifstream shaderFile(aPath);
		std::string fileData;

		if (shaderFile.is_open())
		{
			std::string line;
			while (getline(shaderFile, line))
			{
				line += "\n";
				fileData += line.c_str();
			}
			shaderFile.close();
		}

		const std::vector<std::string> shaderLines = StringUtils::getLines(fileData);

		mImpl->parseEffect(shaderLines);

		for (size_t i = 0; i < mImpl->techniques.size(); i++)
		{
			Technique* tech = mImpl->techniques[i];
			size_t numPasses = tech->getNumPasses();
			Pass** passes = tech->getPasses();

			for (uint32_t j = 0; j < numPasses; j++)
			{
				passes[j]->compile();
			}
		}
	*/
}

/*
Effect::Effect(const char* aPath, QHandle<QEntityManager> aEntityManager)
{
	mImpl = new EffectImpl(this);
	mImpl->entityManager = aEntityManager;

	std::ifstream shaderFile(aPath);
	std::string fileData;

	if (shaderFile.is_open())
	{
		std::string line;
		while (getline(shaderFile, line))
		{
			line += "\n";
			fileData += line.c_str();
		}
		shaderFile.close();
	}

	const std::vector<std::string> shaderLines = StringUtils::getLines(fileData);

	mImpl->parseEffect(shaderLines);

	for (size_t i = 0; i < mImpl->techniques.size(); i++)
	{
		Technique* tech = mImpl->techniques[i];
		size_t numPasses = tech->getNumPasses();
		Pass** passes = tech->getPasses();

		for (uint32_t j = 0; j < numPasses; j++)
		{
			passes[j]->compile();
		}
	}
}
*/

Effect::~Effect()
{
	//	delete mImpl;
	for (uint32_t i = 0; i < techniques.size(); i++)
	{
		delete techniques[i];
	}

	techniques.clear();
}

size_t Effect::getNumTechniques() const
{
	return techniques.size();
}

std::vector<std::string> Effect::getGUIUniformNames() const
{
	return mGUIUniformNames;
}

void Effect::setOutputBuffers(const std::vector<OutputBufferInfo*>& aBuffers)
{
	_outputBuffers = aBuffers;
}

std::vector<OutputBufferInfo*> Effect::getOutputBuffers() const
{
	return _outputBuffers;
}

std::vector<Technique*>& Effect::getTechniques()
{
	return techniques;
}

Technique* Effect::getTechnique(const char* aName) const
{
	for (size_t i = 0; i < techniques.size(); i++)
	{
		if (techniques[i]->getName() == aName)
		{
			return techniques[i];
		}
	}

	return nullptr;
}

/*
void Effect::addMaterialToEntity(const std::string& aTechName, QHandle<QEntity> aEntity)
{
	for(auto material : mImpl->entityManager->getComponents<MaterialComponentNew>())
	{
		std::string materialName = material.name;

		if(getTechnique(aTechName.c_str())->hasUBO(materialName))
		{
			bool attached = false;
			for (auto entityMaterial : aEntity->getComponents<MaterialComponentNew>())
			{
				if (entityMaterial->name.compare(materialName) == 0)
				{
					attached = true;
					break;
				}
			}

			if(!attached)
				aEntity->addComponent<MaterialComponentNew>(material);
		}
	}
}
*/

void Effect::addTechnique(Technique* tech)
{
	techniques.push_back(tech);
}

void Effect::reset()
{
	for (uint32_t i = 0; i < techniques.size(); i++)
	{
		delete techniques[i];
	}

	techniques.clear();

	includes.clear();
	mUniformGUIData.clear();
	mUniformGUIElementType.clear();
	mUniformNames.clear();
	mGUIUniformNames.clear();
	_outputBuffers.clear();
}

const char* Effect::getName() const
{
	return name.c_str();
}

void Effect::setName(const std::string& aName)
{
	name = aName;
}

void Effect::setSourceCopy(const std::string& aSource)
{
	sourceCopy = aSource;
}

std::string Effect::getSourceCopy() const
{
	return sourceCopy;
}

std::vector<std::pair<std::string, std::string>> Effect::getIncludes()
{
	if (includes.empty())
	{
		std::vector<std::string> lines = StringUtils::getLines(sourceCopy.c_str());
		for (auto line : lines)
		{
			line.erase(std::remove(line.begin(), line.end(), '\t'), line.end());
			if (StringUtils::startsWith(line, "#include"))
			{
				std::vector<std::string> tokens = StringUtils::split(line, ' ');

				std::string includeData = FileSystem::readFile("Media/Effects/" + tokens[1]).c_str();
				includes.push_back({ tokens[1].c_str(), includeData });
			}
		}
	}

	return includes;
}

std::string Effect::getUniformGUIDebugElementType(const std::string& aUniformName)
{
	auto iter = mUniformGUIElementType.find(aUniformName);
	if(iter != mUniformGUIElementType.end())
	{
		return iter->second;
	}

	QUADRION_ERROR("Uniform with name: {0} not found!", aUniformName);
	return "";
}

void Effect::setUniformGUIDebugElementType(const std::string& aUniformName, const std::string& aGUIElementType)
{
	mUniformGUIElementType[aUniformName] = aGUIElementType;
}

std::pair<float, float> Effect::getUniformGUIDebugElementData(const std::string& aUniformName)
{
	auto iter = mUniformGUIData.find(aUniformName);
	if(iter != mUniformGUIData.end())
	{
		return iter->second;
	}

	QUADRION_ERROR("Uniform with name: {0} not found!", aUniformName);

	return std::make_pair<float, float>(0.0f, 0.0f);
}

void Effect::setUniformGUIDebugElementData(const std::string& aUniformName, std::pair<float, float> aData)
{
	mUniformGUIData[aUniformName] = aData;
}

float Effect::getUniformElementDefaultValue(const std::string& aUniformName)
{
	const auto iter = mGUIUniformDefaults.find(aUniformName);
	if (iter != mGUIUniformDefaults.end())
	{
		return iter->second;
	}

	QUADRION_ERROR("Uniform with name: {0} not found!", aUniformName);

//	return 0;
	return FLT_MIN;
}

void Effect::setUniformElementDefaultValue(const std::string& aUniformName, float aData)
{
	mGUIUniformDefaults[aUniformName] = aData;
}

void Effect::addUniformName(const std::string& aName)
{
	mUniformNames.push_back(aName);
}

std::vector<std::string> Effect::getUniformNames() const
{
	return mUniformNames;
}

void Effect::addGUIUniformName(const std::string& aName)
{
	mGUIUniformNames.push_back(aName);
}

/*
void EffectImpl::parseEffect(const std::vector<std::string>& aSourceLines)
{

	std::vector<std::string> shaderLines = aSourceLines;

	bool hasGeometryShader = false;
	bool hasTesselationShader = false;

	std::string versionString;

	// Check first "real" line being QuadrionShader
	int numLinesToRemove = 0;
	for (uint32_t i = 0; i < shaderLines.size(); i++)
	{
		std::string l = shaderLines[i];
		if (StringUtils::startsWith(l, "//"))
		{
			// This is comment, skip
			numLinesToRemove++;
			continue;
		}
		if (l == "/n")
		{
			// Empty line, skip
			numLinesToRemove++;
			continue;
		}
		if (!StringUtils::startsWith(l, "QuadrionShader"))
		{
			// Error, QuadrionShader not first "real" line of shader
			std::cout << "ERROR" << std::endl;
			break;
		}
		else
		{
			// Get the name of shader:
			const uint32_t start = static_cast<uint32_t>(l.find('\"'));
			const uint32_t end = static_cast<uint32_t>(l.find_last_of('\"'));
			const std::string shaderName = l.substr(start + 1, end - start - 1); // Get the substring and strip the quotes
			name = shaderName.c_str();
			numLinesToRemove += 2;
			break;
		}
	}

	shaderLines.erase(shaderLines.begin(), shaderLines.begin() + numLinesToRemove);
	shaderLines.erase(shaderLines.end() - 1);

	for (uint32_t i = 0; i < shaderLines.size(); i++)
	{
		source += shaderLines[i] + "\n";

		if (strstr(shaderLines[i].c_str(), "GeometryShader"))
		{
			hasGeometryShader = true;
		}

		if (strstr(shaderLines[i].c_str(), "TEShader"))
		{
			hasTesselationShader = true;
		}

		if (strstr(shaderLines[i].c_str(), "TCShader"))
		{
			hasTesselationShader = true;
		}

		if (strstr(shaderLines[i].c_str(), "ShaderVersion"))
		{
			std::vector<std::string> versionParts = StringUtils::splitWithRegex(shaderLines[i], "\\s+");
			versionString = "#version " + versionParts[1];
			if (strstr(shaderLines[i].c_str(), "core"))
			{
				versionString += " core";
			}
		}
	}

	// Check for techniques
	uint32_t numTechniques = 0;
	std::vector<uint32_t> techniqueStarts;
	for (uint32_t i = 0; i < shaderLines.size(); i++)
	{
		std::string l = shaderLines[i];
		if (strstr(l.c_str(), "Technique")) // If line includes technique, add it
		{
			numTechniques++;
			techniqueStarts.push_back(i);
		}
	}

	// Parse the techniques
	std::vector<uint32_t> techniqueEnds;
	std::unordered_map<Pass*, uint32_t> passStarts;
	for (uint32_t i = 0; i < numTechniques; i++)
	{
		const uint32_t sizeLeft = static_cast<uint32_t>(shaderLines.size() - techniqueStarts[i]);
		techniqueEnds.push_back(StringUtils::findEndOfBlock(shaderLines, techniqueStarts[i], techniqueStarts[i] + sizeLeft, nullptr));

		Technique* tech = new Technique("", effect, entityManager);
		std::string techSource;
		for (uint32_t j = techniqueStarts[i]; j < techniqueEnds[i]; j++)
		{
			std::string l = shaderLines[j];
			techSource += l + "\n";
			if (strstr(l.c_str(), "Technique")) // If line includes technique, get the name
			{
				const uint32_t spaceLocation = static_cast<uint32_t>(l.find_last_of(' '));
				tech->_setName(l.substr(spaceLocation + 1));
			}

			if (strstr(l.c_str(), "Pass")) // If line includes pass, get the name
			{
				Pass* pass = new Pass("", tech, entityManager);

				const uint32_t spaceLocation = static_cast<uint32_t>(l.find_last_of(' '));
				pass->_setName(l.substr(spaceLocation + 1));

				tech->_addPass(pass);

				passStarts.insert(std::make_pair(pass, j));
			}
		}
		tech->_setSource(techSource);

		techniques.push_back(tech);
	}

	// Get pass ends & source
	std::unordered_map<Pass*, uint32_t> passEnds;
	for (size_t i = 0; i < numTechniques; i++)
	{
		Technique* tech = techniques[i];
		size_t numPasses = tech->getNumPasses();
		Pass** passes = tech->getPasses();

		for (uint32_t j = 0; j < numPasses; j++)
		{
			Pass* pass = passes[j];
			const uint32_t sizeLeft = static_cast<uint32_t>(shaderLines.size() - passStarts[pass]);
			std::string passSource;
			passEnds.insert(std::make_pair(pass, StringUtils::findEndOfBlock(shaderLines, passStarts[pass], passStarts[pass] + sizeLeft, &passSource)));

			pass->_setSource(passSource);
		}
	}

	// Shader Data pass
	std::string vertexShaderData;
	std::string tcsShaderData;
	std::string tesShaderData;
	std::string geometryShaderData;
	std::string fragmentShaderData;

	std::vector<int> lineNumbersToSkip;

	for (uint32_t h = 0; h < shaderLines.size(); h++)
	{
		std::string line = shaderLines[h];

		if (strstr(line.c_str(), "struct"))
		{
			std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");
			std::string structname = parts[1];

			std::string blockData;
			StringUtils::findEndOfBlock(shaderLines, h, static_cast<uint32_t>(shaderLines.size() - h), &blockData);

			const uint32_t colonPosition = static_cast<uint32_t>(blockData.find_first_of(':'));
			const uint32_t openBracketPosition = static_cast<uint32_t>(blockData.find_first_of('{'));

			std::string firstPart = blockData.substr(0, colonPosition);
			std::string lastPart = blockData.substr(openBracketPosition);

			blockData = firstPart + "\n" + lastPart;

			const uint32_t endBracketLocation = static_cast<uint32_t>(blockData.find_last_of('}'));
			const uint32_t endSemicolonLocation = static_cast<uint32_t>(blockData.find_last_of(';'));

			std::string structVariableName = blockData.substr(endBracketLocation + 1, endSemicolonLocation - endBracketLocation - 1);

			structVariableName.erase(std::remove_if(structVariableName.begin(), structVariableName.end(), isspace), structVariableName.end());

			std::vector<std::string> blockDataLines = StringUtils::getLines(blockData);
			blockDataLines[blockDataLines.size() - 1] = "};\n";
			blockDataLines.push_back("uniform " + structname + " " + structVariableName + ";\n");

			blockData = "";
			for (uint32_t v = 0; v < blockDataLines.size(); v++)
			{
				blockData += blockDataLines[v];
			}

			if (strstr(line.c_str(), "vertex"))
			{
				vertexShaderData += blockData + "\n";
			}

			if (strstr(line.c_str(), "geometry"))
			{
				geometryShaderData += blockData + "\n";
			}

			if (strstr(line.c_str(), "tcs"))
			{
				tcsShaderData += blockData + "\n";
			}

			if (strstr(line.c_str(), "tes"))
			{
				tesShaderData += blockData + "\n";
			}

			if (strstr(line.c_str(), "fragment"))
			{
				fragmentShaderData += blockData + "\n";
			}
		}
	}

	std::vector<std::pair<std::string, std::string>> shaderParams;
	std::string vertexShaderUniforms;
	std::string tcsShaderUniforms;
	std::string tesShaderUniforms;
	std::string geometryShaderUniforms;
	std::string fragmentShaderUniforms;
	std::string globalShaderUniforms;
	std::string fragmentOutVariables;
	for (uint32_t i = 0; i < shaderLines.size(); i++)
	{
		std::string line = shaderLines[i];

		// Check for geometry shaders
		if (strstr(line.c_str(), "GeometryShader"))
		{
			hasGeometryShader = true;
		}

		if (strstr(line.c_str(), "TCShader") || strstr(line.c_str(), "TEShader"))
		{
			hasTesselationShader = true;
		}

		if (strstr(line.c_str(), "interface"))
		{
			std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");
			std::string interfaceName = parts[1];

			std::string blockData;
			StringUtils::findEndOfBlock(shaderLines, i, static_cast<uint32_t>(shaderLines.size() - i), &blockData);

			const uint32_t start = static_cast<uint32_t>(blockData.find_first_of('{'));
			const uint32_t end = static_cast<uint32_t>(blockData.find_last_of('}'));

			std::string data = blockData.substr(start + 1, end - start - 1);
			data.erase(std::remove(data.begin(), data.end(), '\n'), data.end());

			shaderParams.push_back(std::make_pair(interfaceName, data));
		}

		if (strstr(line.c_str(), "uniform"))
		{
			std::string testForUniStart = line;
			testForUniStart.erase(std::remove_if(testForUniStart.begin(), testForUniStart.end(), isspace), testForUniStart.end());

			std::string uni = "uniform";
			if(testForUniStart.compare(0, uni.length(), uni) != 0)
			{
				continue;
			}

			std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");

			if (parts.size() == 2)
			{
				// UBO Expected
				std::string blockData;
				int end = StringUtils::findEndOfBlock(shaderLines, i, static_cast<uint32_t>(shaderLines.size() - i), &blockData);

				vertexShaderData += blockData + "\n";

				if (hasGeometryShader)
					geometryShaderData += blockData + "\n";

				if (hasTesselationShader)
				{
					tcsShaderData += blockData + "\n";
					tesShaderData += blockData + "\n";
				}

				fragmentShaderData += blockData + "\n";

				for (int l = i; l < end; l++)
				{
					lineNumbersToSkip.push_back(l);
				}
			}

			bool skip = false;
			for(int l = 0; l < lineNumbersToSkip.size(); l++)
			{
				if(lineNumbersToSkip[l] == i)
				{
					skip = true;
				}
			}

			if(skip)
			{
				continue;
			}

			if (!strstr(line.c_str(), "vertex") && !strstr(line.c_str(), "fragment") && !strstr(line.c_str(), "geometry") && !strstr(line.c_str(), "tcs") && !strstr(line.c_str(), "tes"))
			{
				// Global uniform
				std::string type = parts[1];
				std::string name = parts[2];

				globalShaderUniforms += "uniform " + type + " " + name + "\n";
			}
			else
			{
				std::string uniformShaderType = parts[0];
				std::string type = parts[2];
				std::string name = parts[3];

				if (strstr(uniformShaderType.c_str(), "vertex"))
				{
					vertexShaderUniforms += "uniform " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "tcs"))
				{
					tcsShaderUniforms += "uniform " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "tes"))
				{
					tesShaderUniforms += "uniform " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "geometry"))
				{
					geometryShaderUniforms += "uniform " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "fragment"))
				{
					fragmentShaderUniforms += "uniform " + type + " " + name + "\n";
				}
			}
		}
		if (strstr(line.c_str(), "const"))
		{
			std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");
			if (!strstr(line.c_str(), "vertex") && !strstr(line.c_str(), "fragment") && !strstr(line.c_str(), "geometry") && !strstr(line.c_str(), "tes") && !strstr(line.c_str(), "tcs"))
			{
				// Global uniform
				std::string type = parts[1];
				std::string name = parts[2];

				std::string initialization = "";
				if (parts.size() > 3)
				{
					for (uint32_t ii = 3; ii < parts.size(); ii++)
					{
						initialization += parts[ii] + " ";
					}
				}

				name += " " + initialization;


				globalShaderUniforms += "const " + type + " " + name + "\n";
			}
			else
			{
				std::string uniformShaderType = parts[0];
				std::string type = parts[2];
				std::string name = parts[3];

				std::string initialization = "";
				if (parts.size() > 4)
				{
					for (uint32_t ii = 4; ii < parts.size(); ii++)
					{
						initialization += parts[ii] + " ";
					}
				}

				name += " " + initialization;

				if (strstr(uniformShaderType.c_str(), "vertex"))
				{
					vertexShaderUniforms += "const " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "tcs"))
				{
					tcsShaderUniforms += "const " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "tes"))
				{
					tesShaderUniforms += "const " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "geometry"))
				{
					geometryShaderUniforms += "const " + type + " " + name + "\n";
				}
				if (strstr(uniformShaderType.c_str(), "fragment"))
				{
					fragmentShaderUniforms += "const " + type + " " + name + "\n";
				}
			}
		}
	}

	std::string geometryLayouts;
	if (hasGeometryShader)
	{
		// Geometry Layout parse
		for (uint32_t i = 0; i < shaderLines.size(); i++)
		{
			std::string line = shaderLines[i];

			if (strstr(line.c_str(), "geometrylayout"))
			{
				if (strstr(line.c_str(), " in;"))
				{
					uint32_t bracketPosition = static_cast<uint32_t>(line.find_first_of('('));
					std::string layout = line.substr(bracketPosition + 1);
					layout = "layout(" + layout;
					geometryLayouts += layout;
				}

				if (strstr(line.c_str(), " out;"))
				{
					uint32_t bracketPosition = static_cast<uint32_t>(line.find_first_of('('));
					std::string layout = line.substr(bracketPosition + 1);
					layout = "layout(" + layout;
					geometryLayouts += layout;
				}
			}
		}
	}

	std::string tcsLayouts;
	std::string tesLayouts;
	if (hasTesselationShader)
	{
		// Tesselation Layout parse
		for (uint32_t i = 0; i < shaderLines.size(); i++)
		{
			std::string line = shaderLines[i];

			if (strstr(line.c_str(), "tcslayout"))
			{
				if (strstr(line.c_str(), " in;"))
				{
					uint32_t bracketPosition = static_cast<uint32_t>(line.find_first_of('('));
					std::string layout = line.substr(bracketPosition + 1);
					layout = "layout(" + layout;
					tcsLayouts += layout;
				}

				if (strstr(line.c_str(), " out;"))
				{
					uint32_t bracketPosition = static_cast<uint32_t>(line.find_first_of('('));
					std::string layout = line.substr(bracketPosition + 1);
					layout = "layout(" + layout;
					tcsLayouts += layout;
				}
			}

			if (strstr(line.c_str(), "teslayout"))
			{
				if (strstr(line.c_str(), " in;"))
				{
					uint32_t bracketPosition = static_cast<uint32_t>(line.find_first_of('('));
					std::string layout = line.substr(bracketPosition + 1);
					layout = "layout(" + layout;
					tesLayouts += layout;
				}

				if (strstr(line.c_str(), " out;"))
				{
					uint32_t bracketPosition = static_cast<uint32_t>(line.find_first_of('('));
					std::string layout = line.substr(bracketPosition + 1);
					layout = "layout(" + layout;
					tesLayouts += layout;
				}
			}
		}
	}

	// Include parse
	std::vector<std::pair<std::string, std::string>> includeParams;
	std::string includeUniforms;

	for (uint32_t i = 0; i < shaderLines.size(); i++)
	{
		std::string line = shaderLines[i];
		if (strstr(line.c_str(), "#include"))
		{
			const uint32_t start = static_cast<uint32_t>(line.find_first_of('"'));
			const uint32_t end = static_cast<uint32_t>(line.find_last_of('"'));

			std::string fileName = line.substr(start + 1, end - start - 1);

			std::ifstream includeFile("data/" + std::string(fileName.c_str()));
			std::string includeData;
			if (includeFile.is_open())
			{
				std::string line;
				while (getline(includeFile, line))
				{
					includeData += line + '\n';
				}
				includeFile.close();
			}
			else
			{
				std::cout << "ERROR NO INCLUDE" << std::endl;
			}

			std::vector<std::string> includeLines = StringUtils::getLines(includeData);

			for (uint32_t i = 0; i < includeLines.size(); i++)
			{
				std::string line = includeLines[i];
				if (strstr(line.c_str(), "interface"))
				{
					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");
					std::string interfaceName = parts[1];

					std::string blockData;
					StringUtils::findEndOfBlock(includeLines, i, static_cast<uint32_t>(includeLines.size() - i), &blockData);

					const uint32_t start = static_cast<uint32_t>(blockData.find_first_of('{'));
					const uint32_t end = static_cast<uint32_t>(blockData.find_last_of('}'));

					std::string data = blockData.substr(start + 1, end - start - 1);
					data.erase(std::remove(data.begin(), data.end(), '\n'), data.end());

					shaderParams.push_back(std::make_pair(interfaceName, data));
				}

				if (strstr(line.c_str(), "uniform"))
				{
					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");
					if (!strstr(line.c_str(), "vertex") && !strstr(line.c_str(), "fragment") && !strstr(line.c_str(), "geometry") && !strstr(line.c_str(), "tcs") && !strstr(line.c_str(), "tes"))
					{
						// Global uniform
						std::string type = parts[1];
						std::string name = parts[2];

						globalShaderUniforms += "uniform " + type + " " + name + "\n";
					}
					else
					{
						std::string uniformShaderType = parts[0];
						std::string type = parts[2];
						std::string name = parts[3];

						if (strstr(uniformShaderType.c_str(), "vertex"))
						{
							vertexShaderUniforms += "uniform " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "tcs"))
						{
							tcsShaderUniforms += "uniform " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "tes"))
						{
							tesShaderUniforms += "uniform " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "geometry"))
						{
							geometryShaderUniforms += "uniform " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "fragment"))
						{
							fragmentShaderUniforms += "uniform " + type + " " + name + "\n";
						}
					}
				}

				if (strstr(line.c_str(), "const"))
				{
					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");
					if (!strstr(line.c_str(), "vertex") && !strstr(line.c_str(), "fragment") && !strstr(line.c_str(), "geometry") && !strstr(line.c_str(), "tes") && !strstr(line.c_str(), "tcs"))
					{
						// Global uniform
						std::string type = parts[1];
						std::string name = parts[2];

						globalShaderUniforms += "const " + type + " " + name + "\n";
					}
					else
					{
						std::string uniformShaderType = parts[0];
						std::string type = parts[2];
						std::string name = parts[3];

						if (strstr(uniformShaderType.c_str(), "vertex"))
						{
							vertexShaderUniforms += "const " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "tcs"))
						{
							tcsShaderUniforms += "const " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "tes"))
						{
							tesShaderUniforms += "const " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "geometry"))
						{
							geometryShaderUniforms += "const " + type + " " + name + "\n";
						}
						if (strstr(uniformShaderType.c_str(), "fragment"))
						{
							fragmentShaderUniforms += "const " + type + " " + name + "\n";
						}
					}
				}
			}
		}
	}

	std::unordered_map<Pass*, std::pair<std::string, std::string>> vertexInterfaces;
	std::unordered_map<Pass*, std::pair<std::string, std::string>> geometryInterfaces;
	std::unordered_map<Pass*, std::pair<std::string, std::string>> tcsInterfaces;
	std::unordered_map<Pass*, std::pair<std::string, std::string>> tesInterfaces;
	std::unordered_map<Pass*, std::pair<std::string, std::string>> fragmentInterfaces;
	std::unordered_map<Pass*, std::pair<std::string, std::string>> fragmentOutInterfaces;

	for (uint32_t i = 0; i < techniques.size(); i++)
	{
		Technique* tech = techniques[i];
		size_t numPasses = tech->getNumPasses();
		Pass** passes = tech->getPasses();

		for (uint32_t j = 0; j < numPasses; j++)
		{
			Pass* pass = passes[j];
			std::vector<std::string> passSource = StringUtils::getLines(pass->getPassSource());
			for (uint32_t h = 2; h < passSource.size() - 1; h++)
			{
				std::string line = passSource[h];
				if (strstr(line.c_str(), "VertexShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string vertexShaderName = parts[1];
					vertexShaderName.erase(std::remove(vertexShaderName.begin(), vertexShaderName.end(), ';'), vertexShaderName.end());

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + vertexShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							const uint32_t startColonLocation = static_cast<uint32_t>(effectLine.find_first_of('('));
							const uint32_t endColonLocation = static_cast<uint32_t>(effectLine.find_last_of(')'));

							std::string interfaceName = effectLine.substr(startColonLocation + 1, endColonLocation - startColonLocation - 1);

							vertexInterfaces.insert(std::make_pair(pass, std::make_pair("vertex", interfaceName)));
						}
					}
				}

				if (strstr(line.c_str(), "TCShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string tcsShaderName = parts[1];
					tcsShaderName.erase(std::remove(tcsShaderName.begin(), tcsShaderName.end(), ';'), tcsShaderName.end());

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + tcsShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							const uint32_t startColonLocation = static_cast<uint32_t>(effectLine.find_first_of('('));
							const uint32_t endColonLocation = static_cast<uint32_t>(effectLine.find_last_of(')'));

							std::string interfaceName = effectLine.substr(startColonLocation + 1, endColonLocation - startColonLocation - 1);

							tcsInterfaces.insert(std::make_pair(pass, std::make_pair("tcs", interfaceName)));
						}
					}
				}

				if (strstr(line.c_str(), "TEShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string tesShaderName = parts[1];
					tesShaderName.erase(std::remove(tesShaderName.begin(), tesShaderName.end(), ';'), tesShaderName.end());

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + tesShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							const uint32_t startColonLocation = static_cast<uint32_t>(effectLine.find_first_of('('));
							const uint32_t endColonLocation = static_cast<uint32_t>(effectLine.find_last_of(')'));

							std::string interfaceName = effectLine.substr(startColonLocation + 1, endColonLocation - startColonLocation - 1);

							tesInterfaces.insert(std::make_pair(pass, std::make_pair("tes", interfaceName)));
						}
					}
				}

				if (strstr(line.c_str(), "GeometryShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string geometryShaderName = parts[1];
					geometryShaderName.erase(std::remove(geometryShaderName.begin(), geometryShaderName.end(), ';'), geometryShaderName.end());

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + geometryShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							const uint32_t startColonLocation = static_cast<uint32_t>(effectLine.find_first_of('('));
							const uint32_t endColonLocation = static_cast<uint32_t>(effectLine.find_last_of(')'));

							std::string interfaceName = effectLine.substr(startColonLocation + 1, endColonLocation - startColonLocation - 1);

							geometryInterfaces.insert(std::make_pair(pass, std::make_pair("geometry", interfaceName)));
						}
					}
				}

				if (strstr(line.c_str(), "FragmentShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string fragmentDataArray = parts[1];

					const uint32_t bracketOpenLocation = static_cast<uint32_t>(fragmentDataArray.find_first_of('{'));
					const uint32_t bracketCloseLocation = static_cast<uint32_t>(fragmentDataArray.find_first_of('}'));

					std::string fragmentData = fragmentDataArray.substr(bracketOpenLocation + 1, bracketCloseLocation - bracketOpenLocation - 1);

					std::vector<std::string> data = StringUtils::splitWithRegex(fragmentData, ",");

					std::string fragmentShaderName = data[0];
					fragmentShaderName.erase(std::remove(fragmentShaderName.begin(), fragmentShaderName.end(), ';'), fragmentShaderName.end());

					fragmentOutInterfaces.insert(std::make_pair(pass, std::make_pair("fragment", data[1])));

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + fragmentShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							const uint32_t startColonLocation = static_cast<uint32_t>(effectLine.find_first_of('('));
							const uint32_t endColonLocation = static_cast<uint32_t>(effectLine.find_last_of(')'));

							std::string interfaceName = effectLine.substr(startColonLocation + 1, endColonLocation - startColonLocation - 1);

							fragmentInterfaces.insert(std::make_pair(pass, std::make_pair("fragment", interfaceName)));
						}
					}
				}
			}
		}
	}

	std::string fragmentShaderParams;
	std::string tcsShaderParams;
	std::string tesShaderParams;
	std::string geometryShaderParams;
	std::string vertexShaderParams;

	// Parse functions
	std::string fragmentFunctions;
	std::string vertexFunctions;
	std::string geometryFunctions;
	std::string tcsFunctions;
	std::string tesFunctions;
	std::string globalFunctions;
	for (uint32_t i = 0; i < shaderLines.size(); i++)
	{
		std::string line = shaderLines[i];
		line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

		if (!strstr(line.c_str(), ":fragment") && !strstr(line.c_str(), ":vertex") && !strstr(line.c_str(), ":geometry") && !strstr(line.c_str(), ":tes") && !strstr(line.c_str(), ":tcs") && !strstr(line.c_str(), ":global"))
		{
			continue;
		}

		line = shaderLines[i];

		std::vector<std::string> parts = StringUtils::splitWithRegex(line, "\\s+");

		std::string functionBlock;
		StringUtils::findEndOfBlock(shaderLines, i, static_cast<uint32_t>(shaderLines.size() - i), &functionBlock);

		std::string type = parts[0];
		std::string functionWithParams = parts[1];

		const uint32_t colonLocation = static_cast<uint32_t>(line.find_last_of(':'));
		line = line.substr(0, colonLocation);

		const uint32_t bracketLocation = static_cast<uint32_t>(functionBlock.find_first_of('{'));
		functionBlock = functionBlock.substr(bracketLocation, functionBlock.size() - bracketLocation);

		functionBlock = line + functionBlock;

		line = shaderLines[i];
		line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
		if (strstr(line.c_str(), ":fragment"))
		{
			fragmentFunctions += functionBlock + "\n";
		}
		if (strstr(line.c_str(), ":vertex"))
		{
			vertexFunctions += functionBlock + "\n";
		}
		if (strstr(line.c_str(), ":geometry"))
		{
			geometryFunctions += functionBlock + "\n";
		}
		if (strstr(line.c_str(), ":tcs"))
		{
			tcsFunctions += functionBlock + "\n";
		}
		if (strstr(line.c_str(), ":tes"))
		{
			tesFunctions += functionBlock + "\n";
		}
		if (strstr(line.c_str(), ":global"))
		{
			globalFunctions += functionBlock + "\n";
		}
	}

	// Parse Passes
	for (uint32_t i = 0; i < techniques.size(); i++)
	{
		Technique* tech = techniques[i];
		size_t numPasses = tech->getNumPasses();
		Pass** passes = tech->getPasses();

		for (uint32_t j = 0; j < numPasses; j++)
		{
			Pass* pass = passes[j];
			std::vector<std::string> passSource = StringUtils::getLines(pass->getPassSource());
			for (uint32_t h = 2; h < passSource.size() - 1; h++)
			{
				std::string line = passSource[h];
				if (strstr(line.c_str(), "VertexShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string vertexShaderName = parts[1];
					vertexShaderName.erase(std::remove(vertexShaderName.begin(), vertexShaderName.end(), ';'), vertexShaderName.end());

					std::pair<std::string, std::string> interfacePair = vertexInterfaces[pass];

					for (uint32_t c = 0; c < shaderParams.size(); c++)
					{
						std::pair<std::string, std::string> pair = shaderParams[c];
						if (pair.first == interfacePair.second)
						{
							std::vector<std::string> parts = StringUtils::splitWithRegex(pair.second, ";");
							for (uint32_t x = 0; x < parts.size() - 1; x++)
							{
								std::string line = parts[x];

								if (strstr(line.c_str(), ":"))
								{
									// This has semantics
									line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
									std::vector<std::string> locations = StringUtils::splitWithRegex(line, ":");

									std::string semantic = locations[1];

									const uint32_t colonLocation = static_cast<uint32_t>(parts[x].find_first_of(':'));
									parts[x] = parts[x].substr(0, colonLocation - 1);

									std::string layoutLocation = StringUtils::getSemanticLocationString(semantic);

									std::vector<std::string> params = StringUtils::splitWithRegex(parts[x], "\\s+");
									if (params.size() == 2)
									{
										vertexShaderParams += "layout(location=" + layoutLocation + ") in " + params[0] + " " + params[1] + ";\n";
									}
									else if (params.size() == 3)
									{
										// has modifier
										vertexShaderParams += "layout(location=" + layoutLocation + ") in " + params[0] + " " + params[1] + " " + params[2] + ";\n";
									}
								}
								else
								{
									// This doesnt have semantics
									vertexShaderParams += line + ";";
								}
							}
						}
					}

					if (hasTesselationShader)
					{
						interfacePair = tcsInterfaces[pass];
					}
					else
					{
						if (hasGeometryShader)
							interfacePair = geometryInterfaces[pass];
						else
							interfacePair = fragmentInterfaces[pass];
					}

					for (uint32_t c = 0; c < shaderParams.size(); c++)
					{
						std::pair<std::string, std::string> pair = shaderParams[c];
						if (pair.first == interfacePair.second)
						{
							std::vector<std::string> parts = StringUtils::splitWithRegex(pair.second, ";");
							for (uint32_t x = 0; x < parts.size() - 1; x++)
							{
								std::vector<std::string> params = StringUtils::splitWithRegex(parts[x], "\\s+");
								if (params.size() == 2)
								{
									std::string secondParam = params[1];
									if (strstr(params[1].c_str(), "["))
									{
										std::vector<std::string> removedParts = StringUtils::splitWithRegex(secondParam, "[");
										secondParam = removedParts[0];
									}
									vertexShaderParams += "out " + params[0] + " " + secondParam + ";\n";
									if (hasTesselationShader)
									{
										tcsShaderParams += "in " + params[0] + " " + params[1] + "[];\n";
									}
									else
									{
										if (hasGeometryShader)
											geometryShaderParams += "in " + params[0] + " " + params[1] + ";\n";
										else
											fragmentShaderParams += "in " + params[0] + " " + params[1] + ";\n";
									}
								}
								else if (params.size() == 3)
								{
									std::string secondParam = params[2];
									if (strstr(params[2].c_str(), "["))
									{
										std::vector<std::string> removedParts = StringUtils::splitWithRegex(secondParam, "[");
										secondParam = removedParts[0];
									}

									// has modifier
									vertexShaderParams += params[0] + " out " + params[1] + " " + secondParam + ";\n";
									if (hasTesselationShader)
									{
										tcsShaderParams += params[0] + " in " + params[1] + " " + params[2] + "[];\n";
									}
									else
									{
										if (hasGeometryShader)
											geometryShaderParams += params[0] + " in " + params[1] + " " + params[2] + ";\n";
										else
											fragmentShaderParams += params[0] + " in " + params[1] + " " + params[2] + ";\n";
									}
								}
							}
						}
					}

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + vertexShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							std::string voidBlock;
							StringUtils::findEndOfBlock(shaderLines, k, static_cast<uint32_t>(shaderLines.size() - k), &voidBlock);

							const uint32_t firstClosingBracket = static_cast<uint32_t>(voidBlock.find_first_of(')'));
							voidBlock = voidBlock.substr(firstClosingBracket + 1);
							voidBlock = "void " + vertexShaderName + "()" + voidBlock;

							std::string dummyMain = "void main(void) { " + vertexShaderName + "(); }";

							const std::string vertexShaderSource = versionString + "\n" +
								vertexShaderParams + "\n" +
								vertexShaderData + "\n" + globalShaderUniforms + "\n" + vertexShaderUniforms + "\n" +
								vertexFunctions + "\n" + globalFunctions + "\n" + voidBlock + "\n" + dummyMain;

							pass->_setVertexSource(vertexShaderSource);

							vertexShaderParams = "";

							break;
						}
					}

					continue;
				}

				if (strstr(line.c_str(), "TCShader"))
				{
					hasTesselationShader = true;

					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string TCSShaderName = parts[1];
					TCSShaderName.erase(std::remove(TCSShaderName.begin(), TCSShaderName.end(), ';'), TCSShaderName.end());

					std::pair<std::string, std::string> interfacePair = tesInterfaces[pass];

					for (uint32_t c = 0; c < shaderParams.size(); c++)
					{
						std::pair<std::string, std::string> pair = shaderParams[c];
						if (pair.first == interfacePair.second)
						{
							std::vector<std::string> parts = StringUtils::splitWithRegex(pair.second, ";");
							for (uint32_t x = 0; x < parts.size() - 1; x++)
							{
								std::vector<std::string> params = StringUtils::splitWithRegex(parts[x], "\\s+");
								if (params.size() == 2)
								{
									tcsShaderParams += "out " + params[0] + " " + params[1] + "[];\n";
									tesShaderParams += "in " + params[0] + " " + params[1] + "[];\n";
								}
								else if (params.size() == 3)
								{
									// has modifier
									tcsShaderParams += "out " + params[0] + " " + params[1] + " " + params[2] + "[];\n";
									tesShaderParams += "in " + params[0] + " " + params[1] + " " + params[2] + "[];\n";
								}
							}
						}
					}

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + TCSShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							std::string voidBlock;
							StringUtils::findEndOfBlock(shaderLines, k, static_cast<uint32_t>(shaderLines.size() - k), &voidBlock);

							const uint32_t firstClosingBracket = static_cast<uint32_t>(voidBlock.find_first_of(')'));
							voidBlock = voidBlock.substr(firstClosingBracket + 1);
							voidBlock = "void " + TCSShaderName + "()" + voidBlock;

							std::string dummyMain = "void main(void) { " + TCSShaderName + "(); }";

							const std::string TCSShaderSource = versionString + "\n" + tcsLayouts + "\n" +
								tcsShaderParams + "\n" +
								tcsShaderData + "\n" + globalShaderUniforms + "\n" + tcsShaderUniforms + "\n" +
								tcsFunctions + "\n" + globalFunctions + "\n" + voidBlock + "\n" + dummyMain;

							pass->_setTCSSource(TCSShaderSource);

							tcsShaderParams = "";
							break;
						}
					}
				}

				if (strstr(line.c_str(), "TEShader"))
				{
					hasTesselationShader = true;

					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string TEShaderName = parts[1];
					TEShaderName.erase(std::remove(TEShaderName.begin(), TEShaderName.end(), ';'), TEShaderName.end());

					std::pair<std::string, std::string> interfacePair;
					if (hasGeometryShader)
					{
						interfacePair = geometryInterfaces[pass];
					}
					else
					{
						interfacePair = fragmentInterfaces[pass];
					}

					for (uint32_t c = 0; c < shaderParams.size(); c++)
					{
						std::pair<std::string, std::string> pair = shaderParams[c];
						if (pair.first == interfacePair.second)
						{
							std::vector<std::string> parts = StringUtils::splitWithRegex(pair.second, ";");
							for (uint32_t x = 0; x < parts.size() - 1; x++)
							{
								std::vector<std::string> params = StringUtils::splitWithRegex(parts[x], "\\s+");

								if (params.size() == 2)
								{
									std::string secondParam = params[1];
									if (strstr(params[1].c_str(), "["))
									{
										std::vector<std::string> removedParts = StringUtils::splitWithRegex(secondParam, "[");
										secondParam = removedParts[0];
									}
									tesShaderParams += "out " + params[0] + " " + secondParam + ";\n";
									if (hasGeometryShader)
									{
										geometryShaderParams += "in " + params[0] + " " + params[1] + ";\n";
									}
									else
									{
										fragmentShaderParams += "in " + params[0] + " " + params[1] + ";\n";
									}
								}
								else if (params.size() == 3)
								{
									// has modifier
									if (hasGeometryShader)
									{
										geometryShaderParams += "out " + params[0] + " " + params[1] + " " + params[2] + ";\n";
									}
									else
									{
										fragmentShaderParams += "in " + params[0] + " " + params[1] + " " + params[2] + ";\n";
									}
								}
							}
						}
					}

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + TEShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							std::string voidBlock;
							StringUtils::findEndOfBlock(shaderLines, k, static_cast<uint32_t>(shaderLines.size() - k), &voidBlock);

							const uint32_t firstClosingBracket = static_cast<uint32_t>(voidBlock.find_first_of(')'));
							voidBlock = voidBlock.substr(firstClosingBracket + 1);
							voidBlock = "void " + TEShaderName + "()" + voidBlock;

							std::string dummyMain = "void main(void) { " + TEShaderName + "(); }";

							const std::string TESShaderSource = versionString + "\n" + tesLayouts + "\n" +
								tesShaderParams + "\n" +
								tesShaderData + "\n" + globalShaderUniforms + "\n" + tesShaderUniforms + "\n" +
								tesFunctions + "\n" + globalFunctions + "\n" + voidBlock + "\n" + dummyMain;

							pass->_setTESSource(TESShaderSource);

							tesShaderParams = "";
							break;
						}
					}
				}

				if (strstr(line.c_str(), "GeometryShader"))
				{
					hasGeometryShader = true;

					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string geometryShaderName = parts[1];
					geometryShaderName.erase(std::remove(geometryShaderName.begin(), geometryShaderName.end(), ';'), geometryShaderName.end());

					std::pair<std::string, std::string> interfacePair = fragmentInterfaces[pass];

					for (uint32_t c = 0; c < shaderParams.size(); c++)
					{
						std::pair<std::string, std::string> pair = shaderParams[c];
						if (pair.first == interfacePair.second)
						{
							std::vector<std::string> parts = StringUtils::splitWithRegex(pair.second, ";");
							for (uint32_t x = 0; x < parts.size() - 1; x++)
							{
								std::vector<std::string> params = StringUtils::splitWithRegex(parts[x], " ");
								if (params.size() == 2)
								{
									geometryShaderParams += "out " + params[0] + " " + params[1] + ";\n";
									fragmentShaderParams += "in " + params[0] + " " + params[1] + ";\n";
								}
								else if (params.size() == 3)
								{
									// has modifier
									geometryShaderParams += "out " + params[0] + " " + params[1] + " " + params[2] + ";\n";
									fragmentShaderParams += "in " + params[0] + " " + params[1] + " " + params[2] + ";\n";
								}
							}
						}
					}

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + geometryShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							std::string voidBlock;
							StringUtils::findEndOfBlock(shaderLines, k, static_cast<uint32_t>(shaderLines.size() - k), &voidBlock);

							const uint32_t firstClosingBracket = static_cast<uint32_t>(voidBlock.find_first_of(')'));
							voidBlock = voidBlock.substr(firstClosingBracket + 1);
							voidBlock = "void " + geometryShaderName + "()" + voidBlock;

							std::string dummyMain = "void main(void) { " + geometryShaderName + "(); }";

							const std::string geometryShaderSource = versionString + "\n" + geometryLayouts + "\n" +
								geometryShaderParams + "\n" +
								geometryShaderData + "\n" + globalShaderUniforms + "\n" + geometryShaderUniforms + "\n" +
								geometryFunctions + "\n" + globalFunctions + "\n" + voidBlock + "\n" + dummyMain;

							pass->_setGeometrySource(geometryShaderSource);

							geometryShaderParams = "";
							break;
						}
					}

					continue;
				}

				if (strstr(line.c_str(), "FragmentShader"))
				{
					line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());

					std::vector<std::string> parts = StringUtils::splitWithRegex(line, "=");
					std::string fragmentShaderName = parts[1];
					fragmentShaderName.erase(std::remove(fragmentShaderName.begin(), fragmentShaderName.end(), ';'), fragmentShaderName.end());

					fragmentShaderName = StringUtils::splitWithRegex(fragmentShaderName, ",")[0];
					fragmentShaderName.erase(std::remove(fragmentShaderName.begin(), fragmentShaderName.end(), '{'), fragmentShaderName.end());

					for (uint32_t k = 0; k < shaderLines.size(); k++)
					{
						std::string effectLine = shaderLines[k];
						std::string token = "void " + fragmentShaderName;
						if (strstr(effectLine.c_str(), token.c_str()))
						{
							const uint32_t startColonLocation = static_cast<uint32_t>(effectLine.find_first_of('('));
							const uint32_t endColonLocation = static_cast<uint32_t>(effectLine.find_last_of(')'));

							std::string interfaceName = effectLine.substr(startColonLocation + 1, endColonLocation - startColonLocation - 1);

							for (uint32_t c = 0; c < shaderParams.size(); c++)
							{
								std::pair<std::string, std::string> pair = shaderParams[c];
								if (pair.first == "FragOut")
								{
									std::vector<std::string> parts = StringUtils::splitWithRegex(pair.second, ";");
									for (uint32_t x = 0; x < parts.size() - 1; x++)
									{
										std::string line = parts[x];
										if (strstr(line.c_str(), ":"))
										{
											// This has semantics
											std::vector<std::string> params = StringUtils::splitWithRegex(parts[x], "\\s+");
											if (params.size() == 2)
											{
												fragmentShaderParams += "out " + params[0] + " " + params[1] + ";\n";
											}
										}
										else
										{
											// This does not have semantics
											fragmentShaderParams += line + ";";
										}
									}
								}
							}

							std::string voidBlock;
							StringUtils::findEndOfBlock(shaderLines, k, static_cast<uint32_t>(shaderLines.size() - k), &voidBlock);

							const uint32_t firstClosingBracket = static_cast<uint32_t>(voidBlock.find_first_of(')'));
							voidBlock = voidBlock.substr(firstClosingBracket + 1);
							voidBlock = "void " + fragmentShaderName + "()" + voidBlock;

							std::string dummyMain = "void main(void) { " + fragmentShaderName + "(); }";

							const std::string fragmentShaderSource = versionString + "\n" + fragmentOutVariables + "\n" +
								fragmentShaderParams + "\n" +
								fragmentShaderData + "\n" + globalShaderUniforms + "\n" + fragmentShaderUniforms + "\n" +
								fragmentFunctions + "\n" + globalFunctions + "\n" + voidBlock + "\n" + dummyMain;

							pass->_setFragmentSource(fragmentShaderSource);

							fragmentShaderParams = "";
							break;
						}
					}

					continue;
				}
			}
		}
	}
}
*/