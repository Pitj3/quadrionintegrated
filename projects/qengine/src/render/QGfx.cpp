#include <cstdint>
#include <string>
#include <unordered_map>
#include <utility>

#include "render/QGfx.h"
#include "render/MatrixStack.h"
#include "FreeImage.h"

//#include <qtl/unordered_map.h>
//#include <qtl/string.h>

struct QGfxImpl
{
	uint32_t preferredTexFilter;
	bool arePreferencesInitialized = 0;

	
	bool isDepthTestEnabled;
	bool isDepthWriteEnabled;

	MatrixMode currentMatrixMode;
	mat4<float> currentModelMatrix;
	mat4<float> currentViewMatrix;
	mat4<float> currentProjectionMatrix;
	MatrixStack modelMatrixStack;
	MatrixStack viewMatrixStack;
	MatrixStack projectionMatrixStack;

	bool isCullingEnabled;
	CullMode currentCullMode;
	FrontFace currentFrontFace;

	bool isWireframeEnabled;
	bool bAreMatricesDirty;

//	std::unordered_map<std::string, Effect*>					effectMap;
	std::unordered_map<std::string, GLuint>						textureMap;
//	std::unordered_map<std::string, GLuint>						textureMap;
	std::unordered_map<std::string, FboId>						framebufferMap;
//	std::unordered_map<std::string, FboId>						framebufferMap;
	std::unordered_map<std::string, ShadowmapId>				shadowmapMap;
	std::unordered_map<std::string, MultiShadowmapId>			multiShadowmapMap;
};

QGfxImpl* QGfx::mImpl;

std::map<std::string, std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>>> QGfx::mObjectMaterialMap;
std::map<std::string, std::vector<ObjectMaterial*>> QGfx::mEffectMaterialMap;
std::vector<ObjectMaterial*> QGfx::mObjectMaterials;
CompareFunction QGfx::currentDepthCompare;
uint32_t QGfx::mNumRenderObjects;
std::map<std::string, DepthStencilId> QGfx::mDSBufferMap;
std::map<std::string, FboId> QGfx::mFBOMap;
std::map<std::string, GLuint> QGfx::mRTMap;
std::map<uint32_t, std::shared_ptr<RenderObject>> QGfx::mRenderObjectIDMap;
std::map<uint32_t, ObjectMaterial*> QGfx::mRenderObjectMaterialMap;
std::queue<std::string> QGfx::mRenderQueue;
uint32_t QGfx::mCurrentFramebuffer;
vec4<int32_t> QGfx::mDefaultViewport;

QGfx::QGfx()
{
}

QGfx::~QGfx()
{
	FreeImage_DeInitialise();
}

bool QGfx::initializeGraphics()
{
	mImpl = new QGfxImpl;

	// Init Z State //
	mImpl->isDepthTestEnabled =  true;
//	mImpl->currentDepthCompare = LEQUAL;
	currentDepthCompare = LEQUAL;
	mImpl->isDepthWriteEnabled = true;
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(LEQUAL);
	glDepthMask(GL_TRUE);

	// Init Matrix State //
	mImpl->currentMatrixMode = NONE;
	mImpl->currentModelMatrix.loadIdentity();
	mImpl->currentViewMatrix.loadIdentity();
	mImpl->currentProjectionMatrix.loadIdentity();

	mImpl->modelMatrixStack.pushMatrix(mImpl->currentModelMatrix);
	mImpl->viewMatrixStack.pushMatrix(mImpl->currentViewMatrix);
	mImpl->projectionMatrixStack.pushMatrix(mImpl->currentProjectionMatrix);
	
	// Init Culling State //
	mImpl->isCullingEnabled = false;
	mImpl->currentCullMode = BACK;
	mImpl->currentFrontFace = CCW;

	mImpl->bAreMatricesDirty = false;

	// Raster state //
	mImpl->isWireframeEnabled = false;

	FreeImage_Initialise();

	mNumRenderObjects = 0;
	mCurrentFramebuffer = 0;

	GLint vp[4];
	glGetIntegerv(GL_VIEWPORT, vp);
	mDefaultViewport.set(vp[0], vp[1], vp[2], vp[3]);

	return true;
}

bool QGfx::setDefaultState()
{
	// Init Z State //
	mImpl->isDepthTestEnabled = true;
//	mImpl->currentDepthCompare = LEQUAL;
	currentDepthCompare = LEQUAL;
	mImpl->isDepthWriteEnabled = true;
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(LEQUAL);
	glDepthMask(GL_TRUE);

	// Init Culling State //
	mImpl->isCullingEnabled = false;
	mImpl->currentCullMode = BACK;
	mImpl->currentFrontFace = CCW;
	mImpl->bAreMatricesDirty = false;
	glDisable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	// Raster state //
	mImpl->isWireframeEnabled = false;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	return true;
}

void QGfx::enableDepthTest()
{
	if (!mImpl->isDepthTestEnabled)
	{
		glEnable(GL_DEPTH_TEST);
//		glDepthFunc(mImpl->currentDepthCompare);
		glDepthFunc(currentDepthCompare);
		mImpl->isDepthTestEnabled = true;
	}
}

void QGfx::disableDepthTest()
{
	if (mImpl->isDepthTestEnabled)
	{
		glDisable(GL_DEPTH_TEST);
		mImpl->isDepthTestEnabled = false;
	}
}

void QGfx::changeDepthTest(const CompareFunction aFunc)
{
	glDepthFunc(aFunc);
//	mImpl->currentDepthCompare = aFunc;
	currentDepthCompare = aFunc;
}

void QGfx::enableDepthWrite()
{
	if(!mImpl->isDepthWriteEnabled)
	{
		glDepthMask(GL_TRUE);
		mImpl->isDepthWriteEnabled = true;
	}
}

void QGfx::disableDepthWrite()
{
	if(mImpl->isDepthWriteEnabled)
	{
		glDepthMask(GL_FALSE);
		mImpl->isDepthWriteEnabled = false;
	}
}

bool QGfx::isDepthTestEnabled()
{
	return mImpl->isDepthTestEnabled;
}

bool QGfx::isDepthWriteEnabled()
{
	return mImpl->isDepthWriteEnabled;
}

bool QGfx::setMatrix(MatrixMode aMode, const mat4<float>& aMat)
{
	if(aMode == NONE)
		return false;

	if (aMode == MODEL)
	{
		mImpl->modelMatrixStack.popMatrix();
		mImpl->modelMatrixStack.pushMatrix(aMat);
	}

	else if (aMode == VIEW)
	{
		mImpl->viewMatrixStack.popMatrix();
		mImpl->viewMatrixStack.pushMatrix(aMat);
	}

	else if (aMode == PROJECTION)
	{
		mImpl->projectionMatrixStack.popMatrix();
		mImpl->projectionMatrixStack.pushMatrix(aMat);
	}

	else
		return false;

	return true;
}

bool QGfx::pushMatrix(MatrixMode aMode, const mat4<float>& aMat)
{
	if(aMode == NONE)
		return false;

	if (aMode == MODEL)
		mImpl->modelMatrixStack.pushMatrix(aMat);

	else if (aMode == VIEW)
		mImpl->viewMatrixStack.pushMatrix(aMat);

	else if (aMode == PROJECTION)
		mImpl->projectionMatrixStack.pushMatrix(aMat);

	else
		return false;

	return true;
}

bool QGfx::popMatrix(MatrixMode aMode)
{
	if (aMode == NONE)
		return false;

	if (aMode == MODEL)
		mImpl->modelMatrixStack.popMatrix();

	else if (aMode == VIEW)
		mImpl->viewMatrixStack.popMatrix();

	else if (aMode == PROJECTION)
		mImpl->projectionMatrixStack.popMatrix();

	else
		return false;

	return true;
}

mat4<float> QGfx::getMatrix(MatrixMode aMode)
{
	mat4<float> m, v, p;
	m.loadIdentity();
	v.loadIdentity();
	p.loadIdentity();

	if (aMode & MODEL)
		m = mImpl->modelMatrixStack.top();
	if (aMode & VIEW)
		v = mImpl->viewMatrixStack.top();
	if (aMode & PROJECTION)
		p = mImpl->projectionMatrixStack.top();

	const mat4<float> t = p * v;
	mat4<float> f = t * m;


	if (aMode & TRANSPOSE)
		f.transpose();

	else if (aMode & INVERSE)
	{
		const mat4<float> tmp = f.getInverse();
		f = tmp;
	}

	else if (aMode & INVERSE_TRANSPOSE)
	{
		mat4<float> tmp = f.getInverse();

		tmp.transpose();
		f = tmp;
	}

	else
	{
	}

	return f;
}

void QGfx::enableBackfaceCulling()
{
	if (!mImpl->isCullingEnabled)
	{
		glEnable(GL_CULL_FACE);
		mImpl->isCullingEnabled = true;
	}
}

void QGfx::disableBackfaceCulling()
{
	if (mImpl->isCullingEnabled)
	{
		glDisable(GL_CULL_FACE);
		mImpl->isCullingEnabled = false;
	}
}

void QGfx::changeFrontFaceWinding(FrontFace aFace)
{
	glFrontFace(aFace);
}

void QGfx::changeCullMode(CullMode aMode)
{
	glCullFace(aMode);
}

void QGfx::enableWireframe()
{
	if(!mImpl->isWireframeEnabled)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		mImpl->isWireframeEnabled = true;
	}
}

void QGfx::disableWireframe()
{
	if (mImpl->isWireframeEnabled)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		mImpl->isWireframeEnabled = false;
	}
}

/*
void QGfx::addEffect(Effect* aEffect)
{
	std::string name = aEffect->getName();
	mImpl->effectMap.insert(std::make_pair(aEffect->getName(), aEffect));
}

Effect* QGfx::getEffect(const std::string& aName)
{
	auto iter = mImpl->effectMap.find(aName.c_str());
	if(iter != mImpl->effectMap.end())
		return iter->second;

	else 
		return nullptr;
}
*/
void QGfx::addTexture(std::string aTag, GLuint aId)
{
	mImpl->textureMap.insert(std::make_pair(aTag, aId));
//	mImpl->textureMap.insert(std::make_pair(aTag.c_str(), aId));
}

GLuint QGfx::getTexture(std::string aTag)
{
	auto iter = mImpl->textureMap.find(aTag.c_str());
	if(iter != mImpl->textureMap.end())
		return iter->second;

	else
		return 0;
}

void QGfx::addFramebuffer(std::string aTag, FboId aBufferId)
{
//	mImpl->framebufferMap.insert(std::make_pair(aTag, aBufferId));
	auto iter = mFBOMap.find(aTag);
	if(iter == mFBOMap.end())
		mFBOMap[aTag] = aBufferId;
	
//	mImpl->framebufferMap.insert(std::make_pair(aTag.c_str(), aBufferId));
}

void QGfx::createRenderBuffer(OutputBufferInfo* aObi)
{
	if(aObi->name == "DefaultSurface")
		return;

	auto mapIter = mFBOMap.find(aObi->name);
	uint32_t flags = aObi->flags;

	// FBO Exists
	if(mapIter != mFBOMap.end())
		return;

	// DS Surface exists
	DepthStencilId ds;
	std::string dsName = aObi->name + "DS";
	auto dsMapIter = mDSBufferMap.find(dsName);
	if(dsMapIter != mDSBufferMap.end())
		ds = dsMapIter->second;
	else if (aObi->depthFormat == QTEXTURE_FORMAT_NONE)
	{
		DepthStencilId def;
		def.depthId = 0;
		def.stencilId = 0;
		ds = def;
	}
	else
	{
		uint32_t depthBits = 32;
		uint32_t stencilBits = 0;
		if(aObi->depthFormat == QTEXTURE_FORMAT_DEPTH24)
		{
			depthBits = 24;
			stencilBits = 8;
		}
		else if(aObi->depthFormat == QTEXTURE_FORMAT_DEPTH16)
			depthBits = 16;

		ds = addDepthStencilTarget(aObi->width, aObi->height, depthBits, stencilBits, flags);
		mDSBufferMap[dsName] = ds;
	}

	GLuint colorBufs[4];
	for (uint32_t i = 0; i < aObi->colorBuffers; i++)
	{
		GLuint rt = 0;
		std::string rtName;
		if(aObi->colorBuffers > 1)
			rtName = aObi->name + std::to_string(i);
		else
		{
			rtName = aObi->name;
		}
		auto rtMapIter = mRTMap.find(rtName);
		if (rtMapIter != mRTMap.end())
			rt = rtMapIter->second;
		else
		{
			rt = addRenderTarget(aObi->width, aObi->height, aObi->colorFormat, flags);
			mRTMap[rtName] = rt;
		}

		colorBufs[i] = rt;
	}

	FboId fbo;
	fbo = createRenderableSurface(colorBufs, aObi->colorBuffers, ds, aObi->width, aObi->height);
	fbo.clearTarget = true;
	mFBOMap[aObi->name] = fbo;
}

void QGfx::resetBufferClearFlags()
{
	for (auto& fbo : mFBOMap)
	{
		fbo.second.clearTarget = true;	
	}
}

FboId QGfx::getRenderBuffer(const std::string& aName)
{
	FboId fbo;
	auto fboMapIter = mFBOMap.find(aName);
	if(fboMapIter != mFBOMap.end())
		fbo = fboMapIter->second;
	else
	{
		fbo.id = 0;
	}

	return fbo;
}

void QGfx::setBufferClearFlag(const std::string& aName, bool aClear)
{
	auto fboMapIter = mFBOMap.find(aName);
	if(fboMapIter != mFBOMap.end())
		fboMapIter->second.clearTarget = aClear;
}

GLuint QGfx::getRenderTarget(const std::string& aName)
{
	GLuint rt = 0;
	auto rtMapIter = mRTMap.find(aName);
	if (rtMapIter != mRTMap.end())
		rt = rtMapIter->second;

	return rt;
}



void QGfx::addShadowmap(std::string aTag, ShadowmapId aId)
{
	mImpl->shadowmapMap.insert(std::make_pair(aTag, aId));
}

void QGfx::addShadowmap(std::string aTag, MultiShadowmapId aId)
{
	mImpl->multiShadowmapMap.insert(std::make_pair(aTag, aId));
}

void QGfx::bindShadowmap(const std::string& aTag)
{
	if (aTag.compare("default") == 0)
	{
		bindDefaultSurface();
		return;
	}

	auto iter = mImpl->shadowmapMap.find(aTag);
	if (iter != mImpl->shadowmapMap.end())
	{
		bindShadowmapSurface(iter->second);
		return;
	}

	auto iter2 = mImpl->multiShadowmapMap.find(aTag);
	if (iter2 != mImpl->multiShadowmapMap.end())
	{
		bindMultiShadowmapSurface(iter2->second);
		return;
	}
}

bool QGfx::addObjectMaterial(Effect* aEffect, const std::string& aTechName, const std::string& aPassName, const char* aName)
{
	ObjectMaterial* thisMat = new ObjectMaterial(aEffect, aTechName, aPassName, aName);

	const char* effectName = aEffect->getName();
	if(mObjectMaterialMap.find(aName) != mObjectMaterialMap.end())
		return false;

	else
	{
//		std::vector<RenderObject*> v;
		std::vector<std::shared_ptr<RenderObject>> v;
		std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>> p = std::make_pair(thisMat, v);
		mObjectMaterialMap[aName] = p;

		auto thisEffect = mEffectMaterialMap.find(effectName);
		if(thisEffect != mEffectMaterialMap.end())
			thisEffect->second.push_back(thisMat);

		else 
		{
			std::vector<ObjectMaterial*> materialVec;
			materialVec.push_back(thisMat);
			mEffectMaterialMap[effectName] = materialVec;
		}

		mObjectMaterials.push_back(thisMat);
	}

	return true;
}

ObjectMaterial* QGfx::getObjectMaterial(const char* aName)
{
	auto mat = mObjectMaterialMap.find(aName);
	if(mat != mObjectMaterialMap.end())
	{
		return mat->second.first;
	}

	return nullptr;
}

void QGfx::pushToRenderQueue(const std::string& aMaterialName)
{
	mRenderQueue.push(aMaterialName);
}

std::queue<std::string>& QGfx::getRenderQueue()
{
	return mRenderQueue;
}

std::vector<ObjectMaterial*>& QGfx::getObjectMaterials()
{
	return mObjectMaterials;
}

bool QGfx::addRenderableToMaterial(const char* aMaterialName, 
								   std::shared_ptr<MeshAsset> aMeshAsset,
								   mat4<float> aMatrix)
{
	if(mObjectMaterialMap.find(aMaterialName) != mObjectMaterialMap.end())
	{
		auto pair = mObjectMaterialMap[aMaterialName];

		std::string shit = aMeshAsset->getName();
		std::string str = shit + std::to_string(pair.second.size());
		uint32_t objID = mNumRenderObjects + 1;
		std::shared_ptr<RenderObject> ro = std::make_shared<RenderObject>(aMeshAsset->getName().c_str(), str.c_str(), 
																		  objID);
		mNumRenderObjects++;
		ro->setWorldTransform(aMatrix);

		mObjectMaterialMap[aMaterialName].second.push_back(ro);	

		// search for render object ID in map first. Cannot have dupes
		auto idIter = mRenderObjectIDMap.find(objID);
		if(idIter == mRenderObjectIDMap.end())
		{
			mRenderObjectIDMap[objID] = ro;

			auto materialIter = mRenderObjectMaterialMap.find(objID);
			mRenderObjectMaterialMap[objID] = mObjectMaterialMap[aMaterialName].first;
		}

		return true;
	}

	return false;
}

bool QGfx::addRenderableToMaterial(const char* aMaterialName, std::shared_ptr<RenderObject> aRenderObj)
{
	if (mObjectMaterialMap.find(aMaterialName) != mObjectMaterialMap.end())
	{
		uint32_t objID = mNumRenderObjects + 1;
		auto pair = mObjectMaterialMap[aMaterialName];

		mObjectMaterialMap[aMaterialName].second.push_back(aRenderObj);

		// search for render object ID in map first. Cannot have dupes
		auto idIter = mRenderObjectIDMap.find(objID);
		if (idIter == mRenderObjectIDMap.end())
		{
			mRenderObjectIDMap[objID] = aRenderObj;

			auto materialIter = mRenderObjectMaterialMap.find(objID);
			mRenderObjectMaterialMap[objID] = mObjectMaterialMap[aMaterialName].first;
		}

		return true;
	}

	return false;
}

uint32_t QGfx::createRenderObjectID()	
{
	mNumRenderObjects++;
	return mNumRenderObjects;
}

//std::pair<ObjectMaterial*, std::vector<RenderObject*>> QGfx::getRenderablesFromMaterial(const char* aMaterialName)
std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>> QGfx::getRenderablesFromMaterial(const char* aMaterialName)
{
//	std::pair<ObjectMaterial*, std::vector<RenderObject*>> rv;
	std::pair<ObjectMaterial*, std::vector<std::shared_ptr<RenderObject>>> rv;
	auto r = mObjectMaterialMap.find(aMaterialName);
	if(r != mObjectMaterialMap.end())
		rv = r->second;
	
	return rv;
}

std::vector<ObjectMaterial*> QGfx::getObjectMaterialsFromEffect(const char* aEffectName)
{
	std::vector<ObjectMaterial*> rv;
	auto r = mEffectMaterialMap.find(aEffectName);
	if(r != mEffectMaterialMap.end())
		rv = r->second;

	return rv;
}

vec4<int32_t> QGfx::getDefaultViewportDims()
{
	return mDefaultViewport;
}

std::shared_ptr<RenderObject> QGfx::getRenderObject(const uint32_t aId)
{
	auto iter = mRenderObjectIDMap.find(aId);
	if(iter != mRenderObjectIDMap.end())
		return mRenderObjectIDMap[aId];

	return nullptr;
}

ObjectMaterial* QGfx::getObjectMaterial(const uint32_t aId)
{
	auto iter = mRenderObjectMaterialMap.find(aId);
	if(iter != mRenderObjectMaterialMap.end())
		return mRenderObjectMaterialMap[aId];

	return nullptr;
}

void QGfx::bindRenderTarget(OutputBufferInfo* aObi)
{
	if (aObi)
	{
		if (aObi->name == "DefaultSurface")
		{
			bindDefaultSurface();
			mCurrentFramebuffer = 0;
		}

		else
		{
			FboId fbo = QGfx::getRenderBuffer(aObi->name);
			if(!fbo.isCubemap)
				bindRenderableSurface(fbo);
			else
				bindRenderableCubemap(fbo);

			mCurrentFramebuffer = fbo.id;
			if (fbo.clearTarget)
			{
				glClearDepth(1.0);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				QGfx::setBufferClearFlag(aObi->name, false);
			}
		}
	}
}

//==============================================================================
//==============================================================================


void QGfx::GL::gl_GenBuffers(const GLsizei aN, GLuint* aBuffers)
{
	glGenBuffers(aN, aBuffers);
}

void QGfx::GL::gl_BindBuffer(const GLenum aTarget, const GLuint aBuffer)
{
	glBindBuffer(aTarget, aBuffer);
}

void QGfx::GL::gl_BindBufferBase(const GLenum aTarget, 
								 const GLuint aIndex, 
								 const GLuint aBuffer)
{
	glBindBufferBase(aTarget, aIndex, aBuffer);
}

void QGfx::GL::gl_GenVertexArrays(const GLsizei aN, GLuint* aRrays)
{
	glGenVertexArrays(aN, aRrays);
}

void QGfx::GL::gl_BindVertexArray(const GLuint aRray)
{
	glBindVertexArray(aRray);
}

void QGfx::GL::gl_EnableVertexAttribArray(const GLuint aIndex)
{
	glEnableVertexAttribArray(aIndex);
}

void QGfx::GL::gl_VertexAttribPointer(const GLuint aIndex,
	const GLint aSize,
	const GLenum aType,
	const GLboolean aNormalized,
	const GLsizei aStride,
	const GLvoid* aPointer)
{
	glVertexAttribPointer(aIndex, aSize, aType, aNormalized, aStride, aPointer);
}

void QGfx::GL::gl_DeleteVertexArrays(const GLsizei aN, const GLuint* aRrays)
{
	glDeleteVertexArrays(aN, aRrays);
}

void QGfx::GL::gl_BufferData(const GLenum aTarget,
	const GLsizeiptr aSize,
	const GLvoid* aData,
	const GLenum aUsage)
{
	glBufferData(aTarget, aSize, aData, aUsage);
}

void* QGfx::GL::gl_MapBufferRange(const GLenum aTarget,
	const GLintptr aOffset,
	const GLsizeiptr aLength,
	const GLbitfield aCcess)
{
	return glMapBufferRange(aTarget, aOffset, aLength, aCcess);
}

GLboolean QGfx::GL::gl_UnmapBuffer(const GLenum aTarget)
{
	return glUnmapBuffer(aTarget);
}

void QGfx::GL::gl_DeleteBuffers(const GLsizei aN, const GLuint* aBuffers)
{
	glDeleteBuffers(aN, aBuffers);
}


void QGfx::GL::gl_GenTextures(const GLsizei aN, GLuint* aTextures)
{
	glGenTextures(aN, aTextures);
}

void QGfx::GL::gl_BindTexture(const GLenum aTarget, const GLuint aTexture)
{
	glBindTexture(aTarget, aTexture);
}

void QGfx::GL::gl_TexBuffer(const GLenum aTarget, 
							const GLenum aInternalFormat, 
							const GLuint aBuffer)
{
	glTexBuffer(aTarget, aInternalFormat, aBuffer);
}

void QGfx::GL::gl_GenQueries(const GLsizei aN, GLuint* aIds)
{
	glGenQueries(aN, aIds);
}

void QGfx::GL::gl_DeleteQueries(const GLsizei aN, const GLuint* aIds)
{
	glDeleteQueries(aN, aIds);
}

void QGfx::GL::gl_BeginQuery(const GLenum aTarget, const GLuint aId)
{
	glBeginQuery(aTarget, aId);
}

void QGfx::GL::gl_EndQuery(const GLenum aTarget)
{
	glEndQuery(aTarget);
}

void QGfx::GL::gl_GetQueryObjectuiv(const GLuint aId, 
									const GLenum aPname, 
									GLuint* aParams)
{
	glGetQueryObjectuiv(aId, aPname, aParams);
}

void QGfx::GL::gl_BeginTransformFeedback(const GLenum aPrimitiveMode)
{
	glBeginTransformFeedback(aPrimitiveMode);
}

void QGfx::GL::gl_EndTransformFeedback()
{
	glEndTransformFeedback();
}

void QGfx::GL::gl_DrawArrays(const GLenum aMode, 
							 const GLint aFirst, 
							 const GLsizei aCount)
{
	glDrawArrays(aMode, aFirst, aCount);
}

void QGfx::GL::gl_DrawElementsInstanced(const GLenum aMode,
										const GLsizei aCount,
										const GLenum aType,
										const void* aIndices,
										const GLsizei primcount)
{
	glDrawElementsInstanced(aMode, aCount, aType, aIndices, primcount);
}

void QGfx::GL::gl_DrawElements(GLenum mode,
							   GLsizei count,
							   GLenum type,
							   const GLvoid* indices)
{
	glDrawElements(mode, count, type, indices);
}

void QGfx::GL::gl_Enablei(const GLenum aCap, const GLuint aIndex)
{
	glEnablei(aCap, aIndex);
}

void QGfx::GL::gl_Disablei(const GLenum aCap, const GLuint aIndex)
{
	glDisablei(aCap, aIndex);
}

void QGfx::GL::gl_BlendFunci(const GLuint aBuf, 
							 const GLenum aSfactor, 
							 const GLenum aDfactor)
{
	glBlendFunci(aBuf, aSfactor, aDfactor);
}

void QGfx::GL::gl_ClearBufferiv(const GLenum aBuffer, 
								const GLint aDrawbuffer, 
								const GLint* aValue)
{
	glClearBufferiv(aBuffer, aDrawbuffer, aValue);
}

void QGfx::GL::gl_ClearBufferuiv(const GLenum aBuffer, 
								 const GLint aDrawbuffer, 
								 const GLuint* aValue)
{
	glClearBufferuiv(aBuffer, aDrawbuffer, aValue);
}

void QGfx::GL::gl_ClearBufferfv(const GLenum aBuffer, 
							    const GLint aDrawbuffer, 
								const GLfloat* aValue)
{
	glClearBufferfv(aBuffer, aDrawbuffer, aValue);
}

void QGfx::GL::gl_ClearBufferfi(const GLenum aBuffer, 
								const GLint aDrawbuffer, 
								const GLfloat aDepth, 
								const GLint aStencil)
{
	glClearBufferfi(aBuffer, aDrawbuffer, aDepth, aStencil);
}

void QGfx::GL::gl_BlendEquation(const GLenum aMode)
{
	glBlendEquation(aMode);
}

void QGfx::GL::gl_ActiveTexture(const GLenum aSlot)
{
	glActiveTexture(aSlot);
}

void QGfx::GL::gl_BindFramebuffer(const GLenum aTarget, 
								  const GLuint aFramebuffer)
{
	glBindFramebuffer(aTarget, aFramebuffer);
}

void QGfx::GL::gl_FramebufferTexture2D(const GLenum aTarget, 
									   const GLenum aTtachment, 
									   const GLenum aTextarget, 
									   const GLuint aTexture, 
									   const GLint level)
{
	glFramebufferTexture2D(aTarget, aTtachment, aTextarget, aTexture, level);
}

GLenum QGfx::GL::gl_CheckFramebufferStatus(const GLenum aTarget)
{
	return glCheckFramebufferStatus(aTarget);
}

void QGfx::GL::gl_GenFramebuffers(const GLsizei aN, GLuint* aIds)
{
	glGenFramebuffers(aN, aIds);
}

void QGfx::GL::gl_StencilOpSeparate(const GLenum aFace, 
									const GLenum aSfail, 
									const GLenum aDpfail, 
									const GLenum aDppass)
{
	glStencilOpSeparate(aFace, aSfail, aDpfail, aDppass);
}

void QGfx::GL::gl_BlitFramebuffer(const GLint aSrcX0,
								  const GLint aSrcY0,
								  const GLint aSrcX1,
								  const GLint aSrcY1,
								  const GLint aDstX0,
								  const GLint aDstY0,
								  const GLint aDstX1,
								  const GLint aDstY1,
								  const GLbitfield aMask,
								  const GLenum aFilter)
{
	glBlitFramebuffer(aSrcX0, 
					  aSrcY0, 
					  aSrcX1, 
					  aSrcY1, 
					  aDstX0, 
					  aDstY0, 
					  aDstX1, 
					  aDstY1, 
					  aMask, 
					  aFilter);
}

void QGfx::GL::gl_DrawBuffers(const GLsizei aN, const GLenum* aBufs)
{
	glDrawBuffers(aN, aBufs);
}

void QGfx::GL::gl_GetIntegerv(GLenum pName, GLint* params)
{
	glGetIntegerv(pName, params);
}

void QGfx::GL::gl_ReadPixels(GLint x,
							 GLint y,
							 GLsizei width,
							 GLsizei height,
							 GLenum format,
							 GLenum type,
							 GLvoid* data)
{
	glReadPixels(x, y, width, height, format, type, data);
}

void QGfx::GL::gl_PatchParameteri(GLenum pname, GLint value)
{
	glPatchParameteri(pname, value);
}

void QGfx::GL::gl_ReadBuffer(GLenum mode)
{
	glReadBuffer(mode);
}
