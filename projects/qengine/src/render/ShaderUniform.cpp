#include "render/ShaderUniform.h"
#include "render/Shader.h"

ShaderUniformDeclaration::ShaderUniformDeclaration(const Type aType, const std::string& aName, uint32_t aCount) :
	mType(aType), mStruct(nullptr)
{
	mLocation = 0;
	mOffset = 0;
	mName = const_cast<char*>(aName.c_str());
	mCount = aCount;
	mSize = sizeofUniformType(aType) * aCount;
}

ShaderUniformDeclaration::ShaderUniformDeclaration(ShaderStruct* aStruct, const std::string& aName, uint32_t aCount) :
	mType(ShaderUniformDeclaration::Type::STRUCT), mStruct(aStruct)
{
	mLocation = 0;
	mOffset = 0;
	mName = const_cast<char*>(aName.c_str());
	mCount = aCount;
	mSize = mStruct->getSize() * aCount;
}

uint32_t ShaderUniformDeclaration::sizeofUniformType(const Type aType)
{
	switch (aType)
	{
		case ShaderUniformDeclaration::Type::BOOL:				return 1;
		case ShaderUniformDeclaration::Type::INT32:				return 4;
		case ShaderUniformDeclaration::Type::FLOAT32:			return 4;
		case ShaderUniformDeclaration::Type::VEC2:				return 4 * 2;
		case ShaderUniformDeclaration::Type::VEC3:				return 4 * 3;
		case ShaderUniformDeclaration::Type::VEC4:				return 4 * 4;
		case ShaderUniformDeclaration::Type::MAT3:				return 4 * 3 * 3;
		case ShaderUniformDeclaration::Type::MAT4:				return 4 * 4 * 4;
		default:												return 0;
	}
}

uint32_t ShaderUniformDeclaration::sizeofUniformBufferType(const Type aType, const bool aIsArray, const uint32_t aAmount)
{
	if(aIsArray)
	{
		switch (aType)
		{
			case ShaderUniformDeclaration::Type::BOOL:				return 16 * aAmount;
			case ShaderUniformDeclaration::Type::INT32:				return 16 * aAmount;
			case ShaderUniformDeclaration::Type::FLOAT32:			return 16 * aAmount;
			case ShaderUniformDeclaration::Type::VEC2:				return 16 * aAmount;
			case ShaderUniformDeclaration::Type::VEC3:				return 16 * aAmount;
			case ShaderUniformDeclaration::Type::VEC4:				return 16 * aAmount;
			case ShaderUniformDeclaration::Type::MAT3:				return 48 * aAmount;
			case ShaderUniformDeclaration::Type::MAT4:				return 64 * aAmount;
			default:												return 0;
		}
	}

	switch (aType)
	{
		case ShaderUniformDeclaration::Type::BOOL:				return 4;
		case ShaderUniformDeclaration::Type::INT32:				return 4;
		case ShaderUniformDeclaration::Type::FLOAT32:			return 4;
		case ShaderUniformDeclaration::Type::VEC2:				return 8;
		case ShaderUniformDeclaration::Type::VEC3:				return 16;
		case ShaderUniformDeclaration::Type::VEC4:				return 16;
		case ShaderUniformDeclaration::Type::MAT3:				return 48;
		case ShaderUniformDeclaration::Type::MAT4:				return 64;
		default:												return 0;
	}
}

uint32_t ShaderUniformDeclaration::offsetofUniformBufferType(const Type aType, const bool aIsArray, const uint32_t aAmount)
{
	switch (aType)
	{
		case ShaderUniformDeclaration::Type::BOOL:				return 8 * aAmount;
		case ShaderUniformDeclaration::Type::INT32:				return 8 * aAmount;
		case ShaderUniformDeclaration::Type::FLOAT32:			return 8 * aAmount;
		case ShaderUniformDeclaration::Type::VEC2:				return 8 * aAmount;
		case ShaderUniformDeclaration::Type::VEC3:				return 16 * aAmount;
		case ShaderUniformDeclaration::Type::VEC4:				return 16 * aAmount;
		case ShaderUniformDeclaration::Type::MAT3:				return 48 * aAmount;
		case ShaderUniformDeclaration::Type::MAT4:				return 64 * aAmount;
		default:												return 0;
	}
}

ShaderUniformDeclaration::Type ShaderUniformDeclaration::stringToType(const std::string& aType)
{
	if (aType == "bool")		return Type::BOOL;
	if (aType == "int32")		return Type::INT32;
	if (aType == "float")		return Type::FLOAT32;
	if (aType == "vec2")		return Type::VEC2;
	if (aType == "vec3")		return Type::VEC3;
	if (aType == "vec4")		return Type::VEC4;
	if (aType == "mat3")		return Type::MAT3;
	if (aType == "mat4")		return Type::MAT4;

	return Type::NONE;
}

std::string ShaderUniformDeclaration::typeToString(const Type aType)
{
	switch (aType)
	{
		case ShaderUniformDeclaration::Type::BOOL:		return "bool";
		case ShaderUniformDeclaration::Type::INT32:		return "int32";
		case ShaderUniformDeclaration::Type::FLOAT32:	return "float";
		case ShaderUniformDeclaration::Type::VEC2:		return "vec2";
		case ShaderUniformDeclaration::Type::VEC3:		return "vec3";
		case ShaderUniformDeclaration::Type::VEC4:		return "vec4";
		case ShaderUniformDeclaration::Type::MAT3:		return "mat3";
		case ShaderUniformDeclaration::Type::MAT4:		return "mat4";
		default:										return "Invalid Type";
	}
}

void ShaderUniformDeclaration::_setOffset(const uint32_t aOffset)
{
	if (mType == ShaderUniformDeclaration::Type::STRUCT)
	{
		mStruct->setOffset(aOffset);
	}

	mOffset = aOffset;
}

class ShaderUniformBufferDeclarationImpl
{
	public:
		std::vector<ShaderUniformDeclaration*> uniforms;
};

ShaderUniformBufferDeclaration::ShaderUniformBufferDeclaration(const std::string& aName, uint32_t aShaderType) :
	 mRegister(0), mSize(0), mShaderType(aShaderType)
{
	mName = const_cast<char*>(aName.c_str());
	mImpl = new ShaderUniformBufferDeclarationImpl();
}

ShaderUniformBufferDeclaration::~ShaderUniformBufferDeclaration()
{
	delete mImpl;
}


void ShaderUniformBufferDeclaration::pushUniform(ShaderUniformDeclaration* aUniform)
{
	uint32_t offset = 0;
	if (mImpl->uniforms.size())
	{
		ShaderUniformDeclaration* previous = mImpl->uniforms.back();
		offset = previous->mOffset + previous->mSize;
	}

	aUniform->_setOffset(offset);
	mSize += aUniform->getSize();
	mImpl->uniforms.push_back(aUniform);
}

std::vector<ShaderUniformDeclaration*> ShaderUniformBufferDeclaration::getUniformDeclarations() const
{
	return mImpl->uniforms;
}

ShaderUniformDeclaration* ShaderUniformBufferDeclaration::findUniform(const std::string& aName) const
{
	for (ShaderUniformDeclaration* uniform : mImpl->uniforms)
	{
		if (uniform->getName() == aName)
		{
			return uniform;
		}
	}

	return nullptr;
}

