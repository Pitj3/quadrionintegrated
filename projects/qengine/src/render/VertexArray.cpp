#include "render/VertexArray.h"

#include <vector>
#include "Extensions.h"
#include "render/VertexBuffer.h"
#include "render/IndexBuffer.h"

class VertexArrayImpl
{
	public:
		std::vector<VertexBuffer*> vertexBuffers;
		std::vector<IndexBuffer*> indexBuffers;
};

VertexArray::VertexArray()
{
	mImpl = new VertexArrayImpl();
	glGenVertexArrays(1, &mId);
}

VertexArray::VertexArray(VertexBuffer *aVertexBuffer, IndexBuffer *aIndexBuffer)
{
	mImpl = new VertexArrayImpl();

	glGenVertexArrays(1, &mId);
	addVertexBuffer(aVertexBuffer);
	addIndexBuffer(aIndexBuffer);
}

VertexArray::~VertexArray()
{
	delete mImpl;

	glDeleteVertexArrays(1, &mId);
}

void VertexArray::addVertexBuffer(VertexBuffer *aBuffer) const
{
	mImpl->vertexBuffers.push_back(aBuffer);

	bind();
	aBuffer->bind();

	_applyLayout(aBuffer->getLayout());

	unbind();
	aBuffer->unbind();
}

void VertexArray::addIndexBuffer(IndexBuffer *aBuffer) const
{
	mImpl->indexBuffers.push_back(aBuffer);

	bind();
	aBuffer->bind();
	unbind();
	aBuffer->unbind();
}

void VertexArray::bind() const
{
	glBindVertexArray(mId);
}

void VertexArray::unbind() const
{
	glBindVertexArray(0);
}

VertexBuffer* VertexArray::getVertexBuffer(const uint32_t aIndex) const
{
	return mImpl->vertexBuffers[aIndex];
}

IndexBuffer* VertexArray::getIndexBuffer(const uint32_t aIndex) const
{
	return mImpl->indexBuffers[aIndex];
}

void VertexArray::_applyLayout(const VertexBufferLayout &aLayout) const
{
	const std::vector<BufferElement> &elements = aLayout.getLayout();
	for (size_t i = 0; i < elements.size(); i++)
	{
		GLenum err = glGetError();
		const BufferElement &element = elements[i];
		glEnableVertexAttribArray(static_cast<uint32_t>(i));
		err = glGetError();
		const GLsizei stride = aLayout.getStride();
		const size_t offset = element.offset;
		glVertexAttribPointer(static_cast<uint32_t>(i), element.count, element.type, element.normalized, stride, reinterpret_cast<const void *>(offset));
		err = glGetError();
	}
}
