#include "render/effectparser/EffectTypeQualifier.h"


#include <assert.h>
#include "render/effectparser/EffectKeywords.h"

EffectTypeQualifier getEffectTypeQualifier(const std::string& aString)
{
	if (aString == EFFECT_CONST)
	{
		return EffectTypeQualifier::CONSTANT;
	}
	else if (aString == EFFECT_UNIFORM)
	{
		return EffectTypeQualifier::UNIFORM;
	}
	else if (aString == EFFECT_VARYING)
	{
		return EffectTypeQualifier::VARYING;
	}
	else if (aString == EFFECT_READONLY)
	{
		return EffectTypeQualifier::READONLY;
	}
	else if (aString == EFFECT_WRITEONLY)
	{
		return EffectTypeQualifier::WRITEONLY;
	}
	else if (aString == EFFECT_VOLATILE)
	{
		return EffectTypeQualifier::VOLATILE;
	}
	else if (aString == EFFECT_RESTRICT)
	{
		return EffectTypeQualifier::RESTRICT;
	}
	else if (aString == EFFECT_CENTROID)
	{
		return EffectTypeQualifier::CENTROID;
	}
	else if (aString == EFFECT_FLAT)
	{
		return EffectTypeQualifier::FLAT;
	}
	else if (aString == EFFECT_SMOOTH)
	{
		return EffectTypeQualifier::SMOOTH;
	}
	else if (aString == EFFECT_NOPERSPECTIVE)
	{
		return EffectTypeQualifier::NOPERSPECTIVE;
	}
	else if (aString == EFFECT_LOWP)
	{
		return EffectTypeQualifier::LOWP;
	}
	else if (aString == EFFECT_MEDIUMP)
	{
		return EffectTypeQualifier::MEDIUMP;
	}
	else if (aString == EFFECT_HIGHP)
	{
		return EffectTypeQualifier::HIGHP;
	}
	else if(aString == EFFECT_OUT)
	{
		return EffectTypeQualifier::GLSLOUT;
	}
	else if (aString == EFFECT_IN)
	{
		return EffectTypeQualifier::GLSLIN;
	}
	else if (aString == EFFECT_INOUT)
	{
		return EffectTypeQualifier::GLSLINOUT;
	}

	assert("Could not determine effect type qualifier");
	return EffectTypeQualifier::UNKNOWN;
}

std::string getEffectTypeQualifierString(const EffectTypeQualifier& aQualifier)
{
    switch(aQualifier)
    {
	    case EffectTypeQualifier::CONSTANT: return EFFECT_CONST; break;
	    case EffectTypeQualifier::UNIFORM: return EFFECT_UNIFORM; break;
	    case EffectTypeQualifier::VARYING: return EFFECT_VARYING; break;
	    case EffectTypeQualifier::READONLY: return EFFECT_READONLY; break;
	    case EffectTypeQualifier::WRITEONLY: return EFFECT_WRITEONLY; break;
	    case EffectTypeQualifier::VOLATILE: return EFFECT_VOLATILE; break;
	    case EffectTypeQualifier::RESTRICT: return EFFECT_RESTRICT; break;
	    case EffectTypeQualifier::CENTROID: return EFFECT_CENTROID; break;
	    case EffectTypeQualifier::FLAT: return EFFECT_FLAT; break;
	    case EffectTypeQualifier::SMOOTH: return EFFECT_SMOOTH; break;
	    case EffectTypeQualifier::NOPERSPECTIVE: return EFFECT_NOPERSPECTIVE; break;
	    case EffectTypeQualifier::LOWP: return EFFECT_LOWP; break;
	    case EffectTypeQualifier::MEDIUMP: return EFFECT_MEDIUMP; break;
		case EffectTypeQualifier::HIGHP: return EFFECT_HIGHP; break;
		case EffectTypeQualifier::GLSLOUT: return EFFECT_OUT; break;
		case EffectTypeQualifier::GLSLIN: return EFFECT_IN; break;
		case EffectTypeQualifier::GLSLINOUT: return EFFECT_INOUT; break;
	    case EffectTypeQualifier::UNKNOWN: return "ERROR"; break;
	    default: return "ERROR"; break;
    }
}
