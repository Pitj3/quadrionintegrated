#include "render/effectparser/GLSLInterpreter.h"

#include <fstream>
#include <unordered_map>
#include "render/effectparser/EffectParser.h"
#include "render/Pass.h"
#include "render/Effect.h"
#include "render/effectparser/EffectKeywords.h"
#include "render/Image.h"
#include "render/QGfx.h"
#include "Extensions.h"
#include "StringUtils.h"
#include "assets/AssetManager.h"
#include "assets/TextureAsset.h"
#include "assets/EffectAsset.h"


static bool isSamplerOption(const std::string& value)
{
    if(value == EFFECT_MINFILTER || value == EFFECT_MAGFILTER || value == EFFECT_WRAPS || 
		value == EFFECT_WRAPT || value == EFFECT_WRAPR || value == EFFECT_ANISOTROPY)
    {
		return true;
    }

	return false;
}

static bool isRasterizerOption(const std::string& value)
{
    if(value == EFFECT_POLYGONMODE || value == EFFECT_DEPTHTEST || value == EFFECT_DEPTHWRITE || 
		value == EFFECT_CULLENABLE || value == EFFECT_CULLFACE || value == EFFECT_FRONTFACE || 
		value == EFFECT_COLORWRITE || value == EFFECT_BLENDFUNC || value == EFFECT_BLENDENABLED || 
		value == EFFECT_STENCILTEST || value == EFFECT_STENCILOP || value == EFFECT_STENCILFUNC ||
		value == EFFECT_DEPTHFUNC)
    {
		return true;
    }

	return false;
}

std::shared_ptr<Effect> GLSLInterpreter::construct(IParseNode* root, const std::string& aEffectSource, const std::string& aEffectName)
{
	_name = root->name;
	_effectSourceCopy = aEffectSource;


    for(auto child : root->children)
    {
		_interpretNode(child);
    }

	std::shared_ptr<Effect> effect = nullptr;

	const std::shared_ptr<EffectAsset> query = AssetManager::instance().get<EffectAsset>(aEffectName);
	if (query != nullptr)
	{
		effect = query->getEffect();
		effect = _createShaders(_effectSourceCopy, effect);
	}
	else
	{
		effect = _createShaders(_effectSourceCopy, effect);
	}

	return effect;
}

void GLSLInterpreter::_interpretNode(IParseNode* node)
{
	// do stuff with node
	switch (node->type)
	{
		case ParseNodeType::ROOT: _effectSourceCopy = static_cast<IParseNode*>(node)->sourceCopy; break;
	    case ParseNodeType::VERSION: _version = static_cast<ParseEffectVersion*>(node)->version; break;
	    case ParseNodeType::EXTENSION: _extensions.push_back(static_cast<ParseEffectExtension*>(node)->extension); break;
	    case ParseNodeType::VARIABLE: _variables.push_back(static_cast<ParseVariable*>(node)->variable); break;
	    case ParseNodeType::MAINFUNCTION: _mainFunctions.push_back(static_cast<ParseMainFunction*>(node)->function); break;
	    case ParseNodeType::FUNCTION: _functions.push_back(static_cast<ParseFunction*>(node)->function); break;
	    case ParseNodeType::EINTERFACE: _interfaces.push_back(static_cast<ParseInterface*>(node)->effectInterface); break;
	    case ParseNodeType::UNIFORM: _uniforms.push_back(static_cast<ParseUniform*>(node)->uniform); break;
	    case ParseNodeType::UNIFORMBLOCK: _uniformBlocks.push_back(static_cast<ParseUniformBlock*>(node)->block); break;
	    case ParseNodeType::BUFFERBLOCK: _bufferBlocks.push_back(static_cast<ParseBufferBlock*>(node)->block); break;
	    case ParseNodeType::LAYOUT: _layouts.push_back(static_cast<ParseLayout*>(node)->layout); break;
		case ParseNodeType::TECHNIQUE: _techniques.push_back(static_cast<ParseTechnique*>(node)->technique); break;
		case ParseNodeType::INCLUDE: _includes.push_back(static_cast<ParseEffectInclude*>(node)->include); break;
		case ParseNodeType::OUTPUTBUFFER: _outputbufferDescs.push_back(static_cast<ParseOutputBuffer*>(node)->buffer); break;
		case ParseNodeType::PASS: _passes[static_cast<ParseTechnique*>(node->parent)->technique].push_back(static_cast<ParsePass*>(node)->pass); break;
	    default: break;
	}

	for (auto child : node->children)
	{
		_interpretNode(child);
	}
}

OutputBufferInfo* getOutputBufferInfo(const std::string& aBuffer, uint32_t aColorBuffers, uint32_t aWidth, uint32_t aHeight, const std::string& aColorFormat,
	const std::string& aDepthFormat, const std::vector<std::string>& aFlags)
{
	OutputBufferInfo* mOutputBufferInfo = new OutputBufferInfo();

	mOutputBufferInfo->colorBuffers = aColorBuffers;
	mOutputBufferInfo->name = aBuffer;
	mOutputBufferInfo->width = aWidth;
	mOutputBufferInfo->height = aHeight;
	mOutputBufferInfo->colorFormat = mOutputBufferInfo->depthFormat = QTEXTURE_FORMAT_NONE;

	if (aColorFormat == "RGB8")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGB8;
	}
	if (aColorFormat == "RGBA8")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGBA8;
	}
	if (aColorFormat == "RGB16")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGB16;
	}
	if (aColorFormat == "RGBA16")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGB16;
	}
	if (aColorFormat == "RGB16F")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGB16F;
	}
	if (aColorFormat == "RGBA16F")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGBA16F;
	}
	if (aColorFormat == "RGB32F")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGB32F;
	}
	if (aColorFormat == "RGBA32F")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RGBA32F;
	}
	if (aColorFormat == "R8")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_R8;
	}
	if (aColorFormat == "R16")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_R16;
	}
	if (aColorFormat == "R32F")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_R32F;
	}
	if (aColorFormat == "RG8")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RG8;
	}
	if (aColorFormat == "RG16F")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_RG16F;
	}

	if (aColorFormat == "I32F")
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_I32F;

	if (aColorFormat == "I16F")
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_I16F;

	if (aColorFormat == "BGRA")
	{
		mOutputBufferInfo->colorFormat = QTEXTURE_FORMAT_BGRA;
	}

	if (aDepthFormat == "D16")
	{
		mOutputBufferInfo->depthFormat = QTEXTURE_FORMAT_DEPTH16;
	}
	if (aDepthFormat == "D24S8")
	{
		mOutputBufferInfo->depthFormat = QTEXTURE_FORMAT_DEPTH24;
	}

	mOutputBufferInfo->flags = 0;

	for (const auto& flag : aFlags)
	{
		if (flag == "Nearest")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_NEAREST;
			continue;
		}
		if (flag == "Linear")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_LINEAR;
			continue;
		}
		if (flag == "Bilinear")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_BILINEAR;
			continue;
		}
		if (flag == "Trilinear")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_TRILINEAR;
			continue;
		}
		if (flag == "BilinearAniso")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_BILINEAR_ANISO;
			continue;
		}
		if (flag == "TrilinearAniso")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_TRILINEAR_ANISO;
			continue;
		}

		if (flag == "ClampS")
		{
			mOutputBufferInfo->flags |= QTEXTURE_CLAMP_S;
			continue;
		}
		if (flag == "ClampT")
		{
			mOutputBufferInfo->flags |= QTEXTURE_CLAMP_T;
			continue;
		}
		if (flag == "ClampR")
		{
			mOutputBufferInfo->flags |= QTEXTURE_CLAMP_R;
			continue;
		}

		if (flag == "WrapS")
		{
			mOutputBufferInfo->flags |= QTEXTURE_WRAP_S;
			continue;
		}
		if (flag == "WrapT")
		{
			mOutputBufferInfo->flags |= QTEXTURE_WRAP_T;
			continue;
		}
		if (flag == "WrapR")
		{
			mOutputBufferInfo->flags |= QTEXTURE_WRAP_R;
			continue;
		}

		if (flag == "RepeatS")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_REPEAT_S;
			continue;
		}
		if (flag == "RepeatT")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_REPEAT_T;
			continue;
		}
		if (flag == "RepeatR")
		{
			mOutputBufferInfo->flags |= QTEXTURE_FILTER_REPEAT_R;
		}
	}

	return mOutputBufferInfo;
}

std::shared_ptr<Effect> GLSLInterpreter::_createShaders(const std::string& aSource, std::shared_ptr<Effect> aCurrentEffect)
{
	if(aCurrentEffect.get() == nullptr)
	{
		aCurrentEffect = std::make_shared<Effect>();
	}
	else
	{
		aCurrentEffect->reset();
	}

	for(auto buf : _outputbufferDescs)
	{
		OutputBufferInfo* info = getOutputBufferInfo(buf.name, buf.numBuffers, buf.width, buf.height, buf.pixelType, buf.depthStencilType, buf.flags);
		_outputbuffers.push_back(info);
	}

	aCurrentEffect->setOutputBuffers(_outputbuffers);

	aCurrentEffect->setSourceCopy(aSource);
	aCurrentEffect->setName(_name);
    for(const auto& tech : _techniques)
    {
		Technique* t = new Technique(tech.name.c_str(), aCurrentEffect.get());
		aCurrentEffect->addTechnique(t);
    }

	std::string vertexShaderSource = "";
	std::string teShaderSource = "";
	std::string tcShaderSource = "";
	std::string geomShaderSource = "";
	std::string fragmentShaderSource = "";


	std::string header = "";

	header += "#version ";
	header += _version.version;
	header += " ";
	header += _version.profile;
	header += "\n";

	for (const auto& extension : _extensions)
	{
		header += "#extension ";
		header += extension.name;
		header += " : ";
		header += extension.mode;
		header += "\n";
	}
	header += "\n";

    for(const auto& inc : _includes)
    {
		std::string incPath = "Media/effects/" + std::string(inc.path.c_str());
	    std::ifstream stream(incPath.c_str());
	    std::string str((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	    stream.close();

		header += str.c_str();
		header += "\n";
    }

	_getUniforms("");
	_getUniformBlocks("");

	for (const auto& uni : mUniformDebugElements)
	{
		aCurrentEffect->addUniformName(uni.first);
		aCurrentEffect->setUniformGUIDebugElementType(uni.first, uni.second);
		aCurrentEffect->setUniformGUIDebugElementData(uni.first, mUniformDebugData[uni.first]);

		if(uni.second != "")
		{
			aCurrentEffect->addGUIUniformName(uni.first);
		}

		auto iter = mGUIDefaults.find(uni.first);
		if(iter != mGUIDefaults.end())
		{
			aCurrentEffect->setUniformElementDefaultValue(uni.first, iter->second);
		}
	}
    // Vertex shader
	for(const auto& techs : _passes)
	{
		for(const auto& pass : techs.second)
		{
			clearColor.set(0, 0, 0, 1);
			clearMode = 0;

			vertexShaderSource += header;

			vertexShaderSource += _getLayoutString("vertex");

			EffectMainFunction vertFunction = _getMainFunction(pass.vertexFunction);
		
			std::string interfaceIn = _getInterfaceString(vertFunction.parameters[0].name, "in", true);
			std::string interfaceOut = _getInterfaceString(vertFunction.parameters[1].name, "out", false, false, true);

			vertexShaderSource += interfaceIn;
			vertexShaderSource += "\n";
			vertexShaderSource += interfaceOut;
			vertexShaderSource += "\n";

			vertexShaderSource += _getVariables("vertex");
			vertexShaderSource += "\n";

			vertexShaderSource += _getUniforms("vertex");
			vertexShaderSource += "\n";

			vertexShaderSource += _getUniformBlocks("vertex");
			vertexShaderSource += "\n";

			// TODO (Roderick, Nick): BufferBlocks

			vertexShaderSource += _getFunctions("vertex");
			vertexShaderSource += "\n";
			vertexShaderSource += _getMainFunctionString("vertex", pass.vertexFunction);
			vertexShaderSource += "\n";

			// TC shader
			if (pass.tcsFunction != "")
			{
				tcShaderSource += header;

				tcShaderSource += _getLayoutString("tcs");

				EffectMainFunction tcFunction = _getMainFunction(pass.tcsFunction);

				interfaceIn = _getInterfaceString(tcFunction.parameters[0].name, "in", false, true);
				interfaceOut = _getInterfaceString(tcFunction.parameters[1].name, "out", false, true);

				tcShaderSource += interfaceIn;
				tcShaderSource += "\n";
				tcShaderSource += interfaceOut;
				tcShaderSource += "\n";

				tcShaderSource += _getVariables("tcs");
				tcShaderSource += "\n";

				tcShaderSource += _getUniforms("tcs");
				tcShaderSource += "\n";

				tcShaderSource += _getUniformBlocks("tcs");
				tcShaderSource += "\n";

				// TODO (Roderick, Nick): BufferBlocks

				tcShaderSource += _getFunctions("tcs");
				tcShaderSource += "\n";
				tcShaderSource += _getMainFunctionString("tcs", pass.tcsFunction);
				tcShaderSource += "\n";
			}

			// TE shader
			if (pass.tesFunction != "")
			{
				teShaderSource += header;

				teShaderSource += _getLayoutString("tes");

				EffectMainFunction teFunction = _getMainFunction(pass.tesFunction);

				interfaceIn = _getInterfaceString(teFunction.parameters[0].name, "in", false, true);
				interfaceOut = _getInterfaceString(teFunction.parameters[1].name, "out", false, false, true);

				teShaderSource += interfaceIn;
				teShaderSource += "\n";
				teShaderSource += interfaceOut;
				teShaderSource += "\n";

				teShaderSource += _getVariables("tes");
				teShaderSource += "\n";

				teShaderSource += _getUniforms("tes");
				teShaderSource += "\n";

				teShaderSource += _getUniformBlocks("tes");
				teShaderSource += "\n";

				// TODO (Roderick, Nick): BufferBlocks

				teShaderSource += _getFunctions("tes");
				teShaderSource += "\n";
				teShaderSource += _getMainFunctionString("tes", pass.tesFunction);
				teShaderSource += "\n";
			}
	 
			// Geom shader
			if (pass.geometryFunction != "")
			{
				geomShaderSource += header;

				geomShaderSource += _getLayoutString("geometry");

				EffectMainFunction geomFunction = _getMainFunction(pass.geometryFunction);

				interfaceIn = _getInterfaceString(geomFunction.parameters[0].name, "in", true);
				interfaceOut = _getInterfaceString(geomFunction.parameters[1].name, "out");

				geomShaderSource += interfaceIn;
				geomShaderSource += "\n";
				geomShaderSource += interfaceOut;
				geomShaderSource += "\n";

				geomShaderSource += _getVariables("geometry");
				geomShaderSource += "\n";

				geomShaderSource += _getUniforms("geometry");
				geomShaderSource += "\n";

				geomShaderSource += _getUniformBlocks("geometry");
				geomShaderSource += "\n";

				// TODO (Roderick, Nick): BufferBlocks

				geomShaderSource += _getFunctions("geometry");
				geomShaderSource += "\n";
				geomShaderSource += _getMainFunctionString("geometry", pass.geometryFunction);
				geomShaderSource += "\n";
			}

			if (pass.fragmentFunction != "")
			{
				// Fragment shader
				fragmentShaderSource += header;

				fragmentShaderSource += _getLayoutString("fragment");

				EffectMainFunction fragFunction = _getMainFunction(pass.fragmentFunction);

				interfaceIn = _getInterfaceString(fragFunction.parameters[0].name, "in", true);
				interfaceOut = _getInterfaceString(fragFunction.parameters[1].name, "out");

				fragmentShaderSource += interfaceIn;
				fragmentShaderSource += "\n";
				fragmentShaderSource += interfaceOut;
				fragmentShaderSource += "\n";

				fragmentShaderSource += _getVariables("fragment");
				fragmentShaderSource += "\n";

				fragmentShaderSource += _getUniforms("fragment");
				fragmentShaderSource += "\n";

				fragmentShaderSource += _getUniformBlocks("fragment");
				fragmentShaderSource += "\n";

				// TODO (Roderick, Nick): BufferBlocks

				fragmentShaderSource += _getFunctions("fragment");
				fragmentShaderSource += "\n";
				fragmentShaderSource += _getMainFunctionString("fragment", pass.fragmentFunction);
				fragmentShaderSource += "\n";
			}

			Pass* p = new Pass(pass.name.c_str(), aCurrentEffect->getTechnique(techs.first.name.c_str()));
			p->_setVertexSource(vertexShaderSource.c_str());
			p->_setTCSSource(tcShaderSource.c_str());
			p->_setTESSource(teShaderSource.c_str());
			p->_setGeometrySource(geomShaderSource.c_str());
			p->_setFragmentSource(fragmentShaderSource.c_str());

			aCurrentEffect->getTechnique(techs.first.name.c_str())->addPass(p);

			p->compile();

			vertexShaderSource = tcShaderSource = teShaderSource = geomShaderSource = fragmentShaderSource = "";

			RasterizerState* rasterizerState = nullptr;
			std::unordered_map<std::string, SamplerObject*> samplerObjects;

			for (const auto& option : pass.other)
			{
				for(const auto& samplerName : samplerNames)
				{
					if(samplerName == option.first)
					{
						// this is for texture loading
						samplerLoadPaths[samplerName] = {EffectVariableType::SAMPLER2D, option.second[0]};
					}

					for (const auto& uni : _uniforms)
					{
						if (uni.uniformName == samplerName)
						{
							if(samplerLoadPaths.find(samplerName) == samplerLoadPaths.end())
							{
								samplerLoadPaths[samplerName] = {uni.type, ""};
							}
						}
					}
				}

				if(option.first == "OutputBuffer")
				{
					OutputBufferInfo* buf;
					for (const auto& outputbufferDesc : _outputbuffers)
					{
						if(outputbufferDesc->name == option.second[0])
						{
							buf = outputbufferDesc;
						}
					}

					if(option.second[0] == "DefaultSurface" || _outputbuffers.size() == 0)
					{
						buf = new OutputBufferInfo();
						buf->name = "DefaultSurface";
					}

					p->setOutputBuffer(buf);
				}

				if(option.first == "Clear")
				{
					for(const auto& clearModes : option.second)
					{
						clearMode |= StringUtils::parseToGLEnum(clearModes.c_str());
					}
				}

				if(option.first == "ClearColor")
				{
					clearColor.set(std::stof(option.second[0].c_str()), std::stof(option.second[1].c_str()), std::stof(option.second[2].c_str()), std::stof(option.second[3].c_str()));
				}

				if (isSamplerOption(option.first))
				{
					if(samplerObjects.find(option.second[0]) == samplerObjects.end())
					{
						SamplerObject* obj = new SamplerObject();
						uint32_t id = 0;
						glGenSamplers(1, &id);
						obj->samplerID = id;
						obj->name = option.second[0].c_str();
						samplerObjects[option.second[0]] = obj;
					}

					SamplerObject* obj = samplerObjects[option.second[0]];

					if(option.first == EFFECT_MINFILTER)
					{
						obj->minFilter = option.second[1].c_str();
					}
					if(option.first == EFFECT_MAGFILTER)
					{
						obj->magFilter = option.second[1].c_str();
					}
					if(option.first == EFFECT_WRAPS)
					{
						obj->wrapS = option.second[1].c_str();
					}
					if (option.first == EFFECT_WRAPT)
					{
						obj->wrapT = option.second[1].c_str();
					}
					if (option.first == EFFECT_WRAPR)
					{
						obj->wrapR = option.second[1].c_str();
					}
					if(option.first == EFFECT_ANISOTROPY)
					{
						obj->anisotropy = std::stof(option.second[1].c_str());
					}
				}
				else if (isRasterizerOption(option.first))
				{
					if(rasterizerState == nullptr)
					{
						rasterizerState = new RasterizerState();
					}

					if(option.first == EFFECT_POLYGONMODE)
					{
						rasterizerState->polygon = option.second[0].c_str();
						rasterizerState->polygonMode = option.second[1].c_str();
					}
					if(option.first == EFFECT_DEPTHTEST)
					{
						rasterizerState->depthTest = option.second[0].c_str();
					}
					if(option.first == EFFECT_DEPTHWRITE)
					{
						rasterizerState->depthWrite = option.second[0].c_str();
					}
					if(option.first == EFFECT_DEPTHFUNC)
					{
						rasterizerState->depthFunc = option.second[0].c_str();
					}
					if(option.first == EFFECT_CULLENABLE)
					{
						rasterizerState->cullEnable = option.second[0].c_str();
					}
					if(option.first == EFFECT_CULLFACE)
					{
						rasterizerState->cullFace = option.second[0].c_str();
					}
					if(option.first == EFFECT_FRONTFACE)
					{
						rasterizerState->frontFace = option.second[0].c_str();
					}
					if(option.first == EFFECT_COLORWRITE)
					{
						rasterizerState->colorR = option.second[0].c_str();
						rasterizerState->colorG = option.second[1].c_str();
						rasterizerState->colorB = option.second[2].c_str();
						rasterizerState->colorA = option.second[3].c_str();
					}
					if(option.first == EFFECT_BLENDFUNC)
					{
						rasterizerState->alphaSrc = option.second[0].c_str();
						rasterizerState->alphaDst = option.second[1].c_str();
					}
					if(option.first == EFFECT_BLENDENABLED)
					{
						rasterizerState->alphaBlend = option.second[0].c_str();
					}
					if(option.first == EFFECT_STENCILTEST)
					{
						rasterizerState->stencilTest = option.second[0].c_str();
					}
					if(option.first == EFFECT_STENCILOP)
					{
						rasterizerState->stencilsFail = option.second[0].c_str();
						rasterizerState->stencildpFail = option.second[1].c_str();
						rasterizerState->stencilPass = option.second[2].c_str();
					}
					if(option.first == EFFECT_STENCILFUNC)
					{
						rasterizerState->stencilFunc = option.second[0].c_str();
						rasterizerState->stencilRef = atoi(option.second[1].c_str());
						rasterizerState->stencilMask = atoi(option.second[2].c_str());
					}
				}
			}

			if(rasterizerState != nullptr)
			{
				p->setRasterizerState(rasterizerState);
			}

			for(const auto& sampler : samplerObjects)
			{
				if (sampler.second->minFilter != "NONE")
					glSamplerParameteri(sampler.second->samplerID, GL_TEXTURE_MIN_FILTER, StringUtils::parseToGLEnum(sampler.second->minFilter));

				if (sampler.second->magFilter != "NONE")
					glSamplerParameteri(sampler.second->samplerID, GL_TEXTURE_MAG_FILTER, StringUtils::parseToGLEnum(sampler.second->magFilter));

				if (sampler.second->wrapS != "NONE")
					glSamplerParameteri(sampler.second->samplerID, GL_TEXTURE_WRAP_S, StringUtils::parseToGLEnum(sampler.second->wrapS));

				if (sampler.second->wrapR != "NONE")
					glSamplerParameteri(sampler.second->samplerID, GL_TEXTURE_WRAP_R, StringUtils::parseToGLEnum(sampler.second->wrapR));

				if (sampler.second->wrapT != "NONE")
					glSamplerParameteri(sampler.second->samplerID, GL_TEXTURE_WRAP_T, StringUtils::parseToGLEnum(sampler.second->wrapT));

				glSamplerParameterf(sampler.second->samplerID, GL_TEXTURE_MAX_ANISOTROPY_EXT, sampler.second->anisotropy);
			
				p->addSamplerState(sampler.second);
			}

			p->setSamplerLoadPaths(samplerLoadPaths);

			p->_setClearColor(clearColor);
			p->_setClearMode(clearMode);

			for (auto samplerPath : samplerLoadPaths)
			{
//				if(samplerPath.second.first == EffectVariableType::SAMPLER2D)
//					AssetManager::instance().load<TextureAsset>(samplerPath.first, ASSET_LOAD_MED_PRIO, samplerPath.second.second);


//				Image* imgPtr = new Image();
//				imgPtr->setTexFilter(QTEXTURE_FILTER_TRILINEAR_ANISO | QTEXTURE_WRAP);
//				bool succeed = imgPtr->loadTexture(samplerPath.second.c_str(), false, "", false);

//				if(succeed)
//				{
//					AssetManager::instance().load
//					QGfx::addTexture(imgPtr->getFileName(), imgPtr->getOpenGlid());
//				}
//				delete imgPtr;
			}
		}
	}

	return aCurrentEffect;
}



std::string GLSLInterpreter::_getLayoutString(const std::string& shader)
{
	std::string layoutString = "";

	for (const auto& layout : _layouts)
	{
		if (layout.shader == shader || layout.shader == "")
		{
			layoutString += "layout(";
			size_t num = 0;
			for (const auto& layoutOperand : layout.operands)
			{
				layoutString += layoutOperand.name;
				if (layoutOperand.defaultValue != "")
				{
					layoutString += " = ";
					layoutString += layoutOperand.defaultValue;
				}

				if (num < layout.operands.size() - 1)
				{
					// more operands coming, add comma
					layoutString += ", ";
				}

				num++;
			}
			layoutString += ") ";
			layoutString += getEffectInterfaceSpecifierString(layout.specifier);
			layoutString += ";\n\n";
		}
	}

	return layoutString;
}

std::string GLSLInterpreter::_getInterfaceString(const std::string& interfaceName,
    const std::string& interfaceSpecifier, const bool includeLocation, const bool includeArray, const bool stripArray)
{
	std::string interfaceString = "";
	for (const auto& inter : _interfaces)
	{
		if (inter.name == interfaceName)
		{
			for (const auto& variable : inter.variables)
			{
				if (variable.location != EffectSemantic::UNKNOWN && includeLocation)
				{
					interfaceString += "layout(location = ";
					interfaceString += std::to_string(static_cast<uint32_t>(variable.location)).c_str();
					interfaceString += ") ";
				}

				for (const auto& qualifier : variable.typeQualifiers)
				{
					interfaceString += getEffectTypeQualifierString(qualifier);
					interfaceString += " ";
				}

				interfaceString += interfaceSpecifier;
				interfaceString += " ";
				interfaceString += getEffectVariableTypeString(variable.type);
				interfaceString += " ";
				std::string varName = variable.variableName;
                if(stripArray)
                {
                    if(varName.find('[') != std::string::npos)
                    {
						size_t loc = varName.find_first_of('[');
						varName = varName.substr(0, loc).c_str();
                    }
                }
				interfaceString += varName;
                if(includeArray)
                {
					interfaceString += "[]";
                }
				interfaceString += ";\n";
			}

			break;
		}
	}

	return interfaceString;
}

EffectMainFunction GLSLInterpreter::_getMainFunction(const std::string& name)
{
    for(const auto& function : _mainFunctions)
    {
		if (function.name == name)
		{
			return function;
		}
    }

	assert("Could not find main function!");
	return {};
}

std::string GLSLInterpreter::_getVariables(const std::string& shader)
{
	std::string variableString = "";
    for(const auto& variable : _variables)
    {
        if(variable.shader == shader || variable.shader == "")
        {
			for (const auto& qualifier : variable.typeQualifiers)
			{
				variableString += getEffectTypeQualifierString(qualifier);
				variableString += " ";
			}

			variableString += getEffectVariableTypeString(variable.type);
			variableString += " ";
			variableString += variable.variableName;

			if (variable.defaultValue != "")
			{
				variableString += " = ";
				variableString += variable.defaultValue;
			}

			variableString += ";\n";
        }
    }

	return variableString;
}

std::string GLSLInterpreter::_getUniforms(const std::string& shader)
{
	std::string uniformString = "";
	for (const auto& uniform : _uniforms)
	{
		if (uniform.shader == shader || uniform.shader == "")
		{
			uniformString += "uniform ";
			uniformString += getEffectVariableTypeString(uniform.type);
            if(getEffectVariableTypeString(uniform.type).find("sampler") != std::string::npos)
            {
                // this is a sampler
				bool found = false;
				for (const auto& name : samplerNames)
				{
					if(name == uniform.uniformName)
					{
						found = true;
						break;
					}
				}
				if(!found)
					samplerNames.push_back(uniform.uniformName);
            }

			uniformString += " ";
			uniformString += uniform.uniformName;

			if (uniform.defaultValue != "")
			{
				uniformString += " = ";
				uniformString += uniform.defaultValue;

			}
			if (uniform.guiDefaultValue != "")
			{
				mGUIDefaults[uniform.uniformName] = static_cast<float>(atof(uniform.guiDefaultValue.c_str()));
			}

			uniformString += ";\n";

			mUniformDebugElements[uniform.uniformName] = uniform.guiDebugElement;
			mUniformDebugData[uniform.uniformName] = std::make_pair(uniform.min, uniform.max);
		}
	}

	return uniformString;
}

std::string GLSLInterpreter::_getUniformBlocks(const std::string& shader)
{
	std::string uniformBlockString = "";

    for(const auto& block : _uniformBlocks)
    {
        if(block.shader == "shader" || block.shader == "")
        {
			uniformBlockString += "uniform ";
			uniformBlockString += block.blockName;
			uniformBlockString += "\n{\n";

            for(const auto& uniform : block.uniforms)
            {
				uniformBlockString += "\t";
				uniformBlockString += getEffectVariableTypeString(uniform.type);
                uniformBlockString += " ";
				uniformBlockString += uniform.uniformName;
				uniformBlockString += ";\n";

				mUniformDebugElements[uniform.uniformName] = uniform.guiDebugElement;
				mUniformDebugData[uniform.uniformName] = std::make_pair(uniform.min, uniform.max);

				if(uniform.guiDefaultValue != "")
				{
					mGUIDefaults[uniform.uniformName] = static_cast<float>(atof(uniform.guiDefaultValue.c_str()));
				}
            }
			uniformBlockString += "};\n";
        }
    }

	return uniformBlockString;
}

static std::string getNumTabs(const size_t num)
{
	std::string tabs = "";
    for(size_t i = 0; i < num; i++)
    {
		tabs += "\t";
    }

	return tabs;
}

std::string GLSLInterpreter::_getFunctions(const std::string& shader)
{
	std::string functionString;

    for(auto& function : _functions)
    {
        if (function.shaders.empty() || std::find(function.shaders.begin(), function.shaders.end(), shader) != function.shaders.end())
        {
			for (const auto& qualifier : function.typeQualifiers)
			{
				functionString += getEffectTypeQualifierString(qualifier);
				functionString += " ";
			}

			functionString += getEffectVariableTypeString(function.type);
			functionString += " ";
			functionString += function.name;
			functionString += "(";

			size_t num = 0;
            for(const auto& parameter : function.parameters)
            {
				for (const auto& qualifier : parameter.typeQualifiers)
				{
					functionString += getEffectTypeQualifierString(qualifier);
					functionString += " ";
				}

				functionString += getEffectVariableTypeString(parameter.type);
				functionString += " ";
				functionString += parameter.variableName;

				if (num < function.parameters.size() - 1)
				{
					functionString += " ";
				}

				num++;
            }

			functionString += ")";

			functionString += "\n{\n";
			std::vector<std::string> blockLines = splitOnAny(function.block, "\n\t");
			size_t numTabs = 1;
            for(const auto& line : blockLines)
            {
				if (line == "}")
					numTabs--;

				functionString += getNumTabs(numTabs);
				functionString += line;
				functionString += "\n";

				if (line == "{")
					numTabs++;
            }
			functionString += "}\n\n";
        }
    }

	return functionString;
}

std::string GLSLInterpreter::_getMainFunctionString(const std::string& shader, const std::string& name)
{
	std::string functionString;

	for (auto& function : _mainFunctions)
	{
		if ((function.shader == shader || function.shader == "") && function.name == name)
		{
			functionString += getEffectVariableTypeString(function.type);
			functionString += " main";
			functionString += "()";
			functionString += "\n{\n";
			std::vector<std::string> blockLines = splitOnAny(function.block, "\n");
			size_t numTabs = 1;
			for (const auto& line : blockLines)
			{
				if (line == "}")
					numTabs--;

				functionString += getNumTabs(numTabs);
				functionString += line;
				functionString += "\n";

				if (line == "{")
					numTabs++;
			}
			functionString += "}\n\n";
		}
	}

	return functionString;
}
