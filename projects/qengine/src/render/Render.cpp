#include "render/Render.h"
#include "render/Buffer.h"
#include "QMath.h"
#include "FreeImage.h"
#include "render/MatrixStack.h"
#include "render/QGfx.h"
#include "render/VertexArray.h"
#include "render/VertexBuffer.h"
#include "render/VertexBufferLayout.h"
#include "render/IndexBuffer.h"


class RenderImpl
{
	public:
		
		VertexArray* fullscreenVAO;
		VertexBuffer* fullscreenVBO;
		IndexBuffer* fullscreenIBO;
		std::shared_ptr<RenderObject> fullscreenRO;


		Shader* texturedEffect;
		Shader* coloredEffect;
		Shader* texturedOITEffect;

		RenderHandle fullscreenTexturedQuadHandle;
		RenderHandle fullscreenQuadHandle;

		MatrixStack scratchMatrixStack;

		TextureFilter preferredTexFilter;
/*
		void renderNode(GeometryBufferObject* aModel,
						GeometryNode* aNode,
						Shader* aShader,
						const uint32_t aNInstances)
		{
			if (aNode)
			{
				scratchMatrixStack.pushMatrix(scratchMatrixStack.top());
				scratchMatrixStack.multiplyMatrix(aNode->transformation);

				const mat4<float> m = QGfx::getMatrix(MODEL);

				QGfx::setMatrix(MODEL, scratchMatrixStack.top().m);

				aShader->bindStateMatrix("M", MatrixMode(MODEL));
				aShader->bindStateMatrix("MV", MatrixMode(MODEL | VIEW));
				aShader->bindStateMatrix("MVP", MatrixMode(PROJECTION | VIEW | MODEL));

				for (unsigned int i = 0; i < aNode->numMeshes; ++i)
				{
					const unsigned int meshIndex = aNode->meshes[i];

					const GeometryMesh mesh = aModel->meshes[meshIndex];
					uint32_t texId = 0;

					bindBuffer(mesh.geometryBufferId);
					if (aNInstances <= 1)
						Render::renderIndexedList(TRIANGLES, 
												  mesh.numIndices, 
												  UNSIGNED_INT, 
												  0);
					else
						glDrawElementsInstanced(GL_TRIANGLES, 
												mesh.numIndices, 
												GL_UNSIGNED_INT, 
												0,
												aNInstances);
					bindDefaultBuffer();
				}

				for (unsigned int i = 0; i < aNode->numChildren; ++i)
				{
					renderNode(aModel, &aNode->nodes[i], aShader, aNInstances);
				}

				scratchMatrixStack.popMatrix();

				QGfx::setMatrix(MODEL, m);
			}
		}
*/
};

RenderImpl* Render::mImpl;

Render::Render()
{
	if(!mImpl)
		mImpl = new RenderImpl;
}

Render::~Render()
{
	if (mImpl->texturedEffect)
	{
		delete mImpl->texturedEffect;
		mImpl->texturedEffect = nullptr;
	}

	if (mImpl->coloredEffect)
	{
		delete mImpl->coloredEffect;
		mImpl->coloredEffect = nullptr;
	}

	if (mImpl->fullscreenTexturedQuadHandle.handle > 0)
	{
		deleteBuffer(mImpl->fullscreenTexturedQuadHandle.handle);
		mImpl->fullscreenTexturedQuadHandle.handle = 0;
	}

	if (mImpl->fullscreenQuadHandle.handle > 0)
	{
		deleteBuffer(mImpl->fullscreenQuadHandle.handle);
		mImpl->fullscreenQuadHandle.handle = 0;
	}

	if (mImpl->texturedOITEffect)
	{
		delete mImpl->texturedOITEffect;
		mImpl->texturedOITEffect = nullptr;
	}
}

void Render::initialize()
{
	if(!mImpl)
		mImpl = new RenderImpl;

//	ColorOutput out;
//	out.outputLocation = 0;
//	out.outputSemantic = "oColor";

//	ColorOutput OITout[2];
//	OITout[0].outputLocation = 0;
//	OITout[0].outputSemantic = "oColor0";
//	OITout[1].outputLocation = 1;
//	OITout[1].outputSemantic = "oColor1";
	

//	AttribLoc attribTextured[2] = { { LOC_POSITION, "iPosition" },
//									{ LOC_TEXCOORD0, "iTexCoords" }
//								  };

//	AttribLoc attribColored[2] = { { LOC_POSITION, "iPosition" },
//								   { LOC_COLOR, "iColor" }
//								 };

//	mImpl->texturedEffect = new Shader;
//	mImpl->texturedEffect->create("Media/Effects/simple_textured.vert",
//								  "Media/Effects/simple_textured.frag",
//								  nullptr,
//								  &out,
//								  1,
//								  attribTextured,
//								  2, nullptr, 0);

//	mImpl->texturedOITEffect = new Shader;
//	mImpl->texturedOITEffect->create("Media/Effects/textured_oit.vert",
//									 "Media/Effects/oit_blend.frag",
//									 nullptr, 
//									 OITout,
//									 2,
//									 attribTextured,
//									 2, nullptr, 0);

//	mImpl->coloredEffect = new Shader;
//	mImpl->coloredEffect->create("Media/Effects/simple_colored.vert",
//								 "Media/Effects/simple_colored.frag",
//								 nullptr,
//								 &out,
//								 1,
//								 attribColored,
//								 2, nullptr, 0);

	const float vertsTextured[20] = { -1.0f, -1.0f, 1.0f, 0.0f, 0.0f,
									  1.0f, -1.0f, 1.0f, 1.0f, 0.0f,
									  1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
									  -1.0f, 1.0f, 1.0f, 0.0f, 1.0f };

	const uint32_t indices[6] = { 0, 1, 2, 0, 2, 3 };

	VertexAttribute vattrib[2] = { { LOC_POSITION, 3, GL_FLOAT, GL_FALSE, 20, 0 },
								   { LOC_TEXCOORD0, 2, GL_FLOAT, GL_TRUE, 20, 12 }
								 };

	mImpl->fullscreenTexturedQuadHandle.handle = createIndexedInterleavedArray(vattrib,
																		2,
																		vertsTextured,
																		sizeof(float) * 20,
																		BUFFER_STATIC,
																		indices,
																		6);

	float verts[12] = { -1.0f, -1.0f, 1.0f,
						1.0f, -1.0f, 1.0f,
						1.0f, 1.0f, 1.0f,
						-1.0f, 1.0f, 1.0f };

	VertexAttribute vAttrib = { LOC_POSITION, 3, GL_FLOAT, GL_FALSE, 12, 0 };
//	G_FULLSCREEN_QUAD = createIndexedInterleavedArray(&vAttrib, 1, verts, sizeof(float) * 12, BUFFER_STATIC, indices, 6);
	mImpl->fullscreenQuadHandle.handle = createIndexedInterleavedArray(&vAttrib,
																1,
																verts,
																sizeof(float) * 12,
																BUFFER_STATIC,
																indices,
																6);


	// FULLSCREEN VAO
	VertexBufferLayout va;
	va.push<vec3<float>>("position");
	va.push<vec2<float>>("uv0");

	float fsverts[20] = { -1.0f, -1.0f, 1.0f, 0.0f, 0.0f,
					   1.0f, -1.0f, 1.0f, 1.0f, 0.0f,
					   1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
					   -1.0f, 1.0f, 1.0f, 0.0f, 1.0f };

	mImpl->fullscreenVBO = new VertexBuffer(fsverts, sizeof(float) * 20);
	mImpl->fullscreenVBO->setLayout(va);

	uint32_t fsindices[6] = { 0, 1, 2, 0, 2, 3 };
	mImpl->fullscreenIBO = new IndexBuffer(fsindices, 6, GL_TRIANGLES);

	mImpl->fullscreenVAO = new VertexArray(mImpl->fullscreenVBO, mImpl->fullscreenIBO);

	std::vector<std::shared_ptr<MeshNodeRod>> fullscreenMeshes;
	std::shared_ptr<MeshNodeRod> fullscreenMesh = std::make_shared<MeshNodeRod>();
	std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>(mImpl->fullscreenVAO, mImpl->fullscreenIBO);
	mat4<float> rot;
	rot.loadIdentity();
	fullscreenMesh->mesh = mesh;
	fullscreenMesh->position = vec3<float>(0.0f, 0.0f, 0.0f);
//	fullscreenMesh->rotation = vec3<float>(0.0f, 0.0f, 0.0f);
	fullscreenMesh->rotation = rot;
	fullscreenMesh->scale = vec3<float>(0.0f, 0.0f, 0.0f);	
	fullscreenMeshes.push_back(fullscreenMesh);

	uint32_t renderObjID = QGfx::createRenderObjectID();
	mImpl->fullscreenRO = std::make_shared<RenderObject>("FullscreenRO", fullscreenMeshes, renderObjID);
	mImpl->fullscreenRO->setGuiSelected(false);
}

void Render::setPreferredTextureFilter(TextureFilter aFilter)
{
	mImpl->preferredTexFilter = aFilter;
}

TextureFilter Render::getPreferredTextureFilter()
{
	return mImpl->preferredTexFilter;
}

const Shader* const Render::getTexturedEffect()
{
	return mImpl->texturedEffect;
}

const Shader* const Render::getTexturedOITEffect()
{
	return mImpl->texturedOITEffect;
}

vec3<float> Render::getWorldPosFromScreenspace(const int32_t aMouseX, 
											   const int32_t aMouseY)
{
	GLint vp[4];
	float winZ;
	vec4<float> in;

	const mat4<float> m = QGfx::getMatrix(MODEL);
	const mat4<float> v = QGfx::getMatrix(VIEW);
	const mat4<float> p = QGfx::getMatrix(PROJECTION);
	QGfx::GL::gl_GetIntegerv(GL_VIEWPORT, vp);

	const float winX = static_cast<float>(aMouseX);
	const float winY = static_cast<float>(vp[3]) - static_cast<float>(aMouseY);
	QGfx::GL::gl_ReadPixels(aMouseX, static_cast<int>(winY), 1, 1, 
							GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

	const mat4<float> mv = m * v;
	mat4<float> mvp = p * mv;
	mvp.invert();

	in.x = (winX - static_cast<float>(vp[0])) / static_cast<float>(vp[2]) * 2.0f - 1.0f;
	in.y = (winY - static_cast<float>(vp[1])) / static_cast<float>(vp[3]) * 2.0f - 1.0f;
	in.z = 2.0f * winZ - 1.0f;
	in.w = 1.0f;

	vec4<float> out = mvp * in;
	out.w = 1.0f / out.w;

	vec3<float> worldPos;
	worldPos.x = out.x * out.w;
	worldPos.y = out.y * out.w;
	worldPos.z = out.z * out.w;

	return worldPos;
}

/*
void Render::renderFullscreenTexturedQuad(const bool aInvertV, const uint16_t aTexId)
{
	if (aTexId <= 0)
		return;

	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	mat4<float> ID;
	ID.loadIdentity();
	QGfx::pushMatrix(MODEL, ID);
	QGfx::pushMatrix(VIEW, ID);
	QGfx::pushMatrix(PROJECTION, ID);

	const float color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	mImpl->texturedEffect->bind();
	mImpl->texturedEffect->bindTexture("s0", aTexId);
	mImpl->texturedEffect->bindBool("invert", aInvertV);
	mImpl->texturedEffect->bindFloat("globalColor", color, 4);
	mImpl->texturedEffect->bindStateMatrix("MVP", MatrixMode(MODEL | VIEW | PROJECTION));

	bindBuffer(mImpl->fullscreenTexturedQuadHandle.handle);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
	bindDefaultBuffer();

	mImpl->texturedEffect->evictTextures();
	mImpl->texturedEffect->unbind();

	glDisable(GL_BLEND);

	QGfx::popMatrix(MODEL);
	QGfx::popMatrix(VIEW);
	QGfx::popMatrix(PROJECTION);

	if (isDepthEnabled)
		QGfx::enableDepthTest();
}
*/


void Render::renderFullscreenTexturedQuad()
{
	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	glEnable(GL_TEXTURE_2D);

	bindBuffer(mImpl->fullscreenTexturedQuadHandle.handle);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
	bindDefaultBuffer();

	if (isDepthEnabled)
		QGfx::enableDepthTest();
}

void Render::renderFullscreenQuad(const TextureRect& aRect,
								  const uint32_t aW,
								  const uint32_t aH)
{
	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	const float fWidth5 = static_cast<float>(aW) - 0.5f;
	const float fHeight5 = static_cast<float>(aH) - 0.5f;

	const float verts[20] = { -1.0f, -1.0f, 1.0f, aRect.leftU, aRect.bottomV,
							  1.0f, -1.0f, 1.0f, aRect.rightU, aRect.bottomV,
							  1.0f, 1.0f, 1.0f, aRect.rightU, aRect.topV,
							  -1.0f, 1.0f, 1.0f, aRect.leftU, aRect.topV 
							};

	const uint32_t indices[6] = { 0, 1, 2, 0, 2, 3 };

	glEnableVertexAttribArray(LOC_POSITION);
	glEnableVertexAttribArray(LOC_TEXCOORD0);
	glVertexAttribPointer(LOC_POSITION, 3, GL_FLOAT, GL_FALSE, 20, verts);
	glVertexAttribPointer(LOC_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 20, 
						  const_cast<float*>(&verts[3]));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices);
	glDisableVertexAttribArray(LOC_POSITION);
	glDisableVertexAttribArray(LOC_TEXCOORD0);

	if (isDepthEnabled)
		QGfx::enableDepthTest();
}

void Render::renderFullscreenFrustumQuad(float aFOV,
										 float aFarClipDist,
										 float aAspectRatio)
{
	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	float farHalfHeight = aFarClipDist * tanf(aFOV * 0.5f);
	float farHalfWidth = farHalfHeight * aAspectRatio;

	const float verts[24] = { -1.0f, -1.0f, 1.0f, -farHalfWidth, -farHalfHeight, -aFarClipDist,
							  1.0f, -1.0f, 1.0f, farHalfWidth, -farHalfHeight, -aFarClipDist,
							  1.0f, 1.0f, 1.0f, farHalfWidth, farHalfHeight, -aFarClipDist,
							  -1.0f, 1.0f, 1.0f, -farHalfWidth, farHalfHeight , -aFarClipDist
							 };

	const uint32_t indices[6] = { 0, 1, 2, 0, 2, 3 };

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 24, verts);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 24, (float*)(&verts[3]));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	if(isDepthEnabled)
		QGfx::enableDepthTest();
}

/*
void Render::renderFullscreenQuad(const float aR,
								  const float aG,
								  const float aB,
								  const float aA)
{
	const float verts[28] = { -1.0f, -1.0f, 1.0f, aR, aG, aB, aA,
							   1.0f, -1.0f, 1.0f, aR, aG, aB, aA,
							   1.0f, 1.0f, 1.0f, aR, aG, aB, aA,
							   -1.0f, 1.0f, 1.0f, aR, aG, aB, aA };
	uint32_t indices[6] = { 0, 1, 2, 0, 2, 3 };

	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	mat4<float> id;
	id.loadIdentity();
	QGfx::pushMatrix(PROJECTION, id);
	QGfx::pushMatrix(VIEW, id);
	mImpl->coloredEffect->bind();
	mImpl->coloredEffect->bindStateMatrix("MVP", MatrixMode(MODEL | VIEW | PROJECTION));

	glEnableVertexAttribArray(LOC_POSITION);
	glEnableVertexAttribArray(LOC_COLOR);
	glVertexAttribPointer(LOC_POSITION, 3, GL_FLOAT, GL_FALSE, 28, verts);
	glVertexAttribPointer(LOC_COLOR, 4, GL_FLOAT, GL_TRUE, 28, const_cast<float*>(&verts[3]));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices);
	glDisableVertexAttribArray(LOC_POSITION);
	glDisableVertexAttribArray(LOC_COLOR);
	mImpl->coloredEffect->unbind();

	if (isDepthEnabled)
		QGfx::enableDepthTest();

	glDisable(GL_BLEND);
	QGfx::popMatrix(PROJECTION);
	QGfx::popMatrix(VIEW);
}
*/

void Render::renderFullscreenQuad()
{
	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	mat4<float> ID;
	ID.loadIdentity();
	QGfx::pushMatrix(MODEL, ID);
	QGfx::pushMatrix(VIEW, ID);
	QGfx::pushMatrix(PROJECTION, ID);

	const float verts[12] = { -1.0f, -1.0f, 1.0f,
							   1.0f, -1.0f, 1.0f,
							   1.0f, 1.0f, 1.0f,
							   -1.0f, 1.0f, 1.0f
							};

	const uint32_t indices[6] = { 0, 1, 2, 0, 2, 3 };
	glEnableVertexAttribArray(LOC_POSITION);
	glVertexAttribPointer(LOC_POSITION, 3, GL_FLOAT, GL_FALSE, 0, verts);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices);
	glDisableVertexAttribArray(LOC_POSITION);

	QGfx::popMatrix(PROJECTION);
	QGfx::popMatrix(VIEW);
	QGfx::popMatrix(MODEL);

	if (isDepthEnabled)
		QGfx::enableDepthTest();
}

void Render::renderFullscreenQuadNew()
{
	bool isDepthEnabled = QGfx::isDepthTestEnabled();
	QGfx::disableDepthTest();

	mat4<float> ID;
	ID.loadIdentity();
	QGfx::pushMatrix(MODEL, ID);
	QGfx::pushMatrix(VIEW, ID);
	QGfx::pushMatrix(PROJECTION, ID);

	mImpl->fullscreenVAO->bind();
	GLsizei nElements = mImpl->fullscreenVAO->getIndexBuffer()->getCount();
	QGfx::GL::gl_DrawElements(GL_TRIANGLES, nElements, GL_UNSIGNED_INT, 0);
	mImpl->fullscreenVAO->unbind();

	QGfx::popMatrix(PROJECTION);
	QGfx::popMatrix(VIEW);
	QGfx::popMatrix(MODEL);

	if (isDepthEnabled)
		QGfx::enableDepthTest();
}

/*
void Render::renderLine(const vec3<float>& aA, 
						const vec3<float>& aB, 
						const vec4<float>& aColor)
{
	const float verts[14] = { aA.x, aA.y, aA.z, aColor.x, aColor.y, aColor.z, aColor.w,
							  aB.x, aB.y, aB.z, aColor.x, aColor.y, aColor.z, aColor.w };
	const uint32_t indices[2] = { 0, 1 };


	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	mImpl->coloredEffect->bind();
	mImpl->coloredEffect->bindStateMatrix("MVP", MatrixMode(MODEL | VIEW | PROJECTION));

	glEnableVertexAttribArray(LOC_POSITION);
	glEnableVertexAttribArray(LOC_COLOR);
	glVertexAttribPointer(LOC_POSITION, 3, GL_FLOAT, GL_FALSE, 28, verts);
	glVertexAttribPointer(LOC_COLOR, 4, GL_FLOAT, GL_TRUE, 28, const_cast<float*>(&verts[3]));
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, indices);
	glDisableVertexAttribArray(LOC_POSITION);
	glDisableVertexAttribArray(LOC_COLOR);

	mImpl->coloredEffect->unbind();

	glDisable(GL_BLEND);
}
*/

void Render::renderIndexedList(const PrimitiveType aPrimitiveType,
							   const uint32_t aNumIndices,
							   const IndexSize aIndexSize,
							   const uint32_t aDataOffset)
{
	glDrawElements(aPrimitiveType, aNumIndices, aIndexSize, BUFFER_OFFSET(aDataOffset));
}

RenderHandle Render::getFullscreenTexturedGeom()
{
	return mImpl->fullscreenTexturedQuadHandle;
}

/*
void Render::renderMeshAsset(QSmartPtr<MeshAsset> aMeshAsset, 
							 Shader* aShader,
                             const uint32_t nInstances)
{
	mat4<float> ident; ident.loadIdentity();

	const vec3<float> desiredPos = vec3<float>(0, 0, 0);
	const vec3<float> desiredScale = vec3<float>(1, 1, 1);
	mat4<float> desiredPose; desiredPose.loadIdentity();

	//	QGfx::enableDepthTest();
	//	QGfx::changeDepthTest(LEQUAL);

	mat4<float> nt, t, s, x, y, z;
	mImpl->scratchMatrixStack.pushMatrix(ident);
	t.loadTranslation(desiredPos.x, desiredPos.y, desiredPos.z);
	s.loadScale(desiredScale.x, desiredScale.y, desiredScale.z);
	const mat4<float> r = desiredPose;

	mImpl->scratchMatrixStack.multiplyMatrix(t);
	mImpl->scratchMatrixStack.multiplyMatrix(s);
	mImpl->scratchMatrixStack.multiplyMatrix(r);

	GeometryBufferObject* model = aMeshAsset->model;
	mImpl->renderNode(model, aMeshAsset->model->rootNode, aShader, nInstances);
	mImpl->scratchMatrixStack.popMatrix();
}

void Render::renderMeshAsset(const MeshAsset* aMeshAsset,
							 Shader* aShader,
							 const uint32_t nInstances)
{
	mat4<float> ident; ident.loadIdentity();

	const vec3<float> desiredPos = vec3<float>(0, 0, 0);
	const vec3<float> desiredScale = vec3<float>(1, 1, 1);
	mat4<float> desiredPose; desiredPose.loadIdentity();

	//	QGfx::enableDepthTest();
	//	QGfx::changeDepthTest(LEQUAL);

	mat4<float> nt, t, s, x, y, z;
	mImpl->scratchMatrixStack.pushMatrix(ident);
	t.loadTranslation(desiredPos.x, desiredPos.y, desiredPos.z);
	s.loadScale(desiredScale.x, desiredScale.y, desiredScale.z);
	const mat4<float> r = desiredPose;

	mImpl->scratchMatrixStack.multiplyMatrix(t);
	mImpl->scratchMatrixStack.multiplyMatrix(s);
	mImpl->scratchMatrixStack.multiplyMatrix(r);

	GeometryBufferObject* model = aMeshAsset->model;
	mImpl->renderNode(model, aMeshAsset->model->rootNode, aShader, nInstances);
	mImpl->scratchMatrixStack.popMatrix();
}
*/

GeomData Render::getStarBillboardGeometry(const float aWidth, 
										  const float aHeight)
{
	const float hWidth = aWidth / 2.0f;
	const float sqrtWidth = sqrtf(aWidth);

	float starVertices[] =
	{
		-hWidth, 0, 0,
		hWidth, 0, 0,
		hWidth, aHeight, 0,
		-hWidth, aHeight, 0,

		0, 0, -hWidth,
		0, 0, hWidth,
		0, aHeight, hWidth,
		0, aHeight, -hWidth,

		-sqrtWidth, 0, -sqrtWidth,
		sqrtWidth, 0, sqrtWidth,
		sqrtWidth, aHeight, sqrtWidth,
		-sqrtWidth, aHeight, -sqrtWidth,

		-sqrtWidth, 0, sqrtWidth,
		sqrtWidth, 0, -sqrtWidth,
		sqrtWidth, aHeight, -sqrtWidth,
		-sqrtWidth, aHeight, sqrtWidth
	};

	float starUvs[] =
	{
		0, 0,
		1, 0,
		1, 1,
		0, 1,

		0, 0,
		1, 0,
		1, 1,
		0, 1,

		0, 0,
		1, 0,
		1, 1,
		0, 1,

		0, 0,
		1, 0,
		1, 1,
		0, 1
	};

	uint32_t starIndices[] =
	{
		0,1,2,
		2,3,0,

		4,5,6,
		6,7,4,

		8,9,10,
		10,11,8,

		12,13,14,
		14,15,12
	};

	GeomData data = {};

	data.nIndices = 24;
	data.nVertices = 16;

	data.indices = static_cast<uint32_t*>(std::malloc(sizeof(uint32_t) * 24));
	data.vertices = static_cast<float*>(std::malloc(sizeof(float) * 3 * 16));
	data.uvs = static_cast<float*>(std::malloc(sizeof(float) * 2 * 16));

	std::memcpy(data.indices, starIndices, sizeof(uint32_t) * 24);
	std::memcpy(data.vertices, starVertices, sizeof(float) * 16 * 3);
	std::memcpy(data.uvs, starUvs, sizeof(float) * 2 * 16);

	return data;
}

GeomData Render::getQuadGeometry(const float aWidth, const float aHeight)
{
	float quadVertices[] =
	{
		0, 0, 0,
		aWidth, 0, 0,
		aWidth, aHeight, 0,
		0, aHeight, 0,
	};

	uint32_t quadIndices[] =
	{
		0,1,2,
		2,1,3
	};

	GeomData data = {};

	data.nIndices = 6;
	data.nVertices = 4;

	data.indices = static_cast<uint32_t*>(std::malloc(sizeof(uint32_t) * 6));
	data.vertices = static_cast<float*>(std::malloc(sizeof(float) * 3 * 4));

	std::memcpy(data.indices, quadIndices, sizeof(uint32_t) * 6);
	std::memcpy(data.vertices, quadVertices, sizeof(float) * 4 * 3);

	return data;
}

GeomData Render::getFlatQuadGeometry(const float aWidth, const float aHeight)
{
	float quadVertices[] =
	{
		0, 0, 0,
		aWidth, 0, 0,
		aWidth, 0, aHeight,
		0, 0, aHeight,
	};

	float quadUVs[] = 
	{
		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0,
		0.0, 0.0,
	};

	uint32_t quadIndices[] =
	{
		0,3,1,
		1,3,2,
	};

	GeomData data = {};

	data.nIndices = 6;
	data.nVertices = 4;

	data.indices = static_cast<uint32_t*>(std::malloc(sizeof(uint32_t) * 6));
	data.vertices = static_cast<float*>(std::malloc(sizeof(float) * 3 * 4));
	data.uvs = static_cast<float*>(std::malloc(sizeof(float) * 2 * 4));
	std::memcpy(data.indices, quadIndices, sizeof(uint32_t) * 6);
	std::memcpy(data.vertices, quadVertices, sizeof(float) * 4 * 3);
	std::memcpy(data.uvs, quadUVs, sizeof(float) * 4 * 2);

	return data;
}

GeomData Render::getCubeGeometry(const float aWidth, 
								 const float aHeight, 
								 const float aDepth)
{
	float cubeVertices[] =
	{
		-aWidth, -aHeight, -aDepth,
		aWidth, -aHeight, -aDepth,
		aWidth,  aHeight, -aDepth,
		-aWidth,  aHeight, -aDepth,
		-aWidth, -aHeight,  aDepth,
		aWidth, -aHeight,  aDepth,
		aWidth,  aHeight,  aDepth,
		-aWidth,  aHeight,  aDepth
	};

	uint32_t cubeIndices[] =
	{
		0,2,1,
		0,3,2,

		1,2,6,
		6,5,1,

		4,5,6,
		6,7,4,

		2,3,6,
		6,3,7,

		0,7,3,
		0,4,7,

		0,1,5,
		0,5,4
	};

	GeomData data = {};

	data.nIndices = 36;
	data.nVertices = 8;

	data.indices = static_cast<uint32_t*>(std::malloc(sizeof(uint32_t) * 36));
	data.vertices = static_cast<float*>(std::malloc(sizeof(float) * 3 * 8));

	std::memcpy(data.indices, cubeIndices, sizeof(uint32_t) * 36);
	std::memcpy(data.vertices, cubeVertices, sizeof(float) * 3 * 8);

	return data;
}

std::shared_ptr<RenderObject> Render::getFullscreenRenderObj()
{
	return mImpl->fullscreenRO;
}
