#include "render/VertexBufferLayout.h"
#include "StringUtils.h"

VertexBufferLayout::VertexBufferLayout() : mSize(0)
{

}

void VertexBufferLayout::push(const std::string& aName, uint32_t aType, uint32_t aSize, uint32_t aCount, bool aNormalized)
{
	mLayout.push_back({ aName, aType, aSize, aCount, 
						static_cast<size_t>(mSize), 
						static_cast<uint32_t>(StringUtils::getSemanticLocation(aName)),
						aNormalized });
	mSize += aSize * aCount;
}